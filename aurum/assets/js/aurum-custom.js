// @codekit-prepend "joinable/lazysizes.js"
// @codekit-prepend "joinable/image-placeholders.js"
// @codekit-prepend "joinable/jquery.hoverIntent.js"
// @codekit-prepend "joinable/imagesloaded.pkgd.js"
// @codekit-prepend "joinable/wow.js"
// @codekit-prepend "joinable/jquery.fitvids.js"
// @codekit-prepend "joinable/scrollMonitor.js"
// @codekit-prepend "joinable/autosize.js"
// @codekit-prepend "joinable/jquery.perfect-scrollbar.js"
// @codekit-prepend "joinable/jquery.ba-throttle-debounce.js"

// @codekit-append "joinable/aurum-shop.js"

var publicVars = publicVars || {};

;(function( $, window, undefined ) {
	"use strict";

	$( document ).ready( function() {
		// Define global vars
		publicVars.$body          = $( 'body' );
		publicVars.$header        = publicVars.$body.find( '.site-header' );
		publicVars.$footer        = publicVars.$body.find( '.site-footer' );
		publicVars.$headerTopMenu = publicVars.$header.find( '.top-menu' );
		publicVars.$mainMenu      = publicVars.$header.find( 'nav.main-menu' );
		publicVars.$mobileMenu	  = publicVars.$body.find( '.mobile-menu' ).first();

		publicVars.$cartCounter	  = publicVars.$body.find( '.cart-counter' );
		publicVars.$miniCart	  = publicVars.$body.find( '.lab-mini-cart' );

		publicVars.$loginForm	  = publicVars.$body.find( '.woocommerce-form-login' );

		// Setup Menu
		var subMenuVisibleClass = 'sub-visible';

		publicVars.$mainMenu.find( 'li:has(> ul)' ).each( function( i, el ) {
			var $li = $( el ),
				$a = $li.find( '> a' );

			$a.on( 'click', function( ev ) {
				if ( is_touch_device() && ! $a.data( 'clicked' ) ) {
					$a.data( 'clicked', true );
					return false;
				}
			} );
				
			$li.hoverIntent( {
				over: function() {
					$li.addClass( subMenuVisibleClass );
				},
				out: function() {
					$li.removeClass( subMenuVisibleClass );
				},
				interval: 50,
				timeout: 250
			} );
		} );

		// Header Search Form
		var $searchForm = publicVars.$header.find( '.search-form' );

		if ( $searchForm.length === 1 ) {
			var $searchInput = $searchForm.find( '.search-input' );

			$searchInput.blur( function() {
				if ( $.trim( $searchInput.val() ).length === 0 ) {
					$searchForm.removeClass( 'input-visible' );
				}
			} );

			$searchForm.on( 'click', '.search-btn', function( ev ) {
				if ( $.trim( $searchInput.val() ).length === 0 ) {
					ev.preventDefault();

					$searchForm.addClass( 'input-visible' );
					setTimeout( function() {
						$searchInput.focus();
					}, 200 );
				} else {
					$searchForm.submit();
				}
			} );
		}


		// Top Menu Subs
		publicVars.$header.find( '.top-menu nav li:has(> ul)' ).each( function( i, el ) {
			var $li = $( el );

			$li.hoverIntent( {
				over: function() {
					$li.addClass( subMenuVisibleClass );
				},
				out: function() {
					$li.removeClass( subMenuVisibleClass );
				},
				timeout: 200,
				interval: 10
			} );
		} );

		// Sticky Menu
		if ( publicVars.$header.hasClass( 'sticky' ) ) {
			setupStickyMenu();
		}

		// Mobile Menu
		setupMobileMenu();

		// Footer Expand
		publicVars.$footer.find( '.expand-footer' ).on( 'click', function( ev ) {
			ev.preventDefault();

			publicVars.$footer.find( '.footer-widgets' ).removeClass( 'hidden-xs' ).prev().removeClass( 'visible-xs' ).addClass( 'hidden' );
		} );

		// Autosize
		if ( $.isFunction( $.fn.autosize ) ) {
			$( '.autosize, .autogrow' ).autosize();
		}

		// Lightbox
		if ( $.isFunction( $.fn.nivoLightbox ) ) {
			
			var lightbox_options = {
				effect: 'fade',
				theme: 'default',
			};
			
			$( '.nivo a, a.nivo' ).nivoLightbox( lightbox_options );
			
			if( publicVars.$body.hasClass( 'single-post-lightbox-on' ) ) {
				$( '.single-post-lightbox-on .post-content a:has(img)' ).nivoLightbox( lightbox_options );
			}
		}

		$( '[data-toggle="tooltip"]' ).tooltip();

		// Radio Buttons Replacement
		$( 'input[type="radio"] + label' ).each( function( i, el ) {
			$( el ).prev().addClass( 'replaced-radio-buttons' );
		} );

		$( 'input[type="checkbox"] + label' ).each( function( i, el ) {
			$( el ).prev().addClass( 'replaced-checkboxes' );
		} );

		// Select Picker
		if ( $.isFunction( $.fn.selectpicker ) ) {
			$( '.selectpicker' ).selectpicker();
		}

		// Search Go Back
		$( '.go-back' ).on( 'click', function( ev ) {
			ev.preventDefault();

			window.history.go(-1);
		} );


		// Scroll Reveal
		if ( typeof WOW != 'undefined' ) {
			new WOW().init();
		}

		// Testimonials Switcher
		$( '.lab_wpb_testimonials' ).each( function( i, el ) {
			var $testimonials    	= $( el ),
				$inner              = $testimonials.find( '.testimonials-inner' ),
				$items              = $testimonials.find( '.testimonial-entry' ),
				$items_hidden       = $items.filter( '.hidden' ),
				autoswitch          = $testimonials.data( 'autoswitch' ),
				$nav                = $( '<div class="testimonials-nav">' ),
				current_slide       = 0;

			$items.eq(current_slide).addClass( 'current' );

			$items_hidden.removeClass( 'hidden' ).hide();

			if ( $items.length > 1 ) {
				for ( var i = 0; i < $items.length; i++ ) {
					$nav.append( '<a href="#"' + ( i == current_slide ? ' class="active"' : '' ) +' data-index="' + i + '">' + ( i + 1 ) + '</a>' );
				}

				$inner.append( $nav );
			}

			var goToSlide = function( index ) {
				if ( current_slide != index ) {
					index = index % $items.length;

					var $to_hide = $items.filter( '.current' ),
						$to_show = $items.eq( index );

					$to_show.show();
					$to_hide.hide();

					var next_height = $to_show.outerHeight( true ) + $nav.outerHeight();

					$to_hide.show();
					$to_show.hide();

					$nav.find( 'a' ).removeClass( 'active' ).eq( index ).addClass( 'active' );

					var $th_thumbnail = $to_hide.find( '.testimonial-thumbnail' ),
						$th_blockquote = $to_hide.find( '.testimonial-blockquote p' ),
						$th_cite = $to_hide.find( '.testimonial-blockquote cite' );

					var $ts_thumbnail = $to_show.find( '.testimonial-thumbnail' ),
						$ts_blockquote = $to_show.find( '.testimonial-blockquote p' ),
						$ts_cite = $to_show.find( '.testimonial-blockquote cite' );

					TweenLite.to( $th_thumbnail, .10, { css: { autoAlpha: 0 } } );
					TweenLite.to( $th_cite, .25, { css: { autoAlpha: 0, top: 20 } } );

					TweenLite.to( $th_blockquote, .25, { css: { autoAlpha: 0 }, delay: .1, onComplete: function() {
						$th_thumbnail.attr( 'style', '' );
						$th_blockquote.attr( 'style', '' );
						$th_cite.attr( 'style', '' );

						$to_hide.attr( 'style', '' ).removeClass( 'current' ).hide();
						$to_show.show().addClass( 'current' );

						TweenLite.set( $to_show, { css: { autoAlpha: 0 } } );
						TweenLite.set( $ts_cite, { css: { autoAlpha: 0, top: 20 } } );

						TweenLite.to( $ts_cite, .25, { css: { autoAlpha: 1, top: 0 }, onComplete: function() {
							$ts_cite.attr( 'style', '' );
						} } );

						TweenLite.to( $to_show, .25, { css: { autoAlpha: 1 }, onComplete: function() {
							current_slide = index;
						} } );

					} } );

					TweenLite.to( $inner, .35, { css: { height: next_height }, onComplete: function() {
						$inner.attr( 'style', '' );
					} } );
				}
			};

			$nav.on( 'click', 'a', function( ev ) {
				ev.preventDefault();
				goToSlide( parseInt( $( this ).data( 'index' ), 10 ) );
			} );

			if ( autoswitch > 0 ) {
				var hover_tm = 0,
					setupAutoSwitcher = function( on ) {
						window.clearTimeout( hover_tm );

						if ( on ) {
							hover_tm = setTimeout( function() {
								goToSlide( current_slide + 1 );
								setupAutoSwitcher( 1 );

							}, autoswitch * 1000 );
						}
					};

				$testimonials.on( 'mouseover', function() {
					setupAutoSwitcher();
				} ).on( 'mouseleave', function() {
					setupAutoSwitcher( true );
				} );

				setupAutoSwitcher(true);
			}
		} );

		// Lightbox for Gallery items inside blog post
		$( '.post-content .gallery' ).each( function( i, el ) {
			var $gallery = $( el ),
				$items = $gallery.find( 'a' ),
				has_gallery_lb = false;

			$items.each( function( j, item ) {
				var $item = $( item );

				if ( $item.attr( 'href' ).match( /(jpg|jpeg|png|gif)$/i ) ) {
					$item.attr( 'data-lightbox-gallery', 'post-gallery-' + ( i +1 ) );
					has_gallery_lb = true;
				}
			} );

			if ( has_gallery_lb ) {
				$items.nivoLightbox( {
					effect: 'fade',
					theme: 'default',
				} );
			}
		} );
		
		// Sticky in Mobile
		$( '.mobile-menu.sticky-mobile' ).each( function( i, el ) {
			var $stickyMenu = $( el ),
				$spacer = $( '<div class="mobile-menu-fixed-spacer"></div>' ),
				height = 0;
			
			$stickyMenu.addClass( 'is-fixed-mobile' );
			
			height = $stickyMenu.outerHeight();
			
			if ( publicVars.$body.hasClass( 'transparent-header' ) == false ) {
				$stickyMenu.before( $spacer );
				$spacer.height( height );
			}
		} );
		
		// Social Links open in pop up
		var $links = $( '.share-post .share-product, .share-post .share-post-links' ).find( 'a' );

		$links.each( function() {
			var $this = $( this );
			
			$this.click( function( ev ) {
				ev.preventDefault();
				var url = $( this ).attr( 'href' ),
					w = 500,
					h = 420;
				window.open( url, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=" + ( ( screen.height / 2 ) - ( h / 2 ) ) + ", left=" + ( ( screen.width / 2 ) - ( w / 2 ) ) +", width=" + w + ", height=" + h );
			});
			
		} );
		
		// Post gallery
		$( '.post-gallery' ).each( function( i, postGallery ) {
			var $postGallery = $( postGallery );
			
			$postGallery.slick( {
				arrows : true,
				adaptiveHeight : true,
			} );
		} );
		
		// Go to top
		$( '.go-to-top[data-offset-type]' ).each( function( i, el ) {
			var $top = $( el ),
				offsetType = $top.data( 'offset-type' ),
				offsetValue = $top.data( 'offset-value' ),
				watcher;
			
			// Create vertical pipe
			var $vp = $( '<div class="go-to-top--vertical-pipe"></div>' );
			
			$vp.appendTo( 'body' );
			
			// Update Vertical pipe
			var updateVerticalPipe = function() {
				var height = publicVars.$body.outerHeight();
				
				if ( 'footer' == offsetValue ) {
					height -= publicVars.$footer.outerHeight();
				} else if ( 'percentage' == offsetType ) {
					height = offsetValue + '%';
				} else if ( 'pixels' == offsetType ) {
					height = offsetValue + 'px';
				}
				
				$vp.css( 'height', height );
			}
			
			updateVerticalPipe();
			
			// Init watcher
			function initScrollWatcher() {
				if ( watcher ) {
					watcher.destroy();
					watcher = null;
				}
				
				// Update vertical pipeline
				updateVerticalPipe();
				
				// Init watcher
				watcher = scrollMonitor.create( $vp );
				watcher.lock();
				
				// Footer as trigger element
				if ( 'footer' == offsetValue ) {
					watcher.stateChange( function( e ) {
						if ( this.isAboveViewport && ! this.isBelowViewport ) {
							show();
						}
						
						if ( this.isAboveViewport && this.isBelowViewport && this.isFullyInViewport ) {
							hide();
						}
					} );
				}
				// Number driven
				else {
					watcher.exitViewport( function() {
						show();
					} );
					
					watcher.enterViewport( function() {
						hide();
					} );
				}
			}
			
			initScrollWatcher();
			
			// Show button
			function show() {
				$top.addClass( 'go-to-top--visible' );
			}
			
			// Hide button
			function hide() {
				$top.removeClass( 'go-to-top--visible' );
			}
				
			// Go to Top when clicking
			$top.on( 'click', function( ev ) {
				ev.preventDefault();
				
				var obj = { pos: $( window ).scrollTop() };

				TweenMax.to( obj, 1, { pos: 0, ease:Power4.easeOut, onUpdate: function() {
					$( window ).scrollTop( obj.pos );
				} } );
			} );
		} );
		
		// Fit Vids
		publicVars.$body.fitVids();

	} );

	function setupStickyMenu() {
		var headerType = 1;

		if ( publicVars.$header.hasClass( 'header-type-2' ) ) {
			headerType = 2;
		} else if( publicVars.$header.hasClass( 'header-type-3' ) ) {
			headerType = 3;
		} else if ( publicVars.$header.hasClass( 'header-type-4' ) ) {
			headerType = 4;
		}

		// Initialize sticky menu
		var $watcherElement = publicVars.$header.find( '.header-menu' );

		if ( headerType === 2 ) {
			$watcherElement = publicVars.$header.find( '.full-menu' );
		} else if ( headerType === 3 || headerType === 4 ) {
			$watcherElement = publicVars.$header.find( '> .container' );
		}

		var watcher = window.scrollMonitor.create( $watcherElement.get( 0 ), { 
				top: publicVars.$body.hasClass( 'admin-bar' ) ? 32 : 0 
			} ),
			$spacer = null,
			headerHeight = publicVars.$header.outerHeight(),
			minWidth = 768;

		publicVars.$header.before( '<div class="header-spacer hidden"></div>' );

		$spacer = publicVars.$header.prev();

		$spacer.height( headerHeight );

		watcher.lock();

		watcher.partiallyExitViewport( function() {
			if ( minWidth > $( window ).width() ) {
				return;
			}

			publicVars.$header.addClass( 'sticked' );
			$spacer.removeClass( 'hidden' );

			// Header Top Menu
			if ( publicVars.$headerTopMenu.length ) {
				publicVars.$headerTopMenu.addClass( 'hidden' );
			}

			// Menu Type 2 Options
			if ( headerType === 2 ) {
				publicVars.$header.find( '.header-menu' ).addClass( 'hidden') ;
				publicVars.$header.find( '.full-menu .logo' ).addClass( 'visible' ).hide().fadeTo( 200, 1 );
			}
		} );

		watcher.fullyEnterViewport( function() {
			if ( minWidth > $( window ).width() ) {
				return;
			}

			publicVars.$header.removeClass( 'sticked' );
			$spacer.addClass( 'hidden' );

			// Header Top Menu
			if ( publicVars.$headerTopMenu.length ) {
				publicVars.$headerTopMenu.removeClass( 'hidden' );
			}

			// Menu Type 2 Options
			if ( headerType === 2 ) {
				publicVars.$header.find( '.header-menu' ).removeClass( 'hidden' );
				publicVars.$header.find( '.full-menu .logo' ).removeClass( 'visible' ).attr( 'style', '' );
			}
		} );
	}

	function setupMobileMenu() {
		var subMenuVisibleClass = 'sub-visible',
			expandOrCollapseDelay = 0.2;

		publicVars.$mobileMenu.find( '.mobile-menu li:has(> ul)' ).each( function( i, el ) {
			var $li = $( el ),
				$a = $li.children( 'a' ),
				$sub = $li.children( 'ul' );

			$a.append( '<span class="sub-menu-indicator"><i class="entypo-plus"></i></span>' );

			var $sub_i = $a.find( '.sub-menu-indicator' );

			$sub_i.on( 'click', function( ev ) {
				ev.preventDefault();

				if ( ! $li.hasClass( subMenuVisibleClass ) ) {
					$li.addClass( subMenuVisibleClass );

					var subHeight = $sub.outerHeight();

					$sub.height( 0 );

					TweenMax.to( $sub, expandOrCollapseDelay, { css: { height: subHeight }, onComplete: function() {
						$sub.attr( 'style', '' );
					} } );
				} else {
					TweenMax.to( $sub, expandOrCollapseDelay, { css: { height: 0 }, onComplete: function() {
						$sub.attr( 'style', '' );
						$li.removeClass( subMenuVisibleClass );

						$sub.find( '.' + subMenuVisibleClass ).removeClass( subMenuVisibleClass ).children( 'ul' ).attr( 'style', '' );
					} } );
				}
			} );
		} );

		publicVars.$mobileMenu.find( '.toggle-menu' ).on( 'click', function( ev ) {
			ev.preventDefault();
			$( '.mobile-menu--content' ).toggleClass( 'visible' );
		} );

		//publicVars.$mobileMenu.find( '.site-header .right-align' ).removeClass( 'right-align' );
	}	
	
	// Query Parameters
	$.extend( {
	  getQueryParameters : function( str ) {
		  return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
	  }
	} );

} )( jQuery, window );

// Is touch device
function is_touch_device() {
  return 'ontouchstart' in window        // works on most browsers 
      || 'onmsgesturechange' in window;  // works on IE10 with some false positives
};

// Is RTL
function isRTL() {
	return 'rtl' == document.dir;
}
