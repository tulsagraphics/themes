<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );
add_filter( 'jetpack_just_in_time_msgs', '_return_false' );
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Pull List of Product Reviews
function get_woo_reviews()
{
$count = 0;
$html_r = "";
$title="";
$args = array(
'post_type' => 'product'
);

$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );

foreach($comments as $comment) :
$title = ''.get_the_title( $comment->comment_post_ID ).'';
$html_r = $html_r. "" .$title."";
$html_r = $html_r. "" .$comment->comment_content."";
$html_r = $html_r."Posted By".$comment->comment_author." On ".$comment->comment_date. "";
endforeach;
return $html_r;
}

// Create shortcode for Product Review List
add_shortcode('woo_reviews', 'get_woo_reviews');

/** 
 *Reduce the strength requirement on the woocommerce password.
 * 
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
function reduce_woocommerce_min_strength_requirement( $strength ) {
    return 1;
}
add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );

/*/ added for items per page by MH 2018-02-27
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 200 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 200;
  return $cols;
}*/

add_filter('woocommerce_package_rates', 'wf_sort_shipping_methods', 10, 2);

function wf_sort_shipping_methods($available_shipping_methods, $package)
{
	// Arrange shipping methods as per your requirement
	$sort_order	= array(
		'980'		=>	array(),
		'free_shipping'		=>	array(),
		'wf_shipping_ups'	=>	array(),
		'wf_shipping_usps'	=>	array(),
		'local_pickup'		=>	array(),
		'legacy_flat_rate'	=>	array(),		
	);
	
	// unsetting all methods that needs to be sorted
	foreach($available_shipping_methods as $carrier_id => $carrier){
		$carrier_name	=	current(explode(":",$carrier_id));
		if(array_key_exists($carrier_name,$sort_order)){
			$sort_order[$carrier_name][$carrier_id]	=		$available_shipping_methods[$carrier_id];
			unset($available_shipping_methods[$carrier_id]);
		}
	}
	
	// adding methods again according to sort order array
	foreach($sort_order as $carriers){
		$available_shipping_methods	=	array_merge($available_shipping_methods,$carriers);
	}
	return $available_shipping_methods;
}

add_filter( 'woocommerce_package_rates' , 'xa_sort_shipping_services_by_cost', 10, 2 );
function xa_sort_shipping_services_by_cost( $rates, $package ) {
	if ( ! $rates )  return;
	
	$rate_cost = array();
	foreach( $rates as $rate ) {
		$rate_cost[] = $rate->cost;
	}
	
	// using rate_cost, sort rates.
	array_multisort( $rate_cost, $rates );
	
	return $rates;
}

add_filter( 'woocommerce_shipping_chosen_method', 'wf_default_shipping_method', 10 );
function wf_default_shipping_method( $method ) {
        $the_cheapest_cost = 1000000;
	$packages = WC()->shipping()->get_packages()[0]['rates'];

	foreach ( array_keys( $packages ) as $key ) {
        if ( ( $packages[$key]->cost > 0 ) && ( $packages[$key]->cost < $the_cheapest_cost ) ) {
            $the_cheapest_cost = $packages[$key]->cost;
            $method_id = $packages[$key]->id;
        }
	}

	return $method_id;
}
