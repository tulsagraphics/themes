<?php
/**
 * Template for generic post display.
 * @package themify
 * @since 1.0.0
 */

 global $more, $themify;
$more = 0;

?>

<?php themify_post_before(); // hook    ?>
<?php $media_position = strpos($themify->post_layout,'custom_tiles')!==false || strpos($themify->post_layout,'auto_tiles')!==false; ?>
<article id="post-<?php the_id(); ?>" <?php post_class(); ?>>

    <?php themify_post_start(); // hook   ?>

    <?php if ('below' != $themify->media_position || $media_position) get_template_part('includes/post-media', 'loop'); ?>

    <div class="post-content">
        <?php if($media_position &&  $themify->unlink_image != 'yes'):?>
			<?php echo themify_open_link( array( 'class' => 'tiled_overlay_link' ) ); ?></a>
        <?php endif;?>
        <?php if ('below' == $themify->media_position && !$media_position) get_template_part('includes/post-media', 'loop'); ?>

        <?php get_template_part('includes/post-meta', 'loop') ?>

    </div>
    <!-- /.post-content -->
    <?php themify_post_end(); // hook    ?>

</article>
<!-- /.post -->

<?php
themify_post_after(); // hook ?>