<?php

defined( 'ABSPATH' ) or die;

$GLOBALS['processed_terms'] = array();
$GLOBALS['processed_posts'] = array();

require_once ABSPATH . 'wp-admin/includes/post.php';
require_once ABSPATH . 'wp-admin/includes/taxonomy.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

function themify_import_post( $post ) {
	global $processed_posts, $processed_terms;

	if ( ! post_type_exists( $post['post_type'] ) ) {
		return;
	}

	/* Menu items don't have reliable post_title, skip the post_exists check */
	if( $post['post_type'] !== 'nav_menu_item' ) {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
		if ( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
			$processed_posts[ intval( $post['ID'] ) ] = intval( $post_exists );
			return;
		}
	}

	if( $post['post_type'] == 'nav_menu_item' ) {
		if( ! isset( $post['tax_input']['nav_menu'] ) || ! term_exists( $post['tax_input']['nav_menu'], 'nav_menu' ) ) {
			return;
		}
		$_menu_item_type = $post['meta_input']['_menu_item_type'];
		$_menu_item_object_id = $post['meta_input']['_menu_item_object_id'];

		if ( 'taxonomy' == $_menu_item_type && isset( $processed_terms[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_terms[ intval( $_menu_item_object_id ) ];
		} else if ( 'post_type' == $_menu_item_type && isset( $processed_posts[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_posts[ intval( $_menu_item_object_id ) ];
		} else if ( 'custom' != $_menu_item_type ) {
			// associated object is missing or not imported yet, we'll retry later
			// $missing_menu_items[] = $item;
			return;
		}
	}

	$post_parent = ( $post['post_type'] == 'nav_menu_item' ) ? $post['meta_input']['_menu_item_menu_item_parent'] : (int) $post['post_parent'];
	$post['post_parent'] = 0;
	if ( $post_parent ) {
		// if we already know the parent, map it to the new local ID
		if ( isset( $processed_posts[ $post_parent ] ) ) {
			if( $post['post_type'] == 'nav_menu_item' ) {
				$post['meta_input']['_menu_item_menu_item_parent'] = $processed_posts[ $post_parent ];
			} else {
				$post['post_parent'] = $processed_posts[ $post_parent ];
			}
		}
	}

	/**
	 * for hierarchical taxonomies, IDs must be used so wp_set_post_terms can function properly
	 * convert term slugs to IDs for hierarchical taxonomies
	 */
	if( ! empty( $post['tax_input'] ) ) {
		foreach( $post['tax_input'] as $tax => $terms ) {
			if( is_taxonomy_hierarchical( $tax ) ) {
				$terms = explode( ', ', $terms );
				$post['tax_input'][ $tax ] = array_map( 'themify_get_term_id_by_slug', $terms, array_fill( 0, count( $terms ), $tax ) );
			}
		}
	}

	$post['post_author'] = (int) get_current_user_id();
	$post['post_status'] = 'publish';

	$old_id = $post['ID'];

	unset( $post['ID'] );
	$post_id = wp_insert_post( $post, true );
	if( is_wp_error( $post_id ) ) {
		return false;
	} else {
		$processed_posts[ $old_id ] = $post_id;

		if( isset( $post['has_thumbnail'] ) && $post['has_thumbnail'] ) {
			$placeholder = themify_get_placeholder_image();
			if( ! is_wp_error( $placeholder ) ) {
				set_post_thumbnail( $post_id, $placeholder );
			}
		}

		return $post_id;
	}
}

function themify_get_placeholder_image() {
	static $placeholder_image = null;

	if( $placeholder_image == null ) {
		if ( ! function_exists( 'WP_Filesystem' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		WP_Filesystem();
		global $wp_filesystem;
		$upload = wp_upload_bits( $post['post_name'] . '.jpg', null, $wp_filesystem->get_contents( THEMIFY_DIR . '/img/image-placeholder.jpg' ) );

		if ( $info = wp_check_filetype( $upload['file'] ) )
			$post['post_mime_type'] = $info['type'];
		else
			return new WP_Error( 'attachment_processing_error', __( 'Invalid file type', 'themify' ) );

		$post['guid'] = $upload['url'];
		$post_id = wp_insert_attachment( $post, $upload['file'] );
		wp_update_attachment_metadata( $post_id, wp_generate_attachment_metadata( $post_id, $upload['file'] ) );

		$placeholder_image = $post_id;
	}

	return $placeholder_image;
}

function themify_import_term( $term ) {
	global $processed_terms;

	if( $term_id = term_exists( $term['slug'], $term['taxonomy'] ) ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term['term_id'] ) )
			$processed_terms[ intval( $term['term_id'] ) ] = (int) $term_id;
		return (int) $term_id;
	}

	if ( empty( $term['parent'] ) ) {
		$parent = 0;
	} else {
		$parent = term_exists( $term['parent'], $term['taxonomy'] );
		if ( is_array( $parent ) ) $parent = $parent['term_id'];
	}

	$id = wp_insert_term( $term['name'], $term['taxonomy'], array(
		'parent' => $parent,
		'slug' => $term['slug'],
		'description' => $term['description'],
	) );
	if ( ! is_wp_error( $id ) ) {
		if ( isset( $term['term_id'] ) ) {
			$processed_terms[ intval($term['term_id']) ] = $id['term_id'];
			return $term['term_id'];
		}
	}

	return false;
}

function themify_get_term_id_by_slug( $slug, $tax ) {
	$term = get_term_by( 'slug', $slug, $tax );
	if( $term ) {
		return $term->term_id;
	}

	return false;
}

function themify_undo_import_term( $term ) {
	$term_id = term_exists( $term['slug'], $term['term_taxonomy'] );
	if ( $term_id ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term_id ) ) {
			wp_delete_term( $term_id, $term['term_taxonomy'] );
		}
	}
}

/**
 * Determine if a post exists based on title, content, and date
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param array $args array of database parameters to check
 * @return int Post ID if post exists, 0 otherwise.
 */
function themify_post_exists( $args = array() ) {
	global $wpdb;

	$query = "SELECT ID FROM $wpdb->posts WHERE 1=1";
	$db_args = array();

	foreach ( $args as $key => $value ) {
		$value = wp_unslash( sanitize_post_field( $key, $value, 0, 'db' ) );
		if( ! empty( $value ) ) {
			$query .= ' AND ' . $key . ' = %s';
			$db_args[] = $value;
		}
	}

	if ( !empty ( $args ) )
		return (int) $wpdb->get_var( $wpdb->prepare($query, $args) );

	return 0;
}

function themify_undo_import_post( $post ) {
	if( $post['post_type'] == 'nav_menu_item' ) {
		$post_exists = themify_post_exists( array(
			'post_name' => $post['post_name'],
			'post_modified' => $post['post_date'],
			'post_type' => 'nav_menu_item',
		) );
	} else {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
	}
	if( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
		/**
		 * check if the post has been modified, if so leave it be
		 *
		 * NOTE: posts are imported using wp_insert_post() which modifies post_modified field
		 * to be the same as post_date, hence to check if the post has been modified,
		 * the post_modified field is compared against post_date in the original post.
		 */
		if( $post['post_date'] == get_post_field( 'post_modified', $post_exists ) ) {
			wp_delete_post( $post_exists, true ); // true: bypass trash
		}
	}
}

function themify_do_demo_import() {
$term = array (
  'term_id' => 1,
  'name' => 'Travel',
  'slug' => 'travel',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => 'This category covers everything about travelling',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 4,
  'name' => 'News',
  'slug' => 'news',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 5,
  'name' => 'Design',
  'slug' => 'design',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => 'Anything related to design.',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 25,
  'name' => 'Technology',
  'slug' => 'technology',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 27,
  'name' => 'Retro',
  'slug' => 'retro',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 28,
  'name' => 'Music',
  'slug' => 'music',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 15,
  'name' => 'Accessories',
  'slug' => 'accessories',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 17,
  'name' => 'Apparel',
  'slug' => 'apparel',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 19,
  'name' => 'Man',
  'slug' => 'man',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 20,
  'name' => 'Woman',
  'slug' => 'woman',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 26,
  'name' => 'Merchandise',
  'slug' => 'merchandise',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 6,
  'name' => 'App',
  'slug' => 'app',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 8,
  'name' => 'Photography',
  'slug' => 'photography',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 9,
  'name' => 'Art',
  'slug' => 'art',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 10,
  'name' => 'Product',
  'slug' => 'product',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 29,
  'name' => 'Main Menu',
  'slug' => 'main-menu',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 33,
  'name' => 'Multicol Menu',
  'slug' => 'multicol-menu',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$post = array (
  'ID' => 126,
  'post_date' => '2016-01-01 01:11:07',
  'post_date_gmt' => '2016-01-01 01:11:07',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Tips to Choose Best Lens',
  'post_excerpt' => '',
  'post_name' => 'tips-to-choose-best-lens',
  'post_modified' => '2017-08-21 02:49:10',
  'post_modified_gmt' => '2017-08-21 02:49:10',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=126',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'design, news, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 130,
  'post_date' => '2016-04-10 01:26:25',
  'post_date_gmt' => '2016-04-10 01:26:25',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'With Sidebar',
  'post_excerpt' => '',
  'post_name' => 'with-sidebar',
  'post_modified' => '2017-08-21 02:48:44',
  'post_modified_gmt' => '2017-08-21 02:48:44',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=130',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'sidebar1',
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, technology, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 133,
  'post_date' => '2016-04-05 01:28:25',
  'post_date_gmt' => '2016-04-05 01:28:25',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatu',
  'post_title' => 'Photographer Tools',
  'post_excerpt' => '',
  'post_name' => 'photographer-tools',
  'post_modified' => '2017-08-21 02:49:05',
  'post_modified_gmt' => '2017-08-21 02:49:05',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=133',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 137,
  'post_date' => '2016-04-07 01:31:14',
  'post_date_gmt' => '2016-04-07 01:31:14',
  'post_content' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'The Girl',
  'post_excerpt' => '',
  'post_name' => 'the-girl',
  'post_modified' => '2017-08-21 02:48:47',
  'post_modified_gmt' => '2017-08-21 02:48:47',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=137',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, retro',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 153,
  'post_date' => '2016-04-05 01:43:53',
  'post_date_gmt' => '2016-04-05 01:43:53',
  'post_content' => 'Corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Perfect Angle',
  'post_excerpt' => '',
  'post_name' => 'perfect-angle',
  'post_modified' => '2017-08-21 02:49:03',
  'post_modified_gmt' => '2017-08-21 02:49:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=153',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'design',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 156,
  'post_date' => '2016-04-05 01:49:57',
  'post_date_gmt' => '2016-04-05 01:49:57',
  'post_content' => 'Voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatu',
  'post_title' => 'Retro Camera',
  'post_excerpt' => '',
  'post_name' => 'retro-camera',
  'post_modified' => '2017-08-21 02:49:01',
  'post_modified_gmt' => '2017-08-21 02:49:01',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=156',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'design, retro, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 159,
  'post_date' => '2016-04-05 01:52:10',
  'post_date_gmt' => '2016-04-05 01:52:10',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Setting Down',
  'post_excerpt' => '',
  'post_name' => 'setting-down',
  'post_modified' => '2017-08-21 02:48:59',
  'post_modified_gmt' => '2017-08-21 02:48:59',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=159',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, retro',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 162,
  'post_date' => '2016-04-06 01:58:32',
  'post_date_gmt' => '2016-04-06 01:58:32',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'By The Water',
  'post_excerpt' => '',
  'post_name' => 'by-the-water',
  'post_modified' => '2017-08-21 02:48:49',
  'post_modified_gmt' => '2017-08-21 02:48:49',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=162',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, retro, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 165,
  'post_date' => '2016-04-05 02:01:28',
  'post_date_gmt' => '2016-04-05 02:01:28',
  'post_content' => 'Ssimilique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Perfect Piano',
  'post_excerpt' => '',
  'post_name' => 'perfect-piano',
  'post_modified' => '2017-08-21 02:48:58',
  'post_modified_gmt' => '2017-08-21 02:48:58',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=165',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 168,
  'post_date' => '2016-04-05 02:02:38',
  'post_date_gmt' => '2016-04-05 02:02:38',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur',
  'post_title' => 'Guitar Amplifier',
  'post_excerpt' => '',
  'post_name' => 'guitar-amplifier',
  'post_modified' => '2017-08-21 02:48:58',
  'post_modified_gmt' => '2017-08-21 02:48:58',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=168',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 171,
  'post_date' => '2016-04-05 02:03:43',
  'post_date_gmt' => '2016-04-05 02:03:43',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'After Party',
  'post_excerpt' => '',
  'post_name' => 'after-party',
  'post_modified' => '2017-08-21 02:48:54',
  'post_modified_gmt' => '2017-08-21 02:48:54',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=171',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music, retro, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 177,
  'post_date' => '2016-04-05 02:06:35',
  'post_date_gmt' => '2016-04-05 02:06:35',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Music Concert',
  'post_excerpt' => '',
  'post_name' => 'music-concert',
  'post_modified' => '2017-08-21 02:48:52',
  'post_modified_gmt' => '2017-08-21 02:48:52',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=177',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music, news, retro',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 180,
  'post_date' => '2016-04-05 02:09:33',
  'post_date_gmt' => '2016-04-05 02:09:33',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
  'post_title' => 'Acoustic Guitar Tone',
  'post_excerpt' => '',
  'post_name' => 'acoustic-guitar-tone',
  'post_modified' => '2017-08-21 02:48:51',
  'post_modified_gmt' => '2017-08-21 02:48:51',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=180',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 192,
  'post_date' => '2016-04-08 02:14:31',
  'post_date_gmt' => '2016-04-08 02:14:31',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Morning Activity',
  'post_excerpt' => '',
  'post_name' => 'morning-activity',
  'post_modified' => '2017-08-21 02:48:45',
  'post_modified_gmt' => '2017-08-21 02:48:45',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=192',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 195,
  'post_date' => '2016-04-13 02:17:49',
  'post_date_gmt' => '2016-04-13 02:17:49',
  'post_content' => 'Officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Apple Store',
  'post_excerpt' => '',
  'post_name' => 'apple-store',
  'post_modified' => '2017-08-21 02:48:41',
  'post_modified_gmt' => '2017-08-21 02:48:41',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=195',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 199,
  'post_date' => '2016-05-09 02:32:24',
  'post_date_gmt' => '2016-05-09 02:32:24',
  'post_content' => 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Gallery Layout',
  'post_excerpt' => '',
  'post_name' => 'gallery-layout',
  'post_modified' => '2017-08-21 02:48:37',
  'post_modified_gmt' => '2017-08-21 02:48:37',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=199',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'post_layout' => 'gallery',
    'post_layout_gallery' => '[gallery link="file" size="medium" columns="6" ids="745,742,996,1002,931,999,889,838,839,1004"]',
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'news, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 202,
  'post_date' => '2016-04-12 02:23:13',
  'post_date_gmt' => '2016-04-12 02:23:13',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'NYC Conference',
  'post_excerpt' => '',
  'post_name' => 'nyc-conference',
  'post_modified' => '2017-08-21 02:48:42',
  'post_modified_gmt' => '2017-08-21 02:48:42',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=202',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'technology, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 205,
  'post_date' => '2016-05-09 02:29:15',
  'post_date_gmt' => '2016-05-09 02:29:15',
  'post_content' => 'Officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Split Layout',
  'post_excerpt' => '',
  'post_name' => 'split-layout',
  'post_modified' => '2017-08-21 02:48:38',
  'post_modified_gmt' => '2017-08-21 02:48:38',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=205',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'sidebar1 sidebar-left',
    'post_layout' => 'split',
    'content_width' => 'default_width',
    'image_width' => '800',
    'image_height' => '1000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 208,
  'post_date' => '2016-03-05 02:19:54',
  'post_date_gmt' => '2016-03-05 02:19:54',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Romantic Place in Venice',
  'post_excerpt' => '',
  'post_name' => 'romantic-place-in-venice',
  'post_modified' => '2017-08-21 02:49:08',
  'post_modified_gmt' => '2017-08-21 02:49:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=208',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'design, news, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 211,
  'post_date' => '2016-05-10 02:36:54',
  'post_date_gmt' => '2016-05-10 02:36:54',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Slider Layout',
  'post_excerpt' => '',
  'post_name' => 'slider-layout',
  'post_modified' => '2017-08-21 02:48:35',
  'post_modified_gmt' => '2017-08-21 02:48:35',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=211',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'post_layout' => 'slider',
    'post_layout_slider' => '[gallery size="full" link="file" ids="1181,1182,1183"]',
    'content_width' => 'default_width',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1029,
  'post_date' => '2016-04-04 17:12:29',
  'post_date_gmt' => '2016-04-04 17:12:29',
  'post_content' => 'At the enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
  'post_title' => 'Vimeo Video',
  'post_excerpt' => '',
  'post_name' => 'vimeo-video',
  'post_modified' => '2017-08-21 02:49:06',
  'post_modified_gmt' => '2017-08-21 02:49:06',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1029',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'content_width' => 'default_width',
    'video_url' => 'https://vimeo.com/164214179',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'music, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 815,
  'post_date' => '2016-05-10 01:56:34',
  'post_date_gmt' => '2016-05-10 01:56:34',
  'post_content' => '',
  'post_title' => 'Error 404 Page',
  'post_excerpt' => '',
  'post_name' => 'error-404-page',
  'post_modified' => '2016-05-11 01:42:41',
  'post_modified_gmt' => '2016-05-11 01:42:41',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=815',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'auto_tiles',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'auto_tiles',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"gutter\\":\\"gutter-default\\",\\"equal_column_height\\":\\"\\",\\"column_alignment\\":\\"col_align_top\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>404<\\\\/h1><p>Sorry, the page you are looking for doesn’t exist<\\\\/p>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"1.5\\",\\"font_size_unit\\":\\"em\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"20\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"animation_effect\\":\\"fadeInDown\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"small\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"Return to home\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\",\\"button_color_bg\\":\\"default\\"},{\\"label\\":\\"Continue to shopping\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/peak\\\\/shop\\\\/\\",\\"button_color_bg\\":\\"default\\"},{\\"label\\":\\"Visit our blog\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/peak\\\\/blog\\\\/\\",\\"button_color_bg\\":\\"default\\"}],\\"font_family\\":\\"default\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"padding_top_link_unit\\":\\"px\\",\\"padding_right_link_unit\\":\\"px\\",\\"padding_bottom_link_unit\\":\\"px\\",\\"padding_left_link_unit\\":\\"px\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"checkbox_link_padding_apply_all_padding\\":\\"padding\\",\\"link_margin_top_unit\\":\\"px\\",\\"link_margin_right_unit\\":\\"px\\",\\"link_margin_bottom_unit\\":\\"px\\",\\"link_margin_left_unit\\":\\"px\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_margin_apply_all_margin\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"link_checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"3\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"custom_css_column\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"fullheight\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/peak\\\\/files\\\\/2016\\\\/05\\\\/mountain-984046_1920-1-1024x682.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"8a70ff_0.41\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"1\\",\\"gutter\\":\\"gutter-default\\",\\"column_alignment\\":\\"col_align_top\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[]}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 380,
  'post_date' => '2016-04-05 23:57:19',
  'post_date_gmt' => '2016-04-05 23:57:19',
  'post_content' => '[woocommerce_checkout]',
  'post_title' => 'Checkout',
  'post_excerpt' => '',
  'post_name' => 'checkout',
  'post_modified' => '2017-08-21 02:50:29',
  'post_modified_gmt' => '2017-08-21 02:50:29',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/checkout/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'background_repeat' => 'fullcover',
    'portfolio_hide_title' => 'default',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 379,
  'post_date' => '2016-04-05 23:57:19',
  'post_date_gmt' => '2016-04-05 23:57:19',
  'post_content' => '[woocommerce_cart]',
  'post_title' => 'Cart',
  'post_excerpt' => '',
  'post_name' => 'cart',
  'post_modified' => '2017-08-21 02:50:26',
  'post_modified_gmt' => '2017-08-21 02:50:26',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/cart/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 378,
  'post_date' => '2016-04-05 23:57:19',
  'post_date_gmt' => '2016-04-05 23:57:19',
  'post_content' => '',
  'post_title' => 'Shop',
  'post_excerpt' => '',
  'post_name' => 'shop',
  'post_modified' => '2017-08-21 02:51:29',
  'post_modified_gmt' => '2017-08-21 02:51:29',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/shop/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 374,
  'post_date' => '2016-04-05 23:11:24',
  'post_date_gmt' => '2016-04-05 23:11:24',
  'post_content' => '',
  'post_title' => 'Blog',
  'post_excerpt' => '',
  'post_name' => 'blog',
  'post_modified' => '2017-08-21 02:50:17',
  'post_modified_gmt' => '2017-08-21 02:50:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=374',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#70d6d1',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/mt-baker-1281734_1920.jpg',
    'query_category' => '0',
    'more_posts' => 'infinite',
    'posts_per_page' => '7',
    'display_content' => 'none',
    'image_width' => '600',
    'image_height' => '600',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 140,
  'post_date' => '2016-04-05 01:31:54',
  'post_date_gmt' => '2016-04-05 01:31:54',
  'post_content' => '',
  'post_title' => 'Home',
  'post_excerpt' => '',
  'post_name' => 'home',
  'post_modified' => '2017-10-24 21:34:27',
  'post_modified_gmt' => '2017-10-24 21:34:27',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=140',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'more_posts' => 'infinite',
    'display_content' => 'none',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"0|multiple\\",\\"product_cat_post\\":\\"0|multiple\\",\\"product_tag_post\\":\\"0|multiple\\",\\"product_shipping_class_post\\":\\"0|multiple\\",\\"portfolio-category_post\\":\\"0|multiple\\",\\"post_per_page_post\\":\\"7\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"600\\",\\"img_height_post\\":\\"600\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"font_size_title_unit\\":\\"px\\",\\"line_height_title_unit\\":\\"px\\",\\"font_size_meta_unit\\":\\"px\\",\\"line_height_meta_unit\\":\\"px\\",\\"font_size_date_unit\\":\\"px\\",\\"line_height_date_unit\\":\\"px\\",\\"font_size_content_unit\\":\\"px\\",\\"line_height_content_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"background_color\\":\\"f0f2f2_1.00\\",\\"padding_top\\":\\"7\\",\\"padding_top_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Tiles\\",\\"content_feature\\":\\"<p>Peak allows you to display blog posts and portfolio in masonry tile layouts with optional post filter and load more button.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-th-large\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"link_feature\\":\\"https://themify.me/demo/themes/peak/blog/\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Various Layouts\\",\\"content_feature\\":\\"<p>It comes with various archive and single post layouts, available in blog and blog. All layouts are beautifully and responsive.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-gift\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"link_feature\\":\\"https://themify.me/demo/themes/peak/blog/blog-post-layouts/\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"10\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Portfolio\\",\\"content_feature\\":\\"<p>An optional Portfolio post type with post filter is included. The post filter gives visitors the ability to view portfolio based on the category.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-bar-chart\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"link_feature\\":\\"https://themify.me/demo/themes/peak/portfolio/\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Mega Menu\\",\\"content_feature\\":\\"<p>Create optional mega menus to display recent posts, multi-column menus, and drop any widget in the menu.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-question\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"10\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col3-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"WooCommerce\\",\\"content_feature\\":\\"<p>We\\\\\\\\\\\'ve designed the theme to work seamlessly with WooCommerce, a free plugin that allows you build a complete ecommerce site.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-shopping-cart\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"link_feature\\":\\"https://themify.me/demo/themes/peak/shop/\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Unique Page Title\\",\\"content_feature\\":\\"<p>Make each post, page, and category as unique as you by customizing the title banner and overlay color.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"1f7aea\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-magic\\",\\"icon_color_feature\\":\\"1f7aea\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"10\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Beautiful Theme</h1><p>Peak is a very useful masonry grid theme for WordPress. Built to make your content look great at any size, the grid will always stay updated with your latest posts and highlights, plus you can add custom blocks at any time to point users towards specific content.</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"breakpoint_tablet_landscape\\":{\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\"}}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"large\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"LEARN MORE\\",\\"link\\":\\"https://themify.me/\\",\\"button_color_bg\\":\\"default\\"}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"10\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"custom_css_column\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-right\\",\\"url_image\\":\\"https://themify.me/demo/themes/peak/files/2016/05/iMac-3-739x549.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"739\\",\\"auto_fullwidth\\":\\"|\\",\\"height_image\\":\\"549\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_caption\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"col_align_middle\\",\\"styling\\":[]}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/04/bg-theme-1024x446.jpg\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-top\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"4088e6_0.82\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"8\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"3\\",\\"color_divider\\":\\"000000_1.00\\",\\"divider_type\\":\\"custom\\",\\"divider_width\\":\\"80\\",\\"divider_align\\":\\"center\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Explore the Shop</h2>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"10\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[recent_products per_page=\\\\\\\\\\\\\\"8\\\\\\\\\\\\\\" columns=\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\"]</p>\\",\\"add_css_text\\":\\"product-tiles\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 227,
  'post_date' => '2016-04-05 03:00:14',
  'post_date_gmt' => '2016-04-05 03:00:14',
  'post_content' => '',
  'post_title' => 'Portfolio',
  'post_excerpt' => '',
  'post_name' => 'portfolio',
  'post_modified' => '2017-10-27 02:33:28',
  'post_modified_gmt' => '2017-10-27 02:33:28',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=227',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Beautiful portfolio work in masonry grid',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#668cd1',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/yosemite-valey.jpg',
    'layout' => 'grid3',
    'display_content' => 'content',
    'portfolio_query_category' => '0',
    'portfolio_disable_filter' => 'yes',
    'portfolio_disable_masonry' => 'yes',
    'portfolio_more_posts' => 'infinite',
    'portfolio_posts_per_page' => '7',
    'portfolio_display_content' => 'none',
    'portfolio_image_width' => '600',
    'portfolio_image_height' => '600',
    'portfolio_hide_title' => 'default',
    'portfolio_hide_meta_all' => 'no',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 892,
  'post_date' => '2016-05-11 00:17:53',
  'post_date_gmt' => '2016-05-11 00:17:53',
  'post_content' => '',
  'post_title' => 'Blog - Custom Tile Size',
  'post_excerpt' => '',
  'post_name' => 'blog-custom-tile-size',
  'post_modified' => '2017-08-21 02:50:19',
  'post_modified_gmt' => '2017-08-21 02:50:19',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=892',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#865eeb',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/yosemite-valey.jpg',
    'query_category' => '0',
    'layout' => 'custom_tiles',
    'more_posts' => 'infinite',
    'posts_per_page' => '9',
    'display_content' => 'none',
    'image_width' => '600',
    'image_height' => '600',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1219,
  'post_date' => '2016-05-12 23:43:00',
  'post_date_gmt' => '2016-05-12 23:43:00',
  'post_content' => '<h3>Auto Tiles</h3>
[list_posts style="auto_tiles" post_filter="yes" limit="7" load_more="yes" post_meta="yes"  image_w="600" image_h="600"]
<h3>Custom Tiles</h3>
[list_posts style="custom_tiles" load_more="no" limit="7" post_meta="yes" post_date="yes" image_w="600" image_h="600"]
<h3>List Post</h3>
[list_posts style="list-post" limit="1" post_date="yes" display="excerpt" image_w="1160" image_h="600"]
<h3>Grid2 Normal</h3>
[list_posts style="grid2" limit="2" post_date="yes" image_w="560" image_h="380"]
<h3>Grid3 Polaroid</h3>
[list_posts style="grid3 polaroid" limit="3" post_date="yes" image_w="365" image_h="250"]
<h3>Grid4 Overlay</h3>
[list_posts style="grid4 overlay" limit="8" post_date="yes" image_w="290" image_h="290"]
<h3>Grid4 No-Gutter Overlay</h3>
[list_posts style="grid4 no-gutter overlay" limit="8" post_date="yes" image_w="290" image_h="290"]
<h3>Grid3 Masonry Polaroid With Load More</h3>
[list_posts style="grid3 masonry polaroid no-gutter" limit="8" load_more="yes" post_date="yes" image_w="430" image_h="0"]',
  'post_title' => 'Blog - Post Layouts',
  'post_excerpt' => '',
  'post_name' => 'blog-post-layouts',
  'post_modified' => '2017-10-29 15:52:14',
  'post_modified_gmt' => '2017-10-29 15:52:14',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1219',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Show all combine-able post layouts one page',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#96794b',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 381,
  'post_date' => '2016-04-05 23:57:19',
  'post_date_gmt' => '2016-04-05 23:57:19',
  'post_content' => '[woocommerce_my_account]',
  'post_title' => 'My Account',
  'post_excerpt' => '',
  'post_name' => 'my-account',
  'post_modified' => '2017-08-21 02:50:45',
  'post_modified_gmt' => '2017-08-21 02:50:45',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/my-account/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'background_repeat' => 'fullcover',
    'portfolio_hide_title' => 'default',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 791,
  'post_date' => '2016-05-06 01:09:59',
  'post_date_gmt' => '2016-05-06 01:09:59',
  'post_content' => '',
  'post_title' => 'Home - Corporate',
  'post_excerpt' => '',
  'post_name' => 'home-corporate',
  'post_modified' => '2017-08-23 02:29:34',
  'post_modified_gmt' => '2017-08-23 02:29:34',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=791',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Welcome to the Peak</h1><p>Peak is a modern grid based WordPress theme, boasting a masonry grid that adapts to any screen size or device thrown at it. The grid auto-populates, but you have full control of any new tiles you want to put in there.</p><p> </p>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"icon_color_bg\\":\\"default\\",\\"link\\":\\"https://twitter.com/themify\\",\\"new_window\\":[]},{\\"icon\\":\\"fa-facebook\\",\\"icon_color_bg\\":\\"default\\",\\"link\\":\\"https://www.facebook.com/themify\\",\\"new_window\\":[]},{\\"icon\\":\\"fa-youtube-play\\",\\"icon_color_bg\\":\\"default\\",\\"link\\":\\"https://www.youtube.com/user/themifyme/\\",\\"new_window\\":[]}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"10\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_color_icon\\":\\"ffffff_1.00\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"content_icon\\":[{\\"icon\\":\\"fa-angle-double-down\\",\\"icon_color_bg\\":\\"default\\",\\"link\\":\\"#portfolio\\",\\"new_window\\":[]}],\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.6\\",\\"font_size_unit\\":\\"em\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_color_icon\\":\\"ffffff_1.00\\",\\"animation_effect\\":\\"flipInX\\",\\"animation_effect_repeat\\":\\"1000\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/04/mountains-918569_1280.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"25e6cf_0.60\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"18\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"10\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"3\\",\\"color_divider\\":\\"000000_1.00\\",\\"divider_type\\":\\"custom\\",\\"divider_width\\":\\"80\\",\\"divider_align\\":\\"center\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Portfolio</h2>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"10\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"portfolio\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"0|multiple\\",\\"product_cat_post\\":\\"0|multiple\\",\\"product_tag_post\\":\\"0|multiple\\",\\"product_shipping_class_post\\":\\"0|multiple\\",\\"portfolio-category_post\\":\\"0|multiple\\",\\"post_per_page_post\\":\\"7\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"600\\",\\"img_height_post\\":\\"600\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"font_size_title_unit\\":\\"px\\",\\"line_height_title_unit\\":\\"px\\",\\"font_size_meta_unit\\":\\"px\\",\\"line_height_meta_unit\\":\\"px\\",\\"font_size_date_unit\\":\\"px\\",\\"line_height_date_unit\\":\\"px\\",\\"font_size_content_unit\\":\\"px\\",\\"line_height_content_unit\\":\\"px\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"See More\\",\\"link\\":\\"https://themify.me/demo/themes/peak/portfolio/\\",\\"button_color_bg\\":\\"black\\"}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"padding_top_link_unit\\":\\"px\\",\\"padding_right_link_unit\\":\\"px\\",\\"padding_bottom_link_unit\\":\\"px\\",\\"padding_left_link_unit\\":\\"px\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"checkbox_link_padding_apply_all_padding\\":\\"padding\\",\\"link_margin_top_unit\\":\\"px\\",\\"link_margin_right_unit\\":\\"px\\",\\"link_margin_bottom_unit\\":\\"px\\",\\"link_margin_left_unit\\":\\"px\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_margin_apply_all_margin\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"link_checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"portfolio\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"b0ffd0_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"10\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"8\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"10\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"3\\",\\"color_divider\\":\\"fffbbd\\",\\"divider_type\\":\\"custom\\",\\"divider_width\\":\\"80\\",\\"divider_align\\":\\"center\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Our Services</h1><p>We cover a wide range of services from branding to web design.</p><p> </p>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"animation_effect\\":\\"fadeInUp\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Web\\",\\"content_feature\\":\\"<p>We harum quidem rerum facilis est et expedita distinctio.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-desktop\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInLeft\\",\\"animation_effect_delay\\":\\".3\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Photos\\",\\"content_feature\\":\\"<p>Pribus autem quibusdam rerum facilis est et expedita distinctio.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-camera-retro\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInUp\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col3-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Events\\",\\"content_feature\\":\\"<p>Facilis ribus autem quibusdam rerum est et expedita distinctio.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-calendar-check-o\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInRight\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Cloud\\",\\"content_feature\\":\\"<p>Expedita distinctio harum quidem rerum facilis est et.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-cloud-upload\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInLeft\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Database\\",\\"content_feature\\":\\"<p>Edist inctio xpedita harum quidem rerum facilest quidemet.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-database\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInUp\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col3-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Social\\",\\"content_feature\\":\\"<p>The inctio xpedita harum quide quidemet rerum facilest.</p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"fff23d\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-commenting-o\\",\\"icon_color_feature\\":\\"fff23d\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"animation_effect\\":\\"fadeInRight\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/04/landscape-615428_1920.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"7325e8_0.60\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"fffbbd_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"3\\",\\"color_divider\\":\\"ffffff_1.00\\",\\"divider_type\\":\\"custom\\",\\"divider_width\\":\\"80\\",\\"divider_align\\":\\"center\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Tweets Tweets Tweets</h1>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"animation_effect\\":\\"fadeInUp\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1 first\\",\\"modules\\":[],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"widget\\",\\"mod_settings\\":{\\"class_widget\\":\\"Themify_Twitter\\",\\"instance_widget\\":{\\"widget-themify-twitter[3][title]\\":\\"\\",\\"widget-themify-twitter[3][username]\\":\\"themify\\",\\"widget-themify-twitter[3][show_count]\\":\\"5\\",\\"widget-themify-twitter[3][follow_text]\\":\\"→ Follow me\\",\\"widget-themify-twitter[3][include_retweets]\\":\\"on\\"},\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1 last\\",\\"modules\\":[],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"Follow Us\\",\\"link\\":\\"https://twitter.com/themify\\",\\"button_color_bg\\":\\"default\\"}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"padding_top_link_unit\\":\\"px\\",\\"padding_right_link_unit\\":\\"px\\",\\"padding_bottom_link_unit\\":\\"px\\",\\"padding_left_link_unit\\":\\"px\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"checkbox_link_padding_apply_all_padding\\":\\"padding\\",\\"link_margin_top_unit\\":\\"px\\",\\"link_margin_right_unit\\":\\"px\\",\\"link_margin_bottom_unit\\":\\"px\\",\\"link_margin_left_unit\\":\\"px\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_margin_apply_all_margin\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"link_checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/04/sunrise-1244886_1280-1.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"0fd7ff_0.72\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"breakpoint_mobile\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\"}}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"3\\",\\"color_divider\\":\\"f9ffb2\\",\\"divider_type\\":\\"custom\\",\\"divider_width\\":\\"80\\",\\"divider_align\\":\\"center\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Contact</h1><p>1 Younge Street<br />Toronto, Ontario<br />Canada</p>\\",\\"font_family\\":\\"default\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"animation_effect\\":\\"fadeInUp\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1 first\\",\\"modules\\":[],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"map_display_type\\":\\"dynamic\\",\\"address_map\\":\\"1 Younge Street\\\\nToronto, Ontario, canada\\",\\"zoom_map\\":\\"15\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"w_map_static\\":\\"500\\",\\"h_map\\":\\"350\\",\\"unit_h\\":\\"px\\",\\"b_style_map\\":\\"solid\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1 last\\",\\"modules\\":[],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"928cf2_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"f9ffb2_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 604,
  'post_date' => '2016-04-27 02:39:49',
  'post_date_gmt' => '2016-04-27 02:39:49',
  'post_content' => '',
  'post_title' => 'Home - Photographer',
  'post_excerpt' => '',
  'post_name' => 'home-photographer',
  'post_modified' => '2017-08-23 02:08:03',
  'post_modified_gmt' => '2017-08-23 02:08:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=604',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Welcome</h1><p>Hello, My Name is David. I am an artist and photographer</p>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff\\",\\"font_size\\":\\"1.4\\",\\"font_size_unit\\":\\"em\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"breakpoint_tablet\\":{\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right\\":\\"5\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"5\\",\\"margin_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\"}}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"fullheight\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"slider\\",\\"background_slider\\":\\"[gallery link=\\\\\\\\\\\\\\"file\\\\\\\\\\\\\\" size=\\\\\\\\\\\\\\"full\\\\\\\\\\\\\\" _orderbyfield=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" _orderByField=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"616,732,610,733,611,618\\\\\\\\\\\\\\"]\\",\\"background_slider_size\\":\\"large\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color\\":\\"69b2ff_0.53\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/peak/files/2016/05/photographer-1150052_1920-1024x682-400x400.jpg\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"400\\",\\"auto_fullwidth\\":\\"|\\",\\"height_image\\":\\"400\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_caption\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"custom_css_column\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>About Me</h2><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita disnctio.</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"12\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"8\\",\\"margin_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"MORE\\",\\"link\\":\\"https://themify.me/\\",\\"button_color_bg\\":\\"default\\"}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"3\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom\\":\\"5\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left\\":\\"8\\",\\"margin_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"col_align_middle\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>My Latest Work</h2>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"portfolio\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"0|multiple\\",\\"product_cat_post\\":\\"0|multiple\\",\\"product_tag_post\\":\\"0|multiple\\",\\"product_shipping_class_post\\":\\"0|multiple\\",\\"portfolio-category_post\\":\\"0|multiple\\",\\"post_per_page_post\\":\\"4\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"font_size_title_unit\\":\\"px\\",\\"line_height_title_unit\\":\\"px\\",\\"font_size_meta_unit\\":\\"px\\",\\"line_height_meta_unit\\":\\"px\\",\\"font_size_date_unit\\":\\"px\\",\\"line_height_date_unit\\":\\"px\\",\\"font_size_content_unit\\":\\"px\\",\\"line_height_content_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/05/taj-mahal-1209004_1920.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"53e6d2_0.61\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>My Services</h2>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Wedding Photography\\",\\"content_feature\\":\\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>\\",\\"layout_feature\\":\\"icon-left\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"small\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-heart\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"8\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Product Branding\\",\\"content_feature\\":\\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>\\",\\"layout_feature\\":\\"icon-left\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"small\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-gift\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"8\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom\\":\\"15\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Documentary Photography\\",\\"content_feature\\":\\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>\\",\\"layout_feature\\":\\"icon-left\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"small\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-rocket\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Video Editing & Recording \\",\\"content_feature\\":\\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>\\",\\"layout_feature\\":\\"icon-left\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"1\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"small\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-play\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"param_feature\\":\\"|\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"15\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>&lt;h2&gt;My Skills&lt;/h2&gt;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right\\":\\"7\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"MORE\\",\\"link\\":\\"https://themify.me/\\",\\"button_color_bg\\":\\"black\\"}],\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"5\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"progressbar\\",\\"mod_settings\\":{\\"progress_bars\\":[{\\"bar_label\\":\\"Cameras\\",\\"bar_percentage\\":\\"90\\",\\"bar_color\\":\\"daa51e\\"},{\\"bar_label\\":\\"Design\\",\\"bar_percentage\\":\\"85\\",\\"bar_color\\":\\"b1da1e\\"},{\\"bar_label\\":\\"Video Editing\\",\\"bar_percentage\\":\\"90\\",\\"bar_color\\":\\"daa51e\\"},{\\"bar_label\\":\\"Video Recording\\",\\"bar_percentage\\":\\"85\\",\\"bar_color\\":\\"b1da1e\\"},{\\"bar_label\\":\\"Product Branding\\",\\"bar_percentage\\":\\"75\\",\\"bar_color\\":\\"daa51e\\"}],\\"hide_percentage_text\\":\\"no\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>My Journal</h2>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_list_posts style=\\\\\\\\\\\\\\"grid3 overlay no-gutter\\\\\\\\\\\\\\"  limit=\\\\\\\\\\\\\\"6\\\\\\\\\\\\\\" load_more=\\\\\\\\\\\\\\"yes\\\\\\\\\\\\\\" post_meta=\\\\\\\\\\\\\\"yes\\\\\\\\\\\\\\" image_w=\\\\\\\\\\\\\\"400\\\\\\\\\\\\\\" image_h=\\\\\\\\\\\\\\"260\\\\\\\\\\\\\\"]</p>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top\\":\\"3\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"5\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"|\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak/files/2016/05/buildings-1149917_1920.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color\\":\\"a38b48_0.74\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Client Testimonials</h2>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"3\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"slider\\",\\"mod_settings\\":{\\"layout_display_slider\\":\\"text\\",\\"blog_category_slider\\":\\"|single\\",\\"slider_category_slider\\":\\"|single\\",\\"portfolio_category_slider\\":\\"|single\\",\\"testimonial_category_slider\\":\\"|single\\",\\"order_slider\\":\\"desc\\",\\"orderby_slider\\":\\"date\\",\\"display_slider\\":\\"content\\",\\"text_content_slider\\":[{\\"text_caption_slider\\":\\"<p>&lt;h3&gt;Morgan Alstair&lt;/h3&gt;</p><p>David is a professional!  He is my wedding photographer, and what I get is the best wedding picture in the world. very artistic and unique concept. This man is very recommended.</p>\\"},{\\"text_caption_slider\\":\\"<p>&lt;h3&gt;Samantha&lt;/h3&gt;</p><p>Definitely recommended, I use David service to edit my short movie videos, and the result is magnificent</p>\\"},{\\"text_caption_slider\\":\\"<p>&lt;h3&gt;Robert Schurle&lt;/h3&gt;</p><p>David services is terrific, I ask him to take a picture of my product and make a catalogue, and now my product is very popular because the catalogue is very well made</p>\\"}],\\"layout_slider\\":\\"slider-default\\",\\"visible_opt_slider\\":\\"1\\",\\"auto_scroll_opt_slider\\":\\"4\\",\\"scroll_opt_slider\\":\\"1\\",\\"speed_opt_slider\\":\\"normal\\",\\"effect_slider\\":\\"scroll\\",\\"pause_on_hover_slider\\":\\"resume\\",\\"wrap_slider\\":\\"yes\\",\\"show_nav_slider\\":\\"yes\\",\\"show_arrow_slider\\":\\"yes\\",\\"height_slider\\":\\"variable\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"7\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>60 Yorkville Ave, Toronto, ON, Canada</p>\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"CONTACT\\",\\"link\\":\\"https://themify.me/d\\",\\"button_color_bg\\":\\"default\\"}],\\"font_family\\":\\"default\\",\\"text_align\\":\\"center\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"map_display_type\\":\\"dynamic\\",\\"address_map\\":\\"60 Yorkville Ave, Toronto, ON, Canada\\",\\"zoom_map\\":\\"8\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"w_map_static\\":\\"1000\\",\\"h_map\\":\\"500\\",\\"unit_h\\":\\"px\\",\\"b_style_map\\":\\"solid\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"col_align_middle\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"8\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 825,
  'post_date' => '2016-05-10 02:42:03',
  'post_date_gmt' => '2016-05-10 02:42:03',
  'post_content' => '<!--themify_builder_static--><h1>Fullwidth</h1>
 <p>Peak is a modern grid based WordPress theme, boasting a masonry grid that adapts to any screen size or device thrown at it. The grid auto-populates, but you have full control of any new tiles you want to put in there.</p>
 
 <a href="https://themify.me/themes/peak" > BUY ME </a> 
 
 <a href="https://www.youtube.com/user/themifyme/" > 
 
 </a> 
 <h3> <a href="https://www.youtube.com/user/themifyme/" > YouTube </a> </h3> 
 
 
 <a href="https://www.facebook.com/themify" > 
 
 </a> 
 <h3> <a href="https://www.facebook.com/themify" > Facebook </a> </h3> 
 
 
 <a href="https://www.instagram.com" > 
 
 </a> 
 <h3> <a href="https://www.instagram.com" > Instagram </a> </h3> 
 
 
 <a href="https://www.linkedin.com" > 
 
 </a> 
 <h3> <a href="https://www.linkedin.com" > LinkedIn </a> </h3><!--/themify_builder_static-->',
  'post_title' => 'Home - Fullscreen',
  'post_excerpt' => '',
  'post_name' => 'home-fullscreen',
  'post_modified' => '2017-10-29 14:49:57',
  'post_modified_gmt' => '2017-10-29 14:49:57',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=825',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Fullwidth<\\\\/h1>\\",\\"font_size\\":\\"2\\",\\"font_size_unit\\":\\"em\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c14\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>Peak is a modern grid based WordPress theme, boasting a masonry grid that adapts to any screen size or device thrown at it. The grid auto-populates, but you have full control of any new tiles you want to put in there.<\\\\/p>\\",\\"font_family\\":\\"Domine\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"line_height\\":\\"1.6\\",\\"line_height_unit\\":\\"em\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c18\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"large\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"BUY ME\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/themes\\\\/peak\\",\\"button_color_bg\\":\\"black\\",\\"icon\\":\\"fa-cart-arrow-down\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c22\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"abffaf_0.54\\",\\"font_color\\":\\"030303_1.00\\",\\"link_color\\":\\"030303_1.00\\",\\"padding_top\\":\\"8\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"8\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\"}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/car-398964_1920.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"030303_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"ffe6a3_0.47\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"YouTube\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-youtube-play\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"link_feature\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\\\/\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c37\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"4ee0ca_0.61\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"2\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"2\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Facebook\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-facebook\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"link_feature\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c45\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"85ffcc_0.59\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"2\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"2\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Instagram\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-instagram\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"link_feature\\":\\"https:\\\\/\\\\/www.instagram.com\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c53\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"c3e671_0.39\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"2\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"2\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"LinkedIn\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-linkedin\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"link_feature\\":\\"https:\\\\/\\\\/www.linkedin.com\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c61\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"ff85eb_0.35\\",\\"padding_top\\":\\"6\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"2\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"2\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/04\\\\/aurora-1185464_1920-1.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_position\\":\\"center-bottom\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"0|multiple\\",\\"product_cat_post\\":\\"0|multiple\\",\\"product_tag_post\\":\\"0|multiple\\",\\"product_shipping_class_post\\":\\"0|multiple\\",\\"portfolio-category_post\\":\\"0|multiple\\",\\"post_filter\\":\\"no\\",\\"post_gutter\\":\\"no-gutter\\",\\"post_per_page_post\\":\\"10\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"600\\",\\"img_height_post\\":\\"600\\",\\"hide_page_nav_post\\":\\"yes\\"}}]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_size\\":\\"large\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_repeat\\":\\"repeat\\",\\"background_position\\":\\"center-center\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff\\",\\"padding_bottom\\":\\"4\\",\\"padding_bottom_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"row_width\\":\\"fullwidth-content\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 770,
  'post_date' => '2016-05-05 22:04:03',
  'post_date_gmt' => '2016-05-05 22:04:03',
  'post_content' => '',
  'post_title' => 'Portfolio - Fullwidth',
  'post_excerpt' => '',
  'post_name' => 'portfolio-fullwidth',
  'post_modified' => '2017-08-21 02:50:51',
  'post_modified_gmt' => '2017-08-21 02:50:51',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=770',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_query_category' => '0',
    'portfolio_posts_per_page' => '7',
    'portfolio_image_width' => '800',
    'portfolio_image_height' => '800',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1122,
  'post_date' => '2016-05-12 01:17:05',
  'post_date_gmt' => '2016-05-12 01:17:05',
  'post_content' => '',
  'post_title' => 'Blog - Masonry Grid',
  'post_excerpt' => '',
  'post_name' => 'blog-masonry-grid',
  'post_modified' => '2017-09-30 00:53:49',
  'post_modified_gmt' => '2017-09-30 00:53:49',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1122',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#62c7bb',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/yosemite-valey.jpg',
    'query_category' => '0',
    'layout' => 'grid4',
    'disable_filter' => 'yes',
    'post_content_layout' => 'overlay',
    'disable_masonry' => 'yes',
    'more_posts' => 'infinite',
    'posts_per_page' => '12',
    'display_content' => 'none',
    'image_width' => '400',
    'image_height' => '0',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 723,
  'post_date' => '2016-05-02 05:58:08',
  'post_date_gmt' => '2016-05-02 05:58:08',
  'post_content' => '<!--themify_builder_static--><h1>Chole</h1><p>I am English professional model and fashion designer. Check out my new collections</p>
 
 <a href="#contact" > Contact Me </a> 
 <h3><strong>Blog</strong></h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
 
 <a href="https://themify.me/demo/themes/peak/blog/" > READ MORE </a> 
 <h3><strong>Shop</strong></h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
 
 <a href="https://themify.me/demo/themes/peak/shop/" > SHOP NOW </a> 
 <h3><strong>Portfolio</strong></h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
 
 <a href="https://themify.me/demo/themes/peak/portfolio/" > VIEW </a> 
 <h3><strong>Peak Theme</strong></h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
 
 <a href="https://themify.me/themes/peak" > BUY </a> 
 <h2>Portfolio</h2><p>Some of my recent works</p>
 <h2>Contact</h2>
 
 416-122-5678 
 
 <a href="#" > Book me </a> 
 <p>60 Yorkville Ave, Toronto, ON, Canada</p>
 
 <a href="https://themify.me" > CONTACT </a> 
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=60+Yorkville+Ave%2C+Toronto%2C+ON%2C+Canada&amp;t=m&amp;z=8&amp;output=embed&amp;iwloc=near"></iframe><!--/themify_builder_static-->',
  'post_title' => 'Home - Model',
  'post_excerpt' => '',
  'post_name' => 'home-model',
  'post_modified' => '2017-10-29 14:52:54',
  'post_modified_gmt' => '2017-10-29 14:52:54',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=723',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Chole<\\\\/h1><p>I am English professional model and fashion designer. Check out my new collections<\\\\/p>\\",\\"font_size\\":\\"1.3\\",\\"font_size_unit\\":\\"em\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c16\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"Contact Me\\",\\"link\\":\\"#contact\\",\\"button_color_bg\\":\\"black\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c20\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/builder-layouts\\\\/files\\\\/2016\\\\/01\\\\/96732010-2-1024x768.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"fffeab_0.53\\",\\"font_color\\":\\"030303_1.00\\",\\"link_color\\":\\"030303_1.00\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3><strong>Blog<\\\\/strong><\\\\/h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.<\\\\/p>\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c36\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"small\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"READ MORE\\",\\"link\\":\\"https://themify.me/demo/themes/peak\\\\/blog\\\\/\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c40\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/builder-layouts\\\\/files\\\\/2016\\\\/01\\\\/90849914-4-1024x685.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"431175\\",\\"cover_color\\":\\"431175_0.31\\",\\"cover_color_hover\\":\\"431175_0.71\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\".9\\",\\"font_size_unit\\":\\"em\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3><strong>Shop<\\\\/strong><\\\\/h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.<\\\\/p>\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c48\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"small\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"SHOP NOW\\",\\"link\\":\\"https://themify.me/demo/themes/peak\\\\/shop\\\\/\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c52\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/04\\\\/2015-10-15-abigailkeenan-002-2.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"2de3f0\\",\\"cover_color\\":\\"2ee5f2_0.43\\",\\"cover_color_hover\\":\\"2ee5f2_0.77\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\".9\\",\\"font_size_unit\\":\\"em\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\"},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3><strong>Portfolio<\\\\/strong><\\\\/h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.<\\\\/p>\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c64\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"small\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"VIEW\\",\\"link\\":\\"https://themify.me/demo/themes/peak\\\\/portfolio\\\\/\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c68\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/119283466-1024x683.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"ff12df\\",\\"cover_color\\":\\"ff12d8_0.32\\",\\"cover_color_hover\\":\\"ff12d8_0.65\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\".9\\",\\"font_size_unit\\":\\"em\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3><strong>Peak Theme<\\\\/strong><\\\\/h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.<\\\\/p>\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c76\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"small\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"BUY\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/themes\\\\/peak\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c80\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/car-398964_1920.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"c29334\\",\\"cover_color\\":\\"c49735_0.48\\",\\"cover_color_hover\\":\\"c49735_0.67\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\".9\\",\\"font_size_unit\\":\\"em\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\"}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/yosemite-valey.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"ff1f1f_0.52\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"db12ff_0.73\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-3\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Portfolio<\\\\/h2><p>Some of my recent works<\\\\/p>\\",\\"font_size\\":\\"1.2\\",\\"font_size_unit\\":\\"em\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_bottom\\":\\"10\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c95\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"db12ff_0.39\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"40\\",\\"padding_bottom\\":\\"10\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/yosemite-valey.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-top\\",\\"background_color\\":\\"030303_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"portfolio\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"0|multiple\\",\\"product_cat_post\\":\\"0|multiple\\",\\"product_tag_post\\":\\"0|multiple\\",\\"product_shipping_class_post\\":\\"0|multiple\\",\\"portfolio-category_post\\":\\"0|multiple\\",\\"post_filter\\":\\"no\\",\\"post_gutter\\":\\"gutter\\",\\"post_per_page_post\\":\\"7\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"hide_page_nav_post\\":\\"yes\\"}}]}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"portfolio\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"030303_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"padding_top\\":\\"5\\",\\"padding_right\\":\\"5\\",\\"padding_bottom\\":\\"5\\",\\"padding_left\\":\\"5\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Contact<\\\\/h2>\\",\\"font_size\\":\\"1.4\\",\\"font_size_unit\\":\\"em\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_bottom\\":\\"10\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c117\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"icon_size\\":\\"normal\\",\\"icon_style\\":\\"none\\",\\"content_icon\\":[{\\"icon\\":\\"fa-phone\\",\\"label\\":\\"416-122-5678\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"icon_size\\":\\"normal\\",\\"icon_style\\":\\"none\\",\\"content_icon\\":[{\\"icon\\":\\"fa-calendar-o\\",\\"label\\":\\"Book me\\",\\"link\\":\\"#\\"}],\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"12d8ff_0.72\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"40\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"3\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"10\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"12d8ff_0.41\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"contact\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/peak\\\\/files\\\\/2016\\\\/05\\\\/yosemite-valey.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-bottom\\",\\"background_color\\":\\"030303_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>60 Yorkville Ave, Toronto, ON, Canada<\\\\/p>\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c140\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"normal\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"CONTACT\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\"}],\\"text_align\\":\\"center\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_link_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\",\\"cid\\":\\"c144\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"map_display_type\\":\\"dynamic\\",\\"address_map\\":\\"60 Yorkville Ave, Toronto, ON, Canada\\",\\"zoom_map\\":\\"8\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"w_map_static\\":\\"1000\\",\\"h_map\\":\\"500\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile_hide\\":\\"hide\\"}}]}],\\"column_alignment\\":\\"col_align_middle\\",\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"ffd54c_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1409,
  'post_date' => '2016-08-03 19:10:20',
  'post_date_gmt' => '2016-08-03 19:10:20',
  'post_content' => '',
  'post_title' => 'Columns Menu Test',
  'post_excerpt' => '',
  'post_name' => 'columns-menu-test',
  'post_modified' => '2017-08-21 02:50:31',
  'post_modified_gmt' => '2017-08-21 02:50:31',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1409',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'custom_menu' => 'multicol-menu',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1058,
  'post_date' => '2016-05-11 20:07:36',
  'post_date_gmt' => '2016-05-11 20:07:36',
  'post_content' => '',
  'post_title' => 'Portfolio - Grid',
  'post_excerpt' => '',
  'post_name' => 'portfolio-grid',
  'post_modified' => '2017-08-21 02:50:52',
  'post_modified_gmt' => '2017-08-21 02:50:52',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1058',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Grid layout with numbered pagination (no infinite scroll)',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#27c2bd',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/mount-baker.jpg',
    'display_content' => 'content',
    'portfolio_query_category' => '0',
    'portfolio_layout' => 'grid3',
    'portfolio_disable_filter' => 'yes',
    'portfolio_content_layout' => 'polaroid',
    'portfolio_disable_masonry' => 'yes',
    'portfolio_more_posts' => 'pagination',
    'portfolio_posts_per_page' => '9',
    'portfolio_display_content' => 'none',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1071,
  'post_date' => '2016-05-11 21:37:03',
  'post_date_gmt' => '2016-05-11 21:37:03',
  'post_content' => '',
  'post_title' => 'Portfolio - Custom Tile Size',
  'post_excerpt' => '',
  'post_name' => 'portfolio-custom-tile-size',
  'post_modified' => '2017-08-21 02:50:48',
  'post_modified_gmt' => '2017-08-21 02:50:48',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1071',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Set custom tile size for each portfolio category',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_query_category' => '0',
    'portfolio_layout' => 'custom_tiles',
    'portfolio_disable_filter' => 'yes',
    'portfolio_content_layout' => 'polaroid',
    'portfolio_disable_masonry' => 'yes',
    'portfolio_more_posts' => 'infinite',
    'portfolio_posts_per_page' => '9',
    'portfolio_display_content' => 'none',
    'portfolio_image_width' => '600',
    'portfolio_image_height' => '600',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1234,
  'post_date' => '2016-05-13 00:24:55',
  'post_date_gmt' => '2016-05-13 00:24:55',
  'post_content' => '<h2>Auto Tiles</h2>
[themify_portfolio_posts style="auto_tiles" post_filter="yes" limit="7" load_more="yes" post_meta="yes" image_w="600" image_h="600"]
<h2></h2>
<h2>Custom Tiles</h2>
[themify_portfolio_posts style="custom_tiles" post_filter="yes" limit="7" load_more="yes" post_meta="yes" image_w="600" image_h="600"]
<h2></h2>
<h2>List Post</h2>
[themify_portfolio_posts style="list-post" limit="1" post_date="yes" display="content" image_w="1160" image_h="600"]
<h2>Grid2 Normal</h2>
[themify_portfolio_posts style="grid2" limit="2" post_date="yes" image_w="560" image_h="380"]
<h2>Grid3 Polaroid</h2>
[themify_portfolio_posts style="grid3 polaroid" limit="3" post_date="yes" image_w="365" image_h="250"]
<h2>Grid4 Overlay</h2>
[themify_portfolio_posts style="grid4 overlay" limit="8" post_date="yes" image_w="290" image_h="290"]
<h2>Grid4 No-Gutter Overlay</h2>
[themify_portfolio_posts style="grid4 no-gutter overlay" limit="8" post_date="yes" image_w="290" image_h="290"]
<h2>Grid3 Masonry Polaroid With Load More</h2>
[themify_portfolio_posts style="grid3 masonry polaroid no-gutter" limit="8" load_more="yes" post_date="yes" image_w="430" image_h="0"]',
  'post_title' => 'Portfolio Layouts',
  'post_excerpt' => '',
  'post_name' => 'portfolio-layouts',
  'post_modified' => '2017-08-21 02:51:27',
  'post_modified_gmt' => '2017-08-21 02:51:27',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1234',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Show all combine-able post layouts one page',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1265,
  'post_date' => '2016-05-13 17:00:45',
  'post_date_gmt' => '2016-05-13 17:00:45',
  'post_content' => '',
  'post_title' => 'Blog - List Post',
  'post_excerpt' => '',
  'post_name' => 'blog-list-post',
  'post_modified' => '2017-08-21 02:50:20',
  'post_modified_gmt' => '2017-08-21 02:50:20',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1265',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_sub_heading' => 'Showcase your blog posts in list view',
    'page_layout' => 'sidebar-none',
    'background_repeat' => 'fullcover',
    'page_title_background_color' => '#865eeb',
    'page_title_background_image' => 'https://themify.me/demo/themes/peak/files/2016/05/yosemite-valey.jpg',
    'query_category' => '0',
    'layout' => 'list-post',
    'more_posts' => 'infinite',
    'posts_per_page' => '4',
    'display_content' => 'content',
    'image_width' => '1160',
    'image_height' => '670',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1346,
  'post_date' => '2016-05-21 04:25:52',
  'post_date_gmt' => '2016-05-21 04:25:52',
  'post_content' => '',
  'post_title' => 'Home - Magazine',
  'post_excerpt' => '',
  'post_name' => 'home-magazine',
  'post_modified' => '2017-08-23 02:11:47',
  'post_modified_gmt' => '2017-08-23 02:11:47',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?page_id=1346',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'background_repeat' => 'fullcover',
    'display_content' => 'content',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"auto_tiles\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|single\\",\\"post_tag_post\\":\\"0|single\\",\\"product_cat_post\\":\\"0|single\\",\\"product_tag_post\\":\\"0|single\\",\\"product_shipping_class_post\\":\\"0|single\\",\\"portfolio-category_post\\":\\"0|single\\",\\"post_per_page_post\\":\\"7\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"600\\",\\"img_height_post\\":\\"600\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"font_size_title_unit\\":\\"px\\",\\"line_height_title_unit\\":\\"px\\",\\"font_size_meta_unit\\":\\"px\\",\\"line_height_meta_unit\\":\\"px\\",\\"font_size_date_unit\\":\\"px\\",\\"line_height_date_unit\\":\\"px\\",\\"font_size_content_unit\\":\\"px\\",\\"line_height_content_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"1\\",\\"color_divider\\":\\"ccc\\",\\"top_margin_divider\\":\\"0\\",\\"bottom_margin_divider\\":\\"25\\",\\"divider_type\\":\\"fullwidth\\",\\"divider_width\\":\\"200\\",\\"divider_align\\":\\"left\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>LATEST NEWS</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"grid4\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"8\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"300\\",\\"img_height_post\\":\\"200\\",\\"hide_post_date_post\\":\\"yes\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col3-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"1\\",\\"color_divider\\":\\"ccc\\",\\"top_margin_divider\\":\\"0\\",\\"bottom_margin_divider\\":\\"25\\",\\"divider_type\\":\\"fullwidth\\",\\"divider_width\\":\\"200\\",\\"divider_align\\":\\"left\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>DESIGN</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-post\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"design|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"500\\",\\"img_height_post\\":\\"450\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-thumb-image\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"design|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"3\\",\\"offset_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"hide_feat_img_post\\":\\"yes\\",\\"hide_post_date_post\\":\\"yes\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"border_top_color\\":\\"e6e6e6\\",\\"border_top_width\\":\\"1\\",\\"border_top_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"|\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col3-1\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"1\\",\\"color_divider\\":\\"ccc\\",\\"top_margin_divider\\":\\"0\\",\\"bottom_margin_divider\\":\\"25\\",\\"divider_type\\":\\"fullwidth\\",\\"divider_width\\":\\"200\\",\\"divider_align\\":\\"left\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>TRAVEL</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-post\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"travel|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"500\\",\\"img_height_post\\":\\"450\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-thumb-image\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"travel|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"3\\",\\"offset_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"hide_feat_img_post\\":\\"yes\\",\\"hide_post_date_post\\":\\"yes\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"border_top_color\\":\\"e6e6e6\\",\\"border_top_width\\":\\"1\\",\\"border_top_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"|\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col3-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"1\\",\\"color_divider\\":\\"ccc\\",\\"top_margin_divider\\":\\"0\\",\\"bottom_margin_divider\\":\\"25\\",\\"divider_type\\":\\"fullwidth\\",\\"divider_width\\":\\"200\\",\\"divider_align\\":\\"left\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>NEWS</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-post\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"news|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"500\\",\\"img_height_post\\":\\"450\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-thumb-image\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"news|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"3\\",\\"offset_post\\":\\"1\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"hide_feat_img_post\\":\\"yes\\",\\"hide_post_date_post\\":\\"yes\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"border_top_color\\":\\"e6e6e6\\",\\"border_top_width\\":\\"1\\",\\"border_top_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"|\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_title\\":\\"default\\",\\"font_family_meta\\":\\"default\\",\\"font_family_date\\":\\"default\\",\\"font_family_content\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"3\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"3\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"divider\\",\\"mod_settings\\":{\\"style_divider\\":\\"solid\\",\\"stroke_w_divider\\":\\"1\\",\\"color_divider\\":\\"ccc\\",\\"top_margin_divider\\":\\"0\\",\\"bottom_margin_divider\\":\\"25\\",\\"divider_type\\":\\"fullwidth\\",\\"divider_width\\":\\"200\\",\\"divider_align\\":\\"left\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>SHOP</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[recent_products per_page=\\\\\\\\\\\\\\"8\\\\\\\\\\\\\\" columns=\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\"]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"3\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 292,
  'post_date' => '2016-04-06 08:33:27',
  'post_date_gmt' => '2016-04-06 08:33:27',
  'post_content' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
  'post_title' => 'Plymouth Belvedere',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'plymouth-belvedere',
  'post_modified' => '2017-08-21 02:56:07',
  'post_modified_gmt' => '2017-08-21 02:56:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=292',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284048:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200000',
    '_price' => '200000',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'colorful-pillow',
    '_thumbnail_id' => '1203',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/plymouth.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, apparel, man, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 314,
  'post_date' => '2016-04-05 08:36:29',
  'post_date_gmt' => '2016-04-05 08:36:29',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
  'post_title' => 'Woman\'s Boot',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'womans-boot',
  'post_modified' => '2017-08-21 02:56:23',
  'post_modified_gmt' => '2017-08-21 02:56:23',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=314',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284054:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '150',
    '_price' => '150',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'outdoor-sunglasses',
    '_thumbnail_id' => '1198',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/woman-boots.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, merchandise, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 316,
  'post_date' => '2016-04-06 08:47:42',
  'post_date_gmt' => '2016-04-06 08:47:42',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'Computer Keyboard',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'computer-keyboard',
  'post_modified' => '2017-08-21 02:56:05',
  'post_modified_gmt' => '2017-08-21 02:56:05',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=316',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284047:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '150',
    '_price' => '150',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'man-t-shirt',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '1192',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/computer-keyboard.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, apparel, man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 317,
  'post_date' => '2016-04-05 08:49:26',
  'post_date_gmt' => '2016-04-05 08:49:26',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Woman\'s Pink Watch',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'womans-pink-watch',
  'post_modified' => '2017-08-21 02:56:22',
  'post_modified_gmt' => '2017-08-21 02:56:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=317',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284054:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wp_old_slug' => 'man-black-shirt',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '1200',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/pink-watch.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, merchandise, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 319,
  'post_date' => '2016-04-06 08:52:17',
  'post_date_gmt' => '2016-04-06 08:52:17',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur',
  'post_title' => 'Men\'s Boot',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'mens-boot',
  'post_modified' => '2017-08-21 02:56:03',
  'post_modified_gmt' => '2017-08-21 02:56:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=319',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284191:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '100',
    '_price' => '100',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'baby-shoes',
    '_thumbnail_id' => '1196',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/mens-boot.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'apparel, man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 321,
  'post_date' => '2016-04-05 12:51:58',
  'post_date_gmt' => '2016-04-05 12:51:58',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
  'post_title' => 'Red Woman\'s Boot',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'red-womans-boot',
  'post_modified' => '2017-08-21 02:56:20',
  'post_modified_gmt' => '2017-08-21 02:56:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=321',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284053:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '250',
    '_sale_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'red-hat',
    '_thumbnail_id' => '1199',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/red-boot.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, apparel, merchandise, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 322,
  'post_date' => '2016-04-05 12:53:30',
  'post_date_gmt' => '2016-04-05 12:53:30',
  'post_content' => 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Woman\'s Casual Bag',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'womans-casual-bag',
  'post_modified' => '2017-08-21 02:56:18',
  'post_modified_gmt' => '2017-08-21 02:56:18',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=322',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284052:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '70',
    '_price' => '70',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'hoodie',
    '_thumbnail_id' => '1191',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/woman-casual-bag.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 323,
  'post_date' => '2016-04-06 13:01:05',
  'post_date_gmt' => '2016-04-06 13:01:05',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Sneakers',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'sneakers',
  'post_modified' => '2017-08-21 02:56:01',
  'post_modified_gmt' => '2017-08-21 02:56:01',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=323',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284190:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '100',
    '_price' => '100',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'sky-google',
    '_thumbnail_id' => '1188',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/sneakers.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 324,
  'post_date' => '2016-04-05 13:02:34',
  'post_date_gmt' => '2016-04-05 13:02:34',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'Women\'s Sunglasses',
  'post_excerpt' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_name' => 'woman-sunglasses',
  'post_modified' => '2017-08-21 02:56:17',
  'post_modified_gmt' => '2017-08-21 02:56:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=324',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284196:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '50',
    '_price' => '50',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'woman-shoes',
    '_thumbnail_id' => '1193',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/sunglasses-1.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 325,
  'post_date' => '2016-04-06 13:10:12',
  'post_date_gmt' => '2016-04-06 13:10:12',
  'post_content' => 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Stylish Watch',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'stylish-watch',
  'post_modified' => '2017-08-21 02:55:59',
  'post_modified_gmt' => '2017-08-21 02:55:59',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=325',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284190:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '300',
    '_price' => '300',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wp_old_slug' => 'woman-outfit-set',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '1194',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/watch.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 327,
  'post_date' => '2016-04-05 13:13:56',
  'post_date_gmt' => '2016-04-05 13:13:56',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Traveling Bag',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'traveling-bag',
  'post_modified' => '2017-08-21 02:56:15',
  'post_modified_gmt' => '2017-08-21 02:56:15',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=327',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284196:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '90',
    '_price' => '90',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'woman-bag',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '1202',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/woman-bag.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, merchandise, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 328,
  'post_date' => '2016-04-06 13:22:38',
  'post_date_gmt' => '2016-04-06 13:22:38',
  'post_content' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
  'post_title' => 'Smart Watch',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'smart-watch',
  'post_modified' => '2017-08-21 02:55:57',
  'post_modified_gmt' => '2017-08-21 02:55:57',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=328',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284045:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'kid-toys',
    '_thumbnail_id' => '1187',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/smart-watch-1.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, man, merchandise, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 329,
  'post_date' => '2016-04-06 13:27:49',
  'post_date_gmt' => '2016-04-06 13:27:49',
  'post_content' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Beach Vacation Set',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'beach-vacation-set',
  'post_modified' => '2017-08-21 02:55:55',
  'post_modified_gmt' => '2017-08-21 02:55:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=329',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284189:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '1500',
    '_price' => '1500',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '883',
    '_wp_old_slug' => 'make-up-set',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/sandals-691364_1280.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, apparel, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 330,
  'post_date' => '2016-04-06 13:32:31',
  'post_date_gmt' => '2016-04-06 13:32:31',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
  'post_title' => 'Casual Shoes',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'casual-shoes',
  'post_modified' => '2017-08-21 02:55:53',
  'post_modified_gmt' => '2017-08-21 02:55:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=330',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284044:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '99',
    '_price' => '99',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'multi-color-t-shirt',
    '_thumbnail_id' => '884',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/boots-1092767_1920.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, apparel, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 341,
  'post_date' => '2016-04-05 13:59:41',
  'post_date_gmt' => '2016-04-05 13:59:41',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.',
  'post_title' => 'Men\'s Watch',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'mens-watch',
  'post_modified' => '2017-08-21 02:56:14',
  'post_modified_gmt' => '2017-08-21 02:56:14',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=341',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284051:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '873',
    '_wp_old_slug' => 'men-watch',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/wood-690288_1280.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, man',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 346,
  'post_date' => '2016-04-06 14:01:39',
  'post_date_gmt' => '2016-04-06 14:01:39',
  'post_content' => 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.',
  'post_title' => 'Another Vintage Camera',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'another-vntage-camera',
  'post_modified' => '2017-08-21 02:55:51',
  'post_modified_gmt' => '2017-08-21 02:55:51',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=346',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284042:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'mens-bags',
    '_thumbnail_id' => '885',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/photography-603036_1920.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'man, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 348,
  'post_date' => '2016-04-06 14:39:07',
  'post_date_gmt' => '2016-04-06 14:39:07',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
  'post_title' => 'Retro Sneakers',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'retro-sneakers',
  'post_modified' => '2017-08-21 02:55:48',
  'post_modified_gmt' => '2017-08-21 02:55:48',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=348',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284041:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '120',
    '_price' => '120',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_wp_old_slug' => 'italian-leather-sofa',
    '_thumbnail_id' => '1204',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/retro-sneakers.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'apparel, man, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 350,
  'post_date' => '2016-04-05 14:52:53',
  'post_date_gmt' => '2016-04-05 14:52:53',
  'post_content' => 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Leather Bag',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'leather-bag',
  'post_modified' => '2017-08-21 02:56:12',
  'post_modified_gmt' => '2017-08-21 02:56:12',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=350',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284050:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '200',
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_wp_old_slug' => 'vintage-desk-lamp',
    '_thumbnail_id' => '1195',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/leather-bag.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, man, woman',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 353,
  'post_date' => '2016-04-05 15:01:40',
  'post_date_gmt' => '2016-04-05 15:01:40',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.',
  'post_title' => 'Vintage Camera',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'vintage-camera',
  'post_modified' => '2017-08-21 02:56:10',
  'post_modified_gmt' => '2017-08-21 02:56:10',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=353',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284049:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '120',
    '_price' => '120',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wp_old_slug' => '353',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_thumbnail_id' => '879',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/camera-581126_1920.jpg',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'apparel, man, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 355,
  'post_date' => '2016-04-05 15:18:56',
  'post_date_gmt' => '2016-04-05 15:18:56',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Casual Leather Shoes',
  'post_excerpt' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem',
  'post_name' => 'casual-leather-shoes',
  'post_modified' => '2017-08-21 02:56:08',
  'post_modified_gmt' => '2017-08-21 02:56:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=product&#038;p=355',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503284193:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'builder_switch_frontend' => '0',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_regular_price' => '150',
    '_sale_price' => '120',
    '_price' => '120',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wp_old_slug' => 'men-leather-gloves',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_review_count' => '0',
    '_wc_average_rating' => '0',
    '_thumbnail_id' => '886',
    'post_image' => 'https://themify.me/demo/themes/peak/files/2016/04/shoes-925057_1920.jpg',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, man, merchandise',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 285,
  'post_date' => '2016-03-28 07:31:31',
  'post_date_gmt' => '2016-03-28 07:31:31',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'Sadness',
  'post_excerpt' => 'Picture of sad girl ',
  'post_name' => 'sadness',
  'post_modified' => '2017-08-21 02:52:53',
  'post_modified_gmt' => '2017-08-21 02:52:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=285',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar-none',
    'project_date' => 'March, 30 2016',
    'project_client' => 'Private',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 287,
  'post_date' => '2016-04-05 07:35:18',
  'post_date_gmt' => '2016-04-05 07:35:18',
  'post_content' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
  'post_title' => 'Taj Mahal',
  'post_excerpt' => 'Taj Mahal, the eternal love symbol',
  'post_name' => 'taj-mahal',
  'post_modified' => '2017-08-21 02:52:33',
  'post_modified_gmt' => '2017-08-21 02:52:33',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=287',
  'menu_order' => 1,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar1',
    'project_date' => 'April, 1 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 283,
  'post_date' => '2016-04-28 07:03:29',
  'post_date_gmt' => '2016-04-28 07:03:29',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Beautiful Girl',
  'post_excerpt' => 'Beautiful model for fashion product',
  'post_name' => 'beautiful-girl',
  'post_modified' => '2017-08-21 02:52:26',
  'post_modified_gmt' => '2017-08-21 02:52:26',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=283',
  'menu_order' => 2,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar1 sidebar-left',
    'project_date' => 'March, 29 2016',
    'project_client' => 'Company',
    'project_services' => 'Product Branding',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 281,
  'post_date' => '2016-02-01 07:01:18',
  'post_date_gmt' => '2016-02-01 07:01:18',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'E-commerce',
  'post_excerpt' => 'Hand on tablet to checking the online strore',
  'post_name' => 'e-commerce',
  'post_modified' => '2017-08-21 02:52:56',
  'post_modified_gmt' => '2017-08-21 02:52:56',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=281',
  'menu_order' => 3,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 03 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'product',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 279,
  'post_date' => '2016-02-03 06:57:32',
  'post_date_gmt' => '2016-02-03 06:57:32',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
  'post_title' => 'New Car',
  'post_excerpt' => 'New car photo for product brochure',
  'post_name' => 'new-car',
  'post_modified' => '2017-08-21 02:52:55',
  'post_modified_gmt' => '2017-08-21 02:52:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=279',
  'menu_order' => 4,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 04 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'product',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 277,
  'post_date' => '2016-04-14 06:56:11',
  'post_date_gmt' => '2016-04-14 06:56:11',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat',
  'post_title' => 'Golden Gate Bridge',
  'post_excerpt' => 'Strong and complicated architecture design',
  'post_name' => 'golden-gate-bridge',
  'post_modified' => '2017-08-21 02:52:29',
  'post_modified_gmt' => '2017-08-21 02:52:29',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=277',
  'menu_order' => 5,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 03 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 275,
  'post_date' => '2016-04-05 06:52:33',
  'post_date_gmt' => '2016-04-05 06:52:33',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur',
  'post_title' => 'The Children',
  'post_excerpt' => 'Childhood is the happiest time of our life',
  'post_name' => 'the-children',
  'post_modified' => '2017-08-21 02:52:34',
  'post_modified_gmt' => '2017-08-21 02:52:34',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=275',
  'menu_order' => 6,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 04 2016',
    'project_client' => 'Private',
    'project_services' => 'Photography',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 273,
  'post_date' => '2016-04-05 06:49:46',
  'post_date_gmt' => '2016-04-05 06:49:46',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Drawing Tablet',
  'post_excerpt' => 'Hand-draw live images, animations and graphics',
  'post_name' => 'drawing-tablet',
  'post_modified' => '2017-08-21 02:52:37',
  'post_modified_gmt' => '2017-08-21 02:52:37',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=273',
  'menu_order' => 7,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 03 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'app, product',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 271,
  'post_date' => '2016-04-05 06:47:55',
  'post_date_gmt' => '2016-04-05 06:47:55',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Waiting For Sunrise',
  'post_excerpt' => 'Beautiful sunrise from the top of valley',
  'post_name' => 'waiting-for-sunrise',
  'post_modified' => '2017-08-21 02:52:39',
  'post_modified_gmt' => '2017-08-21 02:52:39',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=271',
  'menu_order' => 8,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'April, 05 2016',
    'project_client' => 'Company',
    'project_services' => 'Product Branding',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 269,
  'post_date' => '2016-05-07 06:46:40',
  'post_date_gmt' => '2016-05-07 06:46:40',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Girl With a Hood (Gallery)',
  'post_excerpt' => 'Beautiful blue eyes',
  'post_name' => 'girl-with-a-hood',
  'post_modified' => '2017-08-21 02:52:23',
  'post_modified_gmt' => '2017-08-21 02:52:23',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=269',
  'menu_order' => 9,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'post_layout' => 'gallery',
    'post_layout_gallery' => '[gallery columns="4" link="file" size="large" ids="1108,1113,1105,398,465,889,646,1005"]',
    'project_date' => 'March, 05 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 267,
  'post_date' => '2016-05-10 06:41:57',
  'post_date_gmt' => '2016-05-10 06:41:57',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
  'post_title' => 'Smart Watch',
  'post_excerpt' => 'Manage your daily activities on the watch',
  'post_name' => 'smart-watch',
  'post_modified' => '2017-08-21 02:52:21',
  'post_modified_gmt' => '2017-08-21 02:52:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=267',
  'menu_order' => 10,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar-none',
    'post_layout' => 'split',
    'image_width' => '800',
    'image_height' => '1200',
    'project_date' => 'March, 18 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 265,
  'post_date' => '2016-04-05 06:34:24',
  'post_date_gmt' => '2016-04-05 06:34:24',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
  'post_title' => 'Woman on Snow',
  'post_excerpt' => 'Winter photography collection ',
  'post_name' => 'woman-on-snow',
  'post_modified' => '2017-08-21 02:52:41',
  'post_modified_gmt' => '2017-08-21 02:52:41',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=265',
  'menu_order' => 11,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 19 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 262,
  'post_date' => '2016-04-05 04:58:19',
  'post_date_gmt' => '2016-04-05 04:58:19',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  'post_title' => 'Reader App',
  'post_excerpt' => 'Find the latest information about economy, politic and lifestyle.',
  'post_name' => 'reader-app',
  'post_modified' => '2017-08-21 02:52:43',
  'post_modified_gmt' => '2017-08-21 02:52:43',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=262',
  'menu_order' => 12,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 20 2016',
    'project_client' => 'Private',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'app',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 260,
  'post_date' => '2016-04-05 04:55:32',
  'post_date_gmt' => '2016-04-05 04:55:32',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
  'post_title' => 'Another Girl',
  'post_excerpt' => 'Picture of teen under the snowfall',
  'post_name' => 'another-girl',
  'post_modified' => '2017-08-21 02:52:46',
  'post_modified_gmt' => '2017-08-21 02:52:46',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=260',
  'menu_order' => 13,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 21 2016',
    'project_client' => 'Company',
    'project_services' => 'Product Branding',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 256,
  'post_date' => '2016-04-13 04:46:37',
  'post_date_gmt' => '2016-04-13 04:46:37',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  'post_title' => 'Smart Finder',
  'post_excerpt' => 'App to find accurate location',
  'post_name' => 'smart-finder',
  'post_modified' => '2017-08-21 02:52:31',
  'post_modified_gmt' => '2017-08-21 02:52:31',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=256',
  'menu_order' => 14,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 22 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'app',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 252,
  'post_date' => '2016-04-05 04:26:16',
  'post_date_gmt' => '2016-04-05 04:26:16',
  'post_content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'Blue Dress',
  'post_excerpt' => 'Model wearing nice blue dress',
  'post_name' => 'blue-dress',
  'post_modified' => '2017-08-21 02:52:47',
  'post_modified_gmt' => '2017-08-21 02:52:47',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=252',
  'menu_order' => 15,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 24 2016',
    'project_client' => 'Private',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photography, product',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 250,
  'post_date' => '2016-04-15 04:12:18',
  'post_date_gmt' => '2016-04-15 04:12:18',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae',
  'post_title' => 'Thinking',
  'post_excerpt' => 'Photo shoot for cosmetic product',
  'post_name' => 'thinking',
  'post_modified' => '2017-08-21 02:52:27',
  'post_modified_gmt' => '2017-08-21 02:52:27',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=250',
  'menu_order' => 16,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar-none',
    'post_layout' => 'split',
    'project_date' => 'April, 02 2016',
    'project_client' => 'Skaters',
    'project_services' => 'Product Branding',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 237,
  'post_date' => '2016-04-05 03:49:59',
  'post_date_gmt' => '2016-04-05 03:49:59',
  'post_content' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
  'post_title' => 'Retro Model',
  'post_excerpt' => 'Beautiful model with vintage retro style ',
  'post_name' => 'retro-model',
  'post_modified' => '2017-08-21 02:52:48',
  'post_modified_gmt' => '2017-08-21 02:52:48',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=237',
  'menu_order' => 17,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'post_layout' => 'gallery',
    'post_layout_gallery' => '[gallery size="large" link="file" ids="1042,1043,1046"]',
    'image_width' => '500',
    'image_height' => '300',
    'project_date' => 'March, 26 2016',
    'project_client' => 'Private',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 231,
  'post_date' => '2016-04-05 03:05:44',
  'post_date_gmt' => '2016-04-05 03:05:44',
  'post_content' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio',
  'post_title' => 'Photo Capture',
  'post_excerpt' => 'Smart photo capture app, let you to edit your own photo',
  'post_name' => 'photo-capture',
  'post_modified' => '2017-08-21 02:52:50',
  'post_modified_gmt' => '2017-08-21 02:52:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=231',
  'menu_order' => 18,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'post_layout' => 'slider',
    'project_date' => 'April, 08 2016',
    'project_client' => 'Company',
    'project_services' => 'Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'app, product',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 225,
  'post_date' => '2016-04-05 02:56:41',
  'post_date_gmt' => '2016-04-05 02:56:41',
  'post_content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur',
  'post_title' => 'A Kid',
  'post_excerpt' => 'Image of a boy with beautiful eyes',
  'post_name' => 'a-kid',
  'post_modified' => '2017-08-21 02:52:52',
  'post_modified_gmt' => '2017-08-21 02:52:52',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?post_type=portfolio&#038;p=225',
  'menu_order' => 19,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'project_date' => 'March, 5 2016',
    'project_client' => 'Private',
    'project_services' => 'Painting',
    'project_launch' => 'http://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'art, photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1125,
  'post_date' => '2016-05-12 01:55:03',
  'post_date_gmt' => '2016-05-12 01:55:03',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1125',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1125',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '140',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1402,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => '',
  'post_title' => '2 Columns',
  'post_excerpt' => '',
  'post_name' => '2-columns',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1402',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1402',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
    '_themify_dropdown_columns' => '2',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1126,
  'post_date' => '2016-05-12 01:55:03',
  'post_date_gmt' => '2016-05-12 01:55:03',
  'post_content' => '',
  'post_title' => 'Home – Corporate',
  'post_excerpt' => '',
  'post_name' => 'home-corporate',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1126',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1125',
    '_menu_item_object_id' => '791',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1403,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => 'Anything related to design.',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1403',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1403',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '5',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1127,
  'post_date' => '2016-05-12 01:55:03',
  'post_date_gmt' => '2016-05-12 01:55:03',
  'post_content' => '',
  'post_title' => 'Home – Fullscreen',
  'post_excerpt' => '',
  'post_name' => 'home-fullscreen',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1127',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1125',
    '_menu_item_object_id' => '825',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1404,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1404',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1404',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '28',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1405,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1405',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1405',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '4',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1128,
  'post_date' => '2016-05-12 01:55:03',
  'post_date_gmt' => '2016-05-12 01:55:03',
  'post_content' => '',
  'post_title' => 'Home – Model',
  'post_excerpt' => '',
  'post_name' => 'home-model',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1128',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1125',
    '_menu_item_object_id' => '723',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1129,
  'post_date' => '2016-05-12 01:55:03',
  'post_date_gmt' => '2016-05-12 01:55:03',
  'post_content' => '',
  'post_title' => 'Home – Photographer',
  'post_excerpt' => '',
  'post_name' => 'home-photographer',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1129',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1125',
    '_menu_item_object_id' => '604',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1406,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1406',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1406',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '27',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1407,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1407',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1407',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '25',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1360,
  'post_date' => '2016-05-23 05:41:17',
  'post_date_gmt' => '2016-05-23 05:41:17',
  'post_content' => '',
  'post_title' => 'Home – Magazine',
  'post_excerpt' => '',
  'post_name' => 'home-magazine',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 140,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1360',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1125',
    '_menu_item_object_id' => '1346',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1238,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Layouts',
  'post_excerpt' => '',
  'post_name' => 'layouts',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1238',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1238',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
    '_themify_mega_menu_column' => '1',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1408,
  'post_date' => '2016-08-03 19:08:21',
  'post_date_gmt' => '2016-08-03 19:08:21',
  'post_content' => 'This category covers everything about travelling',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1408',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1408',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1402',
    '_menu_item_object_id' => '1',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1412,
  'post_date' => '2016-08-03 19:15:31',
  'post_date_gmt' => '2016-08-03 19:15:31',
  'post_content' => '',
  'post_title' => '3 Columns',
  'post_excerpt' => '',
  'post_name' => '3-columns',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1412',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1412',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
    '_themify_dropdown_columns' => '3',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1239,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Blog Layouts',
  'post_excerpt' => '',
  'post_name' => 'blog-archive',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1239',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1238',
    '_menu_item_object_id' => '1239',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1413,
  'post_date' => '2016-08-03 19:15:31',
  'post_date_gmt' => '2016-08-03 19:15:31',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1413',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1413',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '15',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_dropdown_columns' => '2',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1244,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Blog - Auto Tiles',
  'post_excerpt' => '',
  'post_name' => 'blog-auto-tiles',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1244',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1239',
    '_menu_item_object_id' => '374',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1414,
  'post_date' => '2016-08-03 19:15:31',
  'post_date_gmt' => '2016-08-03 19:15:31',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1414',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1414',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1413',
    '_menu_item_object_id' => '17',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1243,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Blog – Custom Tile Size',
  'post_excerpt' => '',
  'post_name' => 'blog-custom-tile-size',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1243',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1239',
    '_menu_item_object_id' => '892',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1267,
  'post_date' => '2016-05-13 17:04:10',
  'post_date_gmt' => '2016-05-13 17:04:10',
  'post_content' => '',
  'post_title' => 'Blog – List Post',
  'post_excerpt' => '',
  'post_name' => 'blog-list-post',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1267',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1239',
    '_menu_item_object_id' => '1265',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1415,
  'post_date' => '2016-08-03 19:15:31',
  'post_date_gmt' => '2016-08-03 19:15:31',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1415',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1415',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1413',
    '_menu_item_object_id' => '21',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1416,
  'post_date' => '2016-08-03 19:15:31',
  'post_date_gmt' => '2016-08-03 19:15:31',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1416',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1416',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1413',
    '_menu_item_object_id' => '18',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1241,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Blog – Masonry Grid',
  'post_excerpt' => '',
  'post_name' => 'blog-masonry-grid',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1241',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1239',
    '_menu_item_object_id' => '1122',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1417,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1417',
  'post_modified' => '2016-08-03 19:29:21',
  'post_modified_gmt' => '2016-08-03 19:29:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1417',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '19',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_dropdown_columns' => '3',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1240,
  'post_date' => '2016-05-13 00:34:44',
  'post_date_gmt' => '2016-05-13 00:34:44',
  'post_content' => '',
  'post_title' => 'Blog – Post Layouts',
  'post_excerpt' => '',
  'post_name' => 'blog-post-layouts',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1240',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1239',
    '_menu_item_object_id' => '1219',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1418,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1418',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1418',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1417',
    '_menu_item_object_id' => '26',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1245,
  'post_date' => '2016-05-13 00:38:08',
  'post_date_gmt' => '2016-05-13 00:38:08',
  'post_content' => '',
  'post_title' => 'Post Layouts',
  'post_excerpt' => '',
  'post_name' => 'single-post',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1245',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1238',
    '_menu_item_object_id' => '1245',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1419,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1419',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1419',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1417',
    '_menu_item_object_id' => '20',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1246,
  'post_date' => '2016-05-13 00:38:08',
  'post_date_gmt' => '2016-05-13 00:38:08',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1246',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1246',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1245',
    '_menu_item_object_id' => '211',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1420,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1420',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1420',
  'menu_order' => 16,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1417',
    '_menu_item_object_id' => '267',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1247,
  'post_date' => '2016-05-13 00:38:08',
  'post_date_gmt' => '2016-05-13 00:38:08',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1247',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1247',
  'menu_order' => 16,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1245',
    '_menu_item_object_id' => '199',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1421,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1421',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1421',
  'menu_order' => 17,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '269',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1248,
  'post_date' => '2016-05-13 00:38:08',
  'post_date_gmt' => '2016-05-13 00:38:08',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1248',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1248',
  'menu_order' => 17,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1245',
    '_menu_item_object_id' => '205',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1422,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1422',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1422',
  'menu_order' => 18,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '283',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1249,
  'post_date' => '2016-05-13 00:38:08',
  'post_date_gmt' => '2016-05-13 00:38:08',
  'post_content' => '',
  'post_title' => 'Default Layout',
  'post_excerpt' => '',
  'post_name' => 'default-layout',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1249',
  'menu_order' => 18,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1245',
    '_menu_item_object_id' => '195',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1423,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1423',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1423',
  'menu_order' => 19,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '250',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1268,
  'post_date' => '2016-05-13 17:09:11',
  'post_date_gmt' => '2016-05-13 17:09:11',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1268',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1268',
  'menu_order' => 19,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1238',
    '_menu_item_object_id' => '227',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1269,
  'post_date' => '2016-05-13 17:09:11',
  'post_date_gmt' => '2016-05-13 17:09:11',
  'post_content' => '',
  'post_title' => 'Portfolio – Custom Tile Size',
  'post_excerpt' => '',
  'post_name' => 'portfolio-custom-tile-size',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1269',
  'menu_order' => 20,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1268',
    '_menu_item_object_id' => '1071',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1424,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1424',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1424',
  'menu_order' => 20,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '277',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1270,
  'post_date' => '2016-05-13 17:09:11',
  'post_date_gmt' => '2016-05-13 17:09:11',
  'post_content' => '',
  'post_title' => 'Portfolio – Fullwidth',
  'post_excerpt' => '',
  'post_name' => 'portfolio-fullwidth',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1270',
  'menu_order' => 21,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1268',
    '_menu_item_object_id' => '770',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1425,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1425',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1425',
  'menu_order' => 21,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '256',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1271,
  'post_date' => '2016-05-13 17:09:11',
  'post_date_gmt' => '2016-05-13 17:09:11',
  'post_content' => '',
  'post_title' => 'Portfolio – Grid',
  'post_excerpt' => '',
  'post_name' => 'portfolio-grid',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1271',
  'menu_order' => 22,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1268',
    '_menu_item_object_id' => '1058',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1426,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1426',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1426',
  'menu_order' => 22,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1412',
    '_menu_item_object_id' => '287',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1272,
  'post_date' => '2016-05-13 17:09:11',
  'post_date_gmt' => '2016-05-13 17:09:11',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1272',
  'post_modified' => '2016-08-03 18:53:03',
  'post_modified_gmt' => '2016-08-03 18:53:03',
  'post_content_filtered' => '',
  'post_parent' => 227,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1272',
  'menu_order' => 23,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1268',
    '_menu_item_object_id' => '1234',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1435,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => '',
  'post_title' => 'Columns Mega Menu',
  'post_excerpt' => '',
  'post_name' => 'columns-mega-menu',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1435',
  'menu_order' => 23,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1435',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
    '_themify_mega_menu_column' => '1',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1434,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => '',
  'post_title' => 'My Portfolios',
  'post_excerpt' => '',
  'post_name' => 'my-portfolios',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1434',
  'menu_order' => 24,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1435',
    '_menu_item_object_id' => '237',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_dropdown_columns' => '2',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1252,
  'post_date' => '2016-05-13 00:46:35',
  'post_date_gmt' => '2016-05-13 00:46:35',
  'post_content' => '',
  'post_title' => 'WooCommerce Products',
  'post_excerpt' => '',
  'post_name' => 'woocommerce-products',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1252',
  'menu_order' => 24,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1238',
    '_menu_item_object_id' => '1252',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#WC_Widget_Products',
    '_themify_menu_widget' => 
    array (
      'title' => 'Products',
      'number' => '2',
      'show' => '',
      'orderby' => 'date',
      'order' => 'desc',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1250,
  'post_date' => '2016-05-13 00:45:33',
  'post_date_gmt' => '2016-05-13 00:45:33',
  'post_content' => '',
  'post_title' => 'Widgets',
  'post_excerpt' => '',
  'post_name' => 'widgets',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1250',
  'menu_order' => 25,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1238',
    '_menu_item_object_id' => '1250',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1433,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1433',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1433',
  'menu_order' => 25,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '252',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1251,
  'post_date' => '2016-05-13 00:45:33',
  'post_date_gmt' => '2016-05-13 00:45:33',
  'post_content' => '',
  'post_title' => 'Text',
  'post_excerpt' => '',
  'post_name' => 'text',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1251',
  'menu_order' => 26,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1250',
    '_menu_item_object_id' => '1251',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#WP_Widget_Text',
    '_themify_menu_widget' => 
    array (
      'title' => '',
      'text' => 'You can drop in any widget in the mega menu like Twitter feeds, WooCommerce Products, newsletter subscription form, or display any message like this text widget.',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1432,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1432',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1432',
  'menu_order' => 26,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '260',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1144,
  'post_date' => '2016-05-12 02:12:50',
  'post_date_gmt' => '2016-05-12 02:12:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1144',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1144',
  'menu_order' => 27,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '227',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_mega_menu_item' => '1',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1431,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1431',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1431',
  'menu_order' => 27,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '262',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1145,
  'post_date' => '2016-05-12 02:12:50',
  'post_date_gmt' => '2016-05-12 02:12:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1145',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1145',
  'menu_order' => 28,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1144',
    '_menu_item_object_id' => '8',
    '_menu_item_object' => 'portfolio-category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1430,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1430',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1430',
  'menu_order' => 28,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '265',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1146,
  'post_date' => '2016-05-12 02:12:50',
  'post_date_gmt' => '2016-05-12 02:12:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1146',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1146',
  'menu_order' => 29,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1144',
    '_menu_item_object_id' => '9',
    '_menu_item_object' => 'portfolio-category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1429,
  'post_date' => '2016-08-03 19:15:32',
  'post_date_gmt' => '2016-08-03 19:15:32',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1429',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1429',
  'menu_order' => 29,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '271',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1147,
  'post_date' => '2016-05-12 02:12:50',
  'post_date_gmt' => '2016-05-12 02:12:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1147',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1147',
  'menu_order' => 30,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1144',
    '_menu_item_object_id' => '10',
    '_menu_item_object' => 'portfolio-category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1428,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1428',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1428',
  'menu_order' => 30,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '273',
    '_menu_item_object' => 'portfolio',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1148,
  'post_date' => '2016-05-12 02:12:51',
  'post_date_gmt' => '2016-05-12 02:12:51',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1148',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1148',
  'menu_order' => 31,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1144',
    '_menu_item_object_id' => '6',
    '_menu_item_object' => 'portfolio-category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1438,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1438',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1438',
  'menu_order' => 31,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '211',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1276,
  'post_date' => '2016-05-13 23:02:25',
  'post_date_gmt' => '2016-05-13 23:02:25',
  'post_content' => '',
  'post_title' => 'Blog',
  'post_excerpt' => '',
  'post_name' => 'blog-2',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 374,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1276',
  'menu_order' => 32,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1265',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_mega_menu_item' => '1',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1439,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1439',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1439',
  'menu_order' => 32,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '199',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1138,
  'post_date' => '2016-05-12 02:07:45',
  'post_date_gmt' => '2016-05-12 02:07:45',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1138',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1138',
  'menu_order' => 33,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1276',
    '_menu_item_object_id' => '4',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1440,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1440',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1440',
  'menu_order' => 33,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '205',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1139,
  'post_date' => '2016-05-12 02:07:45',
  'post_date_gmt' => '2016-05-12 02:07:45',
  'post_content' => 'This category covers everything about travelling',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1139',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1139',
  'menu_order' => 34,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1276',
    '_menu_item_object_id' => '1',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1441,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1441',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1441',
  'menu_order' => 34,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '195',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1140,
  'post_date' => '2016-05-12 02:07:45',
  'post_date_gmt' => '2016-05-12 02:07:45',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1140',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1140',
  'menu_order' => 35,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1276',
    '_menu_item_object_id' => '25',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1442,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1442',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1442',
  'menu_order' => 35,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '1434',
    '_menu_item_object_id' => '202',
    '_menu_item_object' => 'post',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1142,
  'post_date' => '2016-05-12 02:07:46',
  'post_date_gmt' => '2016-05-12 02:07:46',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1142',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1142',
  'menu_order' => 36,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1276',
    '_menu_item_object_id' => '28',
    '_menu_item_object' => 'category',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1437,
  'post_date' => '2016-08-03 19:15:33',
  'post_date_gmt' => '2016-08-03 19:15:33',
  'post_content' => '',
  'post_title' => 'Text',
  'post_excerpt' => '',
  'post_name' => 'text-2',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1437',
  'menu_order' => 36,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1435',
    '_menu_item_object_id' => '1437',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#WP_Widget_Text',
    '_themify_menu_widget' => 
    array (
      'title' => '',
      'text' => 'Sample Text widget, displayed in the menu. Isn\'t that cool!
',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1156,
  'post_date' => '2016-05-12 02:20:27',
  'post_date_gmt' => '2016-05-12 02:20:27',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1156',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1156',
  'menu_order' => 37,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '378',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_themify_mega_menu_item' => '1',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1443,
  'post_date' => '2016-08-03 19:16:30',
  'post_date_gmt' => '2016-08-03 19:16:30',
  'post_content' => '',
  'post_title' => 'Themify - Feature Posts',
  'post_excerpt' => '',
  'post_name' => 'themify-feature-posts',
  'post_modified' => '2016-08-03 19:29:22',
  'post_modified_gmt' => '2016-08-03 19:29:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1443',
  'menu_order' => 37,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '1435',
    '_menu_item_object_id' => '1443',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#Themify_Feature_Posts',
    '_themify_menu_widget' => 
    array (
      'title' => 'Recent Posts',
      'category' => '0',
      'orderby' => 'date',
      'order' => 'DESC',
      'show_count' => '5',
      'thumb_width' => '50',
      'thumb_height' => '50',
      'display' => 'none',
      'excerpt_length' => '55',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'multicol-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1154,
  'post_date' => '2016-05-12 02:18:27',
  'post_date_gmt' => '2016-05-12 02:18:27',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1154',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1154',
  'menu_order' => 38,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1156',
    '_menu_item_object_id' => '20',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1150,
  'post_date' => '2016-05-12 02:18:27',
  'post_date_gmt' => '2016-05-12 02:18:27',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1150',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1150',
  'menu_order' => 39,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1156',
    '_menu_item_object_id' => '19',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1152,
  'post_date' => '2016-05-12 02:18:27',
  'post_date_gmt' => '2016-05-12 02:18:27',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1152',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1152',
  'menu_order' => 40,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1156',
    '_menu_item_object_id' => '15',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1237,
  'post_date' => '2016-05-13 00:30:38',
  'post_date_gmt' => '2016-05-13 00:30:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '1237',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1237',
  'menu_order' => 41,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'taxonomy',
    '_menu_item_menu_item_parent' => '1156',
    '_menu_item_object_id' => '17',
    '_menu_item_object' => 'product_cat',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1370,
  'post_date' => '2016-05-24 23:14:01',
  'post_date_gmt' => '2016-05-24 23:14:01',
  'post_content' => '',
  'post_title' => 'Buy',
  'post_excerpt' => '',
  'post_name' => 'buy',
  'post_modified' => '2016-08-03 18:53:04',
  'post_modified_gmt' => '2016-08-03 18:53:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/peak/?p=1370',
  'menu_order' => 42,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '1370',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => 'http://themify.me/themes/peak',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}


function themify_import_get_term_id_from_slug( $slug ) {
	$menu = get_term_by( "slug", $slug, "nav_menu" );
	return is_wp_error( $menu ) ? 0 : (int) $menu->term_id;
}

	$widgets = get_option( "widget_woocommerce_price_filter" );
$widgets[1002] = array (
  'title' => 'Filter by price',
);
update_option( "widget_woocommerce_price_filter", $widgets );

$widgets = get_option( "widget_woocommerce_top_rated_products" );
$widgets[1003] = array (
  'title' => 'Top Rated Products',
  'number' => 3,
);
update_option( "widget_woocommerce_top_rated_products", $widgets );

$widgets = get_option( "widget_themify-social-links" );
$widgets[1004] = array (
  'title' => '',
  'show_link_name' => NULL,
  'open_new_window' => NULL,
  'icon_size' => 'icon-medium',
  'orientation' => 'horizontal',
);
update_option( "widget_themify-social-links", $widgets );

$widgets = get_option( "widget_text" );
$widgets[1005] = array (
  'title' => 'About',
  'text' => 'Peak is a modern grid based WordPress theme, boasting a masonry grid that adapts to any screen size or device thrown at it. The grid auto-populates, but you have full control of any new tiles you want to put in there. Add cards to highlight different categories, authors, testimonials, pages – or just about anything else you can imagine.

[themify_button style="black outline rect"  link="https://themify.me/themes/peak"] Buy Now [/themify_button]',
  'filter' => true,
);
update_option( "widget_text", $widgets );

$widgets = get_option( "widget_themify-feature-posts" );
$widgets[1006] = array (
  'title' => 'Recent Posts',
  'category' => '0',
  'show_count' => '3',
  'show_date' => 'on',
  'show_thumb' => 'on',
  'display' => 'none',
  'hide_title' => NULL,
  'thumb_width' => '50',
  'thumb_height' => '50',
  'excerpt_length' => '55',
  'orderby' => 'date',
  'order' => 'DESC',
);
update_option( "widget_themify-feature-posts", $widgets );

$widgets = get_option( "widget_themify-twitter" );
$widgets[1007] = array (
  'title' => 'Latest Tweets',
  'username' => 'Themify',
  'show_count' => '2',
  'hide_timestamp' => NULL,
  'show_follow' => 'on',
  'follow_text' => '→ Follow me',
  'include_retweets' => NULL,
  'exclude_replies' => NULL,
);
update_option( "widget_themify-twitter", $widgets );

$widgets = get_option( "widget_themify-feature-posts" );
$widgets[1008] = array (
  'title' => 'Featured Posts',
  'category' => '5',
  'show_count' => '2',
  'show_date' => NULL,
  'show_thumb' => 'on',
  'display' => 'excerpt',
  'hide_title' => NULL,
  'thumb_width' => '50',
  'thumb_height' => '50',
  'excerpt_length' => '55',
  'orderby' => 'date',
  'order' => 'DESC',
);
update_option( "widget_themify-feature-posts", $widgets );

$widgets = get_option( "widget_woocommerce_top_rated_products" );
$widgets[1009] = array (
  'title' => 'Top Rated Products',
  'number' => 3,
);
update_option( "widget_woocommerce_top_rated_products", $widgets );



$sidebars_widgets = array (
  'sidebar-main' => 
  array (
    0 => 'woocommerce_price_filter-1002',
    1 => 'woocommerce_top_rated_products-1003',
  ),
  'social-widget' => 
  array (
    0 => 'themify-social-links-1004',
  ),
  'slideout-widgets' => 
  array (
    0 => 'text-1005',
    1 => 'themify-feature-posts-1006',
  ),
  'footer-widget-1' => 
  array (
    0 => 'themify-twitter-1007',
  ),
  'footer-widget-2' => 
  array (
    0 => 'themify-feature-posts-1008',
  ),
  'footer-widget-3' => 
  array (
    0 => 'woocommerce_top_rated_products-1009',
  ),
); 
update_option( "sidebars_widgets", $sidebars_widgets );

$menu_locations = array();
$menu = get_terms( "nav_menu", array( "slug" => "main-menu" ) );
if( is_array( $menu ) && ! empty( $menu ) ) $menu_locations["main-nav"] = $menu[0]->term_id;
set_theme_mod( "nav_menu_locations", $menu_locations );


$homepage = get_posts( array( 'name' => 'home', 'post_type' => 'page' ) );
			if( is_array( $homepage ) && ! empty( $homepage ) ) {
				update_option( 'show_on_front', 'page' );
				update_option( 'page_on_front', $homepage[0]->ID );
			}
			
	ob_start(); ?>a:232:{s:15:"setting-favicon";s:0:"";s:23:"setting-custom_feed_url";s:0:"";s:19:"setting-header_html";s:0:"";s:19:"setting-footer_html";s:0:"";s:23:"setting-search_settings";s:0:"";s:16:"setting-page_404";s:3:"815";s:21:"setting-feed_settings";s:0:"";s:21:"setting-webfonts_list";s:11:"recommended";s:24:"setting-webfonts_subsets";s:0:"";s:22:"setting-default_layout";s:12:"sidebar-none";s:27:"setting-default_post_layout";s:10:"auto_tiles";s:27:"setting-post_content_layout";s:0:"";s:23:"setting-disable_masonry";s:0:"";s:19:"setting-post_gutter";s:6:"gutter";s:30:"setting-default_layout_display";s:4:"none";s:25:"setting-default_more_text";s:4:"More";s:21:"setting-index_orderby";s:4:"date";s:19:"setting-index_order";s:4:"DESC";s:26:"setting-default_post_title";s:0:"";s:33:"setting-default_unlink_post_title";s:0:"";s:25:"setting-default_post_meta";s:0:"";s:32:"setting-default_post_meta_author";s:0:"";s:34:"setting-default_post_meta_category";s:0:"";s:33:"setting-default_post_meta_comment";s:0:"";s:29:"setting-default_post_meta_tag";s:0:"";s:25:"setting-default_post_date";s:0:"";s:30:"setting-default_media_position";s:5:"above";s:26:"setting-default_post_image";s:0:"";s:33:"setting-default_unlink_post_image";s:0:"";s:31:"setting-image_post_feature_size";s:5:"blank";s:24:"setting-image_post_width";s:3:"600";s:25:"setting-image_post_height";s:3:"600";s:32:"setting-default_page_post_layout";s:12:"sidebar-none";s:37:"setting-default_page_post_layout_type";s:0:"";s:31:"setting-default_page_post_title";s:0:"";s:38:"setting-default_page_unlink_post_title";s:0:"";s:30:"setting-default_page_post_meta";s:0:"";s:37:"setting-default_page_post_meta_author";s:0:"";s:39:"setting-default_page_post_meta_category";s:0:"";s:38:"setting-default_page_post_meta_comment";s:0:"";s:34:"setting-default_page_post_meta_tag";s:0:"";s:30:"setting-default_page_post_date";s:0:"";s:31:"setting-default_page_post_image";s:0:"";s:38:"setting-default_page_unlink_post_image";s:0:"";s:38:"setting-image_post_single_feature_size";s:5:"blank";s:31:"setting-image_post_single_width";s:4:"1400";s:32:"setting-image_post_single_height";s:3:"700";s:27:"setting-default_page_layout";s:12:"sidebar-none";s:23:"setting-hide_page_title";s:0:"";s:33:"setting-page_featured_image_width";s:0:"";s:34:"setting-page_featured_image_height";s:0:"";s:38:"setting-default_portfolio_index_layout";s:12:"sidebar-none";s:43:"setting-default_portfolio_index_post_layout";s:10:"auto_tiles";s:32:"setting-portfolio_content_layout";s:0:"";s:33:"setting-portfolio_disable_masonry";s:3:"yes";s:39:"setting-default_portfolio_index_display";s:4:"none";s:37:"setting-default_portfolio_index_title";s:0:"";s:49:"setting-default_portfolio_index_unlink_post_title";s:0:"";s:50:"setting-default_portfolio_index_post_meta_category";s:3:"yes";s:41:"setting-default_portfolio_index_post_date";s:0:"";s:49:"setting-default_portfolio_index_unlink_post_image";s:2:"no";s:48:"setting-default_portfolio_index_image_post_width";s:0:"";s:49:"setting-default_portfolio_index_image_post_height";s:0:"";s:39:"setting-default_portfolio_single_layout";s:12:"sidebar-none";s:54:"setting-default_portfolio_single_portfolio_layout_type";s:0:"";s:38:"setting-default_portfolio_single_title";s:0:"";s:50:"setting-default_portfolio_single_unlink_post_title";s:0:"";s:51:"setting-default_portfolio_single_post_meta_category";s:0:"";s:42:"setting-default_portfolio_single_post_date";s:0:"";s:50:"setting-default_portfolio_single_unlink_post_image";s:3:"yes";s:49:"setting-default_portfolio_single_image_post_width";s:0:"";s:50:"setting-default_portfolio_single_image_post_height";s:0:"";s:34:"setting-portfolio-category_tiled_6";s:12:"square-large";s:34:"setting-portfolio-category_tiled_9";s:12:"square-small";s:34:"setting-portfolio-category_tiled_8";s:9:"landscape";s:35:"setting-portfolio-category_tiled_10";s:8:"portrait";s:35:"setting-portfolio-category_tiled_32";s:12:"square-large";s:22:"themify_portfolio_slug";s:7:"project";s:53:"setting-customizer_responsive_design_tablet_landscape";s:4:"1024";s:43:"setting-customizer_responsive_design_tablet";s:3:"768";s:43:"setting-customizer_responsive_design_mobile";s:3:"480";s:33:"setting-mobile_menu_trigger_point";s:4:"1200";s:24:"setting-gallery_lightbox";s:8:"lightbox";s:27:"setting-script_minification";s:7:"disable";s:26:"setting-page_builder_cache";s:2:"on";s:27:"setting-page_builder_expiry";s:1:"2";s:29:"setting-relationship_taxonomy";s:8:"category";s:37:"setting-relationship_taxonomy_entries";s:1:"3";s:45:"setting-relationship_taxonomy_display_content";s:4:"none";s:18:"setting-more_posts";s:8:"infinite";s:20:"setting-autoinfinite";s:2:"on";s:22:"setting-footer_widgets";s:17:"footerwidget-3col";s:24:"setting-category_tiled_5";s:8:"portrait";s:25:"setting-category_tiled_28";s:12:"square-small";s:24:"setting-category_tiled_4";s:12:"square-large";s:25:"setting-category_tiled_27";s:12:"square-small";s:25:"setting-category_tiled_25";s:8:"portrait";s:24:"setting-category_tiled_1";s:12:"square-small";s:24:"setting-footer_text_left";s:0:"";s:25:"setting-footer_text_right";s:0:"";s:27:"setting-global_feature_size";s:5:"blank";s:22:"setting-link_icon_type";s:9:"font-icon";s:32:"setting-link_type_themify-link-0";s:10:"image-icon";s:33:"setting-link_title_themify-link-0";s:7:"Twitter";s:32:"setting-link_link_themify-link-0";s:0:"";s:31:"setting-link_img_themify-link-0";s:97:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/social/twitter.png";s:32:"setting-link_type_themify-link-1";s:10:"image-icon";s:33:"setting-link_title_themify-link-1";s:8:"Facebook";s:32:"setting-link_link_themify-link-1";s:0:"";s:31:"setting-link_img_themify-link-1";s:98:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/social/facebook.png";s:32:"setting-link_type_themify-link-2";s:10:"image-icon";s:33:"setting-link_title_themify-link-2";s:6:"Google";s:32:"setting-link_link_themify-link-2";s:0:"";s:31:"setting-link_img_themify-link-2";s:101:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/social/google-plus.png";s:32:"setting-link_type_themify-link-3";s:10:"image-icon";s:33:"setting-link_title_themify-link-3";s:7:"YouTube";s:32:"setting-link_link_themify-link-3";s:0:"";s:31:"setting-link_img_themify-link-3";s:97:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/social/youtube.png";s:32:"setting-link_type_themify-link-4";s:10:"image-icon";s:33:"setting-link_title_themify-link-4";s:9:"Pinterest";s:32:"setting-link_link_themify-link-4";s:0:"";s:31:"setting-link_img_themify-link-4";s:99:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/social/pinterest.png";s:32:"setting-link_type_themify-link-6";s:9:"font-icon";s:33:"setting-link_title_themify-link-6";s:8:"Facebook";s:32:"setting-link_link_themify-link-6";s:32:"https://www.facebook.com/themify";s:33:"setting-link_ficon_themify-link-6";s:11:"fa-facebook";s:35:"setting-link_ficolor_themify-link-6";s:0:"";s:37:"setting-link_fibgcolor_themify-link-6";s:0:"";s:32:"setting-link_type_themify-link-5";s:9:"font-icon";s:33:"setting-link_title_themify-link-5";s:7:"Twitter";s:32:"setting-link_link_themify-link-5";s:31:"https://www.twitter.com/themify";s:33:"setting-link_ficon_themify-link-5";s:10:"fa-twitter";s:35:"setting-link_ficolor_themify-link-5";s:0:"";s:37:"setting-link_fibgcolor_themify-link-5";s:0:"";s:32:"setting-link_type_themify-link-7";s:9:"font-icon";s:33:"setting-link_title_themify-link-7";s:6:"Google";s:32:"setting-link_link_themify-link-7";s:0:"";s:33:"setting-link_ficon_themify-link-7";s:14:"fa-google-plus";s:35:"setting-link_ficolor_themify-link-7";s:0:"";s:37:"setting-link_fibgcolor_themify-link-7";s:0:"";s:32:"setting-link_type_themify-link-8";s:9:"font-icon";s:33:"setting-link_title_themify-link-8";s:7:"YouTube";s:32:"setting-link_link_themify-link-8";s:0:"";s:33:"setting-link_ficon_themify-link-8";s:10:"fa-youtube";s:35:"setting-link_ficolor_themify-link-8";s:0:"";s:37:"setting-link_fibgcolor_themify-link-8";s:0:"";s:32:"setting-link_type_themify-link-9";s:9:"font-icon";s:33:"setting-link_title_themify-link-9";s:9:"Pinterest";s:32:"setting-link_link_themify-link-9";s:19:"https://themify.me/";s:33:"setting-link_ficon_themify-link-9";s:12:"fa-pinterest";s:35:"setting-link_ficolor_themify-link-9";s:0:"";s:37:"setting-link_fibgcolor_themify-link-9";s:0:"";s:22:"setting-link_field_ids";s:341:"{"themify-link-0":"themify-link-0","themify-link-1":"themify-link-1","themify-link-2":"themify-link-2","themify-link-3":"themify-link-3","themify-link-4":"themify-link-4","themify-link-6":"themify-link-6","themify-link-5":"themify-link-5","themify-link-7":"themify-link-7","themify-link-8":"themify-link-8","themify-link-9":"themify-link-9"}";s:23:"setting-link_field_hash";s:2:"10";s:30:"setting-page_builder_is_active";s:6:"enable";s:41:"setting-page_builder_animation_appearance";s:0:"";s:42:"setting-page_builder_animation_parallax_bg";s:0:"";s:46:"setting-page_builder_animation_parallax_scroll";s:6:"mobile";s:55:"setting-page_builder_responsive_design_tablet_landscape";s:4:"1024";s:45:"setting-page_builder_responsive_design_tablet";s:3:"768";s:45:"setting-page_builder_responsive_design_mobile";s:3:"680";s:23:"setting-hooks_field_ids";s:2:"[]";s:27:"setting-custom_panel-editor";s:7:"default";s:27:"setting-custom_panel-author";s:7:"default";s:32:"setting-custom_panel-contributor";s:7:"default";s:33:"setting-custom_panel-shop_manager";s:7:"default";s:25:"setting-customizer-editor";s:7:"default";s:25:"setting-customizer-author";s:7:"default";s:30:"setting-customizer-contributor";s:7:"default";s:31:"setting-customizer-shop_manager";s:7:"default";s:22:"setting-backend-editor";s:7:"default";s:22:"setting-backend-author";s:7:"default";s:27:"setting-backend-contributor";s:7:"default";s:28:"setting-backend-shop_manager";s:7:"default";s:23:"setting-frontend-editor";s:7:"default";s:23:"setting-frontend-author";s:7:"default";s:28:"setting-frontend-contributor";s:7:"default";s:29:"setting-frontend-shop_manager";s:7:"default";s:4:"skin";s:91:"https://themify.me/demo/themes/peak/wp-content/themes/themify-peak/themify/img/non-skin.gif";s:27:"setting-search_exclude_post";s:0:"";s:31:"setting-search_settings_exclude";s:0:"";s:30:"setting-search_exclude_product";s:0:"";s:32:"setting-search_exclude_portfolio";s:0:"";s:23:"setting-exclude_img_rss";s:0:"";s:20:"setting-excerpt_more";s:0:"";s:27:"setting-auto_featured_image";s:0:"";s:22:"setting-comments_posts";s:0:"";s:23:"setting-post_author_box";s:0:"";s:24:"setting-post_nav_disable";s:0:"";s:25:"setting-post_nav_same_cat";s:0:"";s:22:"setting-comments_pages";s:0:"";s:29:"setting-portfolio_nav_disable";s:0:"";s:30:"setting-portfolio_nav_same_cat";s:0:"";s:33:"setting-disable_responsive_design";s:0:"";s:31:"setting-lightbox_content_images";s:0:"";s:18:"setting-cache_gzip";s:0:"";s:24:"setting-disable_drop_cap";s:0:"";s:40:"setting-relationship_taxonomy_hide_image";s:0:"";s:19:"setting-exclude_rss";s:0:"";s:27:"setting-exclude_search_form";s:0:"";s:27:"setting-exclude_footer_menu";s:0:"";s:29:"setting-footer_text_left_hide";s:0:"";s:30:"setting-footer_text_right_hide";s:0:"";s:38:"setting-page_builder_disable_shortcuts";s:0:"";s:34:"setting-page_builder_exc_accordion";s:0:"";s:28:"setting-page_builder_exc_box";s:0:"";s:32:"setting-page_builder_exc_buttons";s:0:"";s:32:"setting-page_builder_exc_callout";s:0:"";s:32:"setting-page_builder_exc_divider";s:0:"";s:38:"setting-page_builder_exc_fancy-heading";s:0:"";s:32:"setting-page_builder_exc_feature";s:0:"";s:32:"setting-page_builder_exc_gallery";s:0:"";s:34:"setting-page_builder_exc_highlight";s:0:"";s:29:"setting-page_builder_exc_icon";s:0:"";s:30:"setting-page_builder_exc_image";s:0:"";s:36:"setting-page_builder_exc_layout-part";s:0:"";s:28:"setting-page_builder_exc_map";s:0:"";s:29:"setting-page_builder_exc_menu";s:0:"";s:35:"setting-page_builder_exc_plain-text";s:0:"";s:34:"setting-page_builder_exc_portfolio";s:0:"";s:29:"setting-page_builder_exc_post";s:0:"";s:36:"setting-page_builder_exc_progressbar";s:0:"";s:37:"setting-page_builder_exc_service-menu";s:0:"";s:31:"setting-page_builder_exc_slider";s:0:"";s:28:"setting-page_builder_exc_tab";s:0:"";s:43:"setting-page_builder_exc_testimonial-slider";s:0:"";s:36:"setting-page_builder_exc_testimonial";s:0:"";s:29:"setting-page_builder_exc_text";s:0:"";s:30:"setting-page_builder_exc_video";s:0:"";s:31:"setting-page_builder_exc_widget";s:0:"";s:35:"setting-page_builder_exc_widgetized";s:0:"";s:16:"setting-fontello";s:0:"";}<?php $themify_data = unserialize( ob_get_clean() );

	// fix the weird way "skin" is saved
	if( isset( $themify_data['skin'] ) ) {
		$parsed_skin = parse_url( $themify_data['skin'], PHP_URL_PATH );
		$basedir_skin = basename( dirname( $parsed_skin ) );
		$themify_data['skin'] = trailingslashit( get_template_directory_uri() ) . 'skins/' . $basedir_skin . '/style.css';
	}

	themify_set_data( $themify_data );
	
}
themify_do_demo_import();