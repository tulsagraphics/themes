<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * VIEW OF FW_Extension_Woffice_Map
 */

$js_array = get_option('woffice_map_locations');

if (!empty($js_array)){ ?>
	<div id="members-map-container">
		<div id="members-map"></div>
		<?php if (is_ssl()) { ?>
            <div id="members-map-localize" class="text-center">
                <a href="javascript:void(0)" class="btn btn-default">
                    <i class="fa fa-map-pin"></i> <?php _e('Localize me','woffice'); ?>
                </a>
            </div>
		<?php } ?>
        <a href="javascript:void(0)" id="members-map-trigger" class="btn btn-default">
            <i class="fa fa-map-marker"></i> <?php _e('Members around the world','woffice'); ?>
        </a>
	</div>
<?php } else { ?>
	<div class="center"><p><?php _e('Sorry there is no users locations so we can not display the map. As it is empty.','woffice'); ?></p></div>
<?php
}