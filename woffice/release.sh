echo "/------------------START-------------------/"
version=$(<../dist/version.txt)
echo 'Cleaning the repository!'
echo $version
git checkout .
git pull
git checkout master
echo 'Building Woffice with Gulp'
gulp deploy
echo 'Zipping it'
cd ../dist/$version
zip -r ../../woffice-$version.zip woffice
cd ../../
echo 'Creating the $version tag'
git tag $version
git push origin --tags
echo "/------------------END--------------------/"