<?php
/**
 * Class Woffice_AlkaChat
 *
 * Manage everything related to the AlkaChat
 *
 * @since 2.5.1
 * @author Alkaweb
 */

if( ! class_exists( 'Woffice_AlkaChat' ) ) {
    class Woffice_AlkaChat
    {

        private $endpoint = 'http://localhost:8888/alkahub/public/api/chat/';

        /**
         * Woffice_AlkaChat constructor
         */
        public function __construct()
        {

            if(!function_exists('bp_is_active') || !is_user_logged_in() || !static::isEnabled()) {
                return null;
            }

            add_action( 'wp_footer',                        array($this, 'render')          );
            add_action( 'wp_ajax_woffice_alka_chat',        array($this, 'ajaxCallback')    );
            add_action( 'wp_ajax_nopriv_woffice_alka_chat', array($this, 'ajaxCallback')    );
            add_filter( 'woffice_js_exchanged_data',        array($this, 'exchanger')       );

        }

        /**
         * Pass data to the client
         *
         * @param array $data
         * @return array
         */
        public function exchanger($data) {

            $data['alka_chat'] = array(
                'actions' => array(
                    'new_conversation' => __('New conversation', 'woffice'),
                    //'connect' => __('Connect', 'woffice'),
                    'refresh' => __('Refresh', 'woffice'),
                ),
                'labels' => array(
                    'new_conversation' => __('Create the conversation', 'woffice'),
                    'new_conversation_conversations_placeholder' => __('Please type member ID(s) or username(s)', 'woffice'),
                    'new_conversation_title_label' => __('Conversation title', 'woffice'),
                    'new_conversation_title' => __('Conversation with'),
                    'send' => __('Send'),
                    'not_found' => __('No message found...Send one to start chatting.')
                ),
                'current_user' => get_current_user_id(),
                'nonce' => wp_create_nonce('woffice_alka_chat')
            );

            return $this->formatData($data);

        }

        /**
         * Formatting the data passed to the client
         *
         * @param array $data
         * @return array
         */
        private function formatData($data) {

            // Will be replaced by some options later
            $custom_tab_enabled = woffice_get_settings_option('alka_pro_chat_welcome_enabled');
            $custom_tab_title = woffice_get_settings_option('alka_pro_chat_welcome_title');
            $custom_tab_content = woffice_get_settings_option('alka_pro_chat_welcome_message');

            if(!$custom_tab_enabled)
                return $data;

            $data['alka_chat']['actions']['custom_tab'] = $custom_tab_title;
            $data['alka_chat']['custom_tab'] = $custom_tab_content;

            return $data;

        }

        /**
         * Receive the callbacks from the client
         */
        public function ajaxCallback() {

            // Quick validation
            if (!wp_verify_nonce($_POST['_nonce'], 'woffice_alka_chat' ) || !defined( 'DOING_AJAX' ) || !DOING_AJAX) {
                echo json_encode(array(
                   'type' => 'error',
                    'message' => __('There is a security issue in your request.','woffice')
                ));
                die();
            }

            // Requirements for the API call
            if(!isset($_POST['api_method']) || !isset($_POST['api_target'])) {
                echo json_encode(array(
                    'type' => 'error',
                    'message' => __('Some information is missing from the client request.','woffice')
                ));
                die();
            }

            // We set a default version as it's not a required parameter
            $payload = (!isset($_POST['api_payload'])) ? [] : $_POST['api_payload'];

            echo $this->hubApiHandler($_POST['api_method'], $_POST['api_target'], $payload);
            die();

        }

        /**
         * Handle the Alkaweb API Calls
         *
         * @param string $method - the HTTP method
         * @param string $target - the API target
         * @param array $payload - the data sent in the request
         * @return string - what's returned by the API (encoded in JSON)
         */
        private function hubApiHandler($method, $target, $payload){

            $args = array(
                'headers' => array(
                    'x-email' => base64_encode(get_option('admin_email')),
                    'x-productKey' => base64_encode(get_option('woffice_key'))
                ),
                'body' => $payload,
                'method' => $method
            );

            $response = wp_remote_request( $this->endpoint.$target, $args );

            if(!is_wp_error($response))
                return $this->responseFormat(json_decode($response['body'], true));
            else
                return json_encode(array(
                    'type' => 'error',
                    'message' => $response->get_error_message()
                ));

        }

        /**
         * We apply diverse changes to the request, using the data from the website
         *
         * @param array $response
         * @return string
         */
        private function responseFormat($response) {

            $formatted_response = $response;

            $conversations = (isset($formatted_response['conversations']['data'])) ? $formatted_response['conversations']['data'] : null;
            $conversation = (isset($formatted_response['conversation'])) ? $formatted_response['conversation'] : null;

            if($conversations) {
                foreach ($conversations as $key=>$entry) {
                    $conversations[$key] = $this->formatParticipants($entry);
                }
                $formatted_response['conversations']['data'] = $conversations;
            }

            if($conversation) {
                $conversation = $this->formatParticipants($conversation);
                $formatted_response['conversation'] = $conversation;
            }

            /**
             * Filter to attach custom attributes to the API response
             *
             * @param array $formatted_response - the response
             */
            return json_encode(apply_filters('woffice_alka_chat_api_response', $formatted_response));

        }

        /**
         * Formats the participants by adding extra information
         *
         * @param Object $conversation
         * @return Object
         */
        private function formatParticipants($conversation) {

            if(!isset($conversation['participants']))
                return $conversation;

            /*
             * We attach attributes to the participants
             */
            foreach ($conversation['participants'] as $key2=>$participant) {
                if(get_userdata($participant) !== false) {
                    $conversation['participants'][$key2] = array(
                        '_id' => $participant,
                        '_name' => woffice_get_name_to_display($participant),
                        '_avatar' => get_avatar($participant),
                        '_profile' => bp_core_get_user_domain($participant)
                    );
                }
            }
            /*
             * We make sure the first one is not the current member
             * for the avatar on the frontend
             */
            if($conversation['participants'][0]['_id'] == get_current_user_id()){
                $first_participant = $conversation['participants'][0];
                $second_participant = $conversation['participants'][1];
                $conversation['participants'][0] = $second_participant;
                $conversation['participants'][1] = $first_participant;
            }

            return $conversation;

        }

        /**
         * Checks if the AlkaChat is enabled
         *
         * @return boolean
         */
        static function isEnabled() {

            $enabled = woffice_get_settings_option('alka_pro_chat_enabled');

            // We deactivate it for now
            return FALSE;

        }


        /**
         * Renders the markup
         */
        public function render() {

            get_template_part('template-parts/chat');

        }

    }
}

/**
 * Let's fire it :
 */
new Woffice_AlkaChat();



