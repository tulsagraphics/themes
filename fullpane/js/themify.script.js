;// Themify Theme Scripts - http://themify.me/

// Initialize object literals
var AutoColumnClass = {}, Themify_Carousel_Tools = {}, themifyScript, tbLocalScript;

/////////////////////////////////////////////
// jQuery functions
/////////////////////////////////////////////
(function ($) {

var $sections = $('.type-section'),
	usesRows = ! $sections.length,
	isFullPageScroll = $('body').hasClass('full-section-scrolling') && themifyScript.fullPageScroll && ! usesRows,
	sectionClass = '.section-post:not(.section-post-slide)',
	slideClass = '.module_row_slide',
	sectionsWrapper = 'div:not(.module-layout-part) > #loops-wrapper',
	wowInit2;

// Setup variables if it uses Builder rows instead of section post type
if (usesRows && !Themify.is_builder_active) {
	isFullPageScroll = $('body').hasClass('full-section-scrolling') && themifyScript.fullPageScroll && $('.themify_builder').length;
	sectionClass = '.module_row:not(.module_row_slide)';
	sectionsWrapper = 'div:not(.module-layout-part) > .themify_builder_content:not(.not_editable_builder)';
}

isFullPageScroll && updateFullPage();

// Remove non visible rows
function updateFullPage() {
	var rows = $( sectionsWrapper + ' ' + sectionClass ),
		bp = themifyScript.responsiveBreakpoints || {},
		winWidth = window.innerWidth,
		bpRange = {
			desktop: winWidth >= bp.tablet_landscape,
			tablet: winWidth < bp.tablet_landscape && winWidth >= bp.mobile,
			mobile: winWidth <= bp.mobile
		};

	rows.length && rows.each( function() {
		var $el = $( this );
		
		if( $el.is( ':hidden' ) ) {
			$el.remove();
		} else if ($el.is( '.hide-desktop, .hide-tablet, .hide-mobile' ) ) {
			for( var key in bpRange ) {
				bpRange[key] && $el.is( '.hide-' + key ) && $el.remove();
			}
		}
	} );

	// Set default row column alignment
	window.top._rowColAlign = 'col_align_middle';
}

Themify_Carousel_Tools = {
	intervals: [],
	highlight: function (item) {
		item.addClass('current');
	},
	unhighlight: function ($context) {
		$('li', $context).removeClass('current');
	},
	timer: function ($timer, intervalID, timeout, step) {
		var progress = 0,
				increment = 0;

		this.resetTimer($timer, intervalID);

		this.intervals[intervalID] = setInterval(function () {
			progress += step;
			increment = (progress * 100) / timeout;
			$timer.css('width', increment + '%');
		}, step);
	},
	resetTimer: function ($timer, intervalID) {
		if (null !== this.intervals[intervalID]) {
			clearInterval(this.intervals[intervalID]);
		}
		$timer.width('width', '0%');
	},
	getCenter: function ($context) {
		var visible = $context.triggerHandler('currentVisible'),
				value = typeof visible !== 'undefined' ? visible.length : 1;

		return Math.floor(value / 2);
	},
	getDirection: function ($context, $element) {
		var visible = $context.triggerHandler('currentVisible');
                if(visible){
                    var center = Math.floor(visible.length / 2),
                                    index = $element.index();
                    if (index >= center) {
                            return 'next';
                    }
                    return 'prev';
                }
	},
	adjustCarousel: function ($context) {
		if ($context.closest('.twg-wrap').length > 0) {
			var visible = $context.triggerHandler('currentVisible'),
					visibleLength = typeof visible !== 'undefined' ? visible.length : 1,
					liWidth = $('li:first-child', $context).width();

			$context.triggerHandler('configuration', {width: '' + liWidth * visibleLength, responsive: false});
			$context.parent().css('width', (liWidth * visible) + 'px');
		}
	}
};

// Initialize carousels //////////////////////////////
function createCarousel(obj) {
	obj.each(function () {
		if ($(this).closest('.carousel-ready').length > 0) {
			return true;
		}
		var $this = $(this),
			autoSpeed = 'off' !== $this.data('autoplay') ? parseInt($this.data('autoplay'), 10) : 0,
			sliderArgs = {
				responsive: true,
				circular: ('yes' == $this.data('wrap')),
				infinite: true,
				height: 'auto',
				swipe: true,
				scroll: {
					items: $this.data('scroll') ? parseInt($this.data('scroll'), 10) : 1,
					fx: $this.data('effect'),
					duration: parseInt($this.data('speed')),
					onBefore: function () {
						var $twgWrap = $this.closest('.twg-wrap'),
								$timer = $('.timer-bar', $twgWrap);
						if ($timer.length > 0) {
							Themify_Carousel_Tools.timer($timer, $this.data('id'), autoSpeed, 20);
							Themify_Carousel_Tools.unhighlight($this);
						}
					},
					onAfter: function (items) {
						var newItems = items.items.visible;
						var $twgWrap = $this.closest('.twg-wrap');
						if ($twgWrap.length > 0) {
							var $center = newItems.filter(':eq(' + Themify_Carousel_Tools.getCenter($this) + ')');
							$('.twg-link', $center).trigger(themifyScript.galleryEvent);
							Themify_Carousel_Tools.highlight($center);
						}
					}
				},
				auto: {
					play: ('off' !== $this.data('autoplay')),
					timeoutDuration: autoSpeed
				},
				items: {
					visible: {
						min: 1,
						max: $this.data('visible') ? parseInt($this.data('visible'), 10) : 1
					},
					width: $this.data('width') ? parseInt($this.data('width'), 10) : 222
				},
				prev: {
					button: 'yes' === $this.data('slidernav') ? '#' + $this.data('id') + ' .carousel-prev' : null
				},
				next: {
					button: 'yes' === $this.data('slidernav') ? '#' + $this.data('id') + ' .carousel-next' : null
				},
				pagination: {
					container: 'yes' == $this.data('pager') ? '#' + $this.data('id') + ' .carousel-pager' : null,
					anchorBuilder: function () {
						if ($this.closest('.testimonial.slider').length > 0) {
							var thumb = $('.testimonial-post', this).data('thumb'),
									thumbw = $('.testimonial-post', this).data('thumbw'),
									thumbh = $('.testimonial-post', this).data('thumbh');
							return '<span><a href="#"><img src="' + thumb + '" width="' + thumbw + '" height="' + thumbh + '" /></a></span>';
						}
						if (($this.closest('.portfolio-multiple.slider').length > 0) || ($this.closest('.team-multiple.slider').length > 0)) {
							return '<a href="#"></a>';
						}
						return false;
					}
				},
				onCreate: function () {
					var $slideshowWrap = $this.closest('.slideshow-wrap'),
							$teamSliderWrap = $this.closest('.team-multiple.slider'),
							$portfolioSliderWrap = $this.closest('.portfolio-multiple.slider'),
							$testimonialSlider = $this.closest('.testimonial.slider'),
							$twgWrap = $this.closest('.twg-wrap');

					$this.closest('.slider').prevAll('.slideshow-slider-loader').first().remove(); // remove slider loader

					if ($testimonialSlider.closest('.fp-tableCell').length === 0) {
						$slideshowWrap.css({
							'visibility': 'visible',
							'height': 'auto'
						}).addClass('carousel-ready');
					} else {
						$slideshowWrap.css({
							'height': 'auto'
						}).addClass('carousel-ready');
					}

					if ($testimonialSlider.length > 0) {
						if ($testimonialSlider.closest('.fp-tableCell').length !== 0 || $('body').hasClass('themify_builder_active')) {
							$testimonialSlider.css({
								'visibility': 'visible',
								'height': 'auto'
							});
						}
						$('.carousel-pager', $slideshowWrap).addClass('testimonial-pager');
					}

					if ($teamSliderWrap.length > 0) {
						$teamSliderWrap.css({
							'visibility': 'visible',
							'height': 'auto'
						});
						$('.carousel-prev, .carousel-next', $teamSliderWrap).text('');
					}
					if ($portfolioSliderWrap.length > 0) {
						$portfolioSliderWrap.css({
							'visibility': 'visible',
							'height': 'auto'
						});
						$('.carousel-prev, .carousel-next', $portfolioSliderWrap).text('');
					}

					if ('no' == $this.data('slidernav')) {
						$('.carousel-prev', $slideshowWrap).remove();
						$('.carousel-next', $slideshowWrap).remove();
					}

					if ($twgWrap.length > 0) {

						var center = Themify_Carousel_Tools.getCenter($this),
								$center = $('li', $this).filter(':eq(' + center + ')'),
								$inTableCell = $('.twg-slider').parents('.row_inner').parents('.fp-tableCell');

						Themify_Carousel_Tools.highlight($center);

						$this.trigger('slideTo', [-center, {duration: 0}]);

						$('.carousel-pager', $twgWrap).remove();
						$('.carousel-prev', $twgWrap).addClass('gallery-slider-prev').text('');
						$('.carousel-next', $twgWrap).addClass('gallery-slider-next').text('');

						if ($inTableCell.length > 0) {
							$inTableCell.css('display', 'block');
						}
					}

					$(window).on('debouncedresize', function () {
						// Get all the possible height values from the slides
						var heights = $this.children().map(function () {
							return $(this).height();
						});
						// Find the max height and set it
						$this.parent().add($this).height(Math.max.apply(null, heights));
					}).trigger('resize');

					Themify_Carousel_Tools.adjustCarousel($this);
				}
			};

		// Fix unresponsive js script when there are only one slider item
		if ($this.children().length < 2) {
			sliderArgs.onCreate();
			return true; // skip initialize carousel on this element
		}

		$this.carouFredSel(sliderArgs).find('li').on(themifyScript.galleryEvent, function () {
			if ($this.closest('.twg-wrap').length > 0) {
				var $thisli = $(this);
				$('li', $this).removeClass('current');
				$thisli.addClass('current');
				$thisli.trigger('slideTo', [
					$thisli,
					-Themify_Carousel_Tools.getCenter($this),
					false,
					{
						items: 1,
						duration: 300,
						onBefore: function () {
							var $twgWrap = $this.closest('.twg-wrap'),
									$timer = $('.timer-bar', $twgWrap);
							if ($timer.length > 0) {
								Themify_Carousel_Tools.timer($timer, $this.data('id'), autoSpeed, 20);
								Themify_Carousel_Tools.unhighlight($this);
							}
						},
						onAfter: function (items) {
						}
					},
					null,
					Themify_Carousel_Tools.getDirection($this, $thisli)]
				);
			}
		});

		/////////////////////////////////////////////
		// Resize thumbnail strip on window resize
		/////////////////////////////////////////////
		$(window).on('debouncedresize', Themify_Carousel_Tools.adjustCarousel($this));

	});
}

// Get builder rows anchor class to ID //////////////////////////////
function getClassToId($section) {
	var classes = $section.prop('class').split(' '),
			expr = new RegExp('^tb_section-', 'i'),
			spanClass = null;
	for (var i = 0; i < classes.length; i++) {
		if (expr.test(classes[i])) {
			spanClass = classes[i];
		}
	}

	if (spanClass === null)
		return '';

	return spanClass.replace('tb_section-', '');
}

// Create fullpage scrolling //////////////////////////////
function createFullScrolling() {
	var $body = $('body'),
		autoScrolling = !usesRows && '' != themifyScript.hash.replace('#', '') ? false : true,
		$wrapper = $(sectionsWrapper),
		scrollingStyle = ! $('body.full-section-scrolling-single').length;

	if( $('.module_row').length ) {
		if ( $( 'body.full-section-scrolling-horizontal' ).length ) {

			$( sectionsWrapper )
				.wrapInner( '<div style="display: block" class="section-container ' + $( '.module_row' ).first().removeClass( 'module_row_slide' ).attr( 'class' ).replace( 'module_row_0', '' ) + '"></div>' )
				.children()
				.children()
				.addClass( 'module_row_slide' );

			$('.section-container').removeClass('module_row_0 fullwidth fullcover');

			$( '.module_row_section:not(:first-child)' ).each( function () {
				var clone = $( this ).parent().clone( true, true );
				clone
					.children()
					.eq( $( this ).index() )
					.nextAll()
					.andSelf()
					.remove();
				clone
					.attr( 'class', 'section-container ' + clone.children().first().attr( 'class' ).replace( /module_row_[0-9]+/g, '' ) )
					.removeClass( 'module_row_slide fullwidth fullcover' );

				$( this ).parent().before( clone );
				$( this ).prevAll().remove();
				$( this )
					.parent()
					.attr( 'class', 'section-container ' + $( this ).parent().children().first().attr( 'class' ).replace( /module_row_[0-9]+/g, '' ) )
					.removeClass( 'module_row_slide fullwidth fullcover' );
			} );

		} else {
			$( '.module_row_slide:first-child' ).removeClass( 'module_row_slide' );
			$( sectionClass ).each( function () {
				var $current = $( this ),
					group = [];
				while ( true ) {
					if ( $current.next().is( slideClass ) ) {
						group.push( $current.next().clone( true, true ) );
						$current.next().remove();
					} else {
						break;
					}
				}
				$( this ).wrap( '<div></div>' );
				$( this ).parent()
					.attr( 'class', 'section-container ' + $( this ).attr( 'class' ).replace( /module_row_[0-9]+/g, '' ) )
					.css( 'display', 'block' )
					.removeClass( 'fullwidth fullcover' )
					.append( group );
				$( this ).addClass( 'module_row_slide' );
			} );
		}
	}

	$wrapper.fullpage({
		resize: false,
		sectionSelector: '.section-container',
		slideSelector: slideClass,
		paddingBottom: $body.hasClass('menubar-top') ? 0 : $( '#headerwrap' ).outerHeight( true ),
		scrollOverflow: true,
		navigation: true,
		lockAnchors: true,
		autoScrolling: autoScrolling,
		scrollOverflowOptions: {
			hideScrollbars: true
		},
		scrollHorizontally: scrollingStyle,
		scrollHorizontallyKey: 'QU5ZX1UycmMyTnliMnhzU0c5eWFYcHZiblJoYkd4NWhLbA==',
		slidesNavigation: false,
		afterRender: function () {
			if (!autoScrolling) { // hack deep linking not working when use section row
				$.fn.fullpage.setAutoScrolling(true);
			}

			$(slideClass).each(function(){
				$(this).parent().hide();
				$(this).css({
					'padding-top' : $(this).css('padding-top').replace(/%/g,'vh'),
					'padding-bottom' : $(this).css('padding-bottom').replace(/%/g,'vh')
				});
				$(this).parent().show();
			});

			var $section = $('.section-container.active').find(slideClass + '.active, .section'),
				section_id = usesRows && $section.is('[class*="tb_section-"]') ? getClassToId($section) : $section.prop('id'),
				$aSectionHref = $('#main-nav').find('a[href$="#' + section_id + '"]');

			setTimeout(function () {
				$('.section_loader').hide();
				if ('undefined' !== typeof ThemifyBuilderModuleJs && 'undefined' !== typeof wowInit2) {
					wowInit2();
				}
			}, 1000);

			if (usesRows) {
				var extraEmptyRow = $('#fp-nav').find('li').get($wrapper.children(sectionClass).length);
				
				if ('undefined' !== typeof extraEmptyRow) {
					$(extraEmptyRow).remove();
				}
			}

			if ($aSectionHref.length > 0) {
				$aSectionHref.closest('li').addClass('current_page_item').siblings().removeClass('current_page_item current-menu-item');
				
				if (history.pushState) {
					history.pushState(null, null, '#' + section_id);
				}
			}

			var coverSelectors = '.builder_row_cover, .row-slider, .column-slider, .subrow-slider',
				rowCovers = $( sectionClass ).find( '.fp-tableCell, .fp-scrollable' ).children( coverSelectors );

			if( rowCovers.length ) {
				rowCovers.each( function() {
					var row = $( this ).closest( '.module_row' );
					! row.is( coverSelectors ) && row.prepend( this );
				} );
			}

			$body.trigger('themify_onepage_after_render', [$section, section_id]);
		},
		afterLoad: function () {
			var $section = $wrapper.children(sectionClass + '.active'),
					section_id = usesRows && $section.is('[class*="tb_section-"]') ? getClassToId($section) : $section.prop('id'),
					$aSectionHref = $('#main-nav').find('a[href$="#' + section_id + '"]');

			if ($aSectionHref.length > 0) {
				$aSectionHref.closest('li').addClass('current_page_item').siblings().removeClass('current_page_item current-menu-item');
				
				if (history.pushState) {
					history.pushState(null, null, '#' + section_id);
				}
			}

			$body.trigger('themify_onepage_afterload', [$section, section_id]);
		},
		onLeave: function (index, nextIndex, direction) {

			// when lightbox is active, prevent scrolling the page
			if ($body.find('> .mfp-wrap').length > 0) {
				return false;
			}

			var $rows = usesRows ? $(sectionsWrapper).children('.section-container') : $(sectionsWrapper).find(sectionClass);
			if ($rows.length > 0) {

				if (index > 0 && nextIndex > 0) {
					var sectionIndex = index;
					if ('up' === direction) {
						for (sectionIndex = index; sectionIndex >= nextIndex; sectionIndex--) {
							$rows.eq(sectionIndex - 1).find('.module_row').css('visibility', 'visible');
						}
					} else {
						for (sectionIndex = index; sectionIndex <= nextIndex; sectionIndex++) {
							$rows.eq(sectionIndex - 1).find('.module_row').css('visibility', 'visible');
						}
					}

				}
			}

			// Play BG video when slide is in viewport
			var currentVideo = $( '.fp-section' ).eq( nextIndex - 1 ).find( '.big-video-wrap' ).find( 'iframe, video' );

			if( currentVideo.length ) {
				setTimeout( function() {
					if (typeof currentVideo.get(0).play === 'function' ) {
						currentVideo.get(0).play();
					} else {
						currentVideo.get(0).contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
					}
				} );
			}
		}
	});
}

// Apply auto column position element class /////////////////////////
AutoColumnClass = {
	init: function () {
		this.setup();
	},
	setup: function () {
		// shortcode columns add class
		$('.col2-1.first, .col3-1.first, .col3-2.first, .col4-1.first, .col4-2.first, .col4-3.first', $('#body')).each(function () {
			var $this = $(this);
			if ($this.hasClass('col2-1')) {
				$this.next('.col2-1').addClass('last');
				$this.next('.col4-1').addClass('third').next('.col4-1').addClass('last');
			} else if ($this.hasClass('col3-1')) {
				$this.next('.col3-1').addClass('second').next('.col3-1').addClass('last');
				$this.next('.col3-2').addClass('last');
			} else if ($this.hasClass('col3-2')) {
				$this.next('.col3-1').addClass('last');
			} else if ($this.hasClass('col4-1')) {
				$this.next('.col4-1').addClass('second').next('.col4-1').addClass('third').next('.col4-1').addClass('last');
				$this.next('.col4-2').addClass('second');
			} else if ($this.hasClass('col4-2')) {
				$this.next('.col4-2').addClass('last');
				$this.next('.col4-1').addClass('third').next('.col4-1').addClass('last');
			} else if ($this.hasClass('col4-3')) {
				$this.next('.col4-1').addClass('last');
			}
		});
		$('.col-full').each(function (i) {
			if (i % 2 === 0) {
                            $(this).addClass('animate-last');
			} else {
                            $(this).addClass('animate-first');
			}
		});
	}
};

// Scroll to Element //////////////////////////////
function themeScrollTo(offset) {
	$('body,html').animate({scrollTop: offset}, 800);
}

// DOCUMENT READY
$(document).ready(function () {

	var $window = $(window),
			$body = $('body'),
			$charts = $('.chart', $body),
			$skills = $('.progress-bar', $body);

	//////////////////////
	// Slide menu
	//////////////////////
	$('#menu-icon').themifySideMenu({
		close: '#menu-icon-close'
	});

	var $overlay = $('<div class="body-overlay">');
	$body.append($overlay).on('sidemenushow.themify', function () {
		$overlay.addClass('body-overlay-on');
	}).on('sidemenuhide.themify', function () {
		$overlay.removeClass('body-overlay-on');
	}).on('click.themify touchend.themify', '.body-overlay', function () {
		$('#menu-icon-close').trigger('click');
	})
	/* recalculation of each waypoint’s trigger point, required for modules that use waypoints */
	.on('themify_onepage_afterload themify_onepage_after_render', function (event, $section, section_id) {
		if ($.fn.waypoint) {
			Waypoint.refreshAll();
		}
	})
        .on('themify_onepage_afterload', function (e, $panel) {
		// Trigger wow display for elements in this panel
		if (tbLocalScript && tbLocalScript.animationInviewSelectors && typeof ThemifyBuilderModuleJs !== 'undefined' && typeof ThemifyBuilderModuleJs.wow!== 'undefined' && ThemifyBuilderModuleJs.wow) {
			$(tbLocalScript.animationInviewSelectors).each(function (i, selector) {
				$(selector, $panel).each(function () {
					ThemifyBuilderModuleJs.wow.show(this);
				});
			});
		}
	});
        $window.resize(function () {
		if ($('#menu-icon').is(':visible') && $('#mobile-menu').hasClass('sidemenu-on')) {
			$overlay.addClass('body-overlay-on');
		}
		else {
			$overlay.removeClass('body-overlay-on');
		}
	});
	/////////////////////////////////////////////
	// Chart Initialization
	/////////////////////////////////////////////
	if ($charts.length > 0) {
		if (!$.fn.easyPieChart) {
			Themify.LoadAsync(themify_vars.url + '/js/jquery.easy-pie-chart.min.js', ThemifyChart,null,null,function(){
                            return typeof $.fn.easyPieChart!=='undefined';
                        });
		}
		else {
			ThemifyChart();
		}
	}
	function ThemifyChart() {
		$.each(themifyScript.chart, function (index, value) {
			if ('false' == value || 'true' == value) {
				themifyScript.chart[index] = 'false' != value;
			} else if (parseInt(value)) {
				themifyScript.chart[index] = parseInt(value);
			} else if (parseFloat(value)) {
				themifyScript.chart[index] = parseFloat(value);
			}
		});

		for (var i = 0,len=$charts.length; i < len; ++i) {
			var $self = $charts.eq(i),
					barColor = $self.data('color'),
					percent = $self.data('percent');
			if ('undefined' !== typeof barColor) {
				themifyScript.chart.barColor = '#' + barColor.toString().replace('#', '');
			}
			$self.easyPieChart(themifyScript.chart);
			$self.data('easyPieChart').update(0);
		}

		if (isFullPageScroll && $body.hasClass('query-section')) {
			$body.on('themify_onepage_afterload themify_onepage_after_render', function (event, $section, section_id) {
				var chartLength = $section.find('.chart').length;
				for (var i = 0; i < chartLength; i++) {
					var $self = $section.find('.chart').eq(i),
							percent = $self.data('percent');
					$self.data('easyPieChart').update(percent);
				}
			});
		} else {
			for (var i = 0,len=$charts.length; i < len; ++i) {
				var $self = $charts.eq(i),
						percent = $self.data('percent');
				if (typeof $.fn.waypoint !== 'undefined') {
					$self.waypoint(function (direction) {
						$self.data('easyPieChart').update(percent);
					}, {offset: '80%'});
				} else {
					$self.data('easyPieChart').update(percent);
				}
			}
		}
	}

	/////////////////////////////////////////////
	// Skillset Animation
	/////////////////////////////////////////////
	for (var i = 0; i < $skills.length; i++) {
		$('span', $skills.eq(i)).width('0%');
	}
	if (isFullPageScroll && $body.hasClass('query-section')) {
		$body.on('themify_onepage_afterload themify_onepage_after_render', function (event, $section, section_id) {
			// Skillset
			var $progressBars = $section.find('.progress-bar');
			if ($progressBars.length > 0) {
				$progressBars.find('span').each(function () {
					var $bar = $(this),
							percent = $bar.data('percent');
					if ('undefined' !== typeof percent) {
						$bar.delay(200).animate({
							width: percent
						}, 800);
					}
				});
			}
		});
	} else {
		$skills.each(function(){
			var $this = $(this),
			percent = $('span', $this).data('percent');
			if (typeof $.fn.waypoint !== 'undefined') {
				$this.waypoint(function (direction) {
					$this.find('span').each(function () {
						var $bar = $(this),
							percent = $bar.data('percent');
						if ('undefined' !== typeof percent) {
							$bar.delay(200).animate({
								width: percent
							}, 800);
						}
					});
				}, {offset: '80%'});
			} else {
				$('span', $skills.eq(i)).width(percent);
			}
		});
	}

	/////////////////////////////////////////////
	// Transition Animation ( FlyIn or FadeIn )
	/////////////////////////////////////////////
	AutoColumnClass.init(); // apply auto column class
	if (isFullPageScroll && $body.hasClass('query-section')) {
		var runAnimation = function ($section, section_id) {
			if ('undefined' !== typeof ThemifyBuilderModuleJs && ThemifyBuilderModuleJs.wow !== null && typeof ThemifyBuilderModuleJs.wow.scrollHandler() === 'boolean') {
				ThemifyBuilderModuleJs.wow.scrollHandler();
			}

			if ($section.length > 0 && 'undefined' !== typeof tbLocalScript && tbLocalScript.isAnimationActive) {
				// show animation
				$section.find('.section-title').addClass('animated fadeInLeftBig')
						.end().find('.section-content').addClass('animated flyInBottom');
			}
		};
		$body.on('themify_onepage_afterload themify_onepage_after_render', function (event, $section, section_id) {
			runAnimation($section, section_id);
		});
	} else {
		if ('undefined' !== typeof $.fn.waypoints && 'undefined' !== typeof tbLocalScript && tbLocalScript.isAnimationActive) {
			 $(sectionsWrapper).children(sectionClass).each(function () {
				var $this = $(this);
				$this.waypoint(function () {
					$this.find('.section-title').addClass('animated fadeInLeftBig')
							.end().find('.section-content').addClass('animated flyInBottom');
				}, {offset: '50%'});
			});
		}
	}


	/////////////////////////////////////////////
	// Scroll to top
	/////////////////////////////////////////////
	$('.back-top a').on('click', function (e) {
		e.preventDefault();
		themeScrollTo(0);
	});

	/////////////////////////////////////////////
	// Toggle main nav on mobile
	/////////////////////////////////////////////
	$body.on('click', '#menu-icon', function (e) {
		e.preventDefault();
		//$('#main-nav').fadeToggle();
		$('#top-nav', $('#headerwrap')).hide();
		$(this).toggleClass('active');
	});

	/////////////////////////////////////////////
	// Add class "first" to first elements
	/////////////////////////////////////////////
	$('.highlight-post:odd').addClass('odd');

	/////////////////////////////////////////////
	// Fullscreen bg
	/////////////////////////////////////////////
	if ('undefined' !== typeof $.fn.backstretch) {
		var $sectionPost = $(sectionClass);
		$sectionPost.each(function () {
			var bg = $(this).data('bg');
			if ('undefined' !== typeof bg) {
				if ($(this).hasClass('fullcover')) {
					$(this).backstretch(bg);
				} else {
					$(this).css('background-image', 'url(' + bg + ')');
				}
			}
		});
		$window.on('backstretch.show', function (e, instance) {
			instance.$container.css('z-index', '');
		})
		.on('debouncedresize', function () {
			$sectionPost.each(function () {
				if ($(this).hasClass('fullcover')) {
					var instance = $(this).data("backstretch");
					if ('undefined' !== typeof instance)
						instance.resize();
				}
			});
		});
	}

	/////////////////////////////////////////////
	// Single Gallery Post Type
	/////////////////////////////////////////////
	if ( $('body.single-gallery').length>0 && typeof Themify !== 'undefined' ) {
		Themify.LoadAsync(themify_vars.includesURL + 'js/imagesloaded.min.js', function () {
			Themify.LoadAsync(themify_vars.includesURL + 'js/masonry.min.js', function () {
				$( '.gallery-type-gallery' ).imagesLoaded(function() {
					$('.gallery-type-gallery').masonry({
						itemSelector: '.item',
						isFitWidth: true,
						isAnimated: false
					})
				});
			}, null, null, function () {
				return ('undefined' !== typeof $.fn.masonry);
			});
		}, null, null, function () {
			return ('undefined' !== typeof $.fn.imagesLoaded);
		});
	}

	/////////////////////////////////////////////
	// One Page Scroll
	/////////////////////////////////////////////
	if( isFullPageScroll ){
		if (typeof $.fn.fullpage === 'undefined') {
			Themify.LoadAsync( themifyScript.themeURI + "/js/jquery.fullpage.extensions.min.js", function(){
				$body.trigger('themify_fullpage_afterload');
			}, null, null, function () {
				return "undefined" !== typeof $.fn.fullpage
			} );
		}
	}
	themifyScript.hash = window.location.hash.replace('#', '').replace('!/', '');
	if (isFullPageScroll && $body.hasClass('query-section')) {
		// Get rid of wow js animation since animation is managed with fullpage js
		var callbackTimer = setInterval(function () {
			var call = false;
			try {
				call = ('undefined' !== typeof ThemifyBuilderModuleJs);
			} catch (e) {
			}

			if (call) {
				clearInterval(callbackTimer);
				wowInit2 = ThemifyBuilderModuleJs.wowInit;
				ThemifyBuilderModuleJs.wowInit = function () {
				};
			}
		}, 100);
		$body.on('themify_fullpage_afterload',function(){
			createFullScrolling();
		});
		if( $( window.frameElement ).is( '#themify_builder_site_canvas_iframe' ) ) {
			if (typeof $.fn.fullpage === 'undefined') {
				Themify.LoadAsync( themifyScript.themeURI + "/js/jquery.fullpage.extensions.min.js", function(){
					$body.trigger('themify_fullpage_afterload');
					$.fn.fullpage.destroy('all');
				}, null, null, function () {
					return "undefined" !== typeof $.fn.fullpage
				} );
			}
		}
	}

	var $mainNav = $( '#main-nav' ),
		$mobileMenu = $( '#mobile-menu' ),
		$menuIconClose = $( '#menu-icon-close' );

	function cleanupURL( url ) {
		return url.replace( /#.*$/, '' ).replace( /\/$/, '' );
	}
	$body.on( 'themify_fullpage_afterload', function() {
		$( 'body' ).on( 'click', 'a[href*="#"]:not([href="#"])', function ( e ) {
			var section_id = $( this ).prop( 'hash' ),
				sectionNoHash = section_id.replace( '#', '' ),
				sectionEl = usesRows ? '.tb_section-' + sectionNoHash : section_id;

			if ( $( sectionEl ).length && cleanupURL( window.location.href ) === cleanupURL( $( this ).prop( 'href' ) ) ) {
				e.preventDefault();

				if ( isFullPageScroll && $body.hasClass( 'query-section' ) ) {
					var index_el = $( sectionEl ).index() + 1;
					$.fn.fullpage.moveTo( index_el );
				} else {
					var offset = $( sectionEl ).offset().top;
					themeScrollTo( offset );
				}
				setTimeout( function () {
					window.location.hash = sectionNoHash;
				}, 800 );
			}

			// close mobile menu
			if ( $window.width() <= 1200 && $mainNav.is( ':visible' ) && $mobileMenu.hasClass( 'sidemenu-on' ) ) {
				$menuIconClose.trigger( 'click' );
			}
		} );
	} );

	/////////////////////////////////////////////
	// Portfolio Expander
	/////////////////////////////////////////////
	if ('undefined' !== typeof $.fn.themifyPortfolioExpander) {
		$body.themifyPortfolioExpander({
			itemContainer: '.shortcode.portfolio',
			animeasing: 'easeInQuart',
			animspeed: 500
		});
	}

	/////////////////////////////////////////////
	// Footer Toggle
	/////////////////////////////////////////////
	$('#footer-tab').on('click', 'a', function (e) {
		e.preventDefault();
		$('#footerwrap-inner').slideToggle();
		$('#footerwrap').toggleClass('expanded');
	});

	/////////////////////////////////////////////
	// Fullcover Gallery
	/////////////////////////////////////////////
	function fullCoverGallery(el) {
            var $contexts = $(sectionClass + '.gallery, .twg-slider',el);
            if($contexts.length>0){
		var areaHeight = $window.height() + 5,
                    fullPageOn = isFullPageScroll && $body.hasClass('query-section');
		$contexts.find('.gallery-image-holder').each(function () {
			var $self = $(this),
					thisAreaHeight = areaHeight,
					$section = $self.closest(sectionClass),
					$sectionTitle = $section.find('.section-title'),
					$adminBar = $('#wpadminbar');
			// If this gallery is placed inside a section
			if ( $section.length > 0 ) {
				$section.css( 'paddingBottom', 0 );
				if( $sectionTitle.length > 0) {
					thisAreaHeight -= $sectionTitle.outerHeight(true);
				}
				if ( $( '#headerwrap' ).length > 0 ) {
					thisAreaHeight -= $( '#headerwrap' ).outerHeight(true);
					if( $( 'body' ).hasClass( 'menubar-top' ) ) {
						$self.css( 'marginTop', $( '#headerwrap' ).outerHeight(true) );
					}
				}
			}
			if ($adminBar.length > 0) {
				thisAreaHeight -= $adminBar.outerHeight();
			}
			$self.css({minHeight: thisAreaHeight + 'px'});
		});
		if (! fullPageOn) {
			$contexts.find('.gallery-slider-wrap').css({bottom: ''});
		}
            }
	}
        if(!Themify.is_builder_active){
            fullCoverGallery();
        }
        else{
            $('body').on('builder_load_module_partial',function(e,el,type){
                fullCoverGallery(el);
            });
        }
	

	$window.on('debouncedresize', function () {
                fullCoverGallery();
		var in_customizer = false;

		// check for wp.customize return boolean
		if ( typeof wp !== 'undefined' ) {
			in_customizer =  typeof wp.customize !== 'undefined' ? true : false;
		}
		if ( ! in_customizer ) {
			$('#menu-icon-close').trigger('click');
		}
	});


});

// WINDOW LOAD
$(window).load(function () {
	var $body = $('body');

	// scrolling nav
	if ('undefined' !== typeof $.fn.themifySectionHighlight) {console.log(isFullPageScroll,$body.hasClass('query-section'))
		if (isFullPageScroll && $body.hasClass('query-section')) {
			$body.on('themify_onepage_after_render', function () {
				$body.themifySectionHighlight();
			});
		} else {
			$body.themifySectionHighlight();
		}
		$(window).trigger('scroll');
	}

	if ('undefined' !== typeof $.fn.themifyScrollHighlight) {
		$body.on('scrollhighlight.themify', function (e, section) {
			if ('undefined' !== typeof section && '' != section) {
				$('#fp-nav').find('li').eq($('.tb_section-' + section.replace('#', '')).index()).find('a').trigger('click');
			}
		});
		$(window).trigger('scroll');
	}

	/////////////////////////////////////////////
	// Carousel initialization
	/////////////////////////////////////////////
	var 
		carouselSlideshow = function (el) { 
			var slideShow = $('.slideshow',el);
			if(slideShow.length>0){
				if (!$.fn.carouFredSel) {
					Themify.LoadAsync(themify_vars.url + '/js/carousel.min.js', function () {
							createCarousel(slideShow);
					},null,null,function(){
							return typeof $.fn.carouFredSel!=='undefined';
					});
				}
				else {
					createCarousel(slideShow);
				}
			}
	},
	initGallery = function(el){
		var galler = $('.twg-wrap',el); 
		/////////////////////////////////////////////
		// Initialize WordPress Gallery in Section
		/////////////////////////////////////////////
		if ('undefined' !== typeof $.fn.ThemifyWideGallery && galler.length>0) {
			galler.ThemifyWideGallery({
				speed: parseInt(themifyScript.galleryFadeSpeed, 10),
				event: themifyScript.galleryEvent,
				ajax_url: themifyScript.ajax_url,
				networkError: themifyScript.networkError,
				termSeparator: themifyScript.termSeparator
			});
		}
	};
	if(!Themify.is_builder_active){
		initGallery();
		carouselSlideshow();
	}
	else{
		$('body').on('builder_load_module_partial',function(e,el,type){
			carouselSlideshow(el);
			initGallery(el);
		});
	}

	// hide section loader
	if (!isFullPageScroll && $('.section_loader').length > 0) {
		$('.section_loader').hide();
	}

	// Hack Chrome browser doesn't autoplay the video background
		$body.on('themify_onepage_after_render', function () {
			$.each(tbLocalScript.animationInviewSelectors, function (index, selector) {
				$(selector).css('visibility', 'hidden');
			});

			// Section deep linking
			if (window.location.hash) {
				setTimeout(function () {
					var hashSection = themifyScript.hash;
					if ('' != hashSection && '#' != hashSection) {
						var $sectionEl = usesRows ? $('.tb_section-' + hashSection) : $('#' + hashSection);
						if ( $sectionEl.length > 0 ) {
							$.fn.fullpage.moveTo($sectionEl.index() + 1);
							if( typeof ThemifyBuilderModuleJs!== 'undefined' && ThemifyBuilderModuleJs.wow ) {
								$(tbLocalScript.animationInviewSelectors).each(function (i, selector) {
									$(selector, $sectionEl).addBack().each(function () {
										ThemifyBuilderModuleJs.wow.show(this);
									});
								});
							}
						}
					}
				}, 1500);
			}
		});

	// Make row backgrounds visible.
	$('.module_row').css('visibility', 'visible');

});

})(jQuery);