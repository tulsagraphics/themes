<?php

$base_url = get_template_directory_uri() . '/builder-layouts/';

return array(
	array(
		"title" => "Home",
		"data"  => "home.zip",
		"thumb" => $base_url . "home.jpg"
	),
	array(
		"title" => "Demo 2",
		"data"  => "demo-2.zip",
		"thumb" => $base_url . "demo-2.jpg"
	),
	array(
		"title" => "Demo 3",
		"data"  => "demo-3.zip",
		"thumb" => $base_url . "demo-3.jpg"
	),
);