<?php

defined( 'ABSPATH' ) or die;

$GLOBALS['processed_terms'] = array();
$GLOBALS['processed_posts'] = array();

require_once ABSPATH . 'wp-admin/includes/post.php';
require_once ABSPATH . 'wp-admin/includes/taxonomy.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

function themify_import_post( $post ) {
	global $processed_posts, $processed_terms;

	if ( ! post_type_exists( $post['post_type'] ) ) {
		return;
	}

	/* Menu items don't have reliable post_title, skip the post_exists check */
	if( $post['post_type'] !== 'nav_menu_item' ) {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
		if ( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
			$processed_posts[ intval( $post['ID'] ) ] = intval( $post_exists );
			return;
		}
	}

	if( $post['post_type'] == 'nav_menu_item' ) {
		if( ! isset( $post['tax_input']['nav_menu'] ) || ! term_exists( $post['tax_input']['nav_menu'], 'nav_menu' ) ) {
			return;
		}
		$_menu_item_type = $post['meta_input']['_menu_item_type'];
		$_menu_item_object_id = $post['meta_input']['_menu_item_object_id'];

		if ( 'taxonomy' == $_menu_item_type && isset( $processed_terms[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_terms[ intval( $_menu_item_object_id ) ];
		} else if ( 'post_type' == $_menu_item_type && isset( $processed_posts[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_posts[ intval( $_menu_item_object_id ) ];
		} else if ( 'custom' != $_menu_item_type ) {
			// associated object is missing or not imported yet, we'll retry later
			// $missing_menu_items[] = $item;
			return;
		}
	}

	$post_parent = ( $post['post_type'] == 'nav_menu_item' ) ? $post['meta_input']['_menu_item_menu_item_parent'] : (int) $post['post_parent'];
	$post['post_parent'] = 0;
	if ( $post_parent ) {
		// if we already know the parent, map it to the new local ID
		if ( isset( $processed_posts[ $post_parent ] ) ) {
			if( $post['post_type'] == 'nav_menu_item' ) {
				$post['meta_input']['_menu_item_menu_item_parent'] = $processed_posts[ $post_parent ];
			} else {
				$post['post_parent'] = $processed_posts[ $post_parent ];
			}
		}
	}

	/**
	 * for hierarchical taxonomies, IDs must be used so wp_set_post_terms can function properly
	 * convert term slugs to IDs for hierarchical taxonomies
	 */
	if( ! empty( $post['tax_input'] ) ) {
		foreach( $post['tax_input'] as $tax => $terms ) {
			if( is_taxonomy_hierarchical( $tax ) ) {
				$terms = explode( ', ', $terms );
				$post['tax_input'][ $tax ] = array_map( 'themify_get_term_id_by_slug', $terms, array_fill( 0, count( $terms ), $tax ) );
			}
		}
	}

	$post['post_author'] = (int) get_current_user_id();
	$post['post_status'] = 'publish';

	$old_id = $post['ID'];

	unset( $post['ID'] );
	$post_id = wp_insert_post( $post, true );
	if( is_wp_error( $post_id ) ) {
		return false;
	} else {
		$processed_posts[ $old_id ] = $post_id;

		if( isset( $post['has_thumbnail'] ) && $post['has_thumbnail'] ) {
			$placeholder = themify_get_placeholder_image();
			if( ! is_wp_error( $placeholder ) ) {
				set_post_thumbnail( $post_id, $placeholder );
			}
		}

		return $post_id;
	}
}

function themify_get_placeholder_image() {
	static $placeholder_image = null;

	if( $placeholder_image == null ) {
		if ( ! function_exists( 'WP_Filesystem' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		WP_Filesystem();
		global $wp_filesystem;
		$upload = wp_upload_bits( $post['post_name'] . '.jpg', null, $wp_filesystem->get_contents( THEMIFY_DIR . '/img/image-placeholder.jpg' ) );

		if ( $info = wp_check_filetype( $upload['file'] ) )
			$post['post_mime_type'] = $info['type'];
		else
			return new WP_Error( 'attachment_processing_error', __( 'Invalid file type', 'themify' ) );

		$post['guid'] = $upload['url'];
		$post_id = wp_insert_attachment( $post, $upload['file'] );
		wp_update_attachment_metadata( $post_id, wp_generate_attachment_metadata( $post_id, $upload['file'] ) );

		$placeholder_image = $post_id;
	}

	return $placeholder_image;
}

function themify_import_term( $term ) {
	global $processed_terms;

	if( $term_id = term_exists( $term['slug'], $term['taxonomy'] ) ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term['term_id'] ) )
			$processed_terms[ intval( $term['term_id'] ) ] = (int) $term_id;
		return (int) $term_id;
	}

	if ( empty( $term['parent'] ) ) {
		$parent = 0;
	} else {
		$parent = term_exists( $term['parent'], $term['taxonomy'] );
		if ( is_array( $parent ) ) $parent = $parent['term_id'];
	}

	$id = wp_insert_term( $term['name'], $term['taxonomy'], array(
		'parent' => $parent,
		'slug' => $term['slug'],
		'description' => $term['description'],
	) );
	if ( ! is_wp_error( $id ) ) {
		if ( isset( $term['term_id'] ) ) {
			$processed_terms[ intval($term['term_id']) ] = $id['term_id'];
			return $term['term_id'];
		}
	}

	return false;
}

function themify_get_term_id_by_slug( $slug, $tax ) {
	$term = get_term_by( 'slug', $slug, $tax );
	if( $term ) {
		return $term->term_id;
	}

	return false;
}

function themify_undo_import_term( $term ) {
	$term_id = term_exists( $term['slug'], $term['term_taxonomy'] );
	if ( $term_id ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term_id ) ) {
			wp_delete_term( $term_id, $term['term_taxonomy'] );
		}
	}
}

/**
 * Determine if a post exists based on title, content, and date
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param array $args array of database parameters to check
 * @return int Post ID if post exists, 0 otherwise.
 */
function themify_post_exists( $args = array() ) {
	global $wpdb;

	$query = "SELECT ID FROM $wpdb->posts WHERE 1=1";
	$db_args = array();

	foreach ( $args as $key => $value ) {
		$value = wp_unslash( sanitize_post_field( $key, $value, 0, 'db' ) );
		if( ! empty( $value ) ) {
			$query .= ' AND ' . $key . ' = %s';
			$db_args[] = $value;
		}
	}

	if ( !empty ( $args ) )
		return (int) $wpdb->get_var( $wpdb->prepare($query, $args) );

	return 0;
}

function themify_undo_import_post( $post ) {
	if( $post['post_type'] == 'nav_menu_item' ) {
		$post_exists = themify_post_exists( array(
			'post_name' => $post['post_name'],
			'post_modified' => $post['post_date'],
			'post_type' => 'nav_menu_item',
		) );
	} else {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
	}
	if( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
		/**
		 * check if the post has been modified, if so leave it be
		 *
		 * NOTE: posts are imported using wp_insert_post() which modifies post_modified field
		 * to be the same as post_date, hence to check if the post has been modified,
		 * the post_modified field is compared against post_date in the original post.
		 */
		if( $post['post_date'] == get_post_field( 'post_modified', $post_exists ) ) {
			wp_delete_post( $post_exists, true ); // true: bypass trash
		}
	}
}

function themify_do_demo_import() {
$term = array (
  'term_id' => 19,
  'name' => 'View',
  'slug' => 'view',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 20,
  'name' => 'Travel',
  'slug' => 'travel',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 21,
  'name' => 'Photography',
  'slug' => 'photography',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 22,
  'name' => 'Work',
  'slug' => 'work',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 23,
  'name' => 'Video',
  'slug' => 'video',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 18,
  'name' => 'View',
  'slug' => 'view',
  'term_group' => 0,
  'taxonomy' => 'post_tag',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 14,
  'name' => 'Shoes',
  'slug' => 'shoes',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 15,
  'name' => 'Top',
  'slug' => 'top',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 16,
  'name' => 'Bag',
  'slug' => 'bag',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 24,
  'name' => 'Cameras',
  'slug' => 'cameras',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 25,
  'name' => 'Products',
  'slug' => 'products',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 26,
  'name' => 'Photos',
  'slug' => 'photos',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 27,
  'name' => 'Fashion',
  'slug' => 'fashion',
  'term_group' => 0,
  'taxonomy' => 'portfolio-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 2,
  'name' => 'Main Menu',
  'slug' => 'main-menu',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 8,
  'name' => 'Wedding',
  'slug' => 'wedding',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 9,
  'name' => 'Portfolio',
  'slug' => 'portfolio',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 10,
  'name' => 'Restaurant',
  'slug' => 'restaurant',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 11,
  'name' => 'Product',
  'slug' => 'product',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 12,
  'name' => 'Agency',
  'slug' => 'agency',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 13,
  'name' => 'Shop',
  'slug' => 'shop',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$post = array (
  'ID' => 3198,
  'post_date' => '2015-04-09 17:33:31',
  'post_date_gmt' => '2015-04-09 17:33:31',
  'post_content' => 'Donec vitae volutpat erat. Donec non molestie lacus. Integer euismod leo euismod, fermentum tellus sed, consequat leo. Cras lobortis nisl non dapibus tempor. Donec a finibus tellus. Vivamus laoreet lacinia imperdiet. Fusce tincidunt metus ac sapien feugiat, sit amet laoreet lorem aliquam. Integer pharetra egestas mi vel aliquam.',
  'post_title' => 'Amazing Pose',
  'post_excerpt' => '',
  'post_name' => 'amazing-pose',
  'post_modified' => '2017-08-21 03:30:27',
  'post_modified_gmt' => '2017-08-21 03:30:27',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3198',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'view',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3202,
  'post_date' => '2015-04-09 17:46:12',
  'post_date_gmt' => '2015-04-09 17:46:12',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vitae quam sed leo luctus sagittis. Fusce ut metus scelerisque dui facilisis auctor at sit amet eros. Donec sit amet nibh ac ipsum commodo tempor a a nibh. Suspendisse ornare neque id massa sollicitudin maximus. Vivamus vel dui a velit varius pellentesque nec sed felis. Vestibulum eu mauris viverra ante dictum imperdiet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;',
  'post_title' => 'Long Drive Adventures',
  'post_excerpt' => '',
  'post_name' => 'long-drive-adventures',
  'post_modified' => '2017-08-21 03:30:26',
  'post_modified_gmt' => '2017-08-21 03:30:26',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3202',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'travel',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3205,
  'post_date' => '2015-04-09 17:47:44',
  'post_date_gmt' => '2015-04-09 17:47:44',
  'post_content' => 'Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.

&nbsp;',
  'post_title' => 'My Photography Post',
  'post_excerpt' => '',
  'post_name' => 'my-photography-post',
  'post_modified' => '2017-08-21 03:30:24',
  'post_modified_gmt' => '2017-08-21 03:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3205',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3208,
  'post_date' => '2015-04-09 17:48:26',
  'post_date_gmt' => '2015-04-09 17:48:26',
  'post_content' => 'Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.

&nbsp;',
  'post_title' => 'City Sunrise',
  'post_excerpt' => '',
  'post_name' => 'city-sunrise',
  'post_modified' => '2017-08-21 03:30:24',
  'post_modified_gmt' => '2017-08-21 03:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3208',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'view',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3212,
  'post_date' => '2015-04-09 17:50:20',
  'post_date_gmt' => '2015-04-09 17:50:20',
  'post_content' => 'Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.

&nbsp;',
  'post_title' => 'In the Wild',
  'post_excerpt' => '',
  'post_name' => 'in-the-wild',
  'post_modified' => '2017-08-21 03:30:21',
  'post_modified_gmt' => '2017-08-21 03:30:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3212',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3216,
  'post_date' => '2015-04-09 19:48:45',
  'post_date_gmt' => '2015-04-09 19:48:45',
  'post_content' => 'Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.',
  'post_title' => 'Coffee Shop Office',
  'post_excerpt' => '',
  'post_name' => 'coffee-shop-office',
  'post_modified' => '2017-08-21 03:30:19',
  'post_modified_gmt' => '2017-08-21 03:30:19',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3216',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'work',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3228,
  'post_date' => '2015-04-09 20:00:21',
  'post_date_gmt' => '2015-04-09 20:00:21',
  'post_content' => 'Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'Work/Life Balance',
  'post_excerpt' => '',
  'post_name' => 'worklife-balance',
  'post_modified' => '2017-08-21 03:30:17',
  'post_modified_gmt' => '2017-08-21 03:30:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3228',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'work',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3229,
  'post_date' => '2015-04-09 19:56:54',
  'post_date_gmt' => '2015-04-09 19:56:54',
  'post_content' => 'Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'Travel Shots',
  'post_excerpt' => '',
  'post_name' => 'travel-shots',
  'post_modified' => '2017-08-21 03:30:18',
  'post_modified_gmt' => '2017-08-21 03:30:18',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3229',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, travel, view',
    'post_tag' => 'view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3238,
  'post_date' => '2015-04-09 20:03:51',
  'post_date_gmt' => '2015-04-09 20:03:51',
  'post_content' => '<div id="lipsum">

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Night Life',
  'post_excerpt' => '',
  'post_name' => 'night-life',
  'post_modified' => '2017-08-21 03:30:17',
  'post_modified_gmt' => '2017-08-21 03:30:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3238',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3241,
  'post_date' => '2015-04-09 20:05:08',
  'post_date_gmt' => '2015-04-09 20:05:08',
  'post_content' => '<div id="lipsum">

In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'New York Shot',
  'post_excerpt' => '',
  'post_name' => 'new-york-shot',
  'post_modified' => '2017-08-21 03:30:13',
  'post_modified_gmt' => '2017-08-21 03:30:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3241',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3244,
  'post_date' => '2015-04-09 20:08:17',
  'post_date_gmt' => '2015-04-09 20:08:17',
  'post_content' => '<div id="lipsum">

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat.

</div>
&nbsp;',
  'post_title' => 'Beach Relax',
  'post_excerpt' => '',
  'post_name' => 'beach-relax',
  'post_modified' => '2017-08-21 03:30:06',
  'post_modified_gmt' => '2017-08-21 03:30:06',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3244',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3247,
  'post_date' => '2015-04-09 20:10:27',
  'post_date_gmt' => '2015-04-09 20:10:27',
  'post_content' => '<div id="lipsum">

Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque.

</div>
&nbsp;',
  'post_title' => 'Busy Streets',
  'post_excerpt' => '',
  'post_name' => 'busy-streets',
  'post_modified' => '2017-08-21 03:25:20',
  'post_modified_gmt' => '2017-08-21 03:25:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3247',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3250,
  'post_date' => '2015-04-09 20:11:44',
  'post_date_gmt' => '2015-04-09 20:11:44',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

&nbsp;',
  'post_title' => 'Work Office Space',
  'post_excerpt' => '',
  'post_name' => 'work-office-space',
  'post_modified' => '2017-08-21 03:25:18',
  'post_modified_gmt' => '2017-08-21 03:25:18',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3250',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'work',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3253,
  'post_date' => '2015-04-09 20:12:41',
  'post_date_gmt' => '2015-04-09 20:12:41',
  'post_content' => 'Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'New Shots',
  'post_excerpt' => '',
  'post_name' => 'new-shots',
  'post_modified' => '2017-08-21 03:25:15',
  'post_modified_gmt' => '2017-08-21 03:25:15',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3253',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, work',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3256,
  'post_date' => '2015-04-09 20:20:05',
  'post_date_gmt' => '2015-04-09 20:20:05',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Travelling Photos',
  'post_excerpt' => '',
  'post_name' => 'travelling-photos',
  'post_modified' => '2017-08-21 03:25:11',
  'post_modified_gmt' => '2017-08-21 03:25:11',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3256',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3259,
  'post_date' => '2015-04-09 20:22:34',
  'post_date_gmt' => '2015-04-09 20:22:34',
  'post_content' => 'Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Looking Forward',
  'post_excerpt' => '',
  'post_name' => 'looking-forward',
  'post_modified' => '2017-08-21 03:25:10',
  'post_modified_gmt' => '2017-08-21 03:25:10',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3259',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, travel',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3266,
  'post_date' => '2015-08-15 20:26:22',
  'post_date_gmt' => '2015-08-15 20:26:22',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Sidebar Post',
  'post_excerpt' => '',
  'post_name' => 'sidebar-post',
  'post_modified' => '2017-08-21 03:25:07',
  'post_modified_gmt' => '2017-08-21 03:25:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3266',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'sidebar1',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'work',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3269,
  'post_date' => '2015-04-09 20:13:21',
  'post_date_gmt' => '2015-04-09 20:13:21',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. <a href="https://themify.me">Praesent</a> finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'New Arrival',
  'post_excerpt' => '',
  'post_name' => 'new-arrival',
  'post_modified' => '2017-08-21 03:25:14',
  'post_modified_gmt' => '2017-08-21 03:25:14',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3269',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3379,
  'post_date' => '2015-08-14 20:42:26',
  'post_date_gmt' => '2015-08-14 20:42:26',
  'post_content' => 'Donec egestas lectus et magna vulputate vestibulum. Phasellus pellentesque molestie purus at rhoncus. Morbi et turpis dapibus, interdum nisi in, lacinia urna. Nunc tincidunt arcu egestas, condimentum lorem ut, pretium nulla. Suspendisse scelerisque fermentum erat, semper condimentum sem ullamcorper non. Curabitur lorem nisi, tincidunt ut tristique et, cursus a turpis.',
  'post_title' => 'Vimeo Video',
  'post_excerpt' => '',
  'post_name' => 'vimeo-video',
  'post_modified' => '2017-08-21 03:25:08',
  'post_modified_gmt' => '2017-08-21 03:25:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3379',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'video_url' => 'https://vimeo.com/channels/staffpicks/137531269',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'video, view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3262,
  'post_date' => '2015-08-16 20:24:41',
  'post_date_gmt' => '2015-08-16 20:24:41',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.<!--more-->

Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'No Sidebar Post',
  'post_excerpt' => '',
  'post_name' => 'no-sidebar-post',
  'post_modified' => '2017-08-21 03:25:04',
  'post_modified_gmt' => '2017-08-21 03:25:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?p=3262',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'photography, view',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4328,
  'post_date' => '2015-08-17 22:39:24',
  'post_date_gmt' => '2015-08-17 22:39:24',
  'post_content' => '',
  'post_title' => 'Home',
  'post_excerpt' => '',
  'post_name' => 'home',
  'post_modified' => '2017-09-15 16:26:54',
  'post_modified_gmt' => '2017-09-15 16:26:54',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4328',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'split_scroll' => 'yes',
    'exclude_site_tagline' => 'yes',
    'header_wrap' => 'transparent',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'date',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Split Theme</h1><h3>Awesome split scrolling effect. Scroll down or view demos below:</h3>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"40\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-restaurant.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Restaurant\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/restaurant\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInX\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-agency.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Agency\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/agency\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInY\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-product.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Product\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/product\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInX\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-portfolio.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Portfolio\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/portfolio\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInY\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"column_alignment\\":\\"\\",\\"styling\\":[]},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-personal.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Personal\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/personal\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInY\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-shop.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Shop\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/shop-page\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInX\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-wedding.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Wedding\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/wedding\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInY\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/screenshot-super-heroes.jpg\\",\\"appearance_image\\":\\"bordered\\",\\"width_image\\":\\"200\\",\\"title_image\\":\\"Super Heroes\\",\\"link_image\\":\\"https://themify.me/demo/themes/split/home/super-heroes\\",\\"param_image\\":\\"|\\",\\"animation_effect\\":\\"flipInX\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"column_alignment\\":\\"\\",\\"styling\\":[]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Home\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2017/03/home-top-row-bg-1.png\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"9ba6a8_1.00\\",\\"cover_color-type\\":\\"cover_gradient\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgba(52, 219, 211, 0.682353)|100% rgba(161, 61, 227, 0.760784)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgba(52, 219, 211, 0.682353) 0%, rgba(161, 61, 227, 0.760784) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgba(52, 219, 211, 0.682353) 0%, rgba(161, 61, 227, 0.760784) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgba(52, 219, 211, 0.682353) 0%, rgba(161, 61, 227, 0.760784) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgba(52, 219, 211, 0.682353) 0%, rgba(161, 61, 227, 0.760784) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgba(52, 219, 211, 0.682353) 0%, rgba(161, 61, 227, 0.760784) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"px\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"px\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"solid\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Column Styling</h3><p>Full control of each column styling (font, color, backgroung image, video, slider, and overlay).</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/bg-wolverine.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"ffee00_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"ffff00_0.43\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Image Background</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/09/bg-thor.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"e83c3c_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"ff2b2b_0.55\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Video Background</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"video\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"https://themify.me/demo/demo-videos/Traffic-blurred2_converted.mp4\\",\\"background_image\\":\\"https://themify.me/demo/demo-videos/Traffic-blurred2.jpg\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"849294_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"5cd1ff_0.53\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"custom_css_column\\":\\"\\"}},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1 last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Slider Background</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"8\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"8\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"slider\\",\\"background_slider\\":\\"[gallery _orderByField=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"4683,4680,4681\\\\\\\\\\\\\\"]\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"34dbd0_0.48\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Column-Styling\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Shop</h2><h3>WooCommerce support</h3>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/08/79073116.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"ffc400_0.39\\",\\"cover_color_hover\\":\\"30ffbd_0.52\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[recent_products per_page=\\\\\\\\\\\\\\"6\\\\\\\\\\\\\\" columns=\\\\\\\\\\\\\\"3\\\\\\\\\\\\\\"]</p>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"ffffff_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Shop\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Portfolio</h2><h3>Optional custom Portfolio post type with post filter</h3>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/08/196169018.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"8d54b8_1.00\\",\\"cover_color\\":\\"7729ff_0.37\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_portfolio_posts limit=\\\\\\\\\\\\\\"6\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"grid3\\\\\\\\\\\\\\" image_w=\\\\\\\\\\\\\\"240\\\\\\\\\\\\\\" image_h=\\\\\\\\\\\\\\"140\\\\\\\\\\\\\\"]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"animation_effect\\":\\"fadeInRight\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"f1ccff_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"000000\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Portfolio\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Blog</h2><h3>Display blog posts in various layouts</h3>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/08/red-umbrella.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"70383e_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"post\\",\\"mod_settings\\":{\\"layout_post\\":\\"list-thumb-image\\",\\"post_type_post\\":\\"post\\",\\"type_query_post\\":\\"category\\",\\"category_post\\":\\"0|multiple\\",\\"post_tag_post\\":\\"|single\\",\\"product_cat_post\\":\\"|single\\",\\"product_tag_post\\":\\"|single\\",\\"product_shipping_class_post\\":\\"|single\\",\\"portfolio-category_post\\":\\"|single\\",\\"post_per_page_post\\":\\"4\\",\\"order_post\\":\\"desc\\",\\"orderby_post\\":\\"date\\",\\"display_post\\":\\"none\\",\\"img_width_post\\":\\"115\\",\\"img_height_post\\":\\"75\\",\\"hide_post_meta_post\\":\\"yes\\",\\"hide_page_nav_post\\":\\"yes\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"animation_effect\\":\\"fadeInRight\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"d7e8b1_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"000000_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Blog\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/split/files/2015/08/outdoor-yoga.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"85b3b1_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Highlighted Features</h3>\\",\\"column_divider_style\\":\\"solid\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"icon_size\\":\\"normal\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_vertical\\",\\"content_icon\\":[{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Split Scrolling\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Column Styling\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Transparent Header\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Portfolio\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"WooCommerce\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Infinite scroll & masonry layouts\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"},{\\"icon\\":\\"fa-check\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Animated page loader effect\\",\\"link_options\\":\\"regular\\",\\"lightbox_size_unit_width\\":\\"pixels\\",\\"lightbox_size_unit_height\\":\\"pixels\\"}],\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"fadeInRight\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"buttons_size\\":\\"large\\",\\"buttons_style\\":\\"circle\\",\\"content_button\\":[{\\"label\\":\\"BUY SPLIT\\",\\"link\\":\\"https://themify.me/themes/split/\\",\\"link_options\\":\\"newtab\\",\\"button_color_bg\\":\\"default\\",\\"button_single_style\\":\\"outline\\"}],\\"font_family\\":\\"Arial, Helvetica, sans-serif\\",\\"font_size_unit\\":\\"px\\",\\"line_height_unit\\":\\"px\\",\\"padding_top\\":\\"15\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_style\\":\\"solid\\",\\"border_right_style\\":\\"solid\\",\\"border_bottom_style\\":\\"solid\\",\\"border_left_style\\":\\"solid\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"button_hover_background_color\\":\\"000000_1.00\\",\\"link_color_hover\\":\\"ffffff_1.00\\",\\"link_padding_top\\":\\"15\\",\\"link_padding_right\\":\\"15\\",\\"link_padding_bottom\\":\\"15\\",\\"link_padding_left\\":\\"15\\",\\"link_checkbox_padding_apply_all\\":\\"padding\\",\\"link_checkbox_margin_apply_all\\":\\"margin\\",\\"link_border_top_color\\":\\"000000_1.00\\",\\"link_border_top_width\\":\\"1\\",\\"link_border_top_style\\":\\"solid\\",\\"link_border_right_color\\":\\"000000_1.00\\",\\"link_border_right_width\\":\\"1\\",\\"link_border_right_style\\":\\"solid\\",\\"link_border_bottom_color\\":\\"000000_1.00\\",\\"link_border_bottom_width\\":\\"1\\",\\"link_border_bottom_style\\":\\"solid\\",\\"link_border_left_color\\":\\"000000_1.00\\",\\"link_border_left_width\\":\\"1\\",\\"link_border_left_style\\":\\"solid\\",\\"link_checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"fadeInRight\\",\\"animation_effect_delay\\":\\".5\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"a4ebe6_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"000000_1.00\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"gutter\\":\\"gutter-none\\",\\"column_alignment\\":\\"\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_height\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"Highlighted-Features\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_size\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_gradient-gradient-type\\":\\"linear\\",\\"background_gradient-gradient-angle\\":\\"180\\",\\"background_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"background_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"background_repeat\\":\\"\\",\\"background_position\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"\\",\\"cover_gradient-gradient-type\\":\\"linear\\",\\"cover_gradient-gradient-angle\\":\\"180\\",\\"cover_gradient-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"cover_color_hover\\":\\"\\",\\"cover_gradient_hover-gradient-type\\":\\"linear\\",\\"cover_gradient_hover-gradient-angle\\":\\"180\\",\\"cover_gradient_hover-gradient\\":\\"0% rgb(0, 0, 0)|100% rgb(255, 255, 255)\\",\\"cover_gradient_hover-css\\":\\"background-image: -moz-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -webkit-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -o-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: -ms-linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\nbackground-image: linear-gradient(180deg,rgb(0, 0, 0) 0%, rgb(255, 255, 255) 100%);\\\\r\\\\n\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"line_height\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_mobile\\":\\"show\\",\\"animation_effect\\":\\"\\",\\"animation_effect_delay\\":\\"\\",\\"animation_effect_repeat\\":\\"\\",\\"custom_parallax_scroll_speed\\":\\"\\",\\"custom_parallax_scroll_zindex\\":\\"\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4330,
  'post_date' => '2015-08-17 23:18:34',
  'post_date_gmt' => '2015-08-17 23:18:34',
  'post_content' => '<!--themify_builder_static--><h1>Lucy &#038; John</h1>
 <h3>Feb 14, 2018</h3><h4>Toronto, Ontario</h4>
 <h2>Her</h2><p>Curabitur sit amet diam sed risus elementum hendrerit blandit nec quam. Cras sit amet odio quis dolor interdum pulvinar. In pellentesque eu nisi non porta. Duis volutpat molestie sem, quis sollicitudin metus euismod in. Phasellus in enim a mauris malesuada bibendum sit amet ut turpis. Aenean ut eros suscipit nibh placerat varius ac in turpis.</p><p> </p>
 <h2>Him</h2><p>Aenean lacinia elit ante, a facilisis odio facilisis at. Vestibulum at scelerisque massa. Curabitur feugiat dolor id dictum sagittis. Nunc lobortis enim id hendrerit rutrum. Nulla eleifend sodales odio, at ultrices arcu finibus vel. Morbi vel magna scelerisque neque consectetur maximus. Curabitur sit amet iaculis mi. Pellentesque vulputate mi metus, at pretium magna convallis eu.</p>
[gallery columns="4" _orderbyfield="menu_order ID" link="file" _orderByField="menu_order ID" ids="4409,4335,4336,4399,4400,4401,4403,4404,4402,4398,4405,4408"]
 <h3>Our Story</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu efficitur risus, non molestie urna. Phasellus in enim a mauris malesuada bibendum sit amet ut turpis. Aenean ut eros suscipit nibh placerat varius ac in turpis.</p>
 <h2>Venue</h2><h4>228 Yonge Street<br />Toronto, Ontario</h4><h4>416-455-5656</h4>
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=1+Yonge+Street%2C+Toronto%2C+Ontario&amp;t=m&amp;z=8&amp;output=embed&amp;iwloc=near"></iframe>
 <h2>RSVP</h2>
<form action="/demo/themes/split/wp-admin/admin-ajax.php#wpcf7-f4412-o1" method="post" novalidate="novalidate">
<input type="hidden" name="_wpcf7" value="4412" /> <input type="hidden" name="_wpcf7_version" value="4.9" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4412-o1" /> <input type="hidden" name="_wpcf7_container_post" value="0" />
<p>Your Name (required)<br /> <input type="text" name="your-name" value="" size="40" aria-required="true" aria-invalid="false" /> </p> <p>Your Email (required)<br /> <input type="email" name="your-email" value="" size="40" aria-required="true" aria-invalid="false" /> </p>
<noscript> <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LdIsP4SAAAAAAYl6GQb1xz9cGIuDcS6Uqv-y2gJ" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;"> </iframe> <textarea id="g-recaptcha-response" name="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"> </textarea> </noscript>
<p><input type="submit" value="RSVP" /></p> </form>
 <h1>Thank you!</h1> <p>Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p><!--/themify_builder_static-->',
  'post_title' => 'Wedding',
  'post_excerpt' => '',
  'post_name' => 'wedding',
  'post_modified' => '2017-10-27 23:31:40',
  'post_modified_gmt' => '2017-10-27 23:31:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4330',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'wedding',
    'split_scroll' => 'yes',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Lucy &amp; John<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c17\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Feb 14, 2018<\\\\/h3><h4>Toronto, Ontario<\\\\/h4>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"cid\\":\\"c21\\"}}]}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/dv1935017.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"ff47cb_0.37\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Her<\\\\/h2><p>Curabitur sit amet diam sed risus elementum hendrerit blandit nec quam. Cras sit amet odio quis dolor interdum pulvinar. In pellentesque eu nisi non porta. Duis volutpat molestie sem, quis sollicitudin metus euismod in. Phasellus in enim a mauris malesuada bibendum sit amet ut turpis. Aenean ut eros suscipit nibh placerat varius ac in turpis.<\\\\/p><p> <\\\\/p>\\",\\"animation_effect\\":\\"wobble\\",\\"cid\\":\\"c32\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/134301761.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-center\\",\\"background_color\\":\\"d68189_1.00\\",\\"cover_color\\":\\"b33540_0.77\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Him<\\\\/h2><p>Aenean lacinia elit ante, a facilisis odio facilisis at. Vestibulum at scelerisque massa. Curabitur feugiat dolor id dictum sagittis. Nunc lobortis enim id hendrerit rutrum. Nulla eleifend sodales odio, at ultrices arcu finibus vel. Morbi vel magna scelerisque neque consectetur maximus. Curabitur sit amet iaculis mi. Pellentesque vulputate mi metus, at pretium magna convallis eu.<\\\\/p>\\",\\"animation_effect\\":\\"wobble\\",\\"cid\\":\\"c40\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/205973320.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-center\\",\\"background_color\\":\\"c6e5d9_1.00\\",\\"cover_color\\":\\"42ab83_0.70\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"about\\",\\"background_type\\":\\"image\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"gallery\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"layout_gallery\\":\\"grid\\",\\"shortcode_gallery\\":\\"[gallery columns=\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\" _orderbyfield=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"file\\\\\\\\\\\\\\" _orderByField=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"4409,4335,4336,4399,4400,4401,4403,4404,4402,4398,4405,4408\\\\\\\\\\\\\\"]\\",\\"thumb_w_gallery\\":\\"800\\",\\"gallery_columns\\":\\"0\\",\\"link_opt\\":\\"file\\",\\"link_image_size\\":\\"full\\",\\"appearance_gallery\\":\\"rounded\\",\\"animation_effect\\":\\"bounceInUp\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"0\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Our Story<\\\\/h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu efficitur risus, non molestie urna. Phasellus in enim a mauris malesuada bibendum sit amet ut turpis. Aenean ut eros suscipit nibh placerat varius ac in turpis.<\\\\/p>\\",\\"animation_effect\\":\\"bounceInDown\\",\\"cid\\":\\"c59\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"ourstory\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/114041497.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"f4ead5_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"d6b46b_0.80\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Venue<\\\\/h2><h4>228 Yonge Street<br \\\\/>Toronto, Ontario<\\\\/h4><h4>416-455-5656<\\\\/h4>\\",\\"animation_effect\\":\\"rollIn\\",\\"cid\\":\\"c70\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/outdoor-patio.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"3ca7ba_0.81\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"1 Yonge Street, Toronto, Ontario\\",\\"zoom_map\\":\\"8\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"300\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"animation_effect\\":\\"rollIn\\",\\"css_map\\":\\"fullheight-map\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"0\\",\\"padding_bottom\\":\\"0\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"venue\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"92d1c8_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"left\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"styling\\":{\\"background_type\\":\\"video\\",\\"background_video\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/surface_wind_edited_CLIPCHAMP_720p.mp4\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/surface_wind-1024x575.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"text_align\\":\\"left\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h2>RSVP<\\\\/h2>\\\\n<p>[contact-form-7 id=\\\\\\\\\\\\\\"4412\\\\\\\\\\\\\\" title=\\\\\\\\\\\\\\"Contact form 1\\\\\\\\\\\\\\"]<\\\\/p>\\",\\"animation_effect\\":\\"tada\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/dinner.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-center\\",\\"cover_color\\":\\"000000_0.65\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_border_apply_all\\":\\"border\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"rsvp\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"c6a49a_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"left\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\"},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>Thank you!<\\\\/h1>\\\\n<p>Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"cid\\":\\"c116\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"a57fc9_0.68\\",\\"font_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"3\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"1\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\"}],\\"gutter\\":\\"gutter-none\\"}]}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"thank-you\\",\\"background_type\\":\\"slider\\",\\"background_slider\\":\\"[gallery ids=\\\\\\\\\\\\\\"4409,4404,4403\\\\\\\\\\\\\\"]\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4337,
  'post_date' => '2015-08-20 21:33:21',
  'post_date_gmt' => '2015-08-20 21:33:21',
  'post_content' => '<!--themify_builder_static--><h2><a href="#about">About</a></h2>
 <h2><a href="#portfolio">Portfolio</a></h2>
 <h2><a href="#services">Services</a></h2>
 <h2><a href="#contact">Contact</a></h2>
 <h2>John Smith</h2><p>Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere. Curabitur at arcu id turpis posuere bibendum. Sed commodo mauris eget diam pretium cursus. In sagittis feugiat mauris, in ultrices mauris lacinia eu. Fusce augue velit, vulputate elementum semper congue, rhoncus adipiscing nisl. Curabitur vel risus eros, sed eleifend arcu.</p>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Portfolio</h2>
 <ul> <li><a href="https://themify.me/demo/themes/split/portfolio-category/cameras/" >Cameras</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/fashion/" >Fashion</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/photos/" >Photos</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/products/" >Products</a> </li> </ul>
<article id="portfolio-3298"> <a href="https://themify.me/demo/themes/split/project/builder-project/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/builder-project/"><img src="https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-260x150.jpg" width="260" height="150" alt="cookies-and-flower" srcset="https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-680x390.jpg 680w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/builder-project/">Builder Project</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3298&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3304"> <a href="https://themify.me/demo/themes/split/project/model-liza/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/model-liza/"><img src="https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-260x150.jpg" width="260" height="150" alt="114041515" srcset="https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-2x1.jpg 2w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-5x3.jpg 5w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-8x5.jpg 8w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/model-liza/">Model Liza</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3304&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3295"> <a href="https://themify.me/demo/themes/split/project/model-shots/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/model-shots/"><img src="https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-260x150.jpg" width="260" height="150" alt="79073116" srcset="https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-2x2.jpg 2w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/model-shots/">Model Shots</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3295&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3303"> <a href="https://themify.me/demo/themes/split/project/urban-project/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/urban-project/"><img src="https://themify.me/demo/themes/split/files/2015/08/city-1024x683-260x150.jpg" width="260" height="150" alt="city" srcset="https://themify.me/demo/themes/split/files/2015/08/city-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/city-1024x683-680x390.jpg 680w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/urban-project/">Urban Project</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3303&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3302"> <a href="https://themify.me/demo/themes/split/project/new-york-lights/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/new-york-lights/"><img src="https://themify.me/demo/themes/split/files/2015/08/traffic_out_of_focus_broken-1024x575-260x150.jpg" width="260" height="150" alt="traffic_out_of_focus_broken" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/new-york-lights/">New York Lights</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3302&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3293"> <a href="https://themify.me/demo/themes/split/project/rosy/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/rosy/"><img src="https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-260x150.jpg" width="260" height="150" alt="196169018" srcset="https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-2x2.jpg 2w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/rosy/">Rosy</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3293&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3300"> <a href="https://themify.me/demo/themes/split/project/overwater-shot/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/overwater-shot/"><img src="https://themify.me/demo/themes/split/files/2015/08/outdoor-yoga-260x150.jpg" width="260" height="150" alt="outdoor-yoga" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/overwater-shot/">Overwater Shot</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3300&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3277"> <a href="https://themify.me/demo/themes/split/project/smith-sara/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/smith-sara/"><img src="https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-260x150.jpg" width="260" height="150" alt="187290671" srcset="https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-680x390.jpg 680w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-2x1.jpg 2w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-5x3.jpg 5w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-8x5.jpg 8w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/smith-sara/">Smith &#038; Sara</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3277&#038;action=edit">Edit</a>] 
 </article>
 
 <a href="https://themify.me/demo/themes/split/portfolio/" > MORE PORTFOLIO </a> 
 <h2>Services</h2>
 
 
 
 
 <h3> Web </h3> <p>UX and frontend development</p> 
 
 
 
 
 
 <h3> Print </h3> <p>Graphic &#038; print design</p> 
 
 
 
 
 
 <h3> SEO </h3> <p>Online marketing &#038; SEO</p> 
 
 
 
 
 
 <h3> App </h3> <p>Mobile &#038; desktop apps</p> 
 
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=55+Adelaide+Street+East&amp;t=m&amp;z=4&amp;output=embed&amp;iwloc=near"></iframe>
 <h2>Contact</h2>
<form action="/demo/themes/split/wp-admin/admin-ajax.php#wpcf7-f4429-o1" method="post" novalidate="novalidate">
<input type="hidden" name="_wpcf7" value="4429" /> <input type="hidden" name="_wpcf7_version" value="4.9" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4429-o1" /> <input type="hidden" name="_wpcf7_container_post" value="0" />
<p>Your Email (required)<br /> <input type="email" name="your-email" value="" size="40" aria-required="true" aria-invalid="false" /><br /> Subject<br /> <input type="text" name="your-subject" value="" size="40" aria-invalid="false" /><br /> Your Message<br /> <textarea name="your-message" cols="40" rows="10" aria-invalid="false"></textarea> </p>
<noscript> <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LdIsP4SAAAAAAYl6GQb1xz9cGIuDcS6Uqv-y2gJ" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;"> </iframe> <textarea id="g-recaptcha-response" name="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"> </textarea> </noscript>
<p> <input type="submit" value="Send" /></p> </form><!--/themify_builder_static-->',
  'post_title' => 'Personal',
  'post_excerpt' => '',
  'post_name' => 'personal',
  'post_modified' => '2017-10-28 02:40:14',
  'post_modified_gmt' => '2017-10-28 02:40:14',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4337',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'split_scroll' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'yes',
    'exclude_rss' => 'yes',
    'header_wrap' => 'transparent',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2><a href=\\\\\\\\\\\\\\"#about\\\\\\\\\\\\\\">About<\\\\/a><\\\\/h2>\\",\\"animation_effect\\":\\"pulse\\",\\"margin_top\\":\\"30\\",\\"margin_bottom\\":\\"30\\",\\"cid\\":\\"c16\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"ff47f6_0.85\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2><a href=\\\\\\\\\\\\\\"#portfolio\\\\\\\\\\\\\\">Portfolio<\\\\/a><\\\\/h2>\\",\\"animation_effect\\":\\"rubberBand\\",\\"margin_top\\":\\"30\\",\\"margin_bottom\\":\\"30\\",\\"cid\\":\\"c24\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"24dbff_0.81\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2><a href=\\\\\\\\\\\\\\"#services\\\\\\\\\\\\\\">Services<\\\\/a><\\\\/h2>\\",\\"animation_effect\\":\\"shake\\",\\"margin_top\\":\\"30\\",\\"margin_bottom\\":\\"30\\",\\"cid\\":\\"c32\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"e0dd1b_0.79\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\"}},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2><a href=\\\\\\\\\\\\\\"#contact\\\\\\\\\\\\\\">Contact<\\\\/a><\\\\/h2>\\",\\"animation_effect\\":\\"swing\\",\\"margin_top\\":\\"30\\",\\"margin_bottom\\":\\"30\\",\\"cid\\":\\"c40\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color_hover\\":\\"ff4242_0.74\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"video\\",\\"background_video\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/ink_flow_edited_CLIPCHAMP_720p.mp4\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/ink_flow.jpg\\",\\"background_color\\":\\"ffffff_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"4800ff_0.80\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/1185374081.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"padding_top\\":\\"200\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>John Smith<\\\\/h2><p>Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere. Curabitur at arcu id turpis posuere bibendum. Sed commodo mauris eget diam pretium cursus. In sagittis feugiat mauris, in ultrices mauris lacinia eu. Fusce augue velit, vulputate elementum semper congue, rhoncus adipiscing nisl. Curabitur vel risus eros, sed eleifend arcu.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInDown\\",\\"cid\\":\\"c55\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"font_color_icon\\":\\"#ffffff\\",\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/EnF7DhHROS8OMEp2pCkx_Dufer-food-overhead-hig-res.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"39cbdb_0.86\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"about\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"33c8d6_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Portfolio<\\\\/h2>\\",\\"animation_effect\\":\\"fadeInRightBig\\",\\"cid\\":\\"c70\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_portfolio_posts limit=\\\\\\\\\\\\\\"8\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"grid4\\\\\\\\\\\\\\"]<\\\\/p>\\",\\"animation_effect\\":\\"fadeInLeft\\",\\"cid\\":\\"c74\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"MORE PORTFOLIO\\",\\"link\\":\\"https://themify.me/demo/themes/split\\\\/portfolio\\\\/\\",\\"link_options\\":\\"regular\\"}]}}]}],\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\",\\"styling\\":{\\"row_anchor\\":\\"portfolio\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/192041261.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"b667eb_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"a048db_0.82\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Services<\\\\/h2>\\",\\"animation_effect\\":\\"fadeInLeftBig\\",\\"margin_bottom\\":\\"50\\",\\"cid\\":\\"c89\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Web\\",\\"content_feature\\":\\"<p>UX and frontend development<\\\\/p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-desktop\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"animation_effect\\":\\"fadeInLeft\\",\\"cid\\":\\"c101\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"Print\\",\\"content_feature\\":\\"<p>Graphic & print design<\\\\/p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-print\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"animation_effect\\":\\"fadeInUp\\",\\"cid\\":\\"c109\\"}}]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"SEO\\",\\"content_feature\\":\\"<p>Online marketing & SEO<\\\\/p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-search\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"animation_effect\\":\\"fadeInUp\\",\\"cid\\":\\"c117\\"}}]},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"feature\\",\\"mod_settings\\":{\\"title_feature\\":\\"App\\",\\"content_feature\\":\\"<p>Mobile & desktop apps<\\\\/p>\\",\\"layout_feature\\":\\"icon-top\\",\\"circle_percentage_feature\\":\\"100\\",\\"circle_stroke_feature\\":\\"2\\",\\"circle_color_feature\\":\\"ffffff_1.00\\",\\"circle_size_feature\\":\\"medium\\",\\"icon_type_feature\\":\\"icon\\",\\"icon_feature\\":\\"fa-diamond\\",\\"icon_color_feature\\":\\"ffffff_1.00\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c125\\"}}]}],\\"col_tablet\\":\\"tablet-auto\\"}]}],\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\",\\"styling\\":{\\"row_anchor\\":\\"services\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/156861758.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"40d6c7_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"40d6c7_0.86\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"map_display_type\\":\\"dynamic\\",\\"address_map\\":\\"55 Adelaide Street East\\",\\"zoom_map\\":\\"4\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"w_map_static\\":\\"500\\",\\"h_map\\":\\"300\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"css_map\\":\\"fullheight-map\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Contact<\\\\/h2><p>[contact-form-7 id=\\\\\\\\\\\\\\"4429\\\\\\\\\\\\\\" title=\\\\\\\\\\\\\\"Default Form\\\\\\\\\\\\\\"]<\\\\/p>\\",\\"cid\\":\\"c144\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/city.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"dbb407_0.89\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"contact\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"d9ac4a_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}],\\"col_tablet\\":\\"tablet-auto\\",\\"col_mobile\\":\\"mobile-auto\\"}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4348,
  'post_date' => '2015-08-24 23:29:21',
  'post_date_gmt' => '2015-08-24 23:29:21',
  'post_content' => '<!--themify_builder_static--><h1>YUM Kitchen!</h1>
 <h4>11224 Burlington Avenue<br />Superman Rd.<br />Milton, ON</h4>
 <h3>416-333-2828</h3>
 <h2>Gallery</h2>
[gallery columns="4" link="file" _orderbyfield="menu_order ID" _orderByField="menu_order ID" ids="4386,4379,4380,4381,4383,4384,4385,4387,4388,4391,4392,4393"]
 <h2>Appetizer</h2>
 <h4>Deluxe Omelette <em>$10.95</em></h4><p>nunc fermentum rhoncus et non nisl. Aliquam ultrices turpis eu tortor pellentesque tristique. Etiam pharetra quis magna vitae tempor.</p><h4>Eggs Benedict <em>$10.95</em></h4><p>Nulla hendrerit aliquam lacus. Quisque libero leo, pretium nec semper quis, aliquet semper lorem. Fusce eu nibh congue</p><h4>Eggs Florentine <em>$9.95</em></h4><p>Sed sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
 <h2>Main</h2>
 <h4>Chicken Burger <em>$11.00</em></h4><p>Maecenas libero metus, finibus non consectetur sit amet, aliquam ac urna. Suspendisse eget metus ut leo blandit mattis.</p><h4>Ham Sandwich  <em>$14.50</em></h4><p>Arcu tortor interdum puspendisse in elementum. sapien. Integer at turpis sed dolor aliquam pulvinar id euullamcorper.</p><h4>Sheppards Pie<em> $29.95</em></h4><p>Topd sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
 <h2>Dessert</h2>
 <h4>Creme Brûlée <em>$10.50</em></h4><p>Pulvinar id Sed sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><h4>Fruit Platter <em>$12.00</em></h4><p>Tempus dolor. Sed porttitor nunc molestie tortor ullamcorper interdum. Suspendisse in elementum sapien.</p><h4>Key Lime Pie<em> $14.25</em></h4><p>ullamcorper interdum. Suspendisse in elementum sapien. Integer at turpis sed dolor aliquam pulvinar id eu arcu.</p>
 <h4>11224 Burlington Avenue<br />Superman Rd.<br />Milton, ON</h4> <h3>416-333-2828</h3> <p> </p>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=55+Adelaide+East+Toronto%2C+ON&amp;t=m&amp;z=13&amp;output=embed&amp;iwloc=near"></iframe><!--/themify_builder_static-->',
  'post_title' => 'Restaurant',
  'post_excerpt' => '',
  'post_name' => 'restaurant',
  'post_modified' => '2017-10-27 13:15:49',
  'post_modified_gmt' => '2017-10-27 13:15:49',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4348',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'restaurant',
    'split_scroll' => 'yes',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>YUM Kitchen!<\\\\/h1>\\",\\"animation_effect\\":\\"bounceInDown\\",\\"text_align\\":\\"center\\",\\"cid\\":\\"c17\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>11224 Burlington Avenue<br \\\\/>Superman Rd.<br \\\\/>Milton, ON<\\\\/h4>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"text_align\\":\\"center\\",\\"cid\\":\\"c21\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>416-333-2828<\\\\/h3>\\",\\"animation_effect\\":\\"bounceInUp\\",\\"text_align\\":\\"center\\",\\"cid\\":\\"c25\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/cookies-and-flower.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"7700ff_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"7b00ff_0.78\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/dinner.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"padding_top\\":\\"250\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Gallery<\\\\/h2>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c40\\"}},{\\"mod_name\\":\\"gallery\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"layout_gallery\\":\\"grid\\",\\"shortcode_gallery\\":\\"[gallery columns=\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"file\\\\\\\\\\\\\\" _orderbyfield=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" _orderByField=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"4386,4379,4380,4381,4383,4384,4385,4387,4388,4391,4392,4393\\\\\\\\\\\\\\"]\\",\\"thumb_w_gallery\\":\\"500\\",\\"thumb_h_gallery\\":\\"500\\",\\"gallery_columns\\":\\"0\\",\\"link_opt\\":\\"file\\",\\"link_image_size\\":\\"full\\",\\"animation_effect\\":\\"fadeInRight\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/tea.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"000000_0.77\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"gallery\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"000000_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Appetizer<\\\\/h2>\\",\\"cid\\":\\"c55\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/tea.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Deluxe Omelette <em>$10.95<\\\\/em><\\\\/h4><p>nunc fermentum rhoncus et non nisl. Aliquam ultrices turpis eu tortor pellentesque tristique. Etiam pharetra quis magna vitae tempor.<\\\\/p><h4>Eggs Benedict <em>$10.95<\\\\/em><\\\\/h4><p>Nulla hendrerit aliquam lacus. Quisque libero leo, pretium nec semper quis, aliquet semper lorem. Fusce eu nibh congue<\\\\/p><h4>Eggs Florentine <em>$9.95<\\\\/em><\\\\/h4><p>Sed sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c63\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/veggy.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"3fb8a0_0.88\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"appetizer\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"4db89f_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Main<\\\\/h2>\\",\\"cid\\":\\"c74\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/lemon-clams.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Chicken Burger <em>$11.00<\\\\/em><\\\\/h4><p>Maecenas libero metus, finibus non consectetur sit amet, aliquam ac urna. Suspendisse eget metus ut leo blandit mattis.<\\\\/p><h4>Ham Sandwich  <em>$14.50<\\\\/em><\\\\/h4><p>Arcu tortor interdum puspendisse in elementum. sapien. Integer at turpis sed dolor aliquam pulvinar id euullamcorper.<\\\\/p><h4>Sheppards Pie<em> $29.95<\\\\/em><\\\\/h4><p>Topd sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c82\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/boat-food.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"a675c9_0.87\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"main\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"a373c7_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Dessert<\\\\/h2>\\",\\"cid\\":\\"c93\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/cookies-and-flower.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4>Creme Brûlée <em>$10.50<\\\\/em><\\\\/h4><p>Pulvinar id Sed sodales sapien eu mauris semper, non tempor odio pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\\\/p><h4>Fruit Platter <em>$12.00<\\\\/em><\\\\/h4><p>Tempus dolor. Sed porttitor nunc molestie tortor ullamcorper interdum. Suspendisse in elementum sapien.<\\\\/p><h4>Key Lime Pie<em> $14.25<\\\\/em><\\\\/h4><p>ullamcorper interdum. Suspendisse in elementum sapien. Integer at turpis sed dolor aliquam pulvinar id eu arcu.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c101\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/10\\\\/105703204-1024x681.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"fff647_0.86\\",\\"font_color\\":\\"000000_1.00\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"dessert\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"74d6aa_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h4>11224 Burlington Avenue<br \\\\/>Superman Rd.<br \\\\/>Milton, ON<\\\\/h4>\\\\n<h3>416-333-2828<\\\\/h3>\\\\n<p> <\\\\/p>\\",\\"cid\\":\\"c112\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/leather-sets.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"db4b5c_0.83\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"55 Adelaide East\\\\nToronto, ON\\",\\"zoom_map\\":\\"13\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"300\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"css_map\\":\\"fullheight-map\\"}}]}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"location\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"db4b5c_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4660,
  'post_date' => '2015-09-03 22:42:21',
  'post_date_gmt' => '2015-09-03 22:42:21',
  'post_content' => '',
  'post_title' => 'Shop',
  'post_excerpt' => '',
  'post_name' => 'shop',
  'post_modified' => '2017-08-21 03:30:57',
  'post_modified_gmt' => '2017-08-21 03:30:57',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4660',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'date',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4424,
  'post_date' => '2015-08-31 17:35:33',
  'post_date_gmt' => '2015-08-31 17:35:33',
  'post_content' => '<!--themify_builder_static--><img src="https://themify.me/demo/themes/split/files/2015/08/demo_paria-300x300.jpg" width="300" alt="demo_paria" srcset="https://themify.me/demo/themes/split/files/2015/08/demo_paria-300x300.jpg 300w, https://themify.me/demo/themes/split/files/2015/08/demo_paria-150x150.jpg 150w, https://themify.me/demo/themes/split/files/2015/08/demo_paria-180x180.jpg 180w, https://themify.me/demo/themes/split/files/2015/08/demo_paria.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /> 
 <h1>Jennifer White</h1>
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/desktop.png" width="460" alt="" /> 
 <h2>Project 1</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p>
 
 <a href="http://themify.me/themes/split" > VIEW </a> 
 <h2>Project 2</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p>
 
 <a href="http://themify.me/themes/split" > READ </a> 
 
 <img src="https://themify.me/demo/themes/split/files/2015/08/surge-300x347.png" width="300" alt="surge" srcset="https://themify.me/demo/themes/split/files/2015/08/surge-300x347.png 300w, https://themify.me/demo/themes/split/files/2015/08/surge-259x300.png 259w, https://themify.me/demo/themes/split/files/2015/08/surge-250x289.png 250w, https://themify.me/demo/themes/split/files/2015/08/surge-200x231.png 200w, https://themify.me/demo/themes/split/files/2015/08/surge.png 470w" sizes="(max-width: 300px) 100vw, 300px" /> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/ring-600x222.png" width="400" alt="" /> 
 <h2>Project 3</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p>
 
 <a href="http://themify.me/themes/split" > MY WORK </a> 
 <h2>Project 4</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p> <p> </p>
 
 <a href="http://themify.me/themes/split" > CHECK IT OUT </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-600x214.png" width="500" alt="" /> 
 <h2>Services:</h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. </p>
 <ul><li>Web Template (Interface) Design</li><li>Flash/Interactive</li><li>RSS Feeds</li><li>Blogs &#038; Newsletters</li><li>HTML Email</li></ul><p> </p>
 <ul><li>Online Stores (E-Stores)</li><li>E-Books</li><li>Website Statistics &#038; Analysis</li><li>Web Site Maintenance</li><li>Front End CMS</li></ul>
 <h2>Contact</h2>
<form action="/demo/themes/split/wp-admin/admin-ajax.php#wpcf7-f4429-o1" method="post" novalidate="novalidate">
<input type="hidden" name="_wpcf7" value="4429" /> <input type="hidden" name="_wpcf7_version" value="4.9" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4429-o1" /> <input type="hidden" name="_wpcf7_container_post" value="0" />
<p>Your Email (required)<br /> <input type="email" name="your-email" value="" size="40" aria-required="true" aria-invalid="false" /><br /> Subject<br /> <input type="text" name="your-subject" value="" size="40" aria-invalid="false" /><br /> Your Message<br /> <textarea name="your-message" cols="40" rows="10" aria-invalid="false"></textarea> </p>
<noscript> <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LdIsP4SAAAAAAYl6GQb1xz9cGIuDcS6Uqv-y2gJ" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;"> </iframe> <textarea id="g-recaptcha-response" name="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"> </textarea> </noscript>
<p> <input type="submit" value="Send" /></p> </form><!--/themify_builder_static-->',
  'post_title' => 'Portfolio',
  'post_excerpt' => '',
  'post_name' => 'portfolio',
  'post_modified' => '2017-10-28 02:31:49',
  'post_modified_gmt' => '2017-10-28 02:31:49',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4424',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'portfolio',
    'split_scroll' => 'yes',
    'exclude_site_logo' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'yes',
    'exclude_rss' => 'yes',
    'exclude_social_widget' => 'yes',
    'header_wrap' => 'transparent',
    'background_color' => '333333',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/demo_paria.jpg\\",\\"appearance_image\\":\\"circle\\",\\"width_image\\":\\"300\\",\\"animation_effect\\":\\"bounceIn\\",\\"cid\\":\\"c17\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Jennifer White<\\\\/h1>\\",\\"animation_effect\\":\\"bounceInDown\\",\\"text_align\\":\\"center\\",\\"cid\\":\\"c21\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_type\\":\\"video\\",\\"background_video\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/pencils_on_table_panning_edited_CLIPCHAMP_720p.mp4\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/pencils_on_table_panning.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"d1c5c5_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/desktop.png\\",\\"width_image\\":\\"460\\",\\"cid\\":\\"c32\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/red-umbrella.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Project 1<\\\\/h2>\\",\\"animation_effect\\":\\"bounceInLeft\\",\\"font_color\\":\\"333333\\",\\"cid\\":\\"c40\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"animation_effect\\":\\"bounceInRight\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"VIEW\\",\\"link\\":\\"http:\\\\/\\\\/themify.me\\\\/themes\\\\/split\\",\\"link_options\\":\\"regular\\",\\"button_color_bg\\":\\"black\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/156861758.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"f5f4f0_0.87\\",\\"font_color\\":\\"000000_1.00\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"one\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"f2f1ed_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Project 2<\\\\/h2>\\",\\"animation_effect\\":\\"fadeInDown\\",\\"font_color\\":\\"ffffff_1.00\\",\\"cid\\":\\"c55\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"font_color\\":\\"F2F1ED\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInUp\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"READ\\",\\"link\\":\\"http:\\\\/\\\\/themify.me\\\\/themes\\\\/split\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/surge.png\\",\\"background_repeat\\":\\"repeat-none\\",\\"cover_color\\":\\"706666_0.87\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/surge.png\\",\\"width_image\\":\\"300\\",\\"animation_effect\\":\\"fadeInUpBig\\",\\"cid\\":\\"c67\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/142348745.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"two\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"706666_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/ring-600x222.png\\",\\"width_image\\":\\"400\\",\\"animation_effect\\":\\"rotateInUpLeft\\",\\"cid\\":\\"c78\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/92281490.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Project 3<\\\\/h2>\\",\\"animation_effect\\":\\"rotateIn\\",\\"font_color\\":\\"F2F1ED\\",\\"cid\\":\\"c86\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"font_color\\":\\"ffffff\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"animation_effect\\":\\"rotateInDownLeft\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"MY WORK\\",\\"link\\":\\"http:\\\\/\\\\/themify.me\\\\/themes\\\\/split\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/114041497.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"ada7a1_0.90\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"three\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"ada7a1_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Project 4<\\\\/h2>\\",\\"animation_effect\\":\\"zoomIn\\",\\"font_color\\":\\"ffffff_1.00\\",\\"cid\\":\\"c101\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"font_color\\":\\"F2F1ED\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\\\n<p> <\\\\/p>\\",\\"animation_effect\\":\\"zoomInDown\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"CHECK IT OUT\\",\\"link\\":\\"http:\\\\/\\\\/themify.me\\\\/themes\\\\/split\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/outdoor-yoga.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"9e847e_0.93\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/laptop_angle-600x214.png\\",\\"width_image\\":\\"500\\",\\"animation_effect\\":\\"zoomInUp\\",\\"cid\\":\\"c113\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/5f468e98.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"four\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"9e827e_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Services:<\\\\/h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. <\\\\/p>\\",\\"cid\\":\\"c124\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<ul><li>Web Template (Interface) Design<\\\\/li><li>Flash\\\\/Interactive<\\\\/li><li>RSS Feeds<\\\\/li><li>Blogs &amp; Newsletters<\\\\/li><li>HTML Email<\\\\/li><\\\\/ul><p> <\\\\/p>\\",\\"cid\\":\\"c136\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<ul><li>Online Stores (E-Stores)<\\\\/li><li>E-Books<\\\\/li><li>Website Statistics &amp; Analysis<\\\\/li><li>Web Site Maintenance<\\\\/li><li>Front End CMS<\\\\/li><\\\\/ul>\\",\\"cid\\":\\"c144\\"}}]}]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"7e33ff_0.85\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Contact<\\\\/h2><p>[contact-form-7 id=\\\\\\\\\\\\\\"4429\\\\\\\\\\\\\\" title=\\\\\\\\\\\\\\"Untitled\\\\\\\\\\\\\\"]<\\\\/p>\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"cid\\":\\"c152\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"ffe100_0.88\\",\\"font_color\\":\\"000000_1.00\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"service-contact\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/193500593.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"d4d6b4_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4440,
  'post_date' => '2015-08-31 21:25:19',
  'post_date_gmt' => '2015-08-31 21:25:19',
  'post_content' => '<!--themify_builder_static--><a href="https://www.fitbit.com/uk/surge" > <img src="https://themify.me/demo/themes/split/files/2015/08/surge-250x289.png" width="250" alt="surge" srcset="https://themify.me/demo/themes/split/files/2015/08/surge-250x289.png 250w, https://themify.me/demo/themes/split/files/2015/08/surge-259x300.png 259w, https://themify.me/demo/themes/split/files/2015/08/surge-300x347.png 300w, https://themify.me/demo/themes/split/files/2015/08/surge-200x231.png 200w, https://themify.me/demo/themes/split/files/2015/08/surge.png 470w" sizes="(max-width: 250px) 100vw, 250px" /> </a> 
 <h1>Smart Watch</h1>
 <h3>Stay Active. Live Healthy. Take it anywhere. </h3>
 
 <a href="https://themify.me/demo/themes/split/home/product/#feature-spec" > SPECS </a> 
 <h2>Touch Control</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p>
 
 <a href="https://www.youtube.com/watch?v=AQ-Y1nmkBv0" > WATCH VIDEO </a> 
 
 <a href="https://www.fitbit.com/uk/surge" > <img src="https://themify.me/demo/themes/split/files/2015/08/back-surge-300x235.png" width="300" alt="back-surge" srcset="https://themify.me/demo/themes/split/files/2015/08/back-surge-300x235.png 300w, https://themify.me/demo/themes/split/files/2015/08/back-surge-1440x1130.png 1440w, https://themify.me/demo/themes/split/files/2015/08/back-surge.png 913w" sizes="(max-width: 300px) 100vw, 300px" /> </a> 
 <h2>Stay Active All Day</h2>
 <p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</p>
 <h2>Features:</h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. </p>
  GPS<br /> Water Resistance<br /> Touch Screen<br /> Backlight for visibility<br /> 7 day battery life</p>
  Charge time: 1-2 hours<br /> Vibration motor<br /> Ambient light sensor<br /> Optical heart rate monitor<br /> Monochrome LCD</p>
 <h2>Tech Specs:</h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. </p>
 <ul><li>3-axis gyroscope</li><li>3-axis accelerometers</li><li>Digital Compass</li><li>Altimeter</li><li>Syncing range: 20 feet</li><li>Notifications: Text and call via Bluetooth</li></ul>
 <ul><li>Maximum operating altitude: 30,000 feet</li><li>Sample rate for GPS is 1 Hz</li><li>Radio Transceiver: Bluetooth 4.0</li><li>Size: 20.88mm by 24.66mm</li><li>Wrist size: 14cm &#8211; 23cm</li></ul>
 <h2>Buy Now</h2> <h3>Start experiencing yourself</h3>
 
 <a href="http://themify.me/themes/split" > BUY </a><!--/themify_builder_static-->',
  'post_title' => 'Product',
  'post_excerpt' => '',
  'post_name' => 'product',
  'post_modified' => '2017-10-28 03:23:14',
  'post_modified_gmt' => '2017-10-28 03:23:14',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4440',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'product',
    'split_scroll' => 'yes',
    'exclude_site_logo' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'no',
    'exclude_rss' => 'no',
    'exclude_social_widget' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/surge.png\\",\\"width_image\\":\\"250\\",\\"link_image\\":\\"https:\\\\/\\\\/www.fitbit.com\\\\/uk\\\\/surge\\",\\"param_image\\":\\"lightbox\\",\\"animation_effect\\":\\"pulse\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"cid\\":\\"c16\\"}}],\\"styling\\":{\\"background_type\\":\\"slider\\",\\"background_slider\\":\\"[gallery link=\\\\\\\\\\\\\\"file\\\\\\\\\\\\\\" _orderByField=\\\\\\\\\\\\\\"menu_order ID\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"4451,4450,4449,4447\\\\\\\\\\\\\\"]\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Smart Watch<\\\\/h1>\\",\\"animation_effect\\":\\"bounceInDown\\",\\"cid\\":\\"c24\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h3>Stay Active. Live Healthy. Take it anywhere. <\\\\/h3>\\",\\"animation_effect\\":\\"bounceInDown\\",\\"cid\\":\\"c28\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"link_color_hover\\":\\"#000000\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"SPECS\\",\\"link\\":\\"https://themify.me/demo/themes/split\\\\/home\\\\/product\\\\/#feature-spec\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/skateboard.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"2c98c7_0.83\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"425961_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Touch Control<\\\\/h2>\\",\\"animation_effect\\":\\"fadeInRight\\",\\"font_color\\":\\"ffffff_1.00\\",\\"cid\\":\\"c43\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"font_color\\":\\"F2F1ED\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInLeftBig\\",\\"cid\\":\\"c47\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"link_color_hover\\":\\"#000000\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"WATCH VIDEO\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/watch?v=AQ-Y1nmkBv0\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/surge.png\\",\\"background_repeat\\":\\"repeat-none\\",\\"cover_color\\":\\"000000_0.74\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/fitbit-product.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"video\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"1d2829_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/back-surge.png\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"300\\",\\"link_image\\":\\"https:\\\\/\\\\/www.fitbit.com\\\\/uk\\\\/surge\\",\\"param_image\\":\\"lightbox\\",\\"animation_effect\\":\\"rubberBand\\",\\"margin_top\\":\\"5\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right\\":\\"5\\",\\"margin_right_unit\\":\\"%\\",\\"margin_bottom\\":\\"5\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left\\":\\"5\\",\\"margin_left_unit\\":\\"%\\",\\"cid\\":\\"c66\\"}}],\\"styling\\":{\\"background_type\\":\\"video\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_video\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/ScreenFlow.mp4\\",\\"background_video_options\\":\\"mute\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/fullscreen2-1024x641.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"text_align\\":\\"center\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Stay Active All Day<\\\\/h2>\\",\\"animation_effect\\":\\"rotateInDownLeft\\",\\"font_color\\":\\"f7f7f7_1.00\\",\\"cid\\":\\"c74\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>In eleifend et tortor ut ultrices. Curabitur felis felis, luctus nec posuere ac, suscipit sed est. Duis eleifend nibh vitae sem pharetra scelerisque. In hac habitasse platea dictumst. Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/p>\\",\\"animation_effect\\":\\"rotateInUpLeft\\",\\"font_color\\":\\"ffffff_1.00\\",\\"cid\\":\\"c78\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/shoe-nike-sky.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"4fa2b5_0.85\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"active\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"719899_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Features:<\\\\/h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. <\\\\/p>\\",\\"animation_effect\\":\\"zoomInDown\\",\\"font_color\\":\\"0D252A\\",\\"cid\\":\\"c89\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] GPS<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Water Resistance<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Touch Screen<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Backlight for visibility<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] 7 day battery life<\\\\/p>\\",\\"animation_effect\\":\\"zoomInLeft\\",\\"font_color\\":\\"1F3536\\",\\"cid\\":\\"c101\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Charge time: 1-2 hours<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Vibration motor<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Ambient light sensor<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Optical heart rate monitor<br \\\\/>[themify_icon icon=\\\\\\\\\\\\\\"fa-check\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" icon_color=\\\\\\\\\\\\\\"#26A5AA\\\\\\\\\\\\\\" ] Monochrome LCD<\\\\/p>\\",\\"animation_effect\\":\\"zoomInRight\\",\\"font_color\\":\\"1F3536\\",\\"cid\\":\\"c109\\"}}]}]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider_mode\\":\\"fullcover\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Tech Specs:<\\\\/h2><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh. <\\\\/p>\\",\\"animation_effect\\":\\"zoomInRight\\",\\"font_color\\":\\"FBFBFB\\",\\"link_color\\":\\"FBFBFB\\",\\"cid\\":\\"c117\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<ul><li>3-axis gyroscope<\\\\/li><li>3-axis accelerometers<\\\\/li><li>Digital Compass<\\\\/li><li>Altimeter<\\\\/li><li>Syncing range: 20 feet<\\\\/li><li>Notifications: Text and call via Bluetooth<\\\\/li><\\\\/ul>\\",\\"animation_effect\\":\\"fadeInUpBig\\",\\"font_color\\":\\"FBFBFB\\",\\"cid\\":\\"c129\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<ul><li>Maximum operating altitude: 30,000 feet<\\\\/li><li>Sample rate for GPS is 1 Hz<\\\\/li><li>Radio Transceiver: Bluetooth 4.0<\\\\/li><li>Size: 20.88mm by 24.66mm<\\\\/li><li>Wrist size: 14cm - 23cm<\\\\/li><\\\\/ul>\\",\\"animation_effect\\":\\"zoomInUp\\",\\"font_color\\":\\"FBFBFB\\",\\"cid\\":\\"c137\\"}}]}]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/outdoor-yoga.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"5c5757_0.57\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"feature-spec\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"fbfbfb_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h2>Buy Now<\\\\/h2>\\\\n<h3>Start experiencing yourself<\\\\/h3>\\",\\"cid\\":\\"c148\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"link_color_hover\\":\\"#000000\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"outline\\",\\"content_button\\":[{\\"label\\":\\"BUY\\",\\"link\\":\\"http:\\\\/\\\\/themify.me\\\\/themes\\\\/split\\",\\"link_options\\":\\"regular\\"}]}}]}],\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"buy\\",\\"background_type\\":\\"video\\",\\"background_video\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/time-lapse_day_to_night_city_edited_CLIPCHAMP_720p.mp4\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/demo-videos\\\\/time-lapse_day_to_night_city.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"000000_0.80\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"2393b8_0.78\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"padding_top\\":\\"10\\",\\"padding_top_unit\\":\\"%\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4458,
  'post_date' => '2015-08-31 23:04:40',
  'post_date_gmt' => '2015-08-31 23:04:40',
  'post_content' => '<!--themify_builder_static--><h1>Agency</h1><h3>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.</h3>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Portfolio</h2>
 <ul> <li><a href="https://themify.me/demo/themes/split/portfolio-category/cameras/" >Cameras</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/fashion/" >Fashion</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/photos/" >Photos</a> </li> <li><a href="https://themify.me/demo/themes/split/portfolio-category/products/" >Products</a> </li> </ul>
<article id="portfolio-3298"> <a href="https://themify.me/demo/themes/split/project/builder-project/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/builder-project/"><img src="https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-260x150.jpg" width="260" height="150" alt="cookies-and-flower" srcset="https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/cookies-and-flower-680x390.jpg 680w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/builder-project/">Builder Project</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3298&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3304"> <a href="https://themify.me/demo/themes/split/project/model-liza/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/model-liza/"><img src="https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-260x150.jpg" width="260" height="150" alt="114041515" srcset="https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-2x1.jpg 2w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-5x3.jpg 5w, https://themify.me/demo/themes/split/files/2015/08/114041515-1024x683-8x5.jpg 8w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/model-liza/">Model Liza</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3304&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3295"> <a href="https://themify.me/demo/themes/split/project/model-shots/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/model-shots/"><img src="https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-260x150.jpg" width="260" height="150" alt="79073116" srcset="https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/79073116-683x1024-2x2.jpg 2w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/model-shots/">Model Shots</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3295&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3303"> <a href="https://themify.me/demo/themes/split/project/urban-project/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/urban-project/"><img src="https://themify.me/demo/themes/split/files/2015/08/city-1024x683-260x150.jpg" width="260" height="150" alt="city" srcset="https://themify.me/demo/themes/split/files/2015/08/city-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/city-1024x683-680x390.jpg 680w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/urban-project/">Urban Project</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3303&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3302"> <a href="https://themify.me/demo/themes/split/project/new-york-lights/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/new-york-lights/"><img src="https://themify.me/demo/themes/split/files/2015/08/traffic_out_of_focus_broken-1024x575-260x150.jpg" width="260" height="150" alt="traffic_out_of_focus_broken" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/new-york-lights/">New York Lights</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3302&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3293"> <a href="https://themify.me/demo/themes/split/project/rosy/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/rosy/"><img src="https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-260x150.jpg" width="260" height="150" alt="196169018" srcset="https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/196169018-695x1024-2x2.jpg 2w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/rosy/">Rosy</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3293&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3300"> <a href="https://themify.me/demo/themes/split/project/overwater-shot/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/overwater-shot/"><img src="https://themify.me/demo/themes/split/files/2015/08/outdoor-yoga-260x150.jpg" width="260" height="150" alt="outdoor-yoga" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/overwater-shot/">Overwater Shot</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3300&#038;action=edit">Edit</a>] 
 </article>
<article id="portfolio-3277"> <a href="https://themify.me/demo/themes/split/project/smith-sara/" data-post-permalink="yes" style="display: none;"></a>
 <figure>
 <a href="https://themify.me/demo/themes/split/project/smith-sara/"><img src="https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-260x150.jpg" width="260" height="150" alt="187290671" srcset="https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-260x150.jpg 260w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-680x390.jpg 680w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-2x1.jpg 2w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-5x3.jpg 5w, https://themify.me/demo/themes/split/files/2015/08/187290671-1024x683-8x5.jpg 8w" sizes="(max-width: 260px) 100vw, 260px" /></a> </figure>
 
 
 <h2><a href="https://themify.me/demo/themes/split/project/smith-sara/">Smith &#038; Sara</a> </h2> 
 
 [<a href="https://themify.me/demo/themes/split/wp-admin/post.php?post=3277&#038;action=edit">Edit</a>] 
 </article>
 <a href="https://themify.me/demo/themes/split/portfolio/">More Portfolio</a>
 
 <img src="https://themify.me/demo/themes/split/files/2015/08/user_miles-300x300.jpg" width="300" alt="user_miles" srcset="https://themify.me/demo/themes/split/files/2015/08/user_miles-300x300.jpg 300w, https://themify.me/demo/themes/split/files/2015/08/user_miles-150x150.jpg 150w, https://themify.me/demo/themes/split/files/2015/08/user_miles-180x180.jpg 180w, https://themify.me/demo/themes/split/files/2015/08/user_miles.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /> 
 <h2>John Smith</h2><h4>CEO</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 
 <img src="https://themify.me/demo/themes/split/files/2015/08/134301761-1024x1024-300x300.jpg" width="300" alt="134301761" srcset="https://themify.me/demo/themes/split/files/2015/08/134301761-1024x1024-300x300.jpg 300w, https://themify.me/demo/themes/split/files/2015/08/134301761-150x150.jpg 150w, https://themify.me/demo/themes/split/files/2015/08/134301761-1024x1024.jpg 1024w, https://themify.me/demo/themes/split/files/2015/08/134301761-180x180.jpg 180w, https://themify.me/demo/themes/split/files/2015/08/134301761-600x600.jpg 600w, https://themify.me/demo/themes/split/files/2015/08/134301761.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /> 
 <h2>Linda White</h2><h4>VP</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Catherine McAfee</h2><h4>Product Management</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Mark Donahue</h2><h4>Customer Specialist</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Rex Deacon</h2><h4>Creative Director</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h2>Peter Harlow</h2><h4>Audio Master</h4>
 
 <a href="https://twitter.com/themify" > </a> <a href="https://www.facebook.com/themify" > </a> <a href="https://www.youtube.com/user/themifyme" > </a> 
 <h1>Contact</h1><p>We are located at the downtown core where you can drop by anytime for a cup of coffee or beer. We&#8217;re open to any visitors with any needs. </p>
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=55+Adelaide+East+Toronto+Ontario&amp;t=m&amp;z=15&amp;output=embed&amp;iwloc=near"></iframe><!--/themify_builder_static-->',
  'post_title' => 'Agency',
  'post_excerpt' => '',
  'post_name' => 'agency',
  'post_modified' => '2017-10-27 13:23:47',
  'post_modified_gmt' => '2017-10-27 13:23:47',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4458',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'agency',
    'split_scroll' => 'yes',
    'exclude_site_logo' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'yes',
    'exclude_rss' => 'yes',
    'exclude_social_widget' => 'yes',
    'header_wrap' => 'transparent',
    'background_color' => '26A5AA',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"2eafff_0.39\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Agency<\\\\/h1><h3>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.<\\\\/h3>\\",\\"cid\\":\\"c20\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"font_color_icon\\":\\"#ffffff\\",\\"icon_size\\":\\"large\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"f0388e_0.41\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"2eafff_0.39\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/city.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Portfolio<\\\\/h2>\\",\\"animation_effect\\":\\"zoomInLeft\\",\\"cid\\":\\"c39\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_portfolio_posts limit=\\\\\\\\\\\\\\"8\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"grid4\\\\\\\\\\\\\\"]<\\\\/p>\\",\\"animation_effect\\":\\"shake\\",\\"cid\\":\\"c43\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_button link=\\\\\\\\\\\\\\"https://themify.me/demo/themes/split\\\\/portfolio\\\\/\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"outline large white\\\\\\\\\\\\\\"]More Portfolio[\\\\/themify_button]<\\\\/p>\\",\\"animation_effect\\":\\"zoomInRight\\",\\"cid\\":\\"c47\\"}}]}],\\"styling\\":{\\"row_anchor\\":\\"portfolio\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/10\\\\/131869148-1024x757.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"84cecd_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"86d1d1_0.86\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/user_miles.jpg\\",\\"appearance_image\\":\\"circle\\",\\"width_image\\":\\"300\\",\\"animation_effect\\":\\"zoomInLeft\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"cid\\":\\"c58\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>John Smith<\\\\/h2><h4>CEO<\\\\/h4>\\",\\"animation_effect\\":\\"zoomInLeft\\",\\"cid\\":\\"c62\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"slider\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"f0eb88_1.00\\",\\"font_color\\":\\"000000_1.00\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-center\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/134301761.jpg\\",\\"appearance_image\\":\\"circle\\",\\"width_image\\":\\"300\\",\\"animation_effect\\":\\"zoomInRight\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"cid\\":\\"c74\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Linda White<\\\\/h2><h4>VP<\\\\/h4>\\",\\"animation_effect\\":\\"zoomInRight\\",\\"cid\\":\\"c78\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"font_color_icon\\":\\"#ffffff\\",\\"icon_size\\":\\"xlarge\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_color\\":\\"b294d4_1.00\\",\\"font_color\\":\\"ffffff_1.00\\",\\"link_color\\":\\"e1e8f0\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"ceo-vp\\",\\"background_type\\":\\"image\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Catherine McAfee<\\\\/h2><h4>Product Management<\\\\/h4>\\",\\"animation_effect\\":\\"shake\\",\\"cid\\":\\"c93\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"large\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"icon_color_bg\\":\\"black\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/114041515.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"e4eaf2_0.54\\",\\"cover_color_hover\\":\\"e4eaf2_0.75\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Mark Donahue<\\\\/h2><h4>Customer Specialist<\\\\/h4>\\",\\"animation_effect\\":\\"pulse\\",\\"cid\\":\\"c105\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"font_color_icon\\":\\"#ffffff\\",\\"icon_size\\":\\"large\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/1185374081.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"9169bf_0.37\\",\\"cover_color_hover\\":\\"9169bf_0.67\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Rex Deacon<\\\\/h2><h4>Creative Director<\\\\/h4>\\",\\"animation_effect\\":\\"swing\\",\\"cid\\":\\"c117\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"large\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"icon_color_bg\\":\\"black\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/180680633.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"70d6d3_0.49\\",\\"cover_color_hover\\":\\"70d6d3_0.62\\",\\"font_color\\":\\"000000_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"000000_1.00\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Peter Harlow<\\\\/h2><h4>Audio Master<\\\\/h4>\\",\\"animation_effect\\":\\"bounce\\",\\"cid\\":\\"c129\\"}},{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"font_color_icon\\":\\"#ffffff\\",\\"icon_size\\":\\"large\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_horizontal\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/154622096.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"e82737_0.57\\",\\"cover_color_hover\\":\\"e82737_0.71\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"padding_top\\":\\"15\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"team\\",\\"background_type\\":\\"image\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Contact<\\\\/h1><p>We are located at the downtown core where you can drop by anytime for a cup of coffee or beer. We\\\\\\\\\\\'re open to any visitors with any needs. <\\\\/p>\\",\\"cid\\":\\"c144\\"}},{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"55 Adelaide East Toronto Ontario\\",\\"zoom_map\\":\\"15\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"400\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left_unit\\":\\"%\\"}}],\\"styling\\":{\\"background_type\\":\\"video\\",\\"background_slider_size\\":\\"large\\",\\"background_slider_mode\\":\\"fullcover\\",\\"background_video\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/08\\\\/ink_flow_edited_CLIPCHAMP_720p.mp4\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/time-lapse_day_to_night_city.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"background_position\\":\\"center-center\\",\\"background_color\\":\\"e1e8f0\\",\\"cover_color-type\\":\\"color\\",\\"cover_color\\":\\"e7edf5_0.91\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"row_anchor\\":\\"contact\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4511,
  'post_date' => '2015-09-01 20:46:57',
  'post_date_gmt' => '2015-09-01 20:46:57',
  'post_content' => '<!--themify_builder_static--><h1>Shop Page</h1> <p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p>
 
 <a href="https://themify.me/" > BUY NOW </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/desktop.png" width="600" alt="" /> 
 <h1>Early Bird</h1> <p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p> <p> </p>
 
 
 
 <a href="https://themify.me/" > SHOP </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/straight_jeans-482x380.png" width="600" alt="" /> 
 <h1>Dreamer Style</h1> <p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p>
 
 
 
 <a href="https://themify.me/" > VIEW MORE STYLES </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/shoes_brown.png" width="600" alt="" /> 
 <h1>&#8216;Bring-It&#8217; Line</h1> <p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p>
 
 
 
 <a href="https://themify.me/" > VIEW </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/nice_hat-587x380.png" width="600" alt="" /> 
 <h1>Reimagined Perfection</h1> <p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p>
 
 
 
 <a href="https://themify.me/" > SHOP NOW </a> 
 
 <img src="https://themify.me/demo/themes/flatshop/files/2013/10/led_tv-579x380.png" width="600" alt="" /> 
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=55+Adelaide+East+Toronto+Ontario&amp;t=m&amp;z=8&amp;output=embed&amp;iwloc=near"></iframe>
 <h2>Connect with us</h2>
 
 55 Adelaide St. East 647-999-1234 https://themify.me 
 
 <a href="https://twitter.com/themify" > Twitter </a> <a href="https://www.facebook.com/themify" > Facebook </a> <a href="https://www.youtube.com/user/themifyme" > Youtube </a><!--/themify_builder_static-->',
  'post_title' => 'Shop Page',
  'post_excerpt' => '',
  'post_name' => 'shop-page',
  'post_modified' => '2017-10-28 02:02:09',
  'post_modified_gmt' => '2017-10-28 02:02:09',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4511',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'shop',
    'split_scroll' => 'yes',
    'exclude_site_logo' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'yes',
    'exclude_rss' => 'yes',
    'exclude_social_widget' => 'yes',
    'header_wrap' => 'solid',
    'background_color' => '252525',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>Shop Page<\\\\/h1>\\\\n<p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.<\\\\/p>\\",\\"animation_effect\\":\\"fadeInRight\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"squared\\",\\"content_button\\":[{\\"label\\":\\"BUY NOW\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/\\",\\"link_options\\":\\"regular\\",\\"button_color_bg\\":\\"green\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/desktop.png\\",\\"width_image\\":\\"600\\",\\"animation_effect\\":\\"fadeInUpBig\\",\\"cid\\":\\"c25\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"4c6108_0.47\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"welcome\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-desktop-pair.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>Early Bird<\\\\/h1>\\\\n<p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.<\\\\/p>\\\\n<p> <\\\\/p>\\",\\"animation_effect\\":\\"fadeInLeft\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"cid\\":\\"c40\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"squared\\",\\"content_button\\":[{\\"label\\":\\"SHOP\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/\\",\\"link_options\\":\\"regular\\",\\"button_color_bg\\":\\"blue\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/straight_jeans-482x380.png\\",\\"width_image\\":\\"600\\",\\"animation_effect\\":\\"bounceInDown\\",\\"cid\\":\\"c48\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"3672a3_0.48\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"jeans\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-straight_jeans.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>Dreamer Style<\\\\/h1>\\\\n<p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.<\\\\/p>\\",\\"animation_effect\\":\\"bounceInRight\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"cid\\":\\"c63\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"squared\\",\\"content_button\\":[{\\"label\\":\\"VIEW MORE STYLES\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/\\",\\"link_options\\":\\"regular\\",\\"button_color_bg\\":\\"black\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/shoes_brown.png\\",\\"width_image\\":\\"600\\",\\"animation_effect\\":\\"fadeInRight\\",\\"cid\\":\\"c71\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"ab7844_0.51\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"shoes\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-brown_shoes.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"000000_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>\\\\\\\\\\\'Bring-It\\\\\\\\\\\' Line<\\\\/h1>\\\\n<p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.<\\\\/p>\\",\\"animation_effect\\":\\"zoomInDown\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"cid\\":\\"c86\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"squared\\",\\"content_button\\":[{\\"label\\":\\"VIEW\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/\\",\\"link_options\\":\\"regular\\",\\"button_color_bg\\":\\"black\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/nice_hat-587x380.png\\",\\"width_image\\":\\"600\\",\\"animation_effect\\":\\"zoomInLeft\\",\\"cid\\":\\"c94\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"707070_0.51\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"hat\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-nice_hat.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"000000_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"background_repeat\\":\\"repeat\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"content_text\\":\\"<h1>Reimagined Perfection<\\\\/h1>\\\\n<p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo. Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.<\\\\/p>\\",\\"animation_effect\\":\\"bounceInRight\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"cid\\":\\"c109\\"}},{\\"mod_name\\":\\"buttons\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_family\\":\\"Open Sans\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"checkbox_padding_link_apply_all\\":\\"1\\",\\"checkbox_link_margin_apply_all\\":\\"1\\",\\"checkbox_link_border_apply_all\\":\\"1\\",\\"buttons_size\\":\\"xlarge\\",\\"buttons_style\\":\\"squared\\",\\"content_button\\":[{\\"label\\":\\"SHOP NOW\\",\\"link\\":\\"https:\\\\/\\\\/themify.me\\\\/\\",\\"link_options\\":\\"regular\\"}]}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/led_tv-579x380.png\\",\\"width_image\\":\\"600\\",\\"animation_effect\\":\\"bounceInRight\\",\\"cid\\":\\"c117\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"cover_color\\":\\"e0ab8f_0.51\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"led-tv\\",\\"background_type\\":\\"image\\",\\"background_image\\":\\"https:\\\\/\\\\/themify.me\\\\/demo\\\\/themes\\\\/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-led_tvb.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"55 Adelaide East Toronto Ontario\\",\\"zoom_map\\":\\"8\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"400\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Connect with us<\\\\/h2>\\",\\"cid\\":\\"c136\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_weight\\":\\"bold\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"normal\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_vertical\\",\\"content_icon\\":[{\\"icon\\":\\"fa-home\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"55 Adelaide St. East\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-phone\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"647-999-1234\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-globe\\",\\"label\\":\\"https:\\\\/\\\\/themify.me  \\",\\"link_options\\":\\"regular\\"}]}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"icon\\",\\"mod_settings\\":{\\"background_image-type\\":\\"image\\",\\"font_weight\\":\\"bold\\",\\"checkbox_padding_apply_all\\":\\"1\\",\\"checkbox_margin_apply_all\\":\\"1\\",\\"checkbox_border_apply_all\\":\\"1\\",\\"icon_size\\":\\"normal\\",\\"icon_style\\":\\"none\\",\\"icon_arrangement\\":\\"icon_vertical\\",\\"content_icon\\":[{\\"icon\\":\\"fa-twitter\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Twitter\\",\\"link\\":\\"https:\\\\/\\\\/twitter.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-facebook\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Facebook\\",\\"link\\":\\"https:\\\\/\\\\/www.facebook.com\\\\/themify\\",\\"link_options\\":\\"regular\\"},{\\"icon\\":\\"fa-youtube-play\\",\\"icon_color_bg\\":\\"black\\",\\"label\\":\\"Youtube\\",\\"link\\":\\"https:\\\\/\\\\/www.youtube.com\\\\/user\\\\/themifyme\\",\\"link_options\\":\\"regular\\"}]}}]}]}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"row_anchor\\":\\"contact\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"9ed9ba_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"000000_1.00\\",\\"link_color\\":\\"000000_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"animation_effect\\":\\"shake\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4534,
  'post_date' => '2015-09-01 22:13:46',
  'post_date_gmt' => '2015-09-01 22:13:46',
  'post_content' => '',
  'post_title' => 'Shop - Minimal',
  'post_excerpt' => '',
  'post_name' => 'shop-minimal',
  'post_modified' => '2015-09-01 22:29:19',
  'post_modified_gmt' => '2015-09-01 22:29:19',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4534',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'custom_menu' => 'shop',
    'split_scroll' => 'yes',
    'hide_footer' => 'on',
    'exclude_site_tagline' => 'yes',
    'exclude_search_form' => 'yes',
    'exclude_rss' => 'yes',
    'header_wrap' => 'transparent',
    'background_repeat' => 'fullcover',
    'headerwrap_text_color' => '000000',
    'headerwrap_link_color' => '000000',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Shop Minimal</h1><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.\\\\u00a0Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p><p>[themify_button style=\\\\\\\\\\\\\\"xlarge rect flat green\\\\\\\\\\\\\\" ]Buy Now[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop/files/2013/10/desktop.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"600\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"welcome\\"}},{\\"row_order\\":\\"1\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Early Bird</h1><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.\\\\u00a0Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p><p>[themify_button style=\\\\\\\\\\\\\\"xlarge rect flat blue\\\\\\\\\\\\\\" ]Shop[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop/files/2013/10/straight_jeans-482x380.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"600\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"jeans\\"}},{\\"row_order\\":\\"2\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Dreamer Style</h1><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.\\\\u00a0Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p><p>[themify_button style=\\\\\\\\\\\\\\"xlarge rect flat black\\\\\\\\\\\\\\" ]View More Styles[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop/files/2013/10/shoes_brown.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"600\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"shoes\\"}},{\\"row_order\\":\\"3\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>\\\\\\\\\\\'Bring-It\\\\\\\\\\\' Line</h1><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.\\\\u00a0Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p><p>[themify_button style=\\\\\\\\\\\\\\"xlarge rect flat black\\\\\\\\\\\\\\" ]View[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop/files/2013/10/nice_hat-587x380.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"600\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"hat\\"}},{\\"row_order\\":\\"4\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Reimagined Perfection</h1><p>Aliquam ut odio tincidunt, fringilla elit a, egestas enim. Maecenas metus purus, eleifend ut felis eget, auctor ornare justo.\\\\u00a0Proin eros nisl, suscipit eget mollis sit amet, vulputate a nibh.</p><p>[themify_button style=\\\\\\\\\\\\\\"xlarge rect flat\\\\\\\\\\\\\\" ]Shop Now[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop/files/2013/10/led_tv-579x380.png\\",\\"appearance_image\\":\\"|\\",\\"width_image\\":\\"600\\",\\"param_image\\":\\"|\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"fullcover\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"000000_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"led-tv\\"}},{\\"row_order\\":\\"5\\",\\"gutter\\":\\"gutter-none\\",\\"equal_column_height\\":\\"\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2 first\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Address\\\\u00a0</h2>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"55 Adelaide East Toronto Ontario\\",\\"zoom_map\\":\\"8\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"300\\",\\"unit_h\\":\\"px\\",\\"b_style_map\\":\\"solid\\",\\"type_map\\":\\"ROADMAP\\",\\"scrollwheel_map\\":\\"disable\\",\\"draggable_map\\":\\"enable\\",\\"draggable_disable_mobile_map\\":\\"yes\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2 last\\",\\"modules\\":[null,{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>Connect with us</h2>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>[themify_col grid=\\\\\\\\\\\\\\"2-1 first\\\\\\\\\\\\\\"]</p><p><strong>[themify_icon icon=\\\\\\\\\\\\\\"fa-home\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large grey\\\\\\\\\\\\\\" ] 55 Adelaide St. East\\\\u00a0</strong><strong><br /></strong><strong>[themify_icon icon=\\\\\\\\\\\\\\"fa-phone\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large \\\\\\\\\\\\\\" ]\\\\u00a0647-999-1234<br /></strong><strong>[themify_icon icon=\\\\\\\\\\\\\\"fa-globe\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large\\\\\\\\\\\\\\" ] https://themify.me/</strong></p><p>[/themify_col]</p><p>[themify_col grid=\\\\\\\\\\\\\\"2-1\\\\\\\\\\\\\\"]</p><p>[themify_icon icon=\\\\\\\\\\\\\\"fa-facebook-square\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://www.facebook.com/themify\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large grey\\\\\\\\\\\\\\" ] Facebook<br />[themify_icon icon=\\\\\\\\\\\\\\"fa-twitter-square\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://twitter.com/themify\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large grey\\\\\\\\\\\\\\" ] Twitter<br />[themify_icon icon=\\\\\\\\\\\\\\"fa-youtube-play\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://youtube.com/user/themifyme\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large grey\\\\\\\\\\\\\\" ] Youtube<br />[themify_icon icon=\\\\\\\\\\\\\\"fa-instagram\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://instagram.com/\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\"large grey\\\\\\\\\\\\\\" ] Instagram</p><p>[/themify_col]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\"}}],\\"styling\\":{\\"row_width\\":\\"fullwidth\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"contact\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[]}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4671,
  'post_date' => '2015-09-04 00:20:44',
  'post_date_gmt' => '2015-09-04 00:20:44',
  'post_content' => '',
  'post_title' => 'Super Heroes',
  'post_excerpt' => '',
  'post_name' => 'super-heroes',
  'post_modified' => '2017-08-08 20:23:19',
  'post_modified_gmt' => '2017-08-08 20:23:19',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4671',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'full_width',
    'hide_page_title' => 'yes',
    'split_scroll' => 'yes',
    'exclude_site_tagline' => 'yes',
    'exclude_rss' => 'yes',
    'header_wrap' => 'transparent',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'date',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>H<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInDownBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/bg-captain-america.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"d65179_0.71\\",\\"cover_color_hover\\":\\"d65179_0.51\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>E<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInUpBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/bg-hulk.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"51c4f5_0.69\\",\\"cover_color_hover\\":\\"51c5f7_0.35\\"}},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>R<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInDownBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/iron_man1.png\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"6e67eb_0.74\\",\\"cover_color_hover\\":\\"985fed_0.47\\"}},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>O<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInUpBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/bg-spider-man.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"b3b120_0.71\\",\\"cover_color_hover\\":\\"e6df78_0.44\\"}},{\\"column_order\\":\\"4\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>E<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInDownBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/bg-thor.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"3ff5e9_0.70\\",\\"cover_color_hover\\":\\"6aeef5_0.39\\"}},{\\"column_order\\":\\"5\\",\\"grid_class\\":\\"col6-1\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>S<\\\\/h1>\\",\\"animation_effect\\":\\"fadeInUpBig\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/bg-wolverine.jpg\\",\\"background_repeat\\":\\"fullcover\\",\\"cover_color\\":\\"5dbd6d_0.70\\",\\"cover_color_hover\\":\\"4ff766_0.37\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"1.8\\",\\"font_size_unit\\":\\"em\\",\\"text_align\\":\\"center\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/captain-america.png\\",\\"animation_effect\\":\\"fadeInLeft\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Captain America<\\\\/h3>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_color\\":\\"ff6666_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/hulk.png\\",\\"animation_effect\\":\\"fadeInRight\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Hulk<\\\\/h3>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"82dae8_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"ffffff_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/iron_man.png\\",\\"animation_effect\\":\\"fadeInLeft\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Iron Man<\\\\/h3>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_color\\":\\"93a6e6_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/spiderman.png\\",\\"animation_effect\\":\\"fadeInRight\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Spider Man<\\\\/h3>\\",\\"animation_effect\\":\\"fadeInUp\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"faef55_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"000000_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/thor.png\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Thor<\\\\/h3>\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"background_color\\":\\"82e8ed_1.00\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-top\\",\\"url_image\\":\\"https://themify.me/demo/themes/split\\\\/files\\\\/2015\\\\/09\\\\/wolvine.png\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h3>Wolverine<\\\\/h3>\\",\\"margin_top\\":\\"30\\"}}],\\"styling\\":{\\"background_type\\":\\"image\\",\\"padding_top\\":\\"5\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"5\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\"}}],\\"gutter\\":\\"gutter-none\\",\\"styling\\":{\\"row_width\\":\\"fullwidth-content\\",\\"background_type\\":\\"image\\",\\"background_color\\":\\"8ade9a_1.00\\",\\"cover_color-type\\":\\"color\\",\\"cover_color_hover-type\\":\\"hover_color\\",\\"font_color\\":\\"000000_1.00\\",\\"text_align\\":\\"center\\",\\"link_color\\":\\"ffffff_1.00\\",\\"checkbox_padding_apply_all\\":\\"padding\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4581,
  'post_date' => '2015-09-01 23:10:26',
  'post_date_gmt' => '2015-09-01 23:10:26',
  'post_content' => '',
  'post_title' => 'Blog',
  'post_excerpt' => '',
  'post_name' => 'blog',
  'post_modified' => '2016-06-27 23:11:39',
  'post_modified_gmt' => '2016-06-27 23:11:39',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4581',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'default',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'query_category' => '0',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'excerpt',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'date',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1467077911:2\\"],\\"_edit_last\\":[\\"2\\"],\\"layout\\":[\\"list-post\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"default\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"query_category\\":[\\"0\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"display_content\\":[\\"excerpt\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"date\\"],\\"portfolio_layout\\":[\\"list-post\\"],\\"portfolio_display_content\\":[\\"content\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"default\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"exclude_site_logo\\":[\\"\\"],\\"exclude_site_tagline\\":[\\"\\"],\\"exclude_search_form\\":[\\"\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4584,
  'post_date' => '2015-09-01 23:12:01',
  'post_date_gmt' => '2015-09-01 23:12:01',
  'post_content' => '',
  'post_title' => 'Blog - List Large Image',
  'post_excerpt' => '',
  'post_name' => 'blog-list-large-image',
  'post_modified' => '2015-09-01 23:16:58',
  'post_modified_gmt' => '2015-09-01 23:16:58',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4584',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'query_category' => '0',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-large-image',
    'display_content' => 'excerpt',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1441149284:115\\"],\\"_edit_last\\":[\\"115\\"],\\"layout\\":[\\"list-large-image\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"query_category\\":[\\"0\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"excerpt\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"list-post\\"],\\"portfolio_display_content\\":[\\"content\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"default\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"_dp_original\\":[\\"4581\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4586,
  'post_date' => '2015-09-01 23:12:34',
  'post_date_gmt' => '2015-09-01 23:12:34',
  'post_content' => '',
  'post_title' => 'Blog - Grid3 Masonry',
  'post_excerpt' => '',
  'post_name' => 'blog-grid3-masonry',
  'post_modified' => '2015-09-01 23:17:19',
  'post_modified_gmt' => '2015-09-01 23:17:19',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4586',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'query_category' => '0',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'grid3',
    'disable_masonry' => 'yes',
    'display_content' => 'excerpt',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1441325545:2\\"],\\"_edit_last\\":[\\"115\\"],\\"layout\\":[\\"grid3\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"query_category\\":[\\"0\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"excerpt\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"list-post\\"],\\"portfolio_display_content\\":[\\"content\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"default\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"_dp_original\\":[\\"4584\\"],\\"disable_masonry\\":[\\"yes\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4588,
  'post_date' => '2015-09-01 23:13:10',
  'post_date_gmt' => '2015-09-01 23:13:10',
  'post_content' => '',
  'post_title' => 'Blog - Grid4 Masonry',
  'post_excerpt' => '',
  'post_name' => 'blog-grid4-masonry',
  'post_modified' => '2015-09-10 02:05:07',
  'post_modified_gmt' => '2015-09-10 02:05:07',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4588',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'query_category' => '0',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'grid4',
    'disable_masonry' => 'yes',
    'display_content' => 'none',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_meta_all' => 'yes',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1458854595:2\\"],\\"_edit_last\\":[\\"2\\"],\\"layout\\":[\\"grid4\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"query_category\\":[\\"0\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"none\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"list-post\\"],\\"portfolio_display_content\\":[\\"content\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"default\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"disable_masonry\\":[\\"yes\\"],\\"_dp_original\\":[\\"4586\\"],\\"hide_meta_all\\":[\\"yes\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4599,
  'post_date' => '2015-09-02 21:34:44',
  'post_date_gmt' => '2015-09-02 21:34:44',
  'post_content' => '',
  'post_title' => 'Portfolio',
  'post_excerpt' => '',
  'post_name' => 'portfolio',
  'post_modified' => '2016-01-05 01:06:47',
  'post_modified_gmt' => '2016-01-05 01:06:47',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4599',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'yes',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_query_category' => '0',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'grid3',
    'portfolio_disable_masonry' => 'yes',
    'portfolio_display_content' => 'none',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_image_width' => '420',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'yes',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[]}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4604,
  'post_date' => '2015-09-02 21:38:25',
  'post_date_gmt' => '2015-09-02 21:38:25',
  'post_content' => '',
  'post_title' => 'Portfolio - Grid2',
  'post_excerpt' => '',
  'post_name' => 'portfolio-grid2',
  'post_modified' => '2015-09-02 21:39:24',
  'post_modified_gmt' => '2015-09-02 21:39:24',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4604',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_query_category' => '0',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'grid2',
    'portfolio_posts_per_page' => '8',
    'portfolio_display_content' => 'none',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_image_width' => '600',
    'portfolio_image_height' => '600',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'yes',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1441230001:115\\"],\\"_edit_last\\":[\\"115\\"],\\"layout\\":[\\"list-post\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"content\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_query_category\\":[\\"0\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"grid2\\"],\\"portfolio_posts_per_page\\":[\\"8\\"],\\"portfolio_display_content\\":[\\"none\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_image_width\\":[\\"600\\"],\\"portfolio_image_height\\":[\\"600\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"yes\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4608,
  'post_date' => '2015-09-02 21:40:20',
  'post_date_gmt' => '2015-09-02 21:40:20',
  'post_content' => '',
  'post_title' => 'Portfolio - Grid3',
  'post_excerpt' => '',
  'post_name' => 'portfolio-grid3',
  'post_modified' => '2015-09-02 21:41:46',
  'post_modified_gmt' => '2015-09-02 21:41:46',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4608',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'yes',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'query_category' => '0',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'grid3',
    'posts_per_page' => '8',
    'display_content' => 'none',
    'feature_size_page' => 'blank',
    'image_width' => '400',
    'image_height' => '400',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_meta_all' => 'yes',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_query_category' => '0',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'grid3',
    'portfolio_posts_per_page' => '9',
    'portfolio_display_content' => 'none',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_image_width' => '400',
    'portfolio_image_height' => '400',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'yes',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1441229966:115\\"],\\"_edit_last\\":[\\"115\\"],\\"layout\\":[\\"grid3\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"yes\\"],\\"split_scroll\\":[\\"no\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"none\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_query_category\\":[\\"0\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"grid3\\"],\\"portfolio_posts_per_page\\":[\\"9\\"],\\"portfolio_display_content\\":[\\"none\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_image_width\\":[\\"400\\"],\\"portfolio_image_height\\":[\\"400\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"yes\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"_dp_original\\":[\\"4604\\"],\\"image_width\\":[\\"400\\"],\\"image_height\\":[\\"400\\"],\\"hide_meta_all\\":[\\"yes\\"],\\"query_category\\":[\\"0\\"],\\"posts_per_page\\":[\\"8\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4611,
  'post_date' => '2015-09-02 21:42:57',
  'post_date_gmt' => '2015-09-02 21:42:57',
  'post_content' => '',
  'post_title' => 'Portfolio - Grid4',
  'post_excerpt' => '',
  'post_name' => 'portfolio-grid4',
  'post_modified' => '2015-09-02 21:42:57',
  'post_modified_gmt' => '2015-09-02 21:42:57',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4611',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_query_category' => '0',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'grid4',
    'portfolio_posts_per_page' => '12',
    'portfolio_display_content' => 'none',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_image_width' => '400',
    'portfolio_image_height' => '400',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'yes',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1441230058:115\\"],\\"_edit_last\\":[\\"115\\"],\\"layout\\":[\\"list-post\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"content\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_query_category\\":[\\"0\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"grid4\\"],\\"portfolio_posts_per_page\\":[\\"12\\"],\\"portfolio_display_content\\":[\\"none\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_image_width\\":[\\"400\\"],\\"portfolio_image_height\\":[\\"400\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"yes\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"_dp_original\\":[\\"4604\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4663,
  'post_date' => '2015-09-03 22:43:53',
  'post_date_gmt' => '2015-09-03 22:43:53',
  'post_content' => '[woocommerce_cart]',
  'post_title' => 'Cart',
  'post_excerpt' => '',
  'post_name' => 'cart',
  'post_modified' => '2015-09-03 22:49:06',
  'post_modified_gmt' => '2015-09-03 22:49:06',
  'post_content_filtered' => '',
  'post_parent' => 4660,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4663',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'default',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'content',
    'layout' => 'list-post',
    'display_content' => 'content',
    'feature_size_page' => 'blank',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'content',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_feature_size_page' => 'blank',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"layout\\":[\\"list-post\\"],\\"content_width\\":[\\"default_width\\"],\\"header_wrap\\":[\\"solid\\"],\\"background_repeat\\":[\\"fullcover\\"],\\"page_layout\\":[\\"default\\"],\\"hide_page_title\\":[\\"default\\"],\\"split_scroll\\":[\\"no\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"content\\"],\\"display_content\\":[\\"content\\"],\\"feature_size_page\\":[\\"blank\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"media_position\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"portfolio_order\\":[\\"desc\\"],\\"portfolio_orderby\\":[\\"content\\"],\\"portfolio_layout\\":[\\"list-post\\"],\\"portfolio_display_content\\":[\\"content\\"],\\"portfolio_feature_size_page\\":[\\"blank\\"],\\"portfolio_hide_title\\":[\\"default\\"],\\"portfolio_unlink_title\\":[\\"default\\"],\\"portfolio_hide_meta_all\\":[\\"default\\"],\\"portfolio_hide_image\\":[\\"default\\"],\\"portfolio_unlink_image\\":[\\"default\\"],\\"portfolio_hide_navigation\\":[\\"default\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"_edit_lock\\":[\\"1441320546:2\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4667,
  'post_date' => '2015-09-03 22:45:20',
  'post_date_gmt' => '2015-09-03 22:45:20',
  'post_content' => '[woocommerce_checkout]',
  'post_title' => 'Checkout',
  'post_excerpt' => '',
  'post_name' => 'checkout',
  'post_modified' => '2017-08-21 03:31:03',
  'post_modified_gmt' => '2017-08-21 03:31:03',
  'post_content_filtered' => '',
  'post_parent' => 4660,
  'guid' => 'https://themify.me/demo/themes/split/?page_id=4667',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'default',
    'content_width' => 'default_width',
    'hide_page_title' => 'default',
    'split_scroll' => 'no',
    'header_wrap' => 'solid',
    'background_repeat' => 'fullcover',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'media_position' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'portfolio_order' => 'desc',
    'portfolio_orderby' => 'date',
    'portfolio_layout' => 'list-post',
    'portfolio_display_content' => 'content',
    'portfolio_hide_title' => 'default',
    'portfolio_unlink_title' => 'default',
    'portfolio_hide_meta_all' => 'default',
    'portfolio_hide_image' => 'default',
    'portfolio_unlink_image' => 'default',
    'portfolio_hide_navigation' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3313,
  'post_date' => '2015-04-09 22:11:55',
  'post_date_gmt' => '2015-04-09 22:11:55',
  'post_content' => '<div id="lipsum">Magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.</div>
&nbsp;',
  'post_title' => 'Smart Watch',
  'post_excerpt' => '<div id="lipsum">

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringill elementum.

</div>',
  'post_name' => 'smart-watch',
  'post_modified' => '2017-08-21 03:34:11',
  'post_modified_gmt' => '2017-08-21 03:34:11',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3313',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '30',
    '_sale_price' => '15',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '15',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286326:172',
    '_wp_old_slug' => 'black-and-white-retro',
    '_product_version' => '3.1.1',
    '_thumbnail_id' => '4454',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/08/back-surge.png',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3321,
  'post_date' => '2015-04-09 22:14:13',
  'post_date_gmt' => '2015-04-09 22:14:13',
  'post_content' => '<div id="lipsum">

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida.

</div>
&nbsp;',
  'post_title' => 'Brown Clarks',
  'post_excerpt' => 'Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.',
  'post_name' => 'brown-clarks',
  'post_modified' => '2017-08-21 03:34:09',
  'post_modified_gmt' => '2017-08-21 03:34:09',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3321',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '4642',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/Chukka-bardenas-720x720.jpg',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '125',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '125',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286325:172',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3322,
  'post_date' => '2015-04-09 22:15:21',
  'post_date_gmt' => '2015-04-09 22:15:21',
  'post_content' => 'Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.
<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

</div>
&nbsp;',
  'post_title' => 'Vintage Sneakers',
  'post_excerpt' => 'Donec accumsan ipsum nec dolor aliquam pretium.',
  'post_name' => 'vintage-sneakers',
  'post_modified' => '2017-08-21 03:34:08',
  'post_modified_gmt' => '2017-08-21 03:34:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3322',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '99',
    '_sale_price' => '90',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '90',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286324:172',
    '_product_version' => '3.1.1',
    '_thumbnail_id' => '4643',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/eraLX.jpg',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3323,
  'post_date' => '2015-04-09 22:16:54',
  'post_date_gmt' => '2015-04-09 22:16:54',
  'post_content' => '<div id="lipsum">

Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies.

</div>
&nbsp;',
  'post_title' => 'Relax Loafers',
  'post_excerpt' => 'Dignissim massa, et venenatis leo justo id urna.',
  'post_name' => 'relax-loafers',
  'post_modified' => '2017-08-21 03:36:06',
  'post_modified_gmt' => '2017-08-21 03:36:06',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3323',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '4645',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/vansera1-720x720.jpg',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '360',
    '_sale_price' => '250',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '250',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286425:172',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3324,
  'post_date' => '2015-04-09 22:17:53',
  'post_date_gmt' => '2015-04-09 22:17:53',
  'post_content' => '<div id="lipsum">

Welit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Workman Boots',
  'post_excerpt' => 'Sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin,',
  'post_name' => 'workman-boots',
  'post_modified' => '2017-08-21 03:34:04',
  'post_modified_gmt' => '2017-08-21 03:34:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3324',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '4644',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/Sk8-HI-720x720.jpg',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '1000',
    '_sale_price' => '800',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '800',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286321:172',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3325,
  'post_date' => '2015-04-09 22:18:38',
  'post_date_gmt' => '2015-04-09 22:18:38',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin,',
  'post_title' => 'Sneaker King',
  'post_excerpt' => 'Suspendisse porttitor laoreet neque',
  'post_name' => 'sneaker-king',
  'post_modified' => '2017-08-21 03:34:02',
  'post_modified_gmt' => '2017-08-21 03:34:02',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3325',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '4643',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/eraLX.jpg',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '30',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '30',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286321:172',
    '_product_version' => '3.1.1',
    '_wp_old_slug' => 'polka-bow',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3326,
  'post_date' => '2015-04-09 22:19:28',
  'post_date_gmt' => '2015-04-09 22:19:28',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Red Kicks',
  'post_excerpt' => 'Suspendisse viverra nibh a fringilla viverra.',
  'post_name' => 'red-kicks',
  'post_modified' => '2017-08-21 03:34:01',
  'post_modified_gmt' => '2017-08-21 03:34:01',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3326',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4645',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/vansera1-720x720.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '220',
    '_sale_price' => '200',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '200',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286465:172',
    '_wp_old_slug' => 'vintage-golf',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3336,
  'post_date' => '2015-04-09 22:27:33',
  'post_date_gmt' => '2015-04-09 22:27:33',
  'post_content' => '<div id="lipsum">

Vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc.

</div>
&nbsp;',
  'post_title' => 'Skull Shirt',
  'post_excerpt' => 'Magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc.',
  'post_name' => 'skull-shirt',
  'post_modified' => '2017-08-21 03:33:59',
  'post_modified_gmt' => '2017-08-21 03:33:59',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3336',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '45',
    '_sale_price' => '35',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '35',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_thumbnail_id' => '4637',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/grateful-dead.jpg',
    '_edit_lock' => '1503286464:172',
    '_wp_old_slug' => 'mickey-shirt',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3337,
  'post_date' => '2015-04-09 22:28:22',
  'post_date_gmt' => '2015-04-09 22:28:22',
  'post_content' => '<div id="lipsum">

Placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Esem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Yellow Sunrise',
  'post_excerpt' => 'Esem enim maximus ipsum, sed ullamcorper est urna suscipit massa.',
  'post_name' => 'yellow-sunrise',
  'post_modified' => '2017-08-21 03:33:58',
  'post_modified_gmt' => '2017-08-21 03:33:58',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3337',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4634',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/days-end.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '300',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '300',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286319:172',
    '_wp_old_slug' => 'black-dress',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3338,
  'post_date' => '2015-04-09 22:29:41',
  'post_date_gmt' => '2015-04-09 22:29:41',
  'post_content' => '<div id="lipsum">

Blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum.

</div>
&nbsp;',
  'post_title' => 'King Kong Fanatic',
  'post_excerpt' => 'Morbi pretium non ex ut volutpat.',
  'post_name' => 'king-kong-fanatic',
  'post_modified' => '2017-08-21 03:33:56',
  'post_modified_gmt' => '2017-08-21 03:33:56',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3338',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4635',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/dk.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '600',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '600',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286319:172',
    '_wp_old_slug' => 'bow-dress',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3339,
  'post_date' => '2015-04-09 22:30:49',
  'post_date_gmt' => '2015-04-09 22:30:49',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida.

</div>
&nbsp;',
  'post_title' => 'Bloody Mary',
  'post_excerpt' => 'Pretium non ex ut volutpat',
  'post_name' => 'bloody-mary',
  'post_modified' => '2017-08-21 03:33:54',
  'post_modified_gmt' => '2017-08-21 03:33:54',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3339',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4633',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/bloody-mary.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '1200',
    '_sale_price' => '1000',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '1000',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286318:172',
    '_wp_old_slug' => 'full-vintage-top-set',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3340,
  'post_date' => '2015-04-09 22:31:45',
  'post_date_gmt' => '2015-04-09 22:31:45',
  'post_content' => '<div id="lipsum">

Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Australia Summit',
  'post_excerpt' => 'Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_name' => 'australia-summit',
  'post_modified' => '2017-08-21 03:33:51',
  'post_modified_gmt' => '2017-08-21 03:33:51',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3340',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4632',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/australia.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '120',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '120',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286462:172',
    '_wp_old_slug' => 'construction-joe',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3341,
  'post_date' => '2015-04-09 22:32:32',
  'post_date_gmt' => '2015-04-09 22:32:32',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

</div>
&nbsp;',
  'post_title' => 'Green Elephant',
  'post_excerpt' => 'Turpis dignissim massa, et venenatis leo justo id urna.',
  'post_name' => 'green-elephant',
  'post_modified' => '2017-08-21 03:33:49',
  'post_modified_gmt' => '2017-08-21 03:33:49',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3341',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4636',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/elephant.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '60',
    '_sale_price' => '50',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '50',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286317:172',
    '_wp_old_slug' => 'maiden-blues',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3343,
  'post_date' => '2015-04-09 22:41:24',
  'post_date_gmt' => '2015-04-09 22:41:24',
  'post_content' => '<div id="lipsum">

Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum.

</div>
&nbsp;',
  'post_title' => 'Red Clutch',
  'post_excerpt' => 'Bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum.',
  'post_name' => 'red-clutch',
  'post_modified' => '2017-08-21 03:33:48',
  'post_modified_gmt' => '2017-08-21 03:33:48',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3343',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4628',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/144212706.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '460',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '460',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286317:172',
    '_wp_old_slug' => 'orange-clutch',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3352,
  'post_date' => '2015-04-09 22:42:04',
  'post_date_gmt' => '2015-04-09 22:42:04',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Black on Black',
  'post_excerpt' => 'Suspendisse viverra nibh a fringilla viverra.',
  'post_name' => 'black-on-black',
  'post_modified' => '2017-08-21 03:33:45',
  'post_modified_gmt' => '2017-08-21 03:33:45',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3352',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4630',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/64049515.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '880',
    '_sale_price' => '600',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '600',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286461:172',
    '_wp_old_slug' => 'leather-bag',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3353,
  'post_date' => '2015-04-09 22:43:00',
  'post_date_gmt' => '2015-04-09 22:43:00',
  'post_content' => '<div id="lipsum">

Wulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Eco Bagpack',
  'post_excerpt' => 'Wulla tortor ex, sodales id mollis ac, feugiat sit amet leo',
  'post_name' => 'eco-bagpack',
  'post_modified' => '2017-08-21 03:33:44',
  'post_modified_gmt' => '2017-08-21 03:33:44',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3353',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4629',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/11881450.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '180',
    '_sale_price' => '80',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '80',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286316:172',
    '_wp_old_slug' => 'green-duffle',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3354,
  'post_date' => '2015-04-09 22:43:56',
  'post_date_gmt' => '2015-04-09 22:43:56',
  'post_content' => '<div id="lipsum">

Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Herschel Bag',
  'post_excerpt' => 'Maecenas consequat, justo sed rutrum sollicitudin',
  'post_name' => 'herschel-bag',
  'post_modified' => '2017-08-21 03:33:42',
  'post_modified_gmt' => '2017-08-21 03:33:42',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3354',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4627',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/simplesack.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '90',
    '_sale_price' => '69',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '69',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286315:172',
    '_wp_old_slug' => 'on-the-go-leather-carry-bag',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3355,
  'post_date' => '2015-04-09 22:44:43',
  'post_date_gmt' => '2015-04-09 22:44:43',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Black Bag Pack',
  'post_excerpt' => 'Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus',
  'post_name' => 'black-bag-pack',
  'post_modified' => '2017-08-21 03:33:40',
  'post_modified_gmt' => '2017-08-21 03:33:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3355',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4626',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/backpack2-720x720.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '333',
    '_sale_price' => '233',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '233',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286459:172',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3356,
  'post_date' => '2015-04-09 22:45:35',
  'post_date_gmt' => '2015-04-09 22:45:35',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vitae quam sed leo luctus sagittis. Fusce ut metus scelerisque dui facilisis auctor at sit amet eros. Donec sit amet nibh ac ipsum commodo tempor a a nibh. Suspendisse ornare neque id massa sollicitudin maximus. Vivamus vel dui Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'Peachy Bahamas',
  'post_excerpt' => 'Donec sit amet nibh ac ipsum commodo',
  'post_name' => 'peachy-bahamas',
  'post_modified' => '2017-08-21 03:33:38',
  'post_modified_gmt' => '2017-08-21 03:33:38',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=product&#038;p=3356',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_thumbnail_id' => '4631',
    'post_image' => 'https://themify.me/demo/themes/split/files/2015/09/73294600.jpg',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '1110',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '1110',
    '_manage_stock' => 'no',
    '_backorders' => 'no',
    '_stock' => NULL,
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_edit_lock' => '1503286457:172',
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_tax_status' => 'taxable',
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'bag',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3272,
  'post_date' => '2013-03-30 20:35:30',
  'post_date_gmt' => '2013-03-30 20:35:30',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Camera Co',
  'post_excerpt' => '',
  'post_name' => 'camera-co',
  'post_modified' => '2017-08-21 03:32:13',
  'post_modified_gmt' => '2017-08-21 03:32:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3272',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'March 30, 2013',
    'project_client' => 'Argo/LCX Camera',
    'project_services' => 'Photography, Photo Edits, Model Scouting',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'cameras',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3276,
  'post_date' => '2013-07-13 20:36:39',
  'post_date_gmt' => '2013-07-13 20:36:39',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'MyTea',
  'post_excerpt' => '',
  'post_name' => 'mytea',
  'post_modified' => '2017-08-21 03:32:06',
  'post_modified_gmt' => '2017-08-21 03:32:06',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3276',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'July 13, 2013',
    'project_client' => 'Vintage Shots',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'cameras',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3277,
  'post_date' => '2013-11-03 20:37:52',
  'post_date_gmt' => '2013-11-03 20:37:52',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Smith & Sara',
  'post_excerpt' => '',
  'post_name' => 'smith-sara',
  'post_modified' => '2017-08-21 03:32:02',
  'post_modified_gmt' => '2017-08-21 03:32:02',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3277',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'November 3, 2013',
    'project_client' => 'Yashica Camera',
    'project_services' => 'Photography, Creative Image Placement, Photo Edits',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'cameras',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3278,
  'post_date' => '2013-10-18 20:39:53',
  'post_date_gmt' => '2013-10-18 20:39:53',
  'post_content' => 'Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.',
  'post_title' => 'Black & White',
  'post_excerpt' => '',
  'post_name' => 'black-white',
  'post_modified' => '2017-08-21 03:32:03',
  'post_modified_gmt' => '2017-08-21 03:32:03',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3278',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'October 18, 2013',
    'project_client' => 'Michelle Reitman',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3280,
  'post_date' => '2013-08-02 20:44:51',
  'post_date_gmt' => '2013-08-02 20:44:51',
  'post_content' => 'In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus.',
  'post_title' => 'Zen Tea',
  'post_excerpt' => '',
  'post_name' => 'zen-tea',
  'post_modified' => '2017-08-21 03:32:05',
  'post_modified_gmt' => '2017-08-21 03:32:05',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3280',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'August 2, 2013',
    'project_client' => 'Gentlemen\'s Club',
    'project_services' => 'Photography, Creative Product Placement',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'products',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3283,
  'post_date' => '2013-06-16 20:50:23',
  'post_date_gmt' => '2013-06-16 20:50:23',
  'post_content' => 'Aliquam arcu antena sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In the agittis eu rutrum, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus.',
  'post_title' => 'Shop App',
  'post_excerpt' => '',
  'post_name' => 'shop-app',
  'post_modified' => '2017-08-21 03:32:08',
  'post_modified_gmt' => '2017-08-21 03:32:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3283',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'June 16, 2013',
    'project_client' => 'Farmers house',
    'project_services' => 'Project Management, Photography',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'products',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3285,
  'post_date' => '2013-03-23 20:52:35',
  'post_date_gmt' => '2013-03-23 20:52:35',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus.',
  'post_title' => 'Office Desk Set',
  'post_excerpt' => '',
  'post_name' => 'office-desk-set',
  'post_modified' => '2017-08-21 03:32:13',
  'post_modified_gmt' => '2017-08-21 03:32:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3285',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'March 23, 2013',
    'project_client' => 'Apple Accessories',
    'project_services' => 'Photography, Creative Material Placement',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'products',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3289,
  'post_date' => '2013-01-09 20:55:13',
  'post_date_gmt' => '2013-01-09 20:55:13',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at.',
  'post_title' => 'Back-to-School Toolkit',
  'post_excerpt' => '',
  'post_name' => 'back-to-school-toolkit',
  'post_modified' => '2017-08-21 03:32:15',
  'post_modified_gmt' => '2017-08-21 03:32:15',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3289',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'January 9, 2013',
    'project_client' => 'Brian Hemington',
    'project_services' => 'Photography, Creative Design',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'products',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3291,
  'post_date' => '2013-04-03 20:56:41',
  'post_date_gmt' => '2013-04-03 20:56:41',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at.',
  'post_title' => 'Mezo Espresso',
  'post_excerpt' => '',
  'post_name' => 'mezo-espresso',
  'post_modified' => '2017-08-21 03:32:13',
  'post_modified_gmt' => '2017-08-21 03:32:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3291',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'April 3, 2013',
    'project_client' => 'Mezo Espresso',
    'project_services' => 'Interior Design',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3293,
  'post_date' => '2014-02-20 21:20:21',
  'post_date_gmt' => '2014-02-20 21:20:21',
  'post_content' => 'Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at.',
  'post_title' => 'Rosy',
  'post_excerpt' => '',
  'post_name' => 'rosy',
  'post_modified' => '2017-08-21 03:31:59',
  'post_modified_gmt' => '2017-08-21 03:31:59',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3293',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'February 20, 2014',
    'project_client' => 'Blue Mountain Cottage',
    'project_services' => 'Interior Design',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3295,
  'post_date' => '2014-09-14 21:28:08',
  'post_date_gmt' => '2014-09-14 21:28:08',
  'post_content' => 'Donec vitae volutpat erat. Donec non molestie lacus. Integer euismod leo euismod, fermentum tellus sed, consequat leo. Cras lobortis nisl non dapibus tempor. Donec a finibus tellus. Vivamus laoreet lacinia imperdiet. Fusce tincidunt metus ac sapien feugiat, sit amet laoreet lorem aliquam. Integer pharetra egestas mi vel aliquam.

Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'Model Shots',
  'post_excerpt' => '',
  'post_name' => 'model-shots',
  'post_modified' => '2017-08-21 03:31:53',
  'post_modified_gmt' => '2017-08-21 03:31:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3295',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'Oct 2014',
    'project_client' => 'Cafe Americano',
    'project_services' => 'Interior Design',
    'project_launch' => 'https://themify.me',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3298,
  'post_date' => '2014-12-21 21:31:27',
  'post_date_gmt' => '2014-12-21 21:31:27',
  'post_content' => '',
  'post_title' => 'Builder Project',
  'post_excerpt' => 'Donec vitae volutpat erat. Donec non molestie lacus. Integer euismod leo euismod, fermentum tellus sed, consequat leo. Cras lobortis nisl non dapibus tempor. Donec a finibus tellus.',
  'post_name' => 'builder-project',
  'post_modified' => '2017-08-21 03:31:50',
  'post_modified_gmt' => '2017-08-21 03:31:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3298',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'full_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>Builder Project</h1><h3>Porfolio Page Completely Customized and Constructed Using the Builder</h3>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"7\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"2\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1>01</h1><h4>Background</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"checkbox_padding_apply_all\\":\\"|\\",\\"checkbox_padding_apply_all_padding\\":\\"padding\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\",\\"checkbox_margin_apply_all\\":\\"margin\\",\\"checkbox_margin_apply_all_margin\\":\\"margin\\",\\"checkbox_border_apply_all\\":\\"border\\",\\"checkbox_border_apply_all_border\\":\\"border\\",\\"font_family_h1\\":\\"default\\",\\"font_family_h2\\":\\"default\\",\\"font_family_h3\\":\\"default\\",\\"font_family_h4\\":\\"default\\",\\"font_family_h5\\":\\"default\\",\\"font_family_h6\\":\\"default\\",\\"visibility_desktop\\":\\"show\\",\\"visibility_desktop_show\\":\\"show\\",\\"visibility_desktop_hide\\":\\"hide\\",\\"visibility_tablet\\":\\"show\\",\\"visibility_tablet_show\\":\\"show\\",\\"visibility_tablet_hide\\":\\"hide\\",\\"visibility_mobile\\":\\"show\\",\\"visibility_mobile_show\\":\\"show\\",\\"visibility_mobile_hide\\":\\"hide\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p>Nulla pellentesque lectus ac augue malesuada condimentum. Cras pellentesque risus a condimentum sodales. Quisque nunc ipsum, posuere sagittis ex mollis, ultricies lacinia tellus. Etiam dictum enim ac ipsum tristique dapibus. Aenean venenatis justo non enim scelerisque, sed pellentesque nisl facilisis. Quisque eget venenatis justo. In vestibulum orci maximus, feugiat arcu nec, volutpat dolor. Curabitur interdum convallis lectus at aliquam. Cras quis varius lorem. Nulla mattis maximus sapien aliquam eleifend.</p><p>[themify_button style=\\\\\\\\\\\\\\"rect outline black\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://themify.me\\\\\\\\\\\\\\"]Launch Site[/themify_button]</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"10\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/161176295.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"20\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"20\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1 style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">02</h1><h4 style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">This is Where It All Begins</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">Faelis facilisis non. Pellentesque eleifend luctus massa at vestibulum. Aliquam nisl odio, congue non sagittis a, viverra et sapien. Quisque ut magna facilisis, consequat mauris non, finibus felis. Aenean leo arcu, tincidunt at nisl tempus, aliquet tristique enim. Donec ullamcorper justo non turpis feugiat fringilla. Integer lacinia diam odio, non finibus lorem convallis sit amet. Proin vestibulum ultrices gravida.</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"slider\\",\\"mod_settings\\":{\\"layout_display_slider\\":\\"image\\",\\"blog_category_slider\\":\\"|single\\",\\"slider_category_slider\\":\\"|single\\",\\"portfolio_category_slider\\":\\"|single\\",\\"testimonial_category_slider\\":\\"|single\\",\\"order_slider\\":\\"desc\\",\\"orderby_slider\\":\\"date\\",\\"display_slider\\":\\"content\\",\\"img_content_slider\\":[{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/2024380661.jpg\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/1988883051.jpg\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/2002890712.jpg\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/195028886.jpg\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/194739149.jpg\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/180333233.jpg\\"}],\\"video_content_slider\\":[[]],\\"text_content_slider\\":[[]],\\"layout_slider\\":\\"slider-default\\",\\"img_w_slider\\":\\"400\\",\\"img_h_slider\\":\\"400\\",\\"visible_opt_slider\\":\\"4\\",\\"auto_scroll_opt_slider\\":\\"4\\",\\"scroll_opt_slider\\":\\"1\\",\\"speed_opt_slider\\":\\"normal\\",\\"effect_slider\\":\\"continuously\\",\\"pause_on_hover_slider\\":\\"resume\\",\\"wrap_slider\\":\\"yes\\",\\"show_nav_slider\\":\\"yes\\",\\"show_arrow_slider\\":\\"yes\\",\\"left_margin_slider\\":\\"5\\",\\"right_margin_slider\\":\\"5\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}},{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<p style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">Faelis facilisis non. Pellentesque eleifend luctus massa at vestibulum. Aliquam nisl odio, congue non sagittis a, viverra et sapien. Quisque ut magna facilisis, consequat mauris non, finibus felis. Aenean leo arcu, tincidunt at nisl tempus, aliquet tristique enim. Donec ullamcorper justo non turpis feugiat fringilla. Integer lacinia diam odio, non finibus lorem convallis sit amet. Proin vestibulum ultrices gravida.</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"4\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4 style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">Moving forward...</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"fullheight\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/214781395.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"afcacc_1.00\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"%\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"%\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h2>03 <em>Strategy</em></h2><p>Donec convallis luctus convallis. Proin gravida porttitor sem eget interdum. Donec fermentum orci nisi. Nunc id imperdiet metus. Nulla quis facilisis mauris, non consequat quam. Quisque ut dictum magna. Suspendisse aliquam at arcu sit amet hendrerit. Nam vel risus ut felis rutrum interdum. Nam sapien arcu, mattis eu diam eleifend, dictum tincidunt ante. Proin venenatis quis purus non tempor. Ut nibh nunc, mollis commodo pulvinar in, auctor sit amet orci. Suspendisse ligula metus, viverra ac rhoncus eu, consectetur quis odio.</p><p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam eget placerat odio, vitae imperdiet urna. Mauris eu ligula eros. Curabitur semper non velit vitae consequat. Aliquam ut ipsum dui. Suspendisse scelerisque nulla non vehicula ullamcorper. Morbi cursus erat sed quam malesuada blandit.</p><p> </p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"8\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"8\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h4 style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">Finally...</h4>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"fullheight\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"https://themify.me/demo/themes/elegant/files/2014/10/183068741.jpg\\",\\"background_repeat\\":\\"builder-parallax-scrolling\\",\\"background_color\\":\\"b8e0d2_1.00\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"ffffff_1.00\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"7\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"content_text\\":\\"<h1 style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\"><em>Final</em></h1><p style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">[themify_button style=\\\\\\\\\\\\\\"large rect outline black\\\\\\\\\\\\\\" link=\\\\\\\\\\\\\\"https://themify.me/addons/counter\\\\\\\\\\\\\\"]Buy Counter Addon[/themify_button] </p><p style=\\\\\\\\\\\\\\"text-align: center;\\\\\\\\\\\\\\">Show off your numbers in an elegant and unique way by purchasing our Counter Addon. Whether it\\\\\\\\\\\'s statistics about your company, followers, profit, or concerts performed the counting animation will make your numbers stand out. Check it below:</p>\\",\\"font_family\\":\\"default\\",\\"text_align_left\\":\\"left\\",\\"text_align_center\\":\\"center\\",\\"text_align_right\\":\\"right\\",\\"text_align_justify\\":\\"justify\\",\\"padding_top_unit\\":\\"px\\",\\"padding_right_unit\\":\\"px\\",\\"padding_bottom_unit\\":\\"px\\",\\"padding_left_unit\\":\\"px\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left_unit\\":\\"px\\"}}],\\"styling\\":[]}],\\"styling\\":{\\"row_width\\":\\"\\",\\"row_height\\":\\"\\",\\"animation_effect\\":\\"\\",\\"background_type\\":\\"image\\",\\"background_slider\\":\\"\\",\\"background_slider_mode\\":\\"\\",\\"background_video\\":\\"\\",\\"background_image\\":\\"\\",\\"background_repeat\\":\\"\\",\\"background_color\\":\\"\\",\\"cover_color\\":\\"\\",\\"cover_color_hover\\":\\"\\",\\"font_family\\":\\"default\\",\\"font_color\\":\\"\\",\\"font_size\\":\\"\\",\\"font_size_unit\\":\\"\\",\\"line_height\\":\\"\\",\\"line_height_unit\\":\\"\\",\\"text_align\\":\\"\\",\\"link_color\\":\\"\\",\\"text_decoration\\":\\"\\",\\"padding_top\\":\\"8\\",\\"padding_top_unit\\":\\"%\\",\\"padding_right\\":\\"5\\",\\"padding_right_unit\\":\\"%\\",\\"padding_bottom\\":\\"\\",\\"padding_bottom_unit\\":\\"%\\",\\"padding_left\\":\\"5\\",\\"padding_left_unit\\":\\"%\\",\\"margin_top\\":\\"\\",\\"margin_top_unit\\":\\"px\\",\\"margin_right\\":\\"\\",\\"margin_right_unit\\":\\"px\\",\\"margin_bottom\\":\\"\\",\\"margin_bottom_unit\\":\\"px\\",\\"margin_left\\":\\"\\",\\"margin_left_unit\\":\\"px\\",\\"border_top_color\\":\\"\\",\\"border_top_width\\":\\"\\",\\"border_top_style\\":\\"\\",\\"border_right_color\\":\\"\\",\\"border_right_width\\":\\"\\",\\"border_right_style\\":\\"\\",\\"border_bottom_color\\":\\"\\",\\"border_bottom_width\\":\\"\\",\\"border_bottom_style\\":\\"\\",\\"border_left_color\\":\\"\\",\\"border_left_width\\":\\"\\",\\"border_left_style\\":\\"\\",\\"custom_css_row\\":\\"\\",\\"row_anchor\\":\\"\\"}},{\\"row_order\\":\\"8\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3300,
  'post_date' => '2014-01-01 21:33:56',
  'post_date_gmt' => '2014-01-01 21:33:56',
  'post_content' => 'Donec vitae volutpat erat. Donec non molestie lacus. Integer euismod leo euismod, fermentum tellus sed, consequat leo. Cras lobortis nisl non dapibus tempor. Donec a finibus tellus. Vivamus laoreet lacinia imperdiet. Fusce tincidunt metus ac sapien feugiat, sit amet laoreet lorem aliquam. Integer pharetra egestas mi vel aliquam.

Mauris pulvinar, massa eget semper imperdiet, sapien nisl vulputate mi, ut commodo mi erat et sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur pellentesque augue nec nisl ultricies aliquet. Integer ipsum ante, interdum ac varius quis, ullamcorper vel ante. Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.',
  'post_title' => 'Overwater Shot',
  'post_excerpt' => '',
  'post_name' => 'overwater-shot',
  'post_modified' => '2017-08-21 03:32:00',
  'post_modified_gmt' => '2017-08-21 03:32:00',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3300',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'January 1, 2014',
    'project_client' => 'Euro Tourists',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3302,
  'post_date' => '2014-03-08 21:38:19',
  'post_date_gmt' => '2014-03-08 21:38:19',
  'post_content' => 'Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue,',
  'post_title' => 'New York Lights',
  'post_excerpt' => '',
  'post_name' => 'new-york-lights',
  'post_modified' => '2017-08-21 03:31:56',
  'post_modified_gmt' => '2017-08-21 03:31:56',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3302',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'March 8, 2014',
    'project_client' => 'Old Medley',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3303,
  'post_date' => '2014-07-22 21:41:02',
  'post_date_gmt' => '2014-07-22 21:41:02',
  'post_content' => 'Sedd aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.',
  'post_title' => 'Urban Project',
  'post_excerpt' => '',
  'post_name' => 'urban-project',
  'post_modified' => '2017-08-21 03:31:54',
  'post_modified_gmt' => '2017-08-21 03:31:54',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3303',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'July 2014',
    'project_client' => 'Chinatown - Boston',
    'project_services' => 'Photography, Photo Edit, Creative Styling',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'photos',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3304,
  'post_date' => '2014-11-11 21:43:04',
  'post_date_gmt' => '2014-11-11 21:43:04',
  'post_content' => 'Donec eu mi vitae ex aliquam porttitor. Donec a mi in mauris finibus venenatis vitae at augue. Maecenas non pulvinar purus. Vestibulum posuere faucibus libero, eu dapibus magna mollis quis. Etiam non ante urna. Curabitur tincidunt ultrices sagittis. Donec quis felis leo. Cras ullamcorper, est eget convallis dapibus, diam lacus viverra magna, volutpat maximus lorem urna ac purus. Nam felis metus, eleifend ut fringilla a, sagittis nec mauris. In id congue justo.

&nbsp;',
  'post_title' => 'Model Liza',
  'post_excerpt' => '',
  'post_name' => 'model-liza',
  'post_modified' => '2017-08-21 03:31:51',
  'post_modified_gmt' => '2017-08-21 03:31:51',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3304',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'November 11, 2014',
    'project_client' => 'Toronto Tourism',
    'project_services' => 'Creative Design, Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3306,
  'post_date' => '2012-04-01 21:46:35',
  'post_date_gmt' => '2012-04-01 21:46:35',
  'post_content' => 'Donec vitae volutpat erat. Donec non molestie lacus. Integer euismod leo euismod, fermentum tellus sed, consequat leo. Cras lobortis nisl non dapibus tempor. Donec a finibus tellus. Vivamus laoreet lacinia imperdiet. Fusce tincidunt metus ac sapien feugiat, sit amet laoreet lorem aliquam. Integer pharetra egestas mi vel aliquam.',
  'post_title' => 'Old English',
  'post_excerpt' => '',
  'post_name' => 'old-english',
  'post_modified' => '2017-08-21 03:32:22',
  'post_modified_gmt' => '2017-08-21 03:32:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3306',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'April 1, 2012',
    'project_client' => 'Old Maiden',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3307,
  'post_date' => '2012-09-05 21:48:03',
  'post_date_gmt' => '2012-09-05 21:48:03',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Camera Lightbox',
  'post_excerpt' => '',
  'post_name' => 'camera-lightbox',
  'post_modified' => '2017-08-21 03:32:16',
  'post_modified_gmt' => '2017-08-21 03:32:16',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3307',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'September 5, 2012',
    'project_client' => 'Vintage Classic',
    'project_services' => 'Creative Product Placement, Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'cameras',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3309,
  'post_date' => '2012-05-29 21:54:07',
  'post_date_gmt' => '2012-05-29 21:54:07',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

<!--more-->

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;

&nbsp;',
  'post_title' => 'Blacksmith Tools',
  'post_excerpt' => '',
  'post_name' => 'blacksmith-tools',
  'post_modified' => '2017-08-21 03:32:20',
  'post_modified_gmt' => '2017-08-21 03:32:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3309',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'May 29, 2012',
    'project_client' => 'Vintage Limited',
    'project_services' => 'Creative Material Placement, Photography',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'products',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 3311,
  'post_date' => '2012-06-09 21:58:04',
  'post_date_gmt' => '2012-06-09 21:58:04',
  'post_content' => '<div id="lipsum">

Donec accumsan ipsum nec dolor aliquam pretium. Suspendisse consequat condimentum augue, vel ultricies quam efficitur quis. Aenean sapien turpis, tincidunt congue risus ac, eleifend condimentum nunc. Quisque porta nulla erat, et maximus nisi venenatis at. Aliquam arcu ante, sagittis eu rutrum a, efficitur eget nibh. In venenatis metus est, a sagittis turpis cursus quis. Fusce odio neque, placerat ut porttitor eget, congue vestibulum purus. In pretium posuere elit sed lobortis. Praesent finibus ultrices augue, eget blandit mauris. Duis pulvinar, quam ut tristique euismod, velit turpis dignissim massa, et venenatis leo justo id urna.

Nulla tortor ex, sodales id mollis ac, feugiat sit amet leo. Suspendisse porttitor laoreet neque, et bibendum lacus euismod id. In tincidunt, tortor vel fringilla elementum, magna purus lacinia ante, id egestas nisi justo vel eros. Pellentesque orci lorem, accumsan sed aliquam sed, pretium sed nunc. Maecenas consequat, justo sed rutrum sollicitudin, velit ante ultricies ante, et euismod arcu purus et leo. Morbi pretium non ex ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat rutrum nisl quis condimentum. Sed eget tincidunt nulla, posuere elementum ligula. Nulla facilisi. Praesent luctus, neque dictum feugiat maximus, sem enim maximus ipsum, sed ullamcorper est urna suscipit massa. Cras commodo eros nec eleifend vehicula. Praesent auctor augue in massa porta gravida. Nullam et ex eget diam mollis hendrerit id dignissim sem. Suspendisse viverra nibh a fringilla viverra.

</div>
&nbsp;',
  'post_title' => 'Summer Rush',
  'post_excerpt' => '',
  'post_name' => 'summer-rush',
  'post_modified' => '2017-08-21 03:32:18',
  'post_modified_gmt' => '2017-08-21 03:32:18',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/ultra/?post_type=portfolio&#038;p=3311',
  'menu_order' => 0,
  'post_type' => 'portfolio',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_meta' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'project_date' => 'June 9, 2012',
    'project_client' => 'Morning Rush Mexico',
    'project_services' => 'Photography, Photo Edits',
    'project_launch' => 'https://themify.me/',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'portfolio-category' => 'fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4357,
  'post_date' => '2015-08-26 20:19:55',
  'post_date_gmt' => '2015-08-26 20:19:55',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4357',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4357',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4489,
  'post_date' => '2015-09-01 20:30:17',
  'post_date_gmt' => '2015-09-01 20:30:17',
  'post_content' => '',
  'post_title' => 'Lucy & John',
  'post_excerpt' => '',
  'post_name' => 'lucy-john-3',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4489',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4489',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4481,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'Welcome',
  'post_excerpt' => '',
  'post_name' => 'welcome',
  'post_modified' => '2015-09-04 17:29:19',
  'post_modified_gmt' => '2015-09-04 17:29:19',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4481',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4481',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4488,
  'post_date' => '2015-09-01 20:27:47',
  'post_date_gmt' => '2015-09-01 20:27:47',
  'post_content' => '',
  'post_title' => 'Jennifer White',
  'post_excerpt' => '',
  'post_name' => 'jennifer-white-2',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4488',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4488',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4499,
  'post_date' => '2015-09-01 20:34:40',
  'post_date_gmt' => '2015-09-01 20:34:40',
  'post_content' => '',
  'post_title' => 'Welcome',
  'post_excerpt' => '',
  'post_name' => 'welcome-2',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4499',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4499',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4504,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Welcome',
  'post_excerpt' => '',
  'post_name' => 'welcome-3',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4504',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4504',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4526,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'Welcome',
  'post_excerpt' => '',
  'post_name' => 'welcome-4',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4526',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4526',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#welcome',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4359,
  'post_date' => '2015-08-26 20:19:55',
  'post_date_gmt' => '2015-08-26 20:19:55',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4359',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4359',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4415,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => '',
  'post_title' => 'About',
  'post_excerpt' => '',
  'post_name' => 'lucy-john',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4415',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4415',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#about',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4435,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => '',
  'post_title' => 'Project 1',
  'post_excerpt' => '',
  'post_name' => 'project-1',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4435',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4435',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#one',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4482,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'Video',
  'post_excerpt' => '',
  'post_name' => 'video',
  'post_modified' => '2015-09-04 17:29:19',
  'post_modified_gmt' => '2015-09-04 17:29:19',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4482',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4482',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#video',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4500,
  'post_date' => '2015-09-01 20:34:40',
  'post_date_gmt' => '2015-09-01 20:34:40',
  'post_content' => '',
  'post_title' => 'Portfolio',
  'post_excerpt' => '',
  'post_name' => 'portfolio',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4500',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4500',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#portfolio',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4505,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Gallery',
  'post_excerpt' => '',
  'post_name' => 'gallery',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4505',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4505',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#gallery',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4525,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'Jeans',
  'post_excerpt' => '',
  'post_name' => 'jeans',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4525',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4525',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#jeans',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4360,
  'post_date' => '2015-08-26 20:19:55',
  'post_date_gmt' => '2015-08-26 20:19:55',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4360',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4360',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4416,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => '',
  'post_title' => 'Our Story',
  'post_excerpt' => '',
  'post_name' => 'our-story',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4416',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4416',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#ourstory',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4436,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => '',
  'post_title' => 'Project 2',
  'post_excerpt' => '',
  'post_name' => 'project-2',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4436',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4436',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#two',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4483,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'Active',
  'post_excerpt' => '',
  'post_name' => 'active',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4483',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4483',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#active',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4501,
  'post_date' => '2015-09-01 20:34:40',
  'post_date_gmt' => '2015-09-01 20:34:40',
  'post_content' => '',
  'post_title' => 'The CEO & VP',
  'post_excerpt' => '',
  'post_name' => 'the-ceo-vp',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4501',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4501',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#ceo-vp',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4506,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Appetizer',
  'post_excerpt' => '',
  'post_name' => 'appetizer',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4506',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4506',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#appetizer',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4524,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'Shoes',
  'post_excerpt' => '',
  'post_name' => 'shoes',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4524',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4524',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#shoes',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4361,
  'post_date' => '2015-08-26 20:19:55',
  'post_date_gmt' => '2015-08-26 20:19:55',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4361',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4361',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4417,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => '',
  'post_title' => 'Venue',
  'post_excerpt' => '',
  'post_name' => 'venue',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4417',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4417',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#venue',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4437,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => '',
  'post_title' => 'Project 3',
  'post_excerpt' => '',
  'post_name' => 'project-3',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4437',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4437',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#three',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4484,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'Features & Tech Specs',
  'post_excerpt' => '',
  'post_name' => 'features-tech-specs',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4484',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4484',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#feature-spec',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4502,
  'post_date' => '2015-09-01 20:34:40',
  'post_date_gmt' => '2015-09-01 20:34:40',
  'post_content' => '',
  'post_title' => 'The Team',
  'post_excerpt' => '',
  'post_name' => 'the-team',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4502',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4502',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#team',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4507,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Main',
  'post_excerpt' => '',
  'post_name' => 'main',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4507',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4507',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#main',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4523,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'Hat',
  'post_excerpt' => '',
  'post_name' => 'hat',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4523',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4523',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#hat',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4418,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => '',
  'post_title' => 'RSVP',
  'post_excerpt' => '',
  'post_name' => 'rsvp',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4418',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4418',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#rsvp',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4438,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => '',
  'post_title' => 'Project 4',
  'post_excerpt' => '',
  'post_name' => 'project-4',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4438',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4438',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#four',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4445,
  'post_date' => '2015-08-31 21:26:40',
  'post_date_gmt' => '2015-08-31 21:26:40',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4445',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4445',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4485,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'Buy Now',
  'post_excerpt' => '',
  'post_name' => 'buy-now',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4485',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4485',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#buy',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4503,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => '',
  'post_title' => 'Contact',
  'post_excerpt' => '',
  'post_name' => 'contact',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4503',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4503',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#contact',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4509,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Dessert',
  'post_excerpt' => '',
  'post_name' => 'dessert',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4509',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4509',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#dessert',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4522,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'LED TV',
  'post_excerpt' => '',
  'post_name' => 'led-tv',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4522',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4522',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#led-tv',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4419,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => '',
  'post_title' => 'Thank You',
  'post_excerpt' => '',
  'post_name' => 'thank-you',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4419',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4419',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#thank-you',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4439,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => '',
  'post_title' => 'Services & Contact',
  'post_excerpt' => '',
  'post_name' => 'services-contact',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4439',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4439',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#service-contact',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4446,
  'post_date' => '2015-08-31 21:26:40',
  'post_date_gmt' => '2015-08-31 21:26:40',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4446',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4446',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4480,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more-2',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4480',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4480',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4498,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more-5',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4498',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4498',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4510,
  'post_date' => '2015-09-01 20:41:06',
  'post_date_gmt' => '2015-09-01 20:41:06',
  'post_content' => '',
  'post_title' => 'Location',
  'post_excerpt' => '',
  'post_name' => 'location',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4510',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4510',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#location',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4527,
  'post_date' => '2015-09-01 22:11:06',
  'post_date_gmt' => '2015-09-01 22:11:06',
  'post_content' => '',
  'post_title' => 'Contact',
  'post_excerpt' => '',
  'post_name' => 'contact-2',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4527',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4527',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#contact',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4466,
  'post_date' => '2015-09-01 17:57:23',
  'post_date_gmt' => '2015-09-01 17:57:23',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4466',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4475,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4475',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4475',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4486,
  'post_date' => '2015-09-01 20:27:48',
  'post_date_gmt' => '2015-09-01 20:27:48',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more-3',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4486',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4486',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4490,
  'post_date' => '2015-09-01 20:30:17',
  'post_date_gmt' => '2015-09-01 20:30:17',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more-4',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4490',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4490',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4493,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4493',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4493',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4521,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => '',
  'post_title' => 'More',
  'post_excerpt' => '',
  'post_name' => 'more-6',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4521',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4521',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => '#',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4596,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4596',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4596',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4420,
  'post_date' => '2015-08-31 17:30:53',
  'post_date_gmt' => '2015-08-31 17:30:53',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4420',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4420',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4444,
  'post_date' => '2015-08-31 21:26:21',
  'post_date_gmt' => '2015-08-31 21:26:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4444',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4444',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4467,
  'post_date' => '2015-09-01 17:57:23',
  'post_date_gmt' => '2015-09-01 17:57:23',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4467',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4467',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4476,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4476',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4476',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4494,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4494',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4494',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4515,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4515',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4515',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4597,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4597',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4597',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4421,
  'post_date' => '2015-08-31 17:30:53',
  'post_date_gmt' => '2015-08-31 17:30:53',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4421',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4421',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4434,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4434',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4434',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4468,
  'post_date' => '2015-09-01 17:57:23',
  'post_date_gmt' => '2015-09-01 17:57:23',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4468',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4468',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4477,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4477',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4477',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4495,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4495',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4495',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4516,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4516',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4516',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4686,
  'post_date' => '2015-09-04 01:07:53',
  'post_date_gmt' => '2015-09-04 01:07:53',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4686',
  'post_modified' => '2015-11-23 20:30:54',
  'post_modified_gmt' => '2015-11-23 20:30:54',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4686',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4357',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4433,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4433',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4433',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4441,
  'post_date' => '2015-08-31 21:25:24',
  'post_date_gmt' => '2015-08-31 21:25:24',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4441',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4441',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4469,
  'post_date' => '2015-09-01 17:57:23',
  'post_date_gmt' => '2015-09-01 17:57:23',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4469',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4469',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4478,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4478',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4478',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4496,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4496',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4496',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4517,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4517',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4517',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4424',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4592,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4592',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4592',
  'menu_order' => 10,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4581',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4432,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4432',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4432',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4443,
  'post_date' => '2015-08-31 21:25:34',
  'post_date_gmt' => '2015-08-31 21:25:34',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4443',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4443',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4440',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4471,
  'post_date' => '2015-09-01 17:57:23',
  'post_date_gmt' => '2015-09-01 17:57:23',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4471',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4471',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4479,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4479',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4479',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4497,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4497',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4497',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4518,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4518',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4518',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4348',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4595,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4595',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?p=4595',
  'menu_order' => 11,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4592',
    '_menu_item_object_id' => '4584',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4487,
  'post_date' => '2015-09-01 20:27:48',
  'post_date_gmt' => '2015-09-01 20:27:48',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4487',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4487',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4491,
  'post_date' => '2015-09-01 20:30:17',
  'post_date_gmt' => '2015-09-01 20:30:17',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4491',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4491',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4458',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4519,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4519',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4519',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4337',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4528,
  'post_date' => '2015-09-01 22:12:02',
  'post_date_gmt' => '2015-09-01 22:12:02',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4528',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4528',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4530,
  'post_date' => '2015-09-01 22:12:38',
  'post_date_gmt' => '2015-09-01 22:12:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4530',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4530',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4531,
  'post_date' => '2015-09-01 22:13:04',
  'post_date_gmt' => '2015-09-01 22:13:04',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4531',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4531',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4593,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4593',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?p=4593',
  'menu_order' => 12,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4592',
    '_menu_item_object_id' => '4588',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4472,
  'post_date' => '2015-09-01 17:57:24',
  'post_date_gmt' => '2015-09-01 17:57:24',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4472',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4472',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4520,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4520',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4520',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4330',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4529,
  'post_date' => '2015-09-01 22:12:19',
  'post_date_gmt' => '2015-09-01 22:12:19',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4529',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4529',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4533,
  'post_date' => '2015-09-01 22:13:19',
  'post_date_gmt' => '2015-09-01 22:13:19',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4533',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4533',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4511',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4594,
  'post_date' => '2015-09-01 23:15:25',
  'post_date_gmt' => '2015-09-01 23:15:25',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4594',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4581,
  'guid' => 'https://themify.me/demo/themes/split/?p=4594',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4592',
    '_menu_item_object_id' => '4586',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4688,
  'post_date' => '2015-09-04 17:28:40',
  'post_date_gmt' => '2015-09-04 17:28:40',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4688',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4688',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4690,
  'post_date' => '2015-09-04 17:29:20',
  'post_date_gmt' => '2015-09-04 17:29:20',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4690',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4690',
  'menu_order' => 13,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4492,
  'post_date' => '2015-09-01 20:34:41',
  'post_date_gmt' => '2015-09-01 20:34:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4492',
  'post_modified' => '2015-09-04 17:28:40',
  'post_modified_gmt' => '2015-09-04 17:28:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4492',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4498',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'agency',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4474,
  'post_date' => '2015-09-01 20:24:38',
  'post_date_gmt' => '2015-09-01 20:24:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4474',
  'post_modified' => '2015-09-04 17:29:20',
  'post_modified_gmt' => '2015-09-04 17:29:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4474',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4480',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'product',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4600,
  'post_date' => '2015-09-02 21:34:44',
  'post_date_gmt' => '2015-09-02 21:34:44',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4600',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/2015/09/02/4600/',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4599',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4691,
  'post_date' => '2015-09-04 17:29:36',
  'post_date_gmt' => '2015-09-04 17:29:36',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4691',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4691',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4692,
  'post_date' => '2015-09-04 17:29:53',
  'post_date_gmt' => '2015-09-04 17:29:53',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4692',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4692',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4693,
  'post_date' => '2015-09-04 17:30:07',
  'post_date_gmt' => '2015-09-04 17:30:07',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4693',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4693',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4694,
  'post_date' => '2015-09-04 17:30:24',
  'post_date_gmt' => '2015-09-04 17:30:24',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4694',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 4328,
  'guid' => 'https://themify.me/demo/themes/split/?p=4694',
  'menu_order' => 14,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4671',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4414,
  'post_date' => '2015-08-31 17:30:22',
  'post_date_gmt' => '2015-08-31 17:30:22',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4414',
  'post_modified' => '2015-09-04 17:30:24',
  'post_modified_gmt' => '2015-09-04 17:30:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4414',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4490',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'wedding',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4430,
  'post_date' => '2015-08-31 21:14:56',
  'post_date_gmt' => '2015-08-31 21:14:56',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4430',
  'post_modified' => '2016-12-03 00:13:50',
  'post_modified_gmt' => '2016-12-03 00:13:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4430',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4486',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'portfolio',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4514,
  'post_date' => '2015-09-01 22:09:50',
  'post_date_gmt' => '2015-09-01 22:09:50',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4514',
  'post_modified' => '2015-09-04 17:30:07',
  'post_modified_gmt' => '2015-09-04 17:30:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4514',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4521',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'shop',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4532,
  'post_date' => '2015-09-01 22:13:05',
  'post_date_gmt' => '2015-09-01 22:13:05',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4532',
  'post_modified' => '2015-09-04 17:29:53',
  'post_modified_gmt' => '2015-09-04 17:29:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/?p=4532',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4466',
    '_menu_item_object_id' => '4328',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'restaurant',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4617,
  'post_date' => '2015-09-02 21:47:19',
  'post_date_gmt' => '2015-09-02 21:47:19',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4617',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?p=4617',
  'menu_order' => 15,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4600',
    '_menu_item_object_id' => '4604',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4616,
  'post_date' => '2015-09-02 21:47:19',
  'post_date_gmt' => '2015-09-02 21:47:19',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4616',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?p=4616',
  'menu_order' => 16,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4600',
    '_menu_item_object_id' => '4608',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4615,
  'post_date' => '2015-09-02 21:47:19',
  'post_date_gmt' => '2015-09-02 21:47:19',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4615',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4599,
  'guid' => 'https://themify.me/demo/themes/split/?p=4615',
  'menu_order' => 17,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4600',
    '_menu_item_object_id' => '4611',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4661,
  'post_date' => '2015-09-03 22:42:21',
  'post_date_gmt' => '2015-09-03 22:42:21',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4661',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/split/2015/09/03/4661/',
  'menu_order' => 18,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '4660',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4664,
  'post_date' => '2015-09-03 22:43:53',
  'post_date_gmt' => '2015-09-03 22:43:53',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4664',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4660,
  'guid' => 'https://themify.me/demo/themes/split/2015/09/03/4664/',
  'menu_order' => 19,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4661',
    '_menu_item_object_id' => '4663',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 4668,
  'post_date' => '2015-09-03 22:45:20',
  'post_date_gmt' => '2015-09-03 22:45:20',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '4668',
  'post_modified' => '2015-11-23 20:30:55',
  'post_modified_gmt' => '2015-11-23 20:30:55',
  'post_content_filtered' => '',
  'post_parent' => 4660,
  'guid' => 'https://themify.me/demo/themes/split/2015/09/03/4668/',
  'menu_order' => 20,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '4661',
    '_menu_item_object_id' => '4667',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-menu',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}


function themify_import_get_term_id_from_slug( $slug ) {
	$menu = get_term_by( "slug", $slug, "nav_menu" );
	return is_wp_error( $menu ) ? 0 : (int) $menu->term_id;
}

	$widgets = get_option( "widget_themify-twitter" );
$widgets[1002] = array (
  'title' => 'Latest Tweets',
  'username' => 'themify',
  'show_count' => '3',
  'hide_timestamp' => NULL,
  'show_follow' => 'on',
  'follow_text' => '→ Follow me',
  'include_retweets' => NULL,
  'exclude_replies' => NULL,
);
update_option( "widget_themify-twitter", $widgets );

$widgets = get_option( "widget_themify-feature-posts" );
$widgets[1003] = array (
  'title' => 'Recent Posts',
  'category' => '0',
  'show_count' => '5',
  'show_date' => 'on',
  'show_thumb' => 'on',
  'display' => 'none',
  'hide_title' => NULL,
  'thumb_width' => '50',
  'thumb_height' => '50',
  'excerpt_length' => '55',
  'orderby' => 'date',
  'order' => 'DESC',
);
update_option( "widget_themify-feature-posts", $widgets );

$widgets = get_option( "widget_themify-social-links" );
$widgets[1004] = array (
  'title' => '',
  'show_link_name' => NULL,
  'open_new_window' => NULL,
  'icon_size' => 'icon-medium',
  'orientation' => 'horizontal',
);
update_option( "widget_themify-social-links", $widgets );

$widgets = get_option( "widget_themify-twitter" );
$widgets[1005] = array (
  'title' => 'Latest Tweets',
  'username' => 'themify',
  'show_count' => '3',
  'hide_timestamp' => NULL,
  'show_follow' => 'on',
  'follow_text' => '→ Follow me',
  'include_retweets' => NULL,
  'exclude_replies' => NULL,
);
update_option( "widget_themify-twitter", $widgets );

$widgets = get_option( "widget_themify-feature-posts" );
$widgets[1006] = array (
  'title' => 'Recent Posts',
  'category' => '0',
  'show_count' => '3',
  'show_date' => 'on',
  'show_thumb' => 'on',
  'display' => 'none',
  'hide_title' => NULL,
  'thumb_width' => '50',
  'thumb_height' => '50',
  'excerpt_length' => '55',
  'orderby' => 'date',
  'order' => 'DESC',
);
update_option( "widget_themify-feature-posts", $widgets );

$widgets = get_option( "widget_themify-social-links" );
$widgets[1007] = array (
  'title' => 'Follow us  →',
  'show_link_name' => NULL,
  'open_new_window' => NULL,
  'icon_size' => 'icon-medium',
  'orientation' => 'horizontal',
);
update_option( "widget_themify-social-links", $widgets );

$widgets = get_option( "widget_text" );
$widgets[1008] = array (
  'title' => '',
  'text' => 'Create the site of your dreams, with the responsive and retina-ready Split theme. It come with the easy-to-use Themify Builder, allowing you to build cool looking pages without having to touch the code.',
  'filter' => false,
);
update_option( "widget_text", $widgets );



$sidebars_widgets = array (
  'sidebar-main' => 
  array (
    0 => 'themify-twitter-1002',
    1 => 'themify-feature-posts-1003',
  ),
  'social-widget' => 
  array (
    0 => 'themify-social-links-1004',
  ),
  'footer-widget-1' => 
  array (
    0 => 'themify-twitter-1005',
  ),
  'footer-widget-2' => 
  array (
    0 => 'themify-feature-posts-1006',
  ),
  'footer-widget-3' => 
  array (
    0 => 'themify-social-links-1007',
    1 => 'text-1008',
  ),
); 
update_option( "sidebars_widgets", $sidebars_widgets );

$menu_locations = array();
$menu = get_terms( "nav_menu", array( "slug" => "main-menu" ) );
if( is_array( $menu ) && ! empty( $menu ) ) $menu_locations["main-nav"] = $menu[0]->term_id;
set_theme_mod( "nav_menu_locations", $menu_locations );


$homepage = get_posts( array( 'name' => 'home', 'post_type' => 'page' ) );
			if( is_array( $homepage ) && ! empty( $homepage ) ) {
				update_option( 'show_on_front', 'page' );
				update_option( 'page_on_front', $homepage[0]->ID );
			}
			
	ob_start(); ?>a:220:{s:15:"setting-favicon";s:0:"";s:23:"setting-custom_feed_url";s:0:"";s:19:"setting-header_html";s:0:"";s:19:"setting-footer_html";s:0:"";s:23:"setting-search_settings";s:0:"";s:16:"setting-page_404";s:1:"0";s:21:"setting-feed_settings";s:0:"";s:21:"setting-webfonts_list";s:11:"recommended";s:24:"setting-webfonts_subsets";s:0:"";s:22:"setting-default_layout";s:8:"sidebar1";s:27:"setting-default_post_layout";s:9:"list-post";s:23:"setting-disable_masonry";s:3:"yes";s:30:"setting-default_layout_display";s:7:"content";s:25:"setting-default_more_text";s:4:"More";s:21:"setting-index_orderby";s:4:"date";s:19:"setting-index_order";s:4:"DESC";s:26:"setting-default_post_title";s:0:"";s:33:"setting-default_unlink_post_title";s:0:"";s:25:"setting-default_post_meta";s:0:"";s:32:"setting-default_post_meta_author";s:0:"";s:34:"setting-default_post_meta_category";s:0:"";s:33:"setting-default_post_meta_comment";s:0:"";s:29:"setting-default_post_meta_tag";s:0:"";s:25:"setting-default_post_date";s:0:"";s:30:"setting-default_media_position";s:5:"above";s:26:"setting-default_post_image";s:0:"";s:33:"setting-default_unlink_post_image";s:0:"";s:31:"setting-image_post_feature_size";s:5:"blank";s:24:"setting-image_post_width";s:0:"";s:25:"setting-image_post_height";s:0:"";s:32:"setting-default_page_post_layout";s:8:"sidebar1";s:31:"setting-default_page_post_title";s:0:"";s:38:"setting-default_page_unlink_post_title";s:0:"";s:30:"setting-default_page_post_meta";s:0:"";s:37:"setting-default_page_post_meta_author";s:0:"";s:39:"setting-default_page_post_meta_category";s:0:"";s:38:"setting-default_page_post_meta_comment";s:0:"";s:34:"setting-default_page_post_meta_tag";s:0:"";s:30:"setting-default_page_post_date";s:0:"";s:31:"setting-default_page_post_image";s:0:"";s:38:"setting-default_page_unlink_post_image";s:0:"";s:38:"setting-image_post_single_feature_size";s:5:"blank";s:31:"setting-image_post_single_width";s:0:"";s:32:"setting-image_post_single_height";s:0:"";s:27:"setting-default_page_layout";s:8:"sidebar1";s:23:"setting-hide_page_title";s:0:"";s:23:"setting-hide_page_image";s:0:"";s:33:"setting-page_featured_image_width";s:0:"";s:34:"setting-page_featured_image_height";s:0:"";s:38:"setting-default_portfolio_index_layout";s:12:"sidebar-none";s:43:"setting-default_portfolio_index_post_layout";s:5:"grid3";s:33:"setting-portfolio_disable_masonry";s:3:"yes";s:39:"setting-default_portfolio_index_display";s:4:"none";s:37:"setting-default_portfolio_index_title";s:0:"";s:49:"setting-default_portfolio_index_unlink_post_title";s:0:"";s:50:"setting-default_portfolio_index_post_meta_category";s:3:"yes";s:49:"setting-default_portfolio_index_unlink_post_image";s:2:"no";s:48:"setting-default_portfolio_index_image_post_width";s:0:"";s:49:"setting-default_portfolio_index_image_post_height";s:0:"";s:38:"setting-default_portfolio_single_title";s:0:"";s:50:"setting-default_portfolio_single_unlink_post_title";s:0:"";s:51:"setting-default_portfolio_single_post_meta_category";s:0:"";s:50:"setting-default_portfolio_single_unlink_post_image";s:3:"yes";s:49:"setting-default_portfolio_single_image_post_width";s:0:"";s:50:"setting-default_portfolio_single_image_post_height";s:0:"";s:22:"themify_portfolio_slug";s:7:"project";s:31:"setting-product_disable_masonry";s:3:"yes";s:34:"setting-product_archive_hide_image";s:3:"yes";s:34:"setting-product_archive_hide_title";s:3:"yes";s:34:"setting-product_archive_hide_price";s:3:"yes";s:35:"setting-product_archive_hide_rating";s:3:"yes";s:40:"setting-product_archive_hide_add_to_cart";s:3:"yes";s:53:"setting-customizer_responsive_design_tablet_landscape";s:4:"1024";s:43:"setting-customizer_responsive_design_tablet";s:3:"768";s:43:"setting-customizer_responsive_design_mobile";s:3:"480";s:33:"setting-mobile_menu_trigger_point";s:4:"1200";s:24:"setting-gallery_lightbox";s:8:"lightbox";s:26:"setting-page_builder_cache";s:2:"on";s:18:"setting-cache_gzip";s:2:"on";s:27:"setting-script_minification";s:7:"disable";s:27:"setting-page_builder_expiry";s:1:"2";s:25:"setting-page_loader_color";s:0:"";s:24:"setting-page_loader_icon";s:0:"";s:18:"setting-more_posts";s:8:"infinite";s:22:"setting-footer_widgets";s:17:"footerwidget-3col";s:24:"setting-footer_text_left";s:0:"";s:25:"setting-footer_text_right";s:0:"";s:27:"setting-global_feature_size";s:5:"blank";s:22:"setting-link_icon_type";s:9:"font-icon";s:32:"setting-link_type_themify-link-0";s:10:"image-icon";s:33:"setting-link_title_themify-link-0";s:7:"Twitter";s:32:"setting-link_link_themify-link-0";s:0:"";s:31:"setting-link_img_themify-link-0";s:99:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/social/twitter.png";s:32:"setting-link_type_themify-link-1";s:10:"image-icon";s:33:"setting-link_title_themify-link-1";s:8:"Facebook";s:32:"setting-link_link_themify-link-1";s:0:"";s:31:"setting-link_img_themify-link-1";s:100:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/social/facebook.png";s:32:"setting-link_type_themify-link-2";s:10:"image-icon";s:33:"setting-link_title_themify-link-2";s:7:"Google+";s:32:"setting-link_link_themify-link-2";s:0:"";s:31:"setting-link_img_themify-link-2";s:103:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/social/google-plus.png";s:32:"setting-link_type_themify-link-3";s:10:"image-icon";s:33:"setting-link_title_themify-link-3";s:7:"YouTube";s:32:"setting-link_link_themify-link-3";s:0:"";s:31:"setting-link_img_themify-link-3";s:99:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/social/youtube.png";s:32:"setting-link_type_themify-link-4";s:10:"image-icon";s:33:"setting-link_title_themify-link-4";s:9:"Pinterest";s:32:"setting-link_link_themify-link-4";s:0:"";s:31:"setting-link_img_themify-link-4";s:101:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/social/pinterest.png";s:32:"setting-link_type_themify-link-5";s:9:"font-icon";s:33:"setting-link_title_themify-link-5";s:7:"Twitter";s:32:"setting-link_link_themify-link-5";s:27:"https://twitter.com/themify";s:33:"setting-link_ficon_themify-link-5";s:10:"fa-twitter";s:35:"setting-link_ficolor_themify-link-5";s:0:"";s:37:"setting-link_fibgcolor_themify-link-5";s:0:"";s:32:"setting-link_type_themify-link-6";s:9:"font-icon";s:33:"setting-link_title_themify-link-6";s:8:"Facebook";s:32:"setting-link_link_themify-link-6";s:32:"https://www.facebook.com/themify";s:33:"setting-link_ficon_themify-link-6";s:11:"fa-facebook";s:35:"setting-link_ficolor_themify-link-6";s:0:"";s:37:"setting-link_fibgcolor_themify-link-6";s:0:"";s:32:"setting-link_type_themify-link-7";s:9:"font-icon";s:33:"setting-link_title_themify-link-7";s:7:"Google+";s:32:"setting-link_link_themify-link-7";s:0:"";s:33:"setting-link_ficon_themify-link-7";s:14:"fa-google-plus";s:35:"setting-link_ficolor_themify-link-7";s:0:"";s:37:"setting-link_fibgcolor_themify-link-7";s:0:"";s:32:"setting-link_type_themify-link-8";s:9:"font-icon";s:33:"setting-link_title_themify-link-8";s:7:"YouTube";s:32:"setting-link_link_themify-link-8";s:37:"http://www.youtube.com/user/themifyme";s:33:"setting-link_ficon_themify-link-8";s:10:"fa-youtube";s:35:"setting-link_ficolor_themify-link-8";s:0:"";s:37:"setting-link_fibgcolor_themify-link-8";s:0:"";s:32:"setting-link_type_themify-link-9";s:9:"font-icon";s:33:"setting-link_title_themify-link-9";s:9:"Pinterest";s:32:"setting-link_link_themify-link-9";s:0:"";s:33:"setting-link_ficon_themify-link-9";s:12:"fa-pinterest";s:35:"setting-link_ficolor_themify-link-9";s:0:"";s:37:"setting-link_fibgcolor_themify-link-9";s:0:"";s:22:"setting-link_field_ids";s:341:"{"themify-link-0":"themify-link-0","themify-link-1":"themify-link-1","themify-link-2":"themify-link-2","themify-link-3":"themify-link-3","themify-link-4":"themify-link-4","themify-link-5":"themify-link-5","themify-link-6":"themify-link-6","themify-link-7":"themify-link-7","themify-link-8":"themify-link-8","themify-link-9":"themify-link-9"}";s:23:"setting-link_field_hash";s:2:"10";s:30:"setting-page_builder_is_active";s:6:"enable";s:41:"setting-page_builder_animation_appearance";s:0:"";s:42:"setting-page_builder_animation_parallax_bg";s:0:"";s:46:"setting-page_builder_animation_parallax_scroll";s:6:"mobile";s:55:"setting-page_builder_responsive_design_tablet_landscape";s:4:"1024";s:45:"setting-page_builder_responsive_design_tablet";s:3:"768";s:45:"setting-page_builder_responsive_design_mobile";s:3:"680";s:23:"setting-hooks_field_ids";s:2:"[]";s:27:"setting-custom_panel-editor";s:7:"default";s:27:"setting-custom_panel-author";s:7:"default";s:32:"setting-custom_panel-contributor";s:7:"default";s:33:"setting-custom_panel-shop_manager";s:7:"default";s:25:"setting-customizer-editor";s:7:"default";s:25:"setting-customizer-author";s:7:"default";s:30:"setting-customizer-contributor";s:7:"default";s:31:"setting-customizer-shop_manager";s:7:"default";s:22:"setting-backend-editor";s:7:"default";s:22:"setting-backend-author";s:7:"default";s:27:"setting-backend-contributor";s:7:"default";s:28:"setting-backend-shop_manager";s:7:"default";s:23:"setting-frontend-editor";s:7:"default";s:23:"setting-frontend-author";s:7:"default";s:28:"setting-frontend-contributor";s:7:"default";s:29:"setting-frontend-shop_manager";s:7:"default";s:4:"skin";s:93:"https://themify.me/demo/themes/split/wp-content/themes/themify-split/themify/img/non-skin.gif";s:27:"setting-search_exclude_post";s:0:"";s:31:"setting-search_settings_exclude";s:0:"";s:30:"setting-search_exclude_product";s:0:"";s:32:"setting-search_exclude_portfolio";s:0:"";s:23:"setting-exclude_img_rss";s:0:"";s:20:"setting-excerpt_more";s:0:"";s:27:"setting-auto_featured_image";s:0:"";s:22:"setting-comments_posts";s:0:"";s:23:"setting-post_author_box";s:0:"";s:24:"setting-post_nav_disable";s:0:"";s:25:"setting-post_nav_same_cat";s:0:"";s:22:"setting-comments_pages";s:0:"";s:29:"setting-portfolio_nav_disable";s:0:"";s:30:"setting-portfolio_nav_same_cat";s:0:"";s:33:"setting-disable_responsive_design";s:0:"";s:31:"setting-lightbox_content_images";s:0:"";s:25:"setting-exclude_site_logo";s:0:"";s:28:"setting-exclude_site_tagline";s:0:"";s:27:"setting-exclude_search_form";s:0:"";s:19:"setting-exclude_rss";s:0:"";s:29:"setting-exclude_social_widget";s:0:"";s:31:"setting-exclude_menu_navigation";s:0:"";s:28:"setting-page_loader_disabled";s:0:"";s:20:"setting-autoinfinite";s:0:"";s:29:"setting-footer_text_left_hide";s:0:"";s:30:"setting-footer_text_right_hide";s:0:"";s:38:"setting-page_builder_disable_shortcuts";s:0:"";s:34:"setting-page_builder_exc_accordion";s:0:"";s:28:"setting-page_builder_exc_box";s:0:"";s:32:"setting-page_builder_exc_buttons";s:0:"";s:32:"setting-page_builder_exc_callout";s:0:"";s:32:"setting-page_builder_exc_divider";s:0:"";s:38:"setting-page_builder_exc_fancy-heading";s:0:"";s:32:"setting-page_builder_exc_feature";s:0:"";s:32:"setting-page_builder_exc_gallery";s:0:"";s:34:"setting-page_builder_exc_highlight";s:0:"";s:29:"setting-page_builder_exc_icon";s:0:"";s:30:"setting-page_builder_exc_image";s:0:"";s:36:"setting-page_builder_exc_layout-part";s:0:"";s:28:"setting-page_builder_exc_map";s:0:"";s:29:"setting-page_builder_exc_menu";s:0:"";s:35:"setting-page_builder_exc_plain-text";s:0:"";s:34:"setting-page_builder_exc_portfolio";s:0:"";s:29:"setting-page_builder_exc_post";s:0:"";s:37:"setting-page_builder_exc_service-menu";s:0:"";s:31:"setting-page_builder_exc_slider";s:0:"";s:28:"setting-page_builder_exc_tab";s:0:"";s:43:"setting-page_builder_exc_testimonial-slider";s:0:"";s:36:"setting-page_builder_exc_testimonial";s:0:"";s:29:"setting-page_builder_exc_text";s:0:"";s:30:"setting-page_builder_exc_video";s:0:"";s:31:"setting-page_builder_exc_widget";s:0:"";s:35:"setting-page_builder_exc_widgetized";s:0:"";s:16:"setting-fontello";s:0:"";}<?php $themify_data = unserialize( ob_get_clean() );

	// fix the weird way "skin" is saved
	if( isset( $themify_data['skin'] ) ) {
		$parsed_skin = parse_url( $themify_data['skin'], PHP_URL_PATH );
		$basedir_skin = basename( dirname( $parsed_skin ) );
		$themify_data['skin'] = trailingslashit( get_template_directory_uri() ) . 'skins/' . $basedir_skin . '/style.css';
	}

	themify_set_data( $themify_data );
	
}
themify_do_demo_import();