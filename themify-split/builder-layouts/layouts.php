<?php

$base_url = get_template_directory_uri() . '/builder-layouts/';

return array(
	array(
		"title" => "Agency",
		"data"  => "agency.zip",
		"thumb" => $base_url . "screenshot-agency.jpg"
	),
	array(
		"title" => "Personal",
		"data"  => "personal.zip",
		"thumb" => $base_url . "screenshot-personal.jpg"
	),
	array(
		"title" => "Portfolio",
		"data"  => "portfolio.zip",
		"thumb" => $base_url . "screenshot-portfolio.jpg"
	),
	array(
		"title" => "Product",
		"data"  => "product.zip",
		"thumb" => $base_url . "screenshot-product.jpg"
	),
	array(
		"title" => "Restaurant",
		"data"  => "restaurant.zip",
		"thumb" => $base_url . "screenshot-restaurant.jpg"
	),
	array(
		"title" => "Shop",
		"data"  => "shop.zip",
		"thumb" => $base_url . "screenshot-shop.jpg"
	),
	array(
		"title" => "Super Heroes",
		"data"  => "super-heroes.zip",
		"thumb" => $base_url . "screenshot-super-heroes.jpg"
	),
	array(
		"title" => "Wedding",
		"data"  => "wedding.zip",
		"thumb" => $base_url . "screenshot-wedding.jpg"
	),
);