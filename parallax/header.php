<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php
/** Themify Default Variables
 *  @var object */
global $themify; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">

<!-- wp_header -->
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php themify_body_start(); // hook ?>
<div id="pagewrap" class="hfeed site">

	<?php if( 'yes' != $themify->hide_header ) : ?>

		<div id="headerwrap" <?php themify_theme_header_background() ?> >
			
			<?php 
				$body_classes = get_body_class();
				$header = themify_theme_get_header_design();
			 
				if(themify_get_mobile_menu_style() == 'slide-menu' ) {
					echo '<a href="#mobile-menu" id="menu-icon" class="mobile-button"></a>';
				}
				else if (in_array( $header, array( 'header-leftpane', 'header-rightpane', 'header-minbar', 'header-slide-out', 'header-overlay', 'none' ) ) ) { 
					echo '<a href="#mobile-menu" id="menu-icon" class="mobile-button"></a>';
				}
				else if (themify_get_mobile_menu_style() == 'dropdown-menu') {
					echo '<a href="#dropdown-menu" id="menu-icon-dropdown" class="mobile-button"><span>Menu</span></a>';
				}
				else {
					echo '<a href="#mobile-menu" id="menu-icon" class="mobile-button"></a>';
				}				
			?>

			<?php   
				$show_mobile_menu = themify_theme_do_not_exclude_all( 'mobile-menu' );
				$show_menu_navigation = $show_mobile_menu && themify_theme_show_area( 'menu_navigation' );
			?>

			<?php themify_header_before(); // hook ?>
			<header id="header" class="pagewidth clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
				<?php themify_header_start(); // hook ?>
				<div class="hgroup">
				
					<div class="header-bar">
						<?php if ( themify_theme_show_area( 'site_logo' ) ) : ?>
							<?php echo themify_logo_image('site_logo'); ?>
						<?php endif; ?>

						<?php if ( themify_theme_show_area( 'site_tagline' ) ) : ?>
							<?php if ( $site_desc = get_bloginfo( 'description' ) ) : ?>
								<?php global $themify_customizer; ?>
								<div id="site-description" class="site-description"><?php echo class_exists( 'Themify_Customizer' ) ? $themify_customizer->site_description( $site_desc ) : $site_desc; ?></div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<!-- /.header-bar -->
					
					<div id="mobile-menu" class="sidemenu sidemenu-off" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
						<div class="navbar-wrapper clearfix">

							<?php if ( themify_theme_show_area( 'social_widget' ) || themify_theme_show_area( 'rss' ) ) : ?>
								<div class="social-widget">
									<?php if ( themify_theme_show_area( 'social_widget' ) ) : ?>
										<?php dynamic_sidebar('social-widget'); ?>
									<?php endif; // exclude social widget ?>

									<?php if ( themify_theme_show_area( 'rss' ) ) : ?>
										<?php if(!themify_check('setting-exclude_rss')): ?>
											<div class="rss">
												<a href="<?php if(themify_get('setting-custom_feed_url') != ""){ echo themify_get('setting-custom_feed_url'); } else { bloginfo('rss2_url'); } ?>"></a>
											</div>
										<?php endif; ?>
									<?php endif; // exclude RSS ?>
								</div>
								<!-- /.social-widget -->
							<?php endif; // exclude social widget or RSS icon ?>

							<?php if ( themify_theme_show_area( 'search_form' ) ) : ?>
								<div id="searchform-wrap">
									<?php if(!themify_check('setting-exclude_search_form')): ?>
										<?php get_search_form(); ?>
									<?php endif ?>
								</div>
								<!-- /#searchform-wrap -->
							<?php endif; // exclude search form ?>

							<?php if ( $show_menu_navigation ) : ?>
								<nav id="main-nav-wrap" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
									<?php themify_theme_menu_nav(); ?>
								</nav>
								<!-- /#main-nav-wrap -->
							<?php endif; // exclude menu navigation ?>

						</div>
						<a id="menu-icon-close" href="#"></a>
					</div>
					<!-- /#mobile-menu -->

					<?php
						// If there's a header background slider, show it.
						global $themify_bg_gallery;
						$themify_bg_gallery->create_controller();
					?>
					<!-- /#gallery-controller -->
					
				</div>

				<?php themify_header_end(); // hook ?>
			</header>
			<!-- /#header -->
			<?php themify_header_after(); // hook ?>

		</div>
		<!-- /#headerwrap -->
		
	<?php endif; // hide_header check ?>

	<div id="body" class="clearfix">
    <?php themify_layout_before(); //hook ?>
