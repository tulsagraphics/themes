<?php

defined( 'ABSPATH' ) or die;

$GLOBALS['processed_terms'] = array();
$GLOBALS['processed_posts'] = array();

require_once ABSPATH . 'wp-admin/includes/post.php';
require_once ABSPATH . 'wp-admin/includes/taxonomy.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

function themify_import_post( $post ) {
	global $processed_posts, $processed_terms;

	if ( ! post_type_exists( $post['post_type'] ) ) {
		return;
	}

	/* Menu items don't have reliable post_title, skip the post_exists check */
	if( $post['post_type'] !== 'nav_menu_item' ) {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
		if ( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
			$processed_posts[ intval( $post['ID'] ) ] = intval( $post_exists );
			return;
		}
	}

	if( $post['post_type'] == 'nav_menu_item' ) {
		if( ! isset( $post['tax_input']['nav_menu'] ) || ! term_exists( $post['tax_input']['nav_menu'], 'nav_menu' ) ) {
			return;
		}
		$_menu_item_type = $post['meta_input']['_menu_item_type'];
		$_menu_item_object_id = $post['meta_input']['_menu_item_object_id'];

		if ( 'taxonomy' == $_menu_item_type && isset( $processed_terms[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_terms[ intval( $_menu_item_object_id ) ];
		} else if ( 'post_type' == $_menu_item_type && isset( $processed_posts[ intval( $_menu_item_object_id ) ] ) ) {
			$post['meta_input']['_menu_item_object_id'] = $processed_posts[ intval( $_menu_item_object_id ) ];
		} else if ( 'custom' != $_menu_item_type ) {
			// associated object is missing or not imported yet, we'll retry later
			// $missing_menu_items[] = $item;
			return;
		}
	}

	$post_parent = ( $post['post_type'] == 'nav_menu_item' ) ? $post['meta_input']['_menu_item_menu_item_parent'] : (int) $post['post_parent'];
	$post['post_parent'] = 0;
	if ( $post_parent ) {
		// if we already know the parent, map it to the new local ID
		if ( isset( $processed_posts[ $post_parent ] ) ) {
			if( $post['post_type'] == 'nav_menu_item' ) {
				$post['meta_input']['_menu_item_menu_item_parent'] = $processed_posts[ $post_parent ];
			} else {
				$post['post_parent'] = $processed_posts[ $post_parent ];
			}
		}
	}

	/**
	 * for hierarchical taxonomies, IDs must be used so wp_set_post_terms can function properly
	 * convert term slugs to IDs for hierarchical taxonomies
	 */
	if( ! empty( $post['tax_input'] ) ) {
		foreach( $post['tax_input'] as $tax => $terms ) {
			if( is_taxonomy_hierarchical( $tax ) ) {
				$terms = explode( ', ', $terms );
				$post['tax_input'][ $tax ] = array_map( 'themify_get_term_id_by_slug', $terms, array_fill( 0, count( $terms ), $tax ) );
			}
		}
	}

	$post['post_author'] = (int) get_current_user_id();
	$post['post_status'] = 'publish';

	$old_id = $post['ID'];

	unset( $post['ID'] );
	$post_id = wp_insert_post( $post, true );
	if( is_wp_error( $post_id ) ) {
		return false;
	} else {
		$processed_posts[ $old_id ] = $post_id;

		if( isset( $post['has_thumbnail'] ) && $post['has_thumbnail'] ) {
			$placeholder = themify_get_placeholder_image();
			if( ! is_wp_error( $placeholder ) ) {
				set_post_thumbnail( $post_id, $placeholder );
			}
		}

		return $post_id;
	}
}

function themify_get_placeholder_image() {
	static $placeholder_image = null;

	if( $placeholder_image == null ) {
		if ( ! function_exists( 'WP_Filesystem' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		WP_Filesystem();
		global $wp_filesystem;
		$upload = wp_upload_bits( $post['post_name'] . '.jpg', null, $wp_filesystem->get_contents( THEMIFY_DIR . '/img/image-placeholder.jpg' ) );

		if ( $info = wp_check_filetype( $upload['file'] ) )
			$post['post_mime_type'] = $info['type'];
		else
			return new WP_Error( 'attachment_processing_error', __( 'Invalid file type', 'themify' ) );

		$post['guid'] = $upload['url'];
		$post_id = wp_insert_attachment( $post, $upload['file'] );
		wp_update_attachment_metadata( $post_id, wp_generate_attachment_metadata( $post_id, $upload['file'] ) );

		$placeholder_image = $post_id;
	}

	return $placeholder_image;
}

function themify_import_term( $term ) {
	global $processed_terms;

	if( $term_id = term_exists( $term['slug'], $term['taxonomy'] ) ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term['term_id'] ) )
			$processed_terms[ intval( $term['term_id'] ) ] = (int) $term_id;
		return (int) $term_id;
	}

	if ( empty( $term['parent'] ) ) {
		$parent = 0;
	} else {
		$parent = term_exists( $term['parent'], $term['taxonomy'] );
		if ( is_array( $parent ) ) $parent = $parent['term_id'];
	}

	$id = wp_insert_term( $term['name'], $term['taxonomy'], array(
		'parent' => $parent,
		'slug' => $term['slug'],
		'description' => $term['description'],
	) );
	if ( ! is_wp_error( $id ) ) {
		if ( isset( $term['term_id'] ) ) {
			$processed_terms[ intval($term['term_id']) ] = $id['term_id'];
			return $term['term_id'];
		}
	}

	return false;
}

function themify_get_term_id_by_slug( $slug, $tax ) {
	$term = get_term_by( 'slug', $slug, $tax );
	if( $term ) {
		return $term->term_id;
	}

	return false;
}

function themify_undo_import_term( $term ) {
	$term_id = term_exists( $term['slug'], $term['term_taxonomy'] );
	if ( $term_id ) {
		if ( is_array( $term_id ) ) $term_id = $term_id['term_id'];
		if ( isset( $term_id ) ) {
			wp_delete_term( $term_id, $term['term_taxonomy'] );
		}
	}
}

/**
 * Determine if a post exists based on title, content, and date
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param array $args array of database parameters to check
 * @return int Post ID if post exists, 0 otherwise.
 */
function themify_post_exists( $args = array() ) {
	global $wpdb;

	$query = "SELECT ID FROM $wpdb->posts WHERE 1=1";
	$db_args = array();

	foreach ( $args as $key => $value ) {
		$value = wp_unslash( sanitize_post_field( $key, $value, 0, 'db' ) );
		if( ! empty( $value ) ) {
			$query .= ' AND ' . $key . ' = %s';
			$db_args[] = $value;
		}
	}

	if ( !empty ( $args ) )
		return (int) $wpdb->get_var( $wpdb->prepare($query, $args) );

	return 0;
}

function themify_undo_import_post( $post ) {
	if( $post['post_type'] == 'nav_menu_item' ) {
		$post_exists = themify_post_exists( array(
			'post_name' => $post['post_name'],
			'post_modified' => $post['post_date'],
			'post_type' => 'nav_menu_item',
		) );
	} else {
		$post_exists = post_exists( $post['post_title'], '', $post['post_date'] );
	}
	if( $post_exists && get_post_type( $post_exists ) == $post['post_type'] ) {
		/**
		 * check if the post has been modified, if so leave it be
		 *
		 * NOTE: posts are imported using wp_insert_post() which modifies post_modified field
		 * to be the same as post_date, hence to check if the post has been modified,
		 * the post_modified field is compared against post_date in the original post.
		 */
		if( $post['post_date'] == get_post_field( 'post_modified', $post_exists ) ) {
			wp_delete_post( $post_exists, true ); // true: bypass trash
		}
	}
}

function themify_do_demo_import() {
$term = array (
  'term_id' => 13,
  'name' => 'Blog',
  'slug' => 'blog',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 15,
  'name' => 'News',
  'slug' => 'news',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 16,
  'name' => 'Sports',
  'slug' => 'sports',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 15,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 19,
  'name' => 'Video',
  'slug' => 'video',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 20,
  'name' => 'World',
  'slug' => 'world',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 15,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 23,
  'name' => 'Lifestyle',
  'slug' => 'lifestyle',
  'term_group' => 0,
  'taxonomy' => 'category',
  'description' => '',
  'parent' => 15,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 43,
  'name' => 'Fashion',
  'slug' => 'fashion',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 44,
  'name' => 'Shoes',
  'slug' => 'shoes',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 45,
  'name' => 'Pants',
  'slug' => 'pants',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 46,
  'name' => 'Accessories',
  'slug' => 'accessories',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 47,
  'name' => 'Technology',
  'slug' => 'technology',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 49,
  'name' => 'Toys',
  'slug' => 'toys',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 50,
  'name' => 'Games',
  'slug' => 'games',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 52,
  'name' => 'Featured',
  'slug' => 'featured',
  'term_group' => 0,
  'taxonomy' => 'product_cat',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 48,
  'name' => 'top',
  'slug' => 'top',
  'term_group' => 0,
  'taxonomy' => 'product_tag',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 53,
  'name' => 'Page Slider',
  'slug' => 'page-slider',
  'term_group' => 0,
  'taxonomy' => 'slider-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 56,
  'name' => 'Home Slider',
  'slug' => 'home-slider',
  'term_group' => 0,
  'taxonomy' => 'slider-category',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 42,
  'name' => 'Main Nav',
  'slug' => 'main-nav',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$term = array (
  'term_id' => 54,
  'name' => 'Horizontal Nav',
  'slug' => 'horizontal-nav',
  'term_group' => 0,
  'taxonomy' => 'nav_menu',
  'description' => '',
  'parent' => 0,
);
if( ERASEDEMO ) {
	themify_undo_import_term( $term );
} else {
	themify_import_term( $term );
}

$post = array (
  'ID' => 1893,
  'post_date' => '2008-06-26 21:19:12',
  'post_date_gmt' => '2008-06-26 21:19:12',
  'post_content' => 'Aliquam blandit, velit elementum bibendum dictum, est leo volutpat quam, id pellentesque nisl arcu quis purus. Pellentesque luctus lacus lorem, id ullamcorper dolor vestibulum id.',
  'post_title' => 'Views of the Burj Khalifa',
  'post_excerpt' => '',
  'post_name' => 'burj-khalifa',
  'post_modified' => '2017-08-21 07:05:36',
  'post_modified_gmt' => '2017-08-21 07:05:36',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1893',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'video_url' => 'http://www.youtube.com/watch?v=cn7AFhVEI5o',
  ),
  'tax_input' => 
  array (
    'category' => 'video',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1822,
  'post_date' => '2008-06-26 23:38:36',
  'post_date_gmt' => '2008-06-26 23:38:36',
  'post_content' => 'Donec auctor consectetur tellus, in hendrerit urna vulputate non. Ut elementum fringilla purus. Nam dui erat, porta eu gravida sit amet, ornare sit amet sem.',
  'post_title' => 'Dirt Championship',
  'post_excerpt' => '',
  'post_name' => 'dirt-championship',
  'post_modified' => '2017-08-21 07:05:35',
  'post_modified_gmt' => '2017-08-21 07:05:35',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1822',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'sports',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1845,
  'post_date' => '2008-06-26 01:36:35',
  'post_date_gmt' => '2008-06-26 01:36:35',
  'post_content' => 'Donec hendrerit, lectus in dapibus consequat, libero arcu dignissim turpis, id dictum odio felis eget ante. In ullamcorper pulvinar rutrum. In id neque pulvinar, tempor orci ac, tincidunt libero. Fusce ultricies arcu at mauris semper bibendum.',
  'post_title' => 'Cooking Courses',
  'post_excerpt' => '',
  'post_name' => 'cooking-courses',
  'post_modified' => '2017-08-21 07:05:50',
  'post_modified_gmt' => '2017-08-21 07:05:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1845',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'world',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1849,
  'post_date' => '2008-06-26 01:38:43',
  'post_date_gmt' => '2008-06-26 01:38:43',
  'post_content' => 'Phasellus dui erat, tincidunt pulvinar tempor at, lacinia eu lacus. Aenean euismod tellus laoreet turpis viverra facilisis. Nunc eu viverra eros, et facilisis dui. Sed pretium id risus eu tincidunt.',
  'post_title' => 'Maritime Shipping',
  'post_excerpt' => '',
  'post_name' => 'maritime-shipping',
  'post_modified' => '2017-08-21 07:05:47',
  'post_modified_gmt' => '2017-08-21 07:05:47',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1849',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'world',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1852,
  'post_date' => '2008-06-26 01:42:25',
  'post_date_gmt' => '2008-06-26 01:42:25',
  'post_content' => 'In lobortis vehicula lectus, et venenatis velit euismod sit amet. Morbi egestas malesuada turpis, dictum consequat mauris scelerisque ac. Mauris luctus commodo lorem, pulvinar sollicitudin ante porttitor id.',
  'post_title' => 'Water Town',
  'post_excerpt' => '',
  'post_name' => 'water-town',
  'post_modified' => '2017-08-21 07:05:45',
  'post_modified_gmt' => '2017-08-21 07:05:45',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1852',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'world',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1857,
  'post_date' => '2008-06-26 02:46:21',
  'post_date_gmt' => '2008-06-26 02:46:21',
  'post_content' => 'Nullam fringilla facilisis ultricies. Ut volutpat ultricies rutrum. In laoreet, nunc et auctor condimentum, enim lacus lacinia dolor, non accumsan leo nisl id lorem. Duis vehicula et turpis fringilla hendrerit.',
  'post_title' => 'Remote Places',
  'post_excerpt' => '',
  'post_name' => 'remote-places',
  'post_modified' => '2017-08-21 07:05:44',
  'post_modified_gmt' => '2017-08-21 07:05:44',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1857',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'lifestyle',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1860,
  'post_date' => '2008-06-26 02:47:20',
  'post_date_gmt' => '2008-06-26 02:47:20',
  'post_content' => 'Duis eget tellus nisl. Donec porta orci vel iaculis porta. Vivamus aliquet, ligula et tempus mattis, tortor ipsum eleifend massa, ac gravida dui est quis dui.',
  'post_title' => 'Evening Rides',
  'post_excerpt' => '',
  'post_name' => 'evening-rides',
  'post_modified' => '2017-08-21 07:05:42',
  'post_modified_gmt' => '2017-08-21 07:05:42',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1860',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'lifestyle',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1863,
  'post_date' => '2008-06-26 02:48:34',
  'post_date_gmt' => '2008-06-26 02:48:34',
  'post_content' => 'Proin vitae lectus eu turpis sollicitudin sagittis. Aliquam nunc odio, semper lacinia tincidunt a, dapibus vitae dolor. Class aptent taciti sociosqu ad litora torquent per conubia.',
  'post_title' => 'Learn Something New',
  'post_excerpt' => '',
  'post_name' => 'learn-something-new',
  'post_modified' => '2017-08-21 07:05:40',
  'post_modified_gmt' => '2017-08-21 07:05:40',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1863',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'lifestyle',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 1865,
  'post_date' => '2008-06-26 02:49:39',
  'post_date_gmt' => '2008-06-26 02:49:39',
  'post_content' => 'Vivamus pharetra magna fermentum tincidunt imperdiet. Aenean venenatis sollicitudin odio in ultrices. Proin a nibh at dolor rhoncus pulvinar. Nullam eget tincidunt enim.',
  'post_title' => 'Clean Air',
  'post_excerpt' => '',
  'post_name' => 'clean-air',
  'post_modified' => '2017-08-21 07:05:38',
  'post_modified_gmt' => '2017-08-21 07:05:38',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/builder/?p=1865',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
  ),
  'tax_input' => 
  array (
    'category' => 'lifestyle',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2362,
  'post_date' => '2013-10-10 22:33:12',
  'post_date_gmt' => '2013-10-10 22:33:12',
  'post_content' => 'Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere. Curabitur at arcu id turpis posuere bibendum. Sed commodo mauris eget diam pretium cursus. In sagittis feugiat mauris, in ultrices mauris lacinia eu. Fusce augue velit, vulputate elementum semper congue, rhoncus adipiscing nisl.<!--more-->

Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.',
  'post_title' => 'New Red Hand Bag',
  'post_excerpt' => '',
  'post_name' => 'new-red-hand-bag',
  'post_modified' => '2017-08-21 07:05:33',
  'post_modified_gmt' => '2017-08-21 07:05:33',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2362',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/red-handbag1.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2369,
  'post_date' => '2013-10-11 05:30:39',
  'post_date_gmt' => '2013-10-11 05:30:39',
  'post_content' => 'Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere. Curabitur at arcu id turpis posuere bibendum. Sed commodo mauris eget diam pretium cursus. In sagittis feugiat mauris, in ultrices mauris lacinia eu. Fusce augue velit, vulputate elementum semper congue, rhoncus adipiscing nisl. Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.

<!--more-->
Nullam rutrum quam ut massa ultricies sed blandit sapien fermentum. Curabitur venenatis vehicula mattis. Nunc eleifend consectetur odio sit amet viverra. Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi. Aliquam erat nulla, sodales at imperdiet vitae, convallis vel dui. Sed ultrices felis ut justo suscipit vestibulum. Pellentesque nisl nisi, vehicula vitae hendrerit vel, mattis eget mauris. Donec consequat eros eget lectus dictum sit amet ultrices neque sodales. Aliquam metus diam, mattis fringilla adipiscing at, lacinia at nulla.',
  'post_title' => 'Smart LED TV',
  'post_excerpt' => '',
  'post_name' => 'smart-led-tv',
  'post_modified' => '2017-08-21 07:05:32',
  'post_modified_gmt' => '2017-08-21 07:05:32',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2369',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ledtv.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2466,
  'post_date' => '2013-10-15 21:46:04',
  'post_date_gmt' => '2013-10-15 21:46:04',
  'post_content' => 'Adipiscing ornare eros. Nunc eu libero at libero feugiat hendrerit. Aenean aliquam neque sed vulputate pharetra. Vivamus volutpat dui justo. Aliquam erat volutpat. Nulla facilisi. Nullam rutrum quam ut massa ultricies sed blandit sapien fermentum. Curabitur venenatis vehicula mattis. Nunc eleifend consectetur odio sit amet viverra. Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi. Aliquam erat nulla, sodales at imperdiet vitae, convallis vel dui. Sed ultrices felis ut justo suscipit vestibulum. Pellentesque nisl nisi, vehicula vitae hendrerit vel, mattis eget mauris.

<!--more-->

Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi. Aliquam erat nulla, sodales at imperdiet vitae, convallis vel dui. Sed ultrices felis ut justo suscipit vestibulum. Pellentesque nisl nisi, vehicula vitae hendrerit vel, mattis eget mauris.

Nullam rutrum quam ut massa ultricies sed blandit sapien fermentum. Curabitur venenatis vehicula mattis. Nunc eleifend consectetur odio sit amet viverra. Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi. Aliquam erat nulla, sodales at imperdiet vitae, convallis vel dui. Sed ultrices felis ut justo suscipit vestibulum. Pellentesque nisl nisi, vehicula vitae hendrerit vel, mattis eget mauris. Donec consequat eros eget lectus dictum sit amet ultrices neque sodales. Aliquam metus diam, mattis fringilla adipiscing at, lacinia at nulla.',
  'post_title' => 'Parallax Scrolling',
  'post_excerpt' => '',
  'post_name' => 'parallax-scrolling',
  'post_modified' => '2017-08-21 07:05:15',
  'post_modified_gmt' => '2017-08-21 07:05:15',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2466',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/phone_tablets2.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2459,
  'post_date' => '2013-10-15 21:24:44',
  'post_date_gmt' => '2013-10-15 21:24:44',
  'post_content' => 'Proin eu enim sit amet enim luctus condimentum non non turpis. Aliquam dictum, orci eget congue eleifend, lorem ante molestie risus, ac adipiscing nibh erat in libero',
  'post_title' => 'Bring The Bear!',
  'post_excerpt' => '',
  'post_name' => 'bring-bear',
  'post_modified' => '2017-08-21 07:05:18',
  'post_modified_gmt' => '2017-08-21 07:05:18',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2459',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/teddybear.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2462,
  'post_date' => '2013-10-15 21:31:50',
  'post_date_gmt' => '2013-10-15 21:31:50',
  'post_content' => 'Nunc et diam lobortis, volutpat purus nec, pretium nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
  'post_title' => 'Build More!',
  'post_excerpt' => '',
  'post_name' => 'build-more',
  'post_modified' => '2017-08-21 07:05:17',
  'post_modified_gmt' => '2017-08-21 07:05:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2462',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/lego.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2414,
  'post_date' => '2013-10-14 19:37:23',
  'post_date_gmt' => '2013-10-14 19:37:23',
  'post_content' => 'Cras molestie sapien sed nunc egestas aliquet. Proin a ullamcorper erat. Suspendisse dictum tortor non elit aliquam, ut consectetur quam dictum. Nullam placerat justo ac erat dictum pulvinar. Ut ac suscipit eros. Integer vel condimentum metus, mattis feugiat sem.',
  'post_title' => 'A New Bundle',
  'post_excerpt' => '',
  'post_name' => 'new-bundle',
  'post_modified' => '2017-08-21 07:05:29',
  'post_modified_gmt' => '2017-08-21 07:05:29',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2414',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/game_console_bundle.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2418,
  'post_date' => '2013-10-14 20:00:21',
  'post_date_gmt' => '2013-10-14 20:00:21',
  'post_content' => 'Praesent auctor placerat augue, quis congue diam lobortis sed. Mauris tincidunt ut enim sit amet dictum. Vestibulum venenatis dignissim neque, sed dignissim mi ullamcorper ut. Quisque dignissim dignissim tellus at tincidunt.',
  'post_title' => 'Latest Leak',
  'post_excerpt' => '',
  'post_name' => 'latest-leak',
  'post_modified' => '2017-08-21 07:05:27',
  'post_modified_gmt' => '2017-08-21 07:05:27',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2418',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ps.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'lightbox_link' => 'https://themify.me/demo/themes/flatshop/files/2013/10/new_tablet.png',
    'lightbox_icon' => 'on',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2423,
  'post_date' => '2013-10-14 20:21:05',
  'post_date_gmt' => '2013-10-14 20:21:05',
  'post_content' => 'Vestibulum consequat ultricies nisi ut tincidunt. Curabitur tincidunt condimentum elementum. Vivamus molestie lorem sed urna egestas, dignissim iaculis lorem rhoncus. Vestibulum iaculis, quam eu consequat convallis, turpis mi condimentum arcu, id cursus nisl elit vel orci.',
  'post_title' => 'The Sneaker',
  'post_excerpt' => '',
  'post_name' => 'sneaker',
  'post_modified' => '2017-08-21 07:05:25',
  'post_modified_gmt' => '2017-08-21 07:05:25',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2423',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/sneaker.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2427,
  'post_date' => '2013-10-14 20:33:14',
  'post_date_gmt' => '2013-10-14 20:33:14',
  'post_content' => 'Vivamus odio urna, aliquam ut blandit id, dapibus eget lacus. Ut nec hendrerit odio, ornare feugiat arcu. Aliquam erat volutpat.',
  'post_title' => 'What\'s Your Favorite?',
  'post_excerpt' => '',
  'post_name' => 'whats-favorite',
  'post_modified' => '2017-08-21 07:05:24',
  'post_modified_gmt' => '2017-08-21 07:05:24',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2427',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/pillows.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2431,
  'post_date' => '2013-10-14 20:54:16',
  'post_date_gmt' => '2013-10-14 20:54:16',
  'post_content' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin sit amet erat nec turpis molestie feugiat ac sed dui. Nunc quis augue nec sem molestie adipiscing. Nullam mollis velit ut sapien sagittis, nec volutpat massa ultrices.',
  'post_title' => 'Hoodies In Stock',
  'post_excerpt' => '',
  'post_name' => 'hoodies-stock',
  'post_modified' => '2017-08-21 07:05:22',
  'post_modified_gmt' => '2017-08-21 07:05:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2431',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/hoodie.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2435,
  'post_date' => '2013-10-14 21:28:55',
  'post_date_gmt' => '2013-10-14 21:28:55',
  'post_content' => 'Phasellus pulvinar semper magna, vel faucibus dui aliquam quis. Donec at dolor id urna auctor laoreet ac at justo. Donec nec ligula vehicula, rhoncus elit eget, malesuada neque. Donec fringilla nibh ac purus faucibus, eu bibendum lacus sodales.',
  'post_title' => 'Mobility',
  'post_excerpt' => '',
  'post_name' => 'mobility',
  'post_modified' => '2017-08-21 07:05:20',
  'post_modified_gmt' => '2017-08-21 07:05:20',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2435',
  'menu_order' => 0,
  'post_type' => 'post',
  'meta_input' => 
  array (
    'layout' => 'default',
    'content_width' => 'default_width',
    'overlap_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/3ds-1.png',
    'hide_post_title' => 'default',
    'unlink_post_title' => 'default',
    'hide_post_date' => 'default',
    'hide_post_image' => 'default',
    'unlink_post_image' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'category' => 'blog',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2102,
  'post_date' => '2013-10-08 02:19:14',
  'post_date_gmt' => '2013-10-08 02:19:14',
  'post_content' => '',
  'post_title' => 'Shop',
  'post_excerpt' => '',
  'post_name' => 'shop',
  'post_modified' => '2013-10-09 18:10:29',
  'post_modified_gmt' => '2013-10-09 18:10:29',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/shop/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'default',
    'hide_page_title' => 'default',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_query_category' => '0',
    'product_layout' => 'list-post',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_posts_per_page' => '5',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2103,
  'post_date' => '2013-10-08 02:19:14',
  'post_date_gmt' => '2013-10-08 02:19:14',
  'post_content' => '[woocommerce_cart]',
  'post_title' => 'Cart',
  'post_excerpt' => '',
  'post_name' => 'cart',
  'post_modified' => '2013-10-08 02:19:14',
  'post_modified_gmt' => '2013-10-08 02:19:14',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/cart/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2104,
  'post_date' => '2013-10-08 02:19:14',
  'post_date_gmt' => '2013-10-08 02:19:14',
  'post_content' => '[woocommerce_checkout]',
  'post_title' => 'Checkout',
  'post_excerpt' => '',
  'post_name' => 'checkout',
  'post_modified' => '2013-10-08 02:19:14',
  'post_modified_gmt' => '2013-10-08 02:19:14',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/checkout/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    '_themify_builder_settings_json' => '{\\"_edit_lock\\":[\\"1424818864:113\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2105,
  'post_date' => '2013-10-08 02:19:14',
  'post_date_gmt' => '2013-10-08 02:19:14',
  'post_content' => '[woocommerce_my_account]',
  'post_title' => 'My Account',
  'post_excerpt' => '',
  'post_name' => 'my-account',
  'post_modified' => '2013-10-08 02:19:14',
  'post_modified_gmt' => '2013-10-08 02:19:14',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/my-account/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2110,
  'post_date' => '2013-10-08 02:19:14',
  'post_date_gmt' => '2013-10-08 02:19:14',
  'post_content' => '',
  'post_title' => 'Logout',
  'post_excerpt' => '',
  'post_name' => 'logout',
  'post_modified' => '2013-10-08 02:19:14',
  'post_modified_gmt' => '2013-10-08 02:19:14',
  'post_content_filtered' => '',
  'post_parent' => 2105,
  'guid' => 'https://themify.me/demo/themes/flatshop/my-account/logout/',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2450,
  'post_date' => '2013-10-14 23:57:38',
  'post_date_gmt' => '2013-10-14 23:57:38',
  'post_content' => '',
  'post_title' => 'Home',
  'post_excerpt' => '',
  'post_name' => 'home',
  'post_modified' => '2017-01-07 03:31:08',
  'post_modified_gmt' => '2017-01-07 03:31:08',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2450',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'content_width' => 'default_width',
    'hide_page_title' => 'yes',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_query_category' => 'featured',
    'product_query_type' => 'all',
    'product_layout' => 'list-post',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_show_sorting_bar' => 'no',
    'product_posts_per_page' => '3',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"gutter\\":\\"gutter-default\\",\\"column_alignment\\":\\"col_align_top\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"grid_width\\":\\"\\",\\"modules\\":[],\\"styling\\":[]}],\\"styling\\":[]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2269,
  'post_date' => '2013-10-08 04:24:45',
  'post_date_gmt' => '2013-10-08 04:24:45',
  'post_content' => '',
  'post_title' => 'Shop - 4 Columns',
  'post_excerpt' => '',
  'post_name' => 'shop-4-columns',
  'post_modified' => '2013-10-11 20:48:49',
  'post_modified_gmt' => '2013-10-11 20:48:49',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2269',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'posts_per_page' => '8',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_query_category' => '0',
    'product_layout' => 'grid4',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_posts_per_page' => '8',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'none',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"_edit_lock\\":[\\"1461008775:115\\"],\\"layout\\":[\\"list-post\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"page_slider\\":[\\"\\"],\\"query_category\\":[\\"\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"posts_per_page\\":[\\"8\\"],\\"display_content\\":[\\"content\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"product_layout\\":[\\"grid4\\"],\\"product_order\\":[\\"desc\\"],\\"product_orderby\\":[\\"date\\"],\\"product_hide_navigation\\":[\\"no\\"],\\"product_archive_show_short\\":[\\"none\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"product_query_category\\":[\\"0\\"],\\"product_posts_per_page\\":[\\"8\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2271,
  'post_date' => '2013-10-08 04:25:29',
  'post_date_gmt' => '2013-10-08 04:25:29',
  'post_content' => '',
  'post_title' => 'Shop - 3 Columns',
  'post_excerpt' => '',
  'post_name' => 'shop-3-columns',
  'post_modified' => '2013-10-11 22:21:47',
  'post_modified_gmt' => '2013-10-11 22:21:47',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2271',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_query_category' => '0',
    'product_layout' => 'grid3',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_posts_per_page' => '6',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'none',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"_edit_lock\\":[\\"1381531743:2\\"],\\"layout\\":[\\"list-post\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"page_slider\\":[\\"\\"],\\"query_category\\":[\\"\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"display_content\\":[\\"content\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"product_layout\\":[\\"grid3\\"],\\"product_order\\":[\\"desc\\"],\\"product_orderby\\":[\\"date\\"],\\"product_hide_navigation\\":[\\"no\\"],\\"product_archive_show_short\\":[\\"none\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"product_query_category\\":[\\"0\\"],\\"product_posts_per_page\\":[\\"6\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2273,
  'post_date' => '2013-10-08 04:26:11',
  'post_date_gmt' => '2013-10-08 04:26:11',
  'post_content' => '',
  'post_title' => 'Shop - 2 Columns',
  'post_excerpt' => '',
  'post_name' => 'shop-2-columns',
  'post_modified' => '2013-10-18 04:09:17',
  'post_modified_gmt' => '2013-10-18 04:09:17',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2273',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_query_category' => '0',
    'product_layout' => 'grid2',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_posts_per_page' => '6',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[]}]}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2367,
  'post_date' => '2013-10-11 05:27:37',
  'post_date_gmt' => '2013-10-11 05:27:37',
  'post_content' => '',
  'post_title' => 'Blog',
  'post_excerpt' => '',
  'post_name' => 'blog',
  'post_modified' => '2013-10-11 05:27:37',
  'post_modified_gmt' => '2013-10-11 05:27:37',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2367',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'default',
    'hide_page_title' => 'default',
    'query_category' => 'blog',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'list-post',
    'posts_per_page' => '4',
    'display_content' => 'content',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_layout' => 'default',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full first last\\",\\"modules\\":[],\\"styling\\":null}],\\"styling\\":null}]',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2375,
  'post_date' => '2013-10-11 06:19:24',
  'post_date_gmt' => '2013-10-11 06:19:24',
  'post_content' => '',
  'post_title' => 'Blog - 4 Columns',
  'post_excerpt' => '',
  'post_name' => 'blog-4-columns',
  'post_modified' => '2013-10-11 06:20:56',
  'post_modified_gmt' => '2013-10-11 06:20:56',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2375',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'query_category' => 'blog',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'grid4',
    'posts_per_page' => '12',
    'display_content' => 'none',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_meta_all' => 'yes',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_layout' => 'default',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"_edit_lock\\":[\\"1381523546:2\\"],\\"layout\\":[\\"grid4\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"query_category\\":[\\"blog\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"posts_per_page\\":[\\"12\\"],\\"display_content\\":[\\"none\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"product_layout\\":[\\"default\\"],\\"product_order\\":[\\"desc\\"],\\"product_orderby\\":[\\"date\\"],\\"product_hide_navigation\\":[\\"no\\"],\\"product_archive_show_short\\":[\\"excerpt\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"hide_meta_all\\":[\\"yes\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2380,
  'post_date' => '2013-10-11 20:33:12',
  'post_date_gmt' => '2013-10-11 20:33:12',
  'post_content' => '',
  'post_title' => 'Blog - 3 Columns',
  'post_excerpt' => '',
  'post_name' => 'blog-3-columns',
  'post_modified' => '2013-10-11 20:33:24',
  'post_modified_gmt' => '2013-10-11 20:33:24',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2380',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'query_category' => 'blog',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'grid3',
    'posts_per_page' => '9',
    'display_content' => 'none',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_meta_all' => 'yes',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_layout' => 'default',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"_edit_lock\\":[\\"1381523520:2\\"],\\"layout\\":[\\"grid3\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"query_category\\":[\\"blog\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"posts_per_page\\":[\\"9\\"],\\"display_content\\":[\\"none\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"product_layout\\":[\\"default\\"],\\"product_order\\":[\\"desc\\"],\\"product_orderby\\":[\\"date\\"],\\"product_hide_navigation\\":[\\"no\\"],\\"product_archive_show_short\\":[\\"excerpt\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"hide_meta_all\\":[\\"yes\\"],\\"_dp_original\\":[\\"2375\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2382,
  'post_date' => '2013-10-11 20:34:08',
  'post_date_gmt' => '2013-10-11 20:34:08',
  'post_content' => '',
  'post_title' => 'Blog - 2 Columns',
  'post_excerpt' => '',
  'post_name' => 'blog-2-columns',
  'post_modified' => '2013-10-11 20:34:08',
  'post_modified_gmt' => '2013-10-11 20:34:08',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?page_id=2382',
  'menu_order' => 0,
  'post_type' => 'page',
  'meta_input' => 
  array (
    'page_layout' => 'sidebar-none',
    'hide_page_title' => 'default',
    'query_category' => 'blog',
    'order' => 'desc',
    'orderby' => 'date',
    'layout' => 'grid2',
    'posts_per_page' => '6',
    'display_content' => 'none',
    'hide_title' => 'default',
    'unlink_title' => 'default',
    'hide_date' => 'default',
    'hide_meta_all' => 'yes',
    'hide_image' => 'default',
    'unlink_image' => 'default',
    'hide_navigation' => 'default',
    'product_layout' => 'default',
    'product_order' => 'desc',
    'product_orderby' => 'date',
    'product_hide_navigation' => 'no',
    'product_archive_show_short' => 'excerpt',
    'builder_switch_frontend' => '0',
    '_themify_builder_settings_json' => '{\\"_edit_last\\":[\\"2\\"],\\"_edit_lock\\":[\\"1381523660:2\\"],\\"layout\\":[\\"grid2\\"],\\"page_layout\\":[\\"sidebar-none\\"],\\"hide_page_title\\":[\\"default\\"],\\"query_category\\":[\\"blog\\"],\\"order\\":[\\"desc\\"],\\"orderby\\":[\\"date\\"],\\"posts_per_page\\":[\\"6\\"],\\"display_content\\":[\\"none\\"],\\"hide_title\\":[\\"default\\"],\\"unlink_title\\":[\\"default\\"],\\"hide_date\\":[\\"default\\"],\\"hide_image\\":[\\"default\\"],\\"unlink_image\\":[\\"default\\"],\\"hide_navigation\\":[\\"default\\"],\\"product_layout\\":[\\"default\\"],\\"product_order\\":[\\"desc\\"],\\"product_orderby\\":[\\"date\\"],\\"product_hide_navigation\\":[\\"no\\"],\\"product_archive_show_short\\":[\\"excerpt\\"],\\"builder_switch_frontend\\":[\\"0\\"],\\"hide_meta_all\\":[\\"yes\\"],\\"_dp_original\\":[\\"2380\\"]}',
  ),
  'tax_input' => 
  array (
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2218,
  'post_date' => '2013-10-07 21:34:14',
  'post_date_gmt' => '2013-10-07 21:34:14',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  'post_title' => 'Cool Shoes',
  'post_excerpt' => 'Intege fermentum condimentum elit ac accumsan. Proin cursus, lectus malesuada pretium condimentum, quam odio malesuada purus, et sodales diam erat vel est.',
  'post_name' => 'cool-shoes',
  'post_modified' => '2017-08-21 07:08:15',
  'post_modified_gmt' => '2017-08-21 07:08:15',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2218',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/shoes_brown.png',
    'title_font_color' => '#000000',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-brown_shoes.jpg',
    '_background_image_attach_id' => '2243',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '18',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_product_image_gallery' => '1997,1995,2424',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
      'size' => 
      array (
        'name' => 'Size',
        'value' => '6 | 7 | 8.5',
        'position' => 0,
        'is_visible' => 0,
        'is_variation' => 1,
        'is_taxonomy' => 0,
      ),
      'color' => 
      array (
        'name' => 'Color',
        'value' => 'brown | black | blue',
        'position' => 1,
        'is_visible' => 0,
        'is_variation' => 1,
        'is_taxonomy' => 0,
      ),
    ),
    '_price' => '70',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'link_font_color' => '#000000',
    'background_color' => '#ebd7af',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#000000',
    'description_font_color' => '#000000',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_edit_last' => '172',
    '_thumbnail_id' => '2244',
    '_edit_lock' => '1503299177:172',
    '_min_variation_price' => '70',
    '_max_variation_price' => '70',
    '_min_variation_regular_price' => '70',
    '_max_variation_regular_price' => '70',
    '_default_attributes' => 
    array (
    ),
    '_min_price_variation_id' => '2543',
    '_max_price_variation_id' => '2543',
    '_min_regular_price_variation_id' => '2543',
    '_max_regular_price_variation_id' => '2543',
    '_post_image_attach_id' => '2244',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_product_version' => '3.1.1',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'variable',
    'product_cat' => 'fashion, featured, shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2223,
  'post_date' => '2013-10-07 21:41:07',
  'post_date_gmt' => '2013-10-07 21:41:07',
  'post_content' => 'Morbi et quam tincidunt, volutpat purus eu, tempus ligula. Praesent tempus commodo nunc eu cursus. In hac habitasse platea dictumst. Sed vitae euismod nisl. In sed leo pulvinar, suscipit tellus non, sagittis eros. Suspendisse semper nibh vitae justo vehicula facilisis.',
  'post_title' => 'Straight Jeans',
  'post_excerpt' => 'Etiam fermentum condimentum elit ac accumsan. Proin cursus, lectus malesuada pretium condimentum, quam odio malesuada purus, et sodales diam erat vel est.',
  'post_name' => 'straight-jeans',
  'post_modified' => '2017-08-21 07:08:13',
  'post_modified_gmt' => '2017-08-21 07:08:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2223',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '2249',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/straight_jeans.png',
    'title_font_color' => '#ffffff',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '14',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '50',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
      'size' => 
      array (
        'name' => 'Size',
        'value' => 'Small | Medum | Large',
        'position' => 0,
        'is_visible' => 0,
        'is_variation' => 1,
        'is_taxonomy' => 0,
      ),
    ),
    '_price' => '50',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'link_font_color' => '#ffffff',
    'background_color' => '#0c2540',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#ffffff',
    'description_font_color' => '#ffffff',
    'button_font_color' => '#000000',
    'button_background_color' => '#ffffff',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_edit_last' => '172',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-straight_jeans.jpg',
    '_background_image_attach_id' => '2248',
    '_edit_lock' => '1503299177:172',
    '_min_variation_price' => '50',
    '_max_variation_price' => '50',
    '_min_variation_regular_price' => '50',
    '_max_variation_regular_price' => '50',
    '_default_attributes' => 
    array (
    ),
    '_post_image_attach_id' => '2249',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'fashion, featured, pants',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2226,
  'post_date' => '2013-10-07 21:01:16',
  'post_date_gmt' => '2013-10-07 21:01:16',
  'post_content' => 'Nulla commodo dolor vel semper tincidunt. Maecenas pellentesque mollis congue. Nulla facilisi. Mauris sed sodales elit. Cras lectus mauris, pretium sit amet est luctus, congue mollis nunc. Curabitur rutrum velit convallis magna vestibulum, euismod imperdiet arcu facilisis.',
  'post_title' => 'Nice Hat',
  'post_excerpt' => 'Integer blandit tellus quis rmentum condimentum elit ac accumsan. Proin cursus, lectus malesuada pretium condimentum, quam odio malesuada purus, et sodales diam erat vel est.',
  'post_name' => 'nice-hat',
  'post_modified' => '2017-08-21 07:08:17',
  'post_modified_gmt' => '2017-08-21 07:08:17',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2226',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_thumbnail_id' => '2251',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/nice_hat.png',
    'title_font_color' => '#000000',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '633',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '70',
    '_product_attributes' => 
    array (
    ),
    '_price' => '70',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'link_font_color' => '#000000',
    'background_color' => '#d6cfc4',
    'product_image_layout' => 'image-right',
    'price_font_color' => '#000000',
    'description_font_color' => '#000000',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-nice_hat.jpg',
    '_background_image_attach_id' => '2250',
    '_edit_last' => '172',
    '_featured' => 'no',
    '_edit_lock' => '1503299322:172',
    '_post_image_attach_id' => '2251',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, fashion, featured',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2232,
  'post_date' => '2013-10-05 22:36:03',
  'post_date_gmt' => '2013-10-05 22:36:03',
  'post_content' => 'Nulla adipiscing faucibus odio, nec sodales augue euismod eget. Sed non lacinia arcu. Morbi nec tristique orci. Proin viverra hendrerit ullamcorper. Donec eget massa ut nibh aliquam ultrices. Nullam molestie, ipsum non luctus iaculis, lacus nibh tempus diam.',
  'post_title' => 'LED TV',
  'post_excerpt' => 'Donec eget massa ut nibh aliquam ultrices. Nullam molestie, ipsum non luctus iaculis, lacus nibh tempus diam, at imperdiet nibh tellus sit amet elit.',
  'post_name' => 'led-tv',
  'post_modified' => '2017-08-21 07:08:19',
  'post_modified_gmt' => '2017-08-21 07:08:19',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2232',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_last' => '172',
    '_downloadable' => 'no',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-led_tvb.jpg',
    '_background_image_attach_id' => '2245',
    '_thumbnail_id' => '2246',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/led_tv.png',
    'title_font_color' => '#ffffff',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '5',
    'link_font_color' => '#ffffff',
    'product_image_layout' => 'image-right',
    'price_font_color' => '#ffffff',
    'description_font_color' => '#ffffff',
    'button_font_color' => '#000000',
    'button_background_color' => '#ffffff',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_color' => '#f5cba1',
    '_manage_stock' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '300',
    '_sale_price' => '249',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '249',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_edit_lock' => '1503299178:172',
    '_post_image_attach_id' => '2246',
    '_wc_rating_count' => 
    array (
      4 => '1',
    ),
    '_wc_average_rating' => '4.00',
    '_wc_review_count' => '1',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'featured, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2236,
  'post_date' => '2013-10-14 23:05:08',
  'post_date_gmt' => '2013-10-14 23:05:08',
  'post_content' => 'Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut posuere pretium aliquam. Integer blandit tellus quis enim lacinia lobortis. Etiam fermentum condimentum elit ac accumsan. Proin cursus, lectus malesuada pretium condimentum, quam odio malesuada purus, et sodales diam erat vel est.<!--themify_builder_static--><img src="https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-500x178.png" width="500" title="Image Module" alt="Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus." srcset="https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-500x178.png 500w, https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-300x107.png 300w, https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-58x20.png 58w, https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-200x71.png 200w, https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle-600x214.png 600w, https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle.png 1015w" sizes="(max-width: 500px) 100vw, 500px" /> <h3> Image Module </h3> Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus. 
<ul><li><h4>FAQ One</h4><p>Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.</p></li><li><h4>Accordion Two</h4><p>In gravida arcu ut neque ornare vitae rutrum turpis vehicula. Nunc ultrices sem mollis metus rutrum non malesuada metus fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque interdum rutrum quam, a pharetra est pulvinar ac.</p></li><li><h4>Accordion Three</h4><p>Aliquam faucibus turpis at libero consectetur euismod. Nam nunc lectus, congue non egestas quis, condimentum ut arcu. Nulla placerat, tortor non egestas rutrum, mi turpis adipiscing dui, et mollis turpis tortor vel orci.</p></li></ul>
 
 <iframe width="1165" height="655" src="https://www.youtube.com/embed/A0JrDX8tpks?feature=oembed" gesture="media" allowfullscreen></iframe> 
 
 <ul data-id="slider-0-" data-visible="4" data-scroll="1" data-auto-scroll="4" data-speed="1" data-wrap="yes" data-arrow="yes" data-pagination="yes" data-effect="scroll" data-height="variable" data-pause-on-hover="resume" > 
 <li> <img src="https://themify.me/demo/themes/flatshop/files/2013/10/128730368-240x131.jpg" width="240" alt="Slider Image One" srcset="https://themify.me/demo/themes/flatshop/files/2013/10/128730368-240x131.jpg 240w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-300x164.jpg 300w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-58x31.jpg 58w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-200x109.jpg 200w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-600x328.jpg 600w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-222x121.jpg 222w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-729x398.jpg 729w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-474x259.jpg 474w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368-306x167.jpg 306w, https://themify.me/demo/themes/flatshop/files/2013/10/128730368.jpg 1000w" sizes="(max-width: 240px) 100vw, 240px" /> 
 <h3> Slider Image One </h3> </li> <li> <img src="https://themify.me/demo/themes/flatshop/files/2013/10/63992509-240x166.jpg" width="240" alt="Slider Image Two" srcset="https://themify.me/demo/themes/flatshop/files/2013/10/63992509-240x166.jpg 240w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-300x207.jpg 300w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-58x40.jpg 58w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-200x138.jpg 200w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-548x380.jpg 548w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-222x153.jpg 222w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-729x505.jpg 729w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-474x328.jpg 474w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509-306x212.jpg 306w, https://themify.me/demo/themes/flatshop/files/2013/10/63992509.jpg 1000w" sizes="(max-width: 240px) 100vw, 240px" /> 
 <h3> Slider Image Two </h3> </li> <li> <img src="https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-240x166.jpg" width="240" alt="Slider Image Three" srcset="https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-240x166.jpg 240w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-300x208.jpg 300w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-58x40.jpg 58w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-200x139.jpg 200w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center1-546x380.jpg 546w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center1.jpg 1000w" sizes="(max-width: 240px) 100vw, 240px" /> 
 <h3> Slider Image Three </h3> </li> <li> <img src="https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-240x160.jpg" width="240" alt="Slider Image Four" srcset="https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-240x160.jpg 240w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-300x200.jpg 300w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-58x38.jpg 58w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-200x133.jpg 200w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2-569x380.jpg 569w, https://themify.me/demo/themes/flatshop/files/2013/10/image-center-2.jpg 1000w" sizes="(max-width: 240px) 100vw, 240px" /> 
 <h3> Slider Image Four </h3> </li> <li> <img src="https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-1024x575-240x134.jpg" width="240" alt="Slider Image Five" srcset="https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-1024x575-240x134.jpg 240w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-300x168.jpg 300w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-1024x575.jpg 1024w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-58x32.jpg 58w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-200x112.jpg 200w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-600x337.jpg 600w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-1064x598.jpg 1064w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2-1024x575-729x409.jpg 729w, https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2.jpg 1400w" sizes="(max-width: 240px) 100vw, 240px" /> 
 <h3> Slider Image Five </h3> </li> </ul> 
<h3></h3><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=1+Yonge+Street+Toronto%2C+ON+Canada&amp;t=m&amp;z=13&amp;output=embed&amp;iwloc=near"></iframe>
 <h3>Where to buy it?</h3> <p>Cras a fringilla nunc. Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.</p><h4>Address</h4><p>123 Street Name</p><p>City Toronto</p><p>Canada</p>
 
 <h3>Get Flatshop Now</h3> Cras a fringilla nunc uspendisse volutpat, eros congue scelerisque iaculis. 
 <a href="https://themify.me/themes/flatshop"> Flatshop </a> 
<ul><li><h4>More Info</h4><p>Nunc eleifend consectetur odio sit amet viverra. Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi.</p><p> </p><p>[gallery columns="7" ids="2425,2415,2357,2365,2338,2340"]</p></li><li><h4>Tab Two</h4><p>Fusce ut sem est. In eu sagittis felis. In gravida arcu ut neque ornare vitae rutrum turpis vehicula. Nunc ultrices sem mollis metus rutrum non malesuada metus fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque interdum rutrum quam, a pharetra est pulvinar ac. Vestibulum congue nisl magna. Ut vulputate odio id dui convallis in adipiscing libero condimentum. </p></li><li><h4>Three</h4><p>Cras a fringilla nunc. Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu. Mauris consequat rhoncus dolor id sagittis. Cras tortor elit, aliquet quis tincidunt eget, dignissim non tortor. Cras ultricies cursus nisl, eget congue tellus consequat nec. Cras id nibh neque, eu dignissim orci. Aenean at adipiscing urna. Suspendisse potenti.</p></li></ul> <h3>Gallery Module</h3>[gallery columns="4" ids="2357,2365,2333,2337,2334,2425,2338,2339"]
 <h4>Box 1</h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.</p> 
 <h4>Box 2</h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.</p> 
 <h4>Box 3</h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.</p> 
 <h4>Box 4</h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.</p><!--/themify_builder_static-->',
  'post_title' => 'Builder Product',
  'post_excerpt' => 'This Product page is created with Themify\'s drag and drop <a href="https://themify.me/builder">Builder</a>. Check this product out!',
  'post_name' => 'builder-product',
  'post_modified' => '2017-10-27 21:42:25',
  'post_modified_gmt' => '2017-10-27 21:42:25',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2236',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_product_attributes' => 
    array (
      'size' => 
      array (
        'name' => 'Size',
        'value' => '24" | 27" | 32"',
        'position' => 0,
        'is_visible' => 0,
        'is_variation' => 1,
        'is_taxonomy' => 0,
      ),
    ),
    '_price' => '500',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'link_font_color' => '#ffffff',
    'background_color' => '#9ca4b6',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#ffffff',
    'description_font_color' => '#ffffff',
    'button_font_color' => '#0a210f',
    'button_background_color' => '#ffffff',
    'background_repeat' => 'fullcover',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop-pair.jpg',
    '_background_image_attach_id' => '2310',
    '_thumbnail_id' => '2241',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/desktop.png',
    'title_font_color' => '#ffffff',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '75',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '500',
    '_featured' => 'no',
    '_edit_last' => '240',
    '_edit_lock' => '1509140547:240',
    '_wp_old_slug' => 'all-in-one-desktop',
    '_min_variation_price' => '500',
    '_max_variation_price' => '800',
    '_min_variation_regular_price' => '500',
    '_max_variation_regular_price' => '800',
    '_default_attributes' => 
    array (
    ),
    '_post_image_attach_id' => '2241',
    '_themify_builder_settings_json' => '[{\\"row_order\\":\\"0\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"image\\",\\"mod_settings\\":{\\"style_image\\":\\"image-left\\",\\"url_image\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/laptop_angle.png\\",\\"appearance_image\\":\\"rounded\\",\\"width_image\\":\\"500\\",\\"title_image\\":\\"Image Module\\",\\"caption_image\\":\\"Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.\\"}}]}]},{\\"row_order\\":\\"1\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"accordion\\",\\"mod_settings\\":{\\"expand_collapse_accordion\\":\\"accordion\\",\\"accordion_appearance_accordion\\":\\"rounded|gradient\\",\\"content_accordion\\":[{\\"title_accordion\\":\\"FAQ One\\",\\"text_accordion\\":\\"<p>Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.<\\\\/p>\\",\\"default_accordion\\":\\"open\\"},{\\"title_accordion\\":\\"Accordion Two\\",\\"text_accordion\\":\\"<p>In gravida arcu ut neque ornare vitae rutrum turpis vehicula. Nunc ultrices sem mollis metus rutrum non malesuada metus fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque interdum rutrum quam, a pharetra est pulvinar ac.<\\\\/p>\\",\\"default_accordion\\":\\"closed\\"},{\\"title_accordion\\":\\"Accordion Three\\",\\"text_accordion\\":\\"<p>Aliquam faucibus turpis at libero consectetur euismod. Nam nunc lectus, congue non egestas quis, condimentum ut arcu. Nulla placerat, tortor non egestas rutrum, mi turpis adipiscing dui, et mollis turpis tortor vel orci.<\\\\/p>\\",\\"default_accordion\\":\\"closed\\"}]}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"video\\",\\"mod_settings\\":{\\"style_video\\":\\"video-top\\",\\"url_video\\":\\"http:\\\\/\\\\/www.youtube.com\\\\/watch?v=A0JrDX8tpks\\",\\"width_video\\":\\"100\\",\\"unit_video\\":\\"%\\"}}]}]},{\\"row_order\\":\\"2\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"slider\\",\\"mod_settings\\":{\\"layout_display_slider\\":\\"image\\",\\"blog_category_slider\\":\\"|single\\",\\"slider_category_slider\\":\\"|single\\",\\"portfolio_category_slider\\":\\"|single\\",\\"img_content_slider\\":[{\\"img_url_slider\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/128730368.jpg\\",\\"img_title_slider\\":\\"Slider Image One\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/63992509.jpg\\",\\"img_title_slider\\":\\"Slider Image Two\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/image-center1.jpg\\",\\"img_title_slider\\":\\"Slider Image Three\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/image-center-2.jpg\\",\\"img_title_slider\\":\\"Slider Image Four\\"},{\\"img_url_slider\\":\\"https://themify.me/demo/themes/flatshop\\\\/files\\\\/2013\\\\/10\\\\/bg-desktop_pair2.jpg\\",\\"img_title_slider\\":\\"Slider Image Five\\"}],\\"layout_slider\\":\\"slider-default\\",\\"img_w_slider\\":\\"240\\",\\"visible_opt_slider\\":\\"4\\",\\"auto_scroll_opt_slider\\":\\"4\\",\\"scroll_opt_slider\\":\\"1\\",\\"speed_opt_slider\\":\\"normal\\",\\"effect_slider\\":\\"scroll\\",\\"wrap_slider\\":\\"yes\\",\\"show_nav_slider\\":\\"yes\\",\\"show_arrow_slider\\":\\"yes\\",\\"left_margin_slider\\":\\"15\\",\\"right_margin_slider\\":\\"15\\"}}]}]},{\\"row_order\\":\\"3\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"map\\",\\"mod_settings\\":{\\"address_map\\":\\"1 Yonge Street\\\\nToronto, ON\\\\nCanada\\",\\"zoom_map\\":\\"13\\",\\"w_map\\":\\"100\\",\\"unit_w\\":\\"%\\",\\"h_map\\":\\"300\\",\\"b_width_map\\":\\"1\\",\\"b_color_map\\":\\"000000\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"text\\",\\"mod_settings\\":{\\"mod_title_text\\":\\"Where to buy it?\\",\\"content_text\\":\\"<p><span style=\\\\\\\\\\\\\\"font-size: 13px;\\\\\\\\\\\\\\">Cras a fringilla nunc. Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.<\\\\/span><\\\\/p><h4>Address<\\\\/h4><p>123 Street Name<\\\\/p><p>City Toronto<\\\\/p><p>Canada<\\\\/p>\\"}}]}]},{\\"row_order\\":\\"4\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\",\\"modules\\":[{\\"mod_name\\":\\"callout\\",\\"mod_settings\\":{\\"layout_callout\\":\\"button-right\\",\\"heading_callout\\":\\"Get Flatshop Now\\",\\"text_callout\\":\\"Cras a fringilla nunc uspendisse volutpat, eros congue scelerisque iaculis.\\",\\"color_callout\\":\\"yellow\\",\\"appearance_callout\\":\\"rounded\\",\\"action_btn_link_callout\\":\\"https:\\\\/\\\\/themify.me\\\\/themes\\\\/flatshop\\",\\"action_btn_text_callout\\":\\"Flatshop\\",\\"action_btn_color_callout\\":\\"black\\",\\"action_btn_appearance_callout\\":\\"rounded\\"}}]}]},{\\"row_order\\":\\"5\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"tab\\",\\"mod_settings\\":{\\"layout_tab\\":\\"minimal\\",\\"color_tab\\":\\"black\\",\\"tab_content_tab\\":[{\\"title_tab\\":\\"More Info\\",\\"text_tab\\":\\"<p>Nunc eleifend consectetur odio sit amet viverra. Ut euismod ligula eu tellus interdum mattis ac eu nulla. Phasellus cursus, lacus quis convallis aliquet, dolor urna ullamcorper mi, eget dapibus velit est vitae nisi.<\\\\/p><p> <\\\\/p><p>[gallery columns=\\\\\\\\\\\\\\"7\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"2425,2415,2357,2365,2338,2340\\\\\\\\\\\\\\"]<\\\\/p>\\"},{\\"title_tab\\":\\"Tab Two\\",\\"text_tab\\":\\"<p>Fusce ut sem est. In eu sagittis felis. In gravida arcu ut neque ornare vitae rutrum turpis vehicula. Nunc ultrices sem mollis metus rutrum non malesuada metus fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque interdum rutrum quam, a pharetra est pulvinar ac. Vestibulum congue nisl magna. Ut vulputate odio id dui convallis in adipiscing libero condimentum. <\\\\/p>\\"},{\\"title_tab\\":\\"Three\\",\\"text_tab\\":\\"<p>Cras a fringilla nunc. Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu. Mauris consequat rhoncus dolor id sagittis. Cras tortor elit, aliquet quis tincidunt eget, dignissim non tortor. Cras ultricies cursus nisl, eget congue tellus consequat nec. Cras id nibh neque, eu dignissim orci. Aenean at adipiscing urna. Suspendisse potenti.<\\\\/p>\\"}]}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-2\\",\\"modules\\":[{\\"mod_name\\":\\"gallery\\",\\"mod_settings\\":{\\"mod_title_gallery\\":\\"Gallery Module\\",\\"shortcode_gallery\\":\\"[gallery columns=\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\" ids=\\\\\\\\\\\\\\"2357,2365,2333,2337,2334,2425,2338,2339\\\\\\\\\\\\\\"]\\",\\"appearance_gallery\\":\\"rounded\\"}}]}]},{\\"row_order\\":\\"6\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"box\\",\\"mod_settings\\":{\\"content_box\\":\\"<h4>Box 1<\\\\/h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.<\\\\/p>\\",\\"color_box\\":\\"yellow\\",\\"appearance_box\\":\\"rounded\\"}}]},{\\"column_order\\":\\"1\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"box\\",\\"mod_settings\\":{\\"content_box\\":\\"<h4>Box 2<\\\\/h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.<\\\\/p>\\",\\"color_box\\":\\"light-blue\\",\\"appearance_box\\":\\"rounded\\"}}]},{\\"column_order\\":\\"2\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"box\\",\\"mod_settings\\":{\\"content_box\\":\\"<h4>Box 3<\\\\/h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.<\\\\/p>\\",\\"color_box\\":\\"purple\\",\\"appearance_box\\":\\"rounded\\"}}]},{\\"column_order\\":\\"3\\",\\"grid_class\\":\\"col4-1\\",\\"modules\\":[{\\"mod_name\\":\\"box\\",\\"mod_settings\\":{\\"content_box\\":\\"<h4>Box 4<\\\\/h4><p>Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu.<\\\\/p>\\",\\"color_box\\":\\"orange\\",\\"appearance_box\\":\\"rounded\\"}}]}]},{\\"row_order\\":\\"7\\",\\"cols\\":[{\\"column_order\\":\\"0\\",\\"grid_class\\":\\"col-full\\"}]}]',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.2',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'featured, technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2256,
  'post_date' => '2013-10-08 04:00:02',
  'post_date_gmt' => '2013-10-08 04:00:02',
  'post_content' => 'Vestibulum augue orci, pellentesque ac scelerisque sit amet, euismod at odio. Curabitur aliquet ipsum in enim porta consequat. Suspendisse cursus, metus eu pellentesque posuere, nibh quam faucibus augue, ac euismod ligula nisl vel velit.',
  'post_title' => 'Sleeveless Top',
  'post_excerpt' => '',
  'post_name' => 'sleeveless-top',
  'post_modified' => '2017-08-21 07:08:12',
  'post_modified_gmt' => '2017-08-21 07:08:12',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2256',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299176:172',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ladies-clothes-bg.jpg',
    '_background_image_attach_id' => '2257',
    '_thumbnail_id' => '2258',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ladies-clothes.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '25',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '25',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#000000',
    'link_font_color' => '#000000',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#000000',
    'description_font_color' => '#4d4d4d',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_color' => '#f5eedb',
    '_post_image_attach_id' => '2258',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'fashion',
    'product_tag' => 'top',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2259,
  'post_date' => '2013-10-08 04:09:13',
  'post_date_gmt' => '2013-10-08 04:09:13',
  'post_content' => 'Pellentesque a libero gravida, vehicula mauris sit amet, tempor augue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas odio erat, egestas in lacinia eu, mollis ac urna.',
  'post_title' => 'Model Airplane',
  'post_excerpt' => '',
  'post_name' => 'model-airplane',
  'post_modified' => '2017-08-21 07:08:09',
  'post_modified_gmt' => '2017-08-21 07:08:09',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2259',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299176:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2261',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/airplane.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '14',
    '_sale_price' => '8',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '8',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#00183d',
    'link_font_color' => '#00183d',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#00183d',
    'description_font_color' => '#00183d',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#00183d',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_color' => '#fff5ea',
    '_post_image_attach_id' => '2261',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2263,
  'post_date' => '2013-10-08 04:14:07',
  'post_date_gmt' => '2013-10-08 04:14:07',
  'post_content' => 'Maecenas in ipsum ac nisl accumsan euismod. Integer vitae euismod felis. Ut pellentesque faucibus velit, in scelerisque felis vestibulum vitae. Aenean accumsan urna faucibus libero ultrices, vitae tincidunt elit accumsan.',
  'post_title' => 'London Doll',
  'post_excerpt' => '',
  'post_name' => 'london-doll',
  'post_modified' => '2017-08-21 07:08:07',
  'post_modified_gmt' => '2017-08-21 07:08:07',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2263',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299175:172',
    '_edit_last' => '172',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/barbie-bg.jpg',
    '_background_image_attach_id' => '2264',
    '_thumbnail_id' => '2265',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/barbie.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '24',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '24',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#3f281f',
    'link_font_color' => '#3f281f',
    'background_color' => '#cfd0cb',
    'product_image_layout' => 'image-right',
    'price_font_color' => '#3f281f',
    'description_font_color' => '#3f281f',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#3f281f',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2265',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2266,
  'post_date' => '2013-10-08 04:20:36',
  'post_date_gmt' => '2013-10-08 04:20:36',
  'post_content' => 'Curabitur tincidunt consectetur nunc eu commodo. Morbi eu quam ut dolor imperdiet commodo ut et justo. Praesent condimentum convallis nisi, eu ornare lacus mollis a. Suspendisse potenti.',
  'post_title' => 'Tiny Rider',
  'post_excerpt' => '',
  'post_name' => 'tiny-rider',
  'post_modified' => '2017-08-21 07:08:06',
  'post_modified_gmt' => '2017-08-21 07:08:06',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2266',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299175:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2268',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bike.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '5',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '5',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#2a3a40',
    'link_font_color' => '#688d99',
    'background_color' => '#e3f3ff',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#2ba7ba',
    'description_font_color' => '#333333',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#688d99',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2268',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2280,
  'post_date' => '2013-10-08 15:37:28',
  'post_date_gmt' => '2013-10-08 15:37:28',
  'post_content' => 'Vivamus tristique tortor magna, vitae cursus nulla hendrerit non. Vivamus tincidunt nisl odio, at imperdiet nisl varius at. Morbi egestas ipsum sit amet mauris dignissim, vulputate dignissim leo congue.',
  'post_title' => 'Fluffy Stegosaurus',
  'post_excerpt' => '',
  'post_name' => 'fluffy-stegosaurus',
  'post_modified' => '2017-08-21 07:08:04',
  'post_modified_gmt' => '2017-08-21 07:08:04',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2280',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299174:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2282',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/dinosaur.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '10',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '10',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#ffe5e5',
    'product_image_layout' => 'image-left',
    'background_repeat' => 'repeat',
    'title_font_color' => '#a84a7a',
    'link_font_color' => '#a84a7a',
    'price_font_color' => '#000000',
    'description_font_color' => '#7a5555',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#a84a7a',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2282',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2288,
  'post_date' => '2013-10-08 15:37:30',
  'post_date_gmt' => '2013-10-08 15:37:30',
  'post_content' => 'Sed viverra eros ipsum, quis gravida quam viverra eget. Proin tempus semper orci nec condimentum. Maecenas consequat metus id urna posuere condimentum.',
  'post_title' => 'Young Builder Pack',
  'post_excerpt' => 'Pellentesque sodales, risus id ornare ullamcorper, arcu dui lobortis velit, sit amet commodo est dui eget mi. Fusce a felis vitae urna tempus feugiat at quis nisi.',
  'post_name' => 'young-builder-pack',
  'post_modified' => '2017-08-21 07:08:02',
  'post_modified_gmt' => '2017-08-21 07:08:02',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2288',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299173:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2284',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/lego.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '15',
    '_sale_price' => '12.99',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '12.99',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#f9f0d4',
    'product_image_layout' => 'image-right',
    'background_repeat' => 'fullcover',
    'title_font_color' => '#333333',
    'link_font_color' => '#333333',
    'price_font_color' => '#333333',
    'description_font_color' => '#333333',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#333333',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2284',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2289,
  'post_date' => '2013-10-08 16:01:52',
  'post_date_gmt' => '2013-10-08 16:01:52',
  'post_content' => 'Vivamus dui mi, tempor ac aliquet sed, ultricies in arcu. Quisque dignissim condimentum auctor. Morbi porttitor cursus est at pharetra.',
  'post_title' => 'The Teddy Bear',
  'post_excerpt' => '',
  'post_name' => 'the-teddy-bear',
  'post_modified' => '2017-08-21 07:08:00',
  'post_modified_gmt' => '2017-08-21 07:08:00',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2289',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299173:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2286',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/teddybear.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '8.58',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '8.58',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#333333',
    'link_font_color' => '#333333',
    'background_color' => '#eeeeee',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#333333',
    'description_font_color' => '#333333',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#333333',
    'background_repeat' => 'repeat',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2286',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'toys',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2312,
  'post_date' => '2013-03-25 20:56:29',
  'post_date_gmt' => '2013-03-25 20:56:29',
  'post_content' => 'Aliquam viverra nisl sed dolor convallis, eget molestie quam sodales. Ut eget rhoncus lorem, non eleifend libero. Aenean facilisis neque nunc, a iaculis arcu placerat a.',
  'post_title' => 'A New Pair',
  'post_excerpt' => '',
  'post_name' => 'a-new-pair',
  'post_modified' => '2017-08-21 07:08:28',
  'post_modified_gmt' => '2017-08-21 07:08:28',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2312',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299182:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '60',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '60',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#ffffff',
    'link_font_color' => '#ffffff',
    'product_image_layout' => 'image-right',
    'price_font_color' => '#e3e3e3',
    'description_font_color' => '#d6d6d6',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'repeat',
    '_thumbnail_id' => '2314',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ladies-shoes.png',
    'builder_switch_frontend' => '0',
    'background_color' => '#454545',
    '_post_image_attach_id' => '2314',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'fashion, shoes',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2404,
  'post_date' => '2013-10-11 22:02:28',
  'post_date_gmt' => '2013-10-11 22:02:28',
  'post_content' => 'Aenean consectetur lacus tellus, sed vestibulum quam. Donec lorem lectus, posuere in pharetra at, vestibulum et magna. Ut viverra, risus eu commodo interdum, nunc ipsum mollis purus, ac varius ante purus sed diam. Vivamus in risus non lacus vehicula vestibulum. In magna leo, malesuada eget pulvinar ut, pellentesque a arcu. Praesent rutrum feugiat nibh elementum posuere. Nulla volutpat porta enim vel consectetur. Etiam orci eros, blandit nec egestas eget, pharetra eget leo. Morbi lobortis adipiscing massa tincidunt dignissim.',
  'post_title' => 'Tablets',
  'post_excerpt' => 'Etiam orci eros, blandit nec egestas eget, pharetra eget leo. Morbi lobortis adipiscing massa tincidunt dignissim.',
  'post_name' => 'tablets',
  'post_modified' => '2017-08-21 07:07:50',
  'post_modified_gmt' => '2017-08-21 07:07:50',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2404',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299169:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '6',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '427',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '427',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#969696',
    'product_image_layout' => 'image-left',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'title_font_color' => '#ffffff',
    'link_font_color' => '#ffffff',
    'price_font_color' => '#f5f5f5',
    'description_font_color' => '#e6e6e6',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#4d4d4d',
    '_wp_old_slug' => 'tablets-bundle',
    '_thumbnail_id' => '2439',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/phone_tablets2.png',
    '_post_image_attach_id' => '2439',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2316,
  'post_date' => '2013-10-08 21:20:49',
  'post_date_gmt' => '2013-10-08 21:20:49',
  'post_content' => 'Quisque lobortis nec orci eu ultrices. Suspendisse posuere non dolor nec ornare. Vestibulum malesuada molestie sollicitudin. Vestibulum nec cursus velit, ut porttitor justo.',
  'post_title' => 'Red Is Back',
  'post_excerpt' => 'Maecenas in ipsum ac nisl accumsan euismod. Integer vitae euismod felis. Ut pellentesque faucibus velit, in scelerisque felis vestibulum vitae.',
  'post_name' => 'red-is-back',
  'post_modified' => '2017-08-21 07:07:56',
  'post_modified_gmt' => '2017-08-21 07:07:56',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2316',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299171:172',
    '_edit_last' => '172',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/red-handbag-bg.jpg',
    '_background_image_attach_id' => '2317',
    '_thumbnail_id' => '2318',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/red-handbag.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '6',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '40',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '40',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#000000',
    'link_font_color' => '#000000',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#000000',
    'description_font_color' => '#000000',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2318',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'accessories, fashion',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2319,
  'post_date' => '2013-10-08 21:44:19',
  'post_date_gmt' => '2013-10-08 21:44:19',
  'post_content' => 'Aenean accumsan urna faucibus libero ultrices, vitae tincidunt elit accumsan. Maecenas venenatis lacus diam, non eleifend tellus sagittis congue. Duis tortor nisl, laoreet sed vulputate sit amet, fringilla nec nisi.',
  'post_title' => 'Game Console',
  'post_excerpt' => 'Aenean facilisis neque nunc, a iaculis arcu placerat a. Quisque lobortis nec orci eu ultrices. Suspendisse posuere non dolor nec ornare. Vestibulum malesuada molestie sollicitudin.',
  'post_name' => 'game-console',
  'post_modified' => '2017-08-21 07:07:55',
  'post_modified_gmt' => '2017-08-21 07:07:55',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2319',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299171:172',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ps-bg.jpg',
    '_background_image_attach_id' => '2320',
    '_edit_last' => '172',
    '_thumbnail_id' => '2321',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ps.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '3',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '200',
    '_sale_price' => '160',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '160',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#000000',
    'link_font_color' => '#000000',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#000000',
    'description_font_color' => '#000000',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_color' => '#ebebeb',
    '_wp_old_slug' => '2319',
    '_post_image_attach_id' => '2321',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'games',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2323,
  'post_date' => '2013-08-08 22:13:43',
  'post_date_gmt' => '2013-08-08 22:13:43',
  'post_content' => 'Maecenas odio erat, egestas in lacinia eu, mollis ac urna. Maecenas in ipsum ac nisl accumsan euismod. Integer vitae euismod felis.',
  'post_title' => 'Portable Console',
  'post_excerpt' => '',
  'post_name' => 'portable-console',
  'post_modified' => '2017-08-21 07:08:25',
  'post_modified_gmt' => '2017-08-21 07:08:25',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2323',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299181:172',
    '_thumbnail_id' => '2325',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/wiiu.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '100',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '100',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#ffffff',
    'link_font_color' => '#ffffff',
    'background_color' => '#00c5e6',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#ffffff',
    'description_font_color' => '#f0ffff',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'repeat-x',
    'builder_switch_frontend' => '0',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/wiiu-bg.png',
    '_background_image_attach_id' => '2392',
    '_post_image_attach_id' => '2325',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'games',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2326,
  'post_date' => '2013-10-08 22:29:36',
  'post_date_gmt' => '2013-10-08 22:29:36',
  'post_content' => 'Integer aliquet nunc porta tellus pulvinar tincidunt. Nullam sodales felis eu ultricies aliquam. Vivamus vestibulum, tortor sed posuere sollicitudin, urna augue rhoncus dolor, at porttitor dui nulla vel leo.',
  'post_title' => 'Gamer\'s Bundle',
  'post_excerpt' => '',
  'post_name' => 'gamers-bundle',
  'post_modified' => '2017-08-21 07:07:53',
  'post_modified_gmt' => '2017-08-21 07:07:53',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2326',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299170:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2327',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/game_console_bundle.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '300',
    '_sale_price' => '269',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '269',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#555555',
    'link_font_color' => '#555555',
    'background_color' => '#dbdae6',
    'product_image_layout' => 'image-center',
    'price_font_color' => '#555555',
    'description_font_color' => '#555555',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#555555',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2327',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'games',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2328,
  'post_date' => '2013-08-09 01:12:45',
  'post_date_gmt' => '2013-08-09 01:12:45',
  'post_content' => 'Maecenas in ipsum ac nisl accumsan euismod. Integer vitae euismod felis. Ut pellentesque faucibus velit, in scelerisque felis vestibulum vitae.',
  'post_title' => 'New Portable Console',
  'post_excerpt' => '',
  'post_name' => 'new-portable-console',
  'post_modified' => '2017-08-21 07:08:25',
  'post_modified_gmt' => '2017-08-21 07:08:25',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2328',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299180:172',
    '_edit_last' => '172',
    '_thumbnail_id' => '2329',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/3ds-1.png',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '0',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_product_image_gallery' => '2331,2330',
    '_regular_price' => '60',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '60',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#000000',
    'link_font_color' => '#000000',
    'background_color' => '#f6f0ff',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#000000',
    'description_font_color' => '#000000',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_wp_old_slug' => 'portable-console-2',
    '_post_image_attach_id' => '2329',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'games',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2389,
  'post_date' => '2013-10-08 21:10:09',
  'post_date_gmt' => '2013-10-08 21:10:09',
  'post_content' => 'Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere. Curabitur at arcu id turpis posuere bibendum. Sed commodo mauris eget diam pretium cursus. In sagittis feugiat mauris, in ultrices mauris lacinia eu. Fusce augue velit, vulputate elementum semper congue, rhoncus adipiscing nisl. Curabitur vel risus eros, sed eleifend arcu. Donec porttitor hendrerit diam et blandit. Curabitur vitae velit ligula, vitae lobortis massa. Mauris mattis est quis dolor venenatis vitae pharetra diam gravida. Vivamus dignissim, ligula vel ultricies varius, nibh velit pretium leo, vel placerat ipsum risus luctus purus.',
  'post_title' => 'Digital Camera',
  'post_excerpt' => 'Sed sagittis, elit egestas rutrum vehicula, neque dolor fringilla lacus, ut rhoncus turpis augue vitae libero. Nam risus velit, rhoncus eget consectetur id, posuere at ligula. Vivamus imperdiet diam ac tortor tempus posuere.',
  'post_name' => 'digital-camera',
  'post_modified' => '2017-08-21 07:07:58',
  'post_modified_gmt' => '2017-08-21 07:07:58',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2389',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299172:172',
    '_thumbnail_id' => '2390',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/digital-camera.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '8',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '200',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '200',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'title_font_color' => '#ffffff',
    'link_font_color' => '#ffffff',
    'background_color' => '#383838',
    'product_image_layout' => 'image-left',
    'price_font_color' => '#ffffd4',
    'description_font_color' => '#cccccc',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#000000',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2390',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2393,
  'post_date' => '2013-10-01 21:20:47',
  'post_date_gmt' => '2013-10-01 21:20:47',
  'post_content' => 'Ut vulputate odio id dui convallis in adipiscing libero condimentum. Nunc et pharetra enim. Praesent pharetra, neque et luctus tempor, leo sapien faucibus leo, a dignissim turpis ipsum sed libero. Sed sed luctus purus. Aliquam faucibus turpis at libero consectetur euismod. Nam nunc lectus, congue non egestas quis, condimentum ut arcu. Nulla placerat, tortor non egestas rutrum, mi turpis adipiscing dui, et mollis turpis tortor vel orci.',
  'post_title' => 'Bracelet',
  'post_excerpt' => 'Fusce ut sem est. In eu sagittis felis. In gravida arcu ut neque ornare vitae rutrum turpis vehicula. Nunc ultrices sem mollis metus rutrum non malesuada metus fermentum.',
  'post_name' => 'bracelet',
  'post_modified' => '2017-08-21 07:08:22',
  'post_modified_gmt' => '2017-08-21 07:08:22',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2393',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299180:172',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '4',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '20',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '20',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'product_image_layout' => 'image-center',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'background_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ring-bg.jpg',
    '_background_image_attach_id' => '2395',
    '_wp_old_slug' => 'blue-bracelet',
    'title_font_color' => '#380a0a',
    'link_font_color' => '#a62e58',
    'price_font_color' => '#9e4a4a',
    'description_font_color' => '#734545',
    'button_font_color' => '#ffffff',
    'button_background_color' => '#a62e58',
    '_thumbnail_id' => '2397',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/ring.png',
    '_post_image_attach_id' => '2397',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_yoast_wpseo_content_score' => '30',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'fashion, featured',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2401,
  'post_date' => '2013-10-05 21:41:46',
  'post_date_gmt' => '2013-10-05 21:41:46',
  'post_content' => 'Donec facilisis diam sed arcu cursus suscipit. Mauris varius fermentum velit sit amet varius. Aenean consectetur lacus tellus, sed vestibulum quam. Donec lorem lectus, posuere in pharetra at, vestibulum et magna. Ut viverra, risus eu commodo interdum, nunc ipsum mollis purus, ac varius ante purus sed diam. Vivamus in risus non lacus vehicula vestibulum. In magna leo, malesuada eget pulvinar ut, pellentesque a arcu. Praesent rutrum feugiat nibh elementum posuere. Nulla volutpat porta enim vel consectetur. Etiam orci eros, blandit nec egestas eget, pharetra eget leo.',
  'post_title' => 'Air',
  'post_excerpt' => 'Praesent rutrum feugiat nibh elementum posuere. Nulla volutpat porta enim vel consectetur. Etiam orci eros, blandit nec egestas eget, pharetra eget leo.',
  'post_name' => 'air',
  'post_modified' => '2017-08-21 07:08:21',
  'post_modified_gmt' => '2017-08-21 07:08:21',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2401',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299323:172',
    '_thumbnail_id' => '2402',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/laptop_side.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '1',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '1036',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '1036',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#ededed',
    'product_image_layout' => 'image-center',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2402',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2407,
  'post_date' => '2013-10-11 22:19:47',
  'post_date_gmt' => '2013-10-11 22:19:47',
  'post_content' => 'Nulla volutpat porta enim vel consectetur. Etiam orci eros, blandit nec egestas eget, pharetra eget leo. Morbi lobortis adipiscing massa tincidunt dignissim. Aenean consectetur lacus tellus, sed vestibulum quam. Donec lorem lectus, posuere in pharetra at, vestibulum et magna. Ut viverra, risus eu commodo interdum, nunc ipsum mollis purus, ac varius ante purus sed diam. Vivamus in risus non lacus vehicula vestibulum. In magna leo, malesuada eget pulvinar ut, pellentesque a arcu. Praesent rutrum feugiat nibh elementum posuere.',
  'post_title' => 'LED',
  'post_excerpt' => 'In magna leo, malesuada eget pulvinar ut, pellentesque a arcu. Praesent rutrum feugiat nibh elementum posuere.',
  'post_name' => 'led',
  'post_modified' => '2017-08-21 07:07:48',
  'post_modified_gmt' => '2017-08-21 07:07:48',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2407',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299168:172',
    '_thumbnail_id' => '2408',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/led_3dtv.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '5',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_regular_price' => '499',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
    ),
    '_price' => '499',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#d4e6e5',
    'product_image_layout' => 'image-right',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    '_post_image_attach_id' => '2408',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_default_attributes' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'simple',
    'product_cat' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2411,
  'post_date' => '2013-10-11 22:30:14',
  'post_date_gmt' => '2013-10-11 22:30:14',
  'post_content' => 'Nulla lobortis laoreet risus, tempor accumsan sem congue vitae. Cras laoreet hendrerit erat, id porttitor nunc blandit adipiscing. Sed ac lacus metus, laoreet accumsan ante. In sit amet lacus ipsum, vitae bibendum diam. Donec ac massa ut nisl vulputate egestas ut quis tortor. Vivamus tristique, mi eget convallis vehicula, metus elit suscipit ligula, et porttitor diam nunc ultricies arcu. Vivamus imperdiet semper mi, ullamcorper dapibus nisl sollicitudin quis. Cras lacinia nibh in quam fermentum quis dignissim enim luctus.',
  'post_title' => 'Duo Laptop',
  'post_excerpt' => 'Vivamus imperdiet semper mi, ullamcorper dapibus nisl sollicitudin quis. Cras lacinia nibh in quam fermentum quis dignissim enim luctus.',
  'post_name' => 'duo-laptop',
  'post_modified' => '2017-08-21 07:07:46',
  'post_modified_gmt' => '2017-08-21 07:07:46',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=product&#038;p=2411',
  'menu_order' => 0,
  'post_type' => 'product',
  'meta_input' => 
  array (
    '_edit_lock' => '1503299168:172',
    '_thumbnail_id' => '2412',
    'post_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/laptop_angle.png',
    '_edit_last' => '172',
    '_visibility' => 'visible',
    '_stock_status' => 'instock',
    'total_sales' => '9',
    '_downloadable' => 'no',
    '_virtual' => 'no',
    '_featured' => 'no',
    '_product_attributes' => 
    array (
      'size' => 
      array (
        'name' => 'Size',
        'value' => '13" | 15" | 17"',
        'position' => 0,
        'is_visible' => 0,
        'is_variation' => 1,
        'is_taxonomy' => 0,
      ),
    ),
    '_price' => '722',
    '_stock' => NULL,
    '_backorders' => 'no',
    '_manage_stock' => 'no',
    'background_color' => '#e8e8e8',
    'product_image_layout' => 'image-center',
    'background_repeat' => 'fullcover',
    'builder_switch_frontend' => '0',
    'title_font_color' => '#000000',
    'link_font_color' => '#454545',
    'price_font_color' => '#9c9c9c',
    'description_font_color' => '#808080',
    'button_font_color' => '#000000',
    'button_background_color' => '#ffffff',
    '_min_variation_price' => '722',
    '_max_variation_price' => '1050',
    '_min_variation_regular_price' => '722',
    '_max_variation_regular_price' => '1050',
    '_default_attributes' => 
    array (
    ),
    '_min_price_variation_id' => '2540',
    '_max_price_variation_id' => '2542',
    '_min_regular_price_variation_id' => '2540',
    '_max_regular_price_variation_id' => '2542',
    '_post_image_attach_id' => '2412',
    '_wc_rating_count' => 
    array (
    ),
    '_wc_average_rating' => '0',
    '_wc_review_count' => '0',
    'content_width' => 'default_width',
    '_upsell_ids' => 
    array (
    ),
    '_crosssell_ids' => 
    array (
    ),
    '_download_limit' => '-1',
    '_download_expiry' => '-1',
    '_product_version' => '3.1.1',
    '_yoast_wpseo_content_score' => '30',
  ),
  'tax_input' => 
  array (
    'product_type' => 'variable',
    'product_cat' => 'technology',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2252,
  'post_date' => '2013-10-08 03:22:02',
  'post_date_gmt' => '2013-10-08 03:22:02',
  'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nisi bibendum, iaculis augue a, blandit urna. Morbi imperdiet lorem pulvinar dolor aliquet ullamcorper.

[button link="https://themify.me/demo/themes/flatshop/shop/" style="black outline"]Go to Shop[/button]

&nbsp;',
  'post_title' => 'Flatshop Theme',
  'post_excerpt' => '',
  'post_name' => 'slider-dummy-item',
  'post_modified' => '2017-08-21 07:06:36',
  'post_modified_gmt' => '2017-08-21 07:06:36',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=slider&#038;p=2252',
  'menu_order' => 0,
  'post_type' => 'slider',
  'meta_input' => 
  array (
    'layout' => 'textleft',
    'feature_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/image-right-4.jpg',
    'image_width' => '1000',
    'image_height' => '500',
    'title_font_size_unit' => 'px',
    'title_font_family' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'slider-category' => 'home-slider',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2345,
  'post_date' => '2013-10-02 14:43:02',
  'post_date_gmt' => '2013-10-02 14:43:02',
  'post_content' => 'Quisque vitae purus sit amet dolor dignissim posuere. Nullam tincidunt dictum adipiscing. Cras turpis libero, ultrices eu mollis eu, sagittis sed velit.

[button link="https://themify.me/demo/themes/flatshop/product-category/fashion/" style="white outline"]View Category[/button]',
  'post_title' => 'New Fashion Category',
  'post_excerpt' => '',
  'post_name' => 'slider-dummy-item-2',
  'post_modified' => '2017-08-21 07:06:38',
  'post_modified_gmt' => '2017-08-21 07:06:38',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=slider&#038;p=2345',
  'menu_order' => 0,
  'post_type' => 'slider',
  'meta_input' => 
  array (
    'layout' => 'textcenter',
    'feature_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/image-center1.jpg',
    'image_width' => '1000',
    'image_height' => '500',
    'title_font_size_unit' => 'px',
    'title_font_family' => 'default',
    'title_font_color' => '#ffffff',
    'text_font_color' => '#ffffff',
    'link_font_color' => '#ffffff',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'slider-category' => 'home-slider',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2472,
  'post_date' => '2013-10-15 22:25:42',
  'post_date_gmt' => '2013-10-15 22:25:42',
  'post_content' => '[button style="black outline" link="https://themify.me/themes/flatshop"]Get Theme Now[/button]',
  'post_title' => 'Custom Slider',
  'post_excerpt' => '',
  'post_name' => 'custom-slider',
  'post_modified' => '2017-08-21 07:06:31',
  'post_modified_gmt' => '2017-08-21 07:06:31',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=slider&#038;p=2472',
  'menu_order' => 0,
  'post_type' => 'slider',
  'meta_input' => 
  array (
    'layout' => 'textright',
    'feature_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/image-left-3.jpg',
    'title_font_size_unit' => 'px',
    'title_font_family' => 'default',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'slider-category' => 'page-slider',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2473,
  'post_date' => '2013-10-15 22:21:00',
  'post_date_gmt' => '2013-10-15 22:21:00',
  'post_content' => 'Cras tortor elit, aliquet quis tincidunt eget, dignissim non tortor. Cras a fringilla nunc. Suspendisse volutpat, eros congue scelerisque iaculis, magna odio sodales dui, vitae vulputate elit metus ac arcu. Mauris consequat rhoncus dolor id sagittis.',
  'post_title' => 'Another Slide',
  'post_excerpt' => '',
  'post_name' => 'another-slide',
  'post_modified' => '2017-08-21 07:06:36',
  'post_modified_gmt' => '2017-08-21 07:06:36',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?post_type=slider&#038;p=2473',
  'menu_order' => 0,
  'post_type' => 'slider',
  'meta_input' => 
  array (
    'layout' => 'textcenter',
    'feature_image' => 'https://themify.me/demo/themes/flatshop/files/2013/10/bg-desktop_pair2.jpg',
    'title_font_size_unit' => 'px',
    'title_font_family' => 'default',
    'title_font_color' => '#ffffff',
    'text_font_color' => '#f0f0f0',
    'link_font_color' => '#ffffff',
    'builder_switch_frontend' => '0',
  ),
  'tax_input' => 
  array (
    'slider-category' => 'page-slider',
  ),
  'has_thumbnail' => true,
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2558,
  'post_date' => '2014-02-13 20:46:40',
  'post_date_gmt' => '2014-02-13 20:46:40',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2558',
  'post_modified' => '2014-02-13 21:03:34',
  'post_modified_gmt' => '2014-02-13 21:03:34',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2558',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2102',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'horizontal-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2451,
  'post_date' => '2013-10-14 23:57:38',
  'post_date_gmt' => '2013-10-14 23:57:38',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2451',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/2013/10/14/2451/',
  'menu_order' => 1,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2450',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2557,
  'post_date' => '2014-02-13 20:46:40',
  'post_date_gmt' => '2014-02-13 20:46:40',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2557',
  'post_modified' => '2014-02-13 21:03:34',
  'post_modified_gmt' => '2014-02-13 21:03:34',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2557',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2367',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'horizontal-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2255,
  'post_date' => '2013-10-08 03:32:57',
  'post_date_gmt' => '2013-10-08 03:32:57',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2255',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2255',
  'menu_order' => 2,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2102',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2559,
  'post_date' => '2014-02-13 20:46:40',
  'post_date_gmt' => '2014-02-13 20:46:40',
  'post_content' => '',
  'post_title' => 'Buy Theme',
  'post_excerpt' => '',
  'post_name' => 'buy-theme',
  'post_modified' => '2014-02-13 21:03:34',
  'post_modified_gmt' => '2014-02-13 21:03:34',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2559',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'custom',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2559',
    '_menu_item_object' => 'custom',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
    '_menu_item_url' => 'https://themify.me/themes/flatshop',
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'horizontal-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2276,
  'post_date' => '2013-10-08 04:50:41',
  'post_date_gmt' => '2013-10-08 04:50:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2276',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2276',
  'menu_order' => 3,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2255',
    '_menu_item_object_id' => '2273',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2277,
  'post_date' => '2013-10-08 04:50:41',
  'post_date_gmt' => '2013-10-08 04:50:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2277',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2277',
  'menu_order' => 4,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2255',
    '_menu_item_object_id' => '2271',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2278,
  'post_date' => '2013-10-08 04:50:41',
  'post_date_gmt' => '2013-10-08 04:50:41',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2278',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2102,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2278',
  'menu_order' => 5,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2255',
    '_menu_item_object_id' => '2269',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2384,
  'post_date' => '2013-10-11 20:37:00',
  'post_date_gmt' => '2013-10-11 20:37:00',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2384',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 0,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2384',
  'menu_order' => 6,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '0',
    '_menu_item_object_id' => '2367',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2385,
  'post_date' => '2013-10-11 20:37:00',
  'post_date_gmt' => '2013-10-11 20:37:00',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2385',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2385',
  'menu_order' => 7,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2384',
    '_menu_item_object_id' => '2382',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2386,
  'post_date' => '2013-10-11 20:37:00',
  'post_date_gmt' => '2013-10-11 20:37:00',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2386',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2386',
  'menu_order' => 8,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2384',
    '_menu_item_object_id' => '2380',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}

$post = array (
  'ID' => 2387,
  'post_date' => '2013-10-11 20:37:00',
  'post_date_gmt' => '2013-10-11 20:37:00',
  'post_content' => ' ',
  'post_title' => '',
  'post_excerpt' => '',
  'post_name' => '2387',
  'post_modified' => '2015-10-30 18:42:13',
  'post_modified_gmt' => '2015-10-30 18:42:13',
  'post_content_filtered' => '',
  'post_parent' => 2367,
  'guid' => 'https://themify.me/demo/themes/flatshop/?p=2387',
  'menu_order' => 9,
  'post_type' => 'nav_menu_item',
  'meta_input' => 
  array (
    '_menu_item_type' => 'post_type',
    '_menu_item_menu_item_parent' => '2384',
    '_menu_item_object_id' => '2375',
    '_menu_item_object' => 'page',
    '_menu_item_classes' => 
    array (
      0 => '',
    ),
  ),
  'tax_input' => 
  array (
    'nav_menu' => 'main-nav',
  ),
);
if( ERASEDEMO ) {
	themify_undo_import_post( $post );
} else {
	themify_import_post( $post );
}


function themify_import_get_term_id_from_slug( $slug ) {
	$menu = get_term_by( "slug", $slug, "nav_menu" );
	return is_wp_error( $menu ) ? 0 : (int) $menu->term_id;
}

	$widgets = get_option( "widget_themify-feature-posts" );
$widgets[1002] = array (
  'title' => 'Recent Posts',
  'category' => '14',
  'show_count' => '3',
  'show_date' => 'on',
  'show_thumb' => 'on',
  'display' => 'none',
  'hide_title' => NULL,
  'thumb_width' => '50',
  'thumb_height' => '50',
  'excerpt_length' => '55',
);
update_option( "widget_themify-feature-posts", $widgets );

$widgets = get_option( "widget_themify-twitter" );
$widgets[1003] = array (
  'title' => 'Latest Tweets',
  'username' => 'themify',
  'show_count' => '2',
  'hide_timestamp' => NULL,
  'show_follow' => 'on',
  'follow_text' => '→ Follow me',
  'include_retweets' => 'on',
  'exclude_replies' => NULL,
);
update_option( "widget_themify-twitter", $widgets );

$widgets = get_option( "widget_themify-social-links" );
$widgets[1004] = array (
  'title' => '',
  'show_link_name' => NULL,
  'open_new_window' => NULL,
  'thumb_width' => '',
  'thumb_height' => '',
);
update_option( "widget_themify-social-links", $widgets );

$widgets = get_option( "widget_themify-social-links" );
$widgets[1005] = array (
  'title' => '',
  'show_link_name' => NULL,
  'open_new_window' => NULL,
  'thumb_width' => '',
  'thumb_height' => '',
);
update_option( "widget_themify-social-links", $widgets );



$sidebars_widgets = array (
  'sidebar-main' => 
  array (
    0 => 'themify-feature-posts-1002',
    1 => 'themify-twitter-1003',
  ),
  'social-widget' => 
  array (
    0 => 'themify-social-links-1004',
  ),
  'social-widget-footer' => 
  array (
    0 => 'themify-social-links-1005',
  ),
); 
update_option( "sidebars_widgets", $sidebars_widgets );

$menu_locations = array();
$menu = get_terms( "nav_menu", array( "slug" => "main-nav" ) );
if( is_array( $menu ) && ! empty( $menu ) ) $menu_locations["main-nav"] = $menu[0]->term_id;
$menu = get_terms( "nav_menu", array( "slug" => "horizontal-nav" ) );
if( is_array( $menu ) && ! empty( $menu ) ) $menu_locations["horizontal-menu"] = $menu[0]->term_id;
set_theme_mod( "nav_menu_locations", $menu_locations );


$homepage = get_posts( array( 'name' => 'home', 'post_type' => 'page' ) );
			if( is_array( $homepage ) && ! empty( $homepage ) ) {
				update_option( 'show_on_front', 'page' );
				update_option( 'page_on_front', $homepage[0]->ID );
			}
			
	ob_start(); ?>a:84:{s:16:"setting-page_404";s:1:"0";s:21:"setting-webfonts_list";s:11:"recommended";s:22:"setting-default_layout";s:12:"sidebar-none";s:27:"setting-default_post_layout";s:9:"list-post";s:30:"setting-default_layout_display";s:7:"content";s:25:"setting-default_more_text";s:4:"More";s:21:"setting-index_orderby";s:4:"date";s:19:"setting-index_order";s:4:"DESC";s:31:"setting-image_post_feature_size";s:5:"blank";s:32:"setting-default_page_post_layout";s:8:"sidebar1";s:38:"setting-image_post_single_feature_size";s:5:"blank";s:27:"setting-default_page_layout";s:8:"sidebar1";s:22:"setting-comments_pages";s:2:"on";s:53:"setting-customizer_responsive_design_tablet_landscape";s:4:"1024";s:43:"setting-customizer_responsive_design_tablet";s:3:"768";s:43:"setting-customizer_responsive_design_mobile";s:3:"480";s:33:"setting-mobile_menu_trigger_point";s:4:"1200";s:24:"setting-gallery_lightbox";s:8:"lightbox";s:26:"setting-page_builder_cache";s:2:"on";s:27:"setting-script_minification";s:7:"disable";s:27:"setting-page_builder_expiry";s:1:"2";s:19:"setting-entries_nav";s:8:"numbered";s:26:"setting-store_info_address";s:37:"123 Street Name, City, Province 23446";s:24:"setting-store_info_phone";s:12:"604-112-2323";s:24:"setting-store_info_hours";s:59:"Mon – Fri : 11:00am – 10:00pm, Sat : 11:00am – 2:00pm";s:22:"setting-store_info_map";s:40:"1 Yonge Street
Toronto, Ontario
Canada";s:29:"setting-store_info_zoom_level";s:1:"8";s:18:"setting-more_posts";s:8:"infinite";s:28:"setting-feature_box_category";s:11:"home-slider";s:26:"setting-feature_box_effect";s:5:"slide";s:25:"setting-feature_box_speed";s:4:"2000";s:24:"setting-feature_box_auto";s:1:"0";s:26:"setting-feature_box_slides";s:1:"3";s:25:"setting-feature_box_title";s:4:"show";s:22:"setting-footer_widgets";s:17:"footerwidget-3col";s:19:"setting-shop_layout";s:12:"sidebar-none";s:30:"setting-shop_products_per_page";s:1:"4";s:23:"setting-products_layout";s:9:"list-post";s:23:"setting-hide_shop_count";s:2:"on";s:25:"setting-hide_shop_sorting";s:2:"on";s:23:"setting-hide_shop_title";s:2:"on";s:34:"setting-product_archive_show_short";s:7:"excerpt";s:29:"setting-single_product_layout";s:12:"sidebar-none";s:28:"default_product_image_layout";s:10:"image-left";s:30:"setting-related_products_limit";s:1:"4";s:36:"setting-shop_search_option_preselect";s:4:"post";s:27:"setting-global_feature_size";s:5:"large";s:22:"setting-link_icon_type";s:9:"font-icon";s:32:"setting-link_type_themify-link-0";s:10:"image-icon";s:33:"setting-link_title_themify-link-0";s:7:"Twitter";s:32:"setting-link_link_themify-link-0";s:26:"http://twitter.com/themify";s:32:"setting-link_type_themify-link-1";s:10:"image-icon";s:33:"setting-link_title_themify-link-1";s:8:"Facebook";s:32:"setting-link_link_themify-link-1";s:27:"http://facebook.com/themify";s:32:"setting-link_type_themify-link-2";s:10:"image-icon";s:33:"setting-link_title_themify-link-2";s:7:"Google+";s:32:"setting-link_link_themify-link-2";s:27:"https://plus.google.com/‎";s:32:"setting-link_type_themify-link-3";s:10:"image-icon";s:33:"setting-link_title_themify-link-3";s:7:"YouTube";s:32:"setting-link_link_themify-link-3";s:37:"http://www.youtube.com/user/themifyme";s:32:"setting-link_type_themify-link-4";s:10:"image-icon";s:33:"setting-link_title_themify-link-4";s:9:"Pinterest";s:32:"setting-link_link_themify-link-4";s:20:"http://pinterest.com";s:32:"setting-link_type_themify-link-5";s:9:"font-icon";s:33:"setting-link_title_themify-link-5";s:7:"Twitter";s:32:"setting-link_link_themify-link-5";s:26:"http://twitter.com/themify";s:33:"setting-link_ficon_themify-link-5";s:10:"fa-twitter";s:32:"setting-link_type_themify-link-6";s:9:"font-icon";s:33:"setting-link_title_themify-link-6";s:8:"Facebook";s:32:"setting-link_link_themify-link-6";s:27:"http://facebook.com/themify";s:33:"setting-link_ficon_themify-link-6";s:11:"fa-facebook";s:32:"setting-link_type_themify-link-7";s:9:"font-icon";s:33:"setting-link_title_themify-link-7";s:7:"Google+";s:32:"setting-link_link_themify-link-7";s:27:"https://plus.google.com/‎";s:33:"setting-link_ficon_themify-link-7";s:14:"fa-google-plus";s:32:"setting-link_type_themify-link-8";s:9:"font-icon";s:33:"setting-link_title_themify-link-8";s:7:"YouTube";s:32:"setting-link_link_themify-link-8";s:37:"http://www.youtube.com/user/themifyme";s:33:"setting-link_ficon_themify-link-8";s:10:"fa-youtube";s:22:"setting-link_field_ids";s:307:"{"themify-link-0":"themify-link-0","themify-link-1":"themify-link-1","themify-link-2":"themify-link-2","themify-link-3":"themify-link-3","themify-link-4":"themify-link-4","themify-link-5":"themify-link-5","themify-link-6":"themify-link-6","themify-link-7":"themify-link-7","themify-link-8":"themify-link-8"}";s:23:"setting-link_field_hash";s:2:"10";s:30:"setting-page_builder_is_active";s:6:"enable";s:46:"setting-page_builder_animation_parallax_scroll";s:6:"mobile";s:4:"skin";s:91:"https://themify.me/demo/themes/flatshop/wp-content/themes/flatshop/themify/img/non-skin.gif";}<?php $themify_data = unserialize( ob_get_clean() );

	// fix the weird way "skin" is saved
	if( isset( $themify_data['skin'] ) ) {
		$parsed_skin = parse_url( $themify_data['skin'], PHP_URL_PATH );
		$basedir_skin = basename( dirname( $parsed_skin ) );
		$themify_data['skin'] = trailingslashit( get_template_directory_uri() ) . 'skins/' . $basedir_skin . '/style.css';
	}

	themify_set_data( $themify_data );
	
}
themify_do_demo_import();