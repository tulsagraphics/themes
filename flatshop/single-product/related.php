<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( isset( $_GET['post_in_lightbox'] ) && 1 == $_GET['post_in_lightbox'] ) exit;

global $woocommerce_loop, $themify;

$grid = themify_get( 'setting-related_products_limit' );

switch ( $grid ) {
	case $grid % 4 == 0:
		$grid = 'grid4';
		break;
	case $grid % 3 == 0:
		$grid = 'grid3';
		break;
	case $grid % 2 == 0:
		$grid = 'grid2';
		break;
	default:
		$grid = 'grid3';
		break;
}

$themify->is_related_loop = true;

if ( !empty( $related_products ) ) : ?>

	<div class="related products <?php echo $grid; ?> noisotope <?php echo 'sidebar-none' == $themify->layout? 'pagewidth' : ''; ?>">

		<h2 class=""><?php _e( 'Related Products', 'woocommerce' ); ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

				<?php
				 	$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>

<?php endif;

$themify->is_related_loop = false;