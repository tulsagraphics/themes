<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// if ( ! $upsells = $product->get_upsell_ids() ) {
// 	return;
// }

$grid = themify_get( 'setting-related_products_limit' );

switch ( $grid ) {
	case $grid % 4 == 0:
		$grid = 'grid4';
		break;
	case $grid % 3 == 0:
		$grid = 'grid3';
		break;
	case $grid % 2 == 0:
		$grid = 'grid2';
		break;
	default:
		$grid = 'grid3';
		break;
}

if ( ! empty( $upsells ) ) : ?>

	<div class="upsells products <?php echo $grid; ?> clearfix noisotope <?php echo 'sidebar-none' == $themify->layout? 'pagewidth' : ''; ?>">

		<h2 class=""><?php _e( 'You may also like&hellip;', 'woocommerce' ) ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $upsells as $upsell ) : ?>

				<?php
				 	$post_object = get_post( $upsell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>

<?php endif;

wp_reset_postdata();
