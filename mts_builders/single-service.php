<?php
get_header();
$mts_options = get_option(MTS_THEME_NAME);
$lists = get_post_meta( get_the_ID(), 'mts_service_info_group', true );
?>

<div class="sub-header clearfix">
  <div class="container">
	<h2 class="title"><?php the_title(); ?></h2>
  </div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>

<div id="page" class="services">
	<div class="article services-single">
		<div id="content_box">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post();
				if ( !empty($lists) && count($lists) >= 1 ) { ?>
					<div class="designing">
						<div class="lists">
							<ul>
								<?php foreach ( (array) $lists as $key => $list ) :
									$list_content = $list_url = '';
									if ( isset( $list['value'] ) ) $list_content = esc_html( $list['value'] );
									if ( isset( $list['url'] ) ) $list_url = esc_html( $list['url'] ); ?>
									<?php if ( !empty( $list_content ) || !empty( $list_url ) ) { ?>
										<li>
											<div class="designing-wrapper">
												<div class="icon"><i class="fa fa-check-square-o"></i></div>
												<?php if( !empty($list_url) ) : ?>
													<a href="<?php echo $list_url; ?>" title="<?php echo $list_content; ?>">
												<?php endif;
													echo $list_content;
												if( !empty($list_url) ) : ?>
													</a>
												<?php endif; ?>
											</div><!-- /designing-wrapper -->
										</li>
									<?php } ?>
								<?php endforeach; ?>
						   </ul>
						</div><!-- /lists -->
					</div><!-- /designing -->
				<?php }

				$files = get_post_meta( get_the_ID(), 'mts_service_gallery', 1 );
				$args = array(
					'post_type'   => 'attachment',
					'numberposts' => -1,
					'post_parent' => get_the_ID(),
					'order' => 'ASC',
					'orderby' => 'menu_order',
					'post_mime_type' => 'image'
				);
				$args = apply_filters( 'builders_single_project_images_args', $args, get_the_ID() );
				$attachments = get_posts( $args );

				if($files) { ?>

					<div class="building">
						<div class="building-container featured-view-posts loading active">
							<div id="slider" class="building-slider">
								<?php	// Loop through them and output an image
								foreach ( (array) $files as $attachment_id => $attachment_url ) {
									$attachment_img = wp_get_attachment_image_src($attachment_id, 'builders-services-slider');
									$attachment_url = $attachment_img[0];
									$image_src	  = $attachment_url; ?>
									<img src="<?php echo $image_src; ?>" width="370px" height="363" />
								   <?php } ?>
							</div>
						</div>
					</div>

				<?php } elseif ( $attachments && count($attachments) > 1 ) { ?>

					<div class="building">
						<div class="building-container featured-view-posts loading active">
							<div id="slider" class="building-slider">
								<?php foreach ( $attachments as $attachment ) {
									$id = $attachment->ID;
									$attachment_img = wp_get_attachment_image_src($id, 'builders-services-slider');
									$attachment_url = $attachment_img[0];
									$image_src	  = $attachment_url; ?>
									<img src="<?php echo $image_src; ?>" width="370px" height="363" />
								<?php } ?>
							</div>
						</div><!-- builder-slider-container -->
					</div>

				<?php } ?>

				<div class="single_post">
					<div class="post-single-content box mark-links entry-content">
						<div class="thecontent">
							<?php the_content(); ?>
						</div>
						<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => __('Next', 'builders' ), 'previouspagelink' => __('Previous', 'builders' ), 'pagelink' => '%','echo' => 1 ));
						if ($mts_options['mts_postend_adcode'] != '') {
							$endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
								<div class="bottomad">
									<?php echo do_shortcode($mts_options['mts_postend_adcode']); ?>
								</div>
							<?php }
						} ?> 
					</div><!--.post-single-content-->
				</div><!--.single_post-->

			<?php endwhile; /* end loop */ ?>   
		</div>
	</div>
	<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
		<div id="categories-2" class="widget widget_categories widget_services">
			<ul>
				<li><a href="<?php echo esc_url( get_post_type_archive_link( 'service' ) ); ?>" title="Posts"><i class="fa fa-ellipsis-h"></i><?php _e('All Services','builders'); ?></a></li>
				<?php $terms = get_terms( 'mts_service_categories' );
				foreach ( $terms as $term ) {
					$single_term = wp_get_post_terms( $post->ID, 'mts_service_categories');
					if($single_term[0]->term_id == $term->term_id) {
						$active_class = 'active';
					} else {
						$active_class = '';
					}
					// The $term is an object, so we don't need to specify the $taxonomy.
					$term_link = get_term_link( $term );
					// If there was an error, continue to the next term.
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					echo '<li class="'.$active_class.'"><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
				} ?>
			</ul>
		</div>
		<div id="categories-2" class="widget widget_categories widget_brochure">
		<?php if ( !empty($mts_options['mts_brochures']) && is_array($mts_options['mts_brochures']) ) { ?>
			<h3 class="widget-title"><?php _e('Brochures', 'builders') ?></h3>
			<ul>
				<?php foreach( $mts_options['mts_brochures'] as $brochure ) : ?>
					<li class="cat-item">
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							<a href="<?php echo $brochure['mts_brochures_link']; ?>" title="<?php echo $brochure['mts_brochures_title']; ?>">
						<?php endif; ?>
								<i class="fa fa-<?php print $brochure['mts_brochures_icon'] ?>"></i><?php echo $brochure['mts_brochures_title']; ?>
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							</a>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php } ?>
		</div><!-- /widget widget_services -->
	</aside><!--#sidebar-->
<?php get_footer(); ?>