<?php
/**
 * The template for displaying search results pages.
 */
$mts_options = get_option(MTS_THEME_NAME);
get_header(); ?>
<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><span><?php _e("Search Results for:", 'builders' ); ?></span> <?php the_search_query(); ?></h2>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>
<div id="page">
	<div class="article">
		<div id="content_box">
			<?php $j = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="latestPost excerpt  <?php echo (++$j % 2 == 0) ? 'last' : ' first'; ?>">
					<?php mts_archive_post(); ?>
				</article><!--.post excerpt-->
			<?php endwhile; else: ?>
				<div class="no-results">
					<h2><?php _e('We apologize for any inconvenience, please hit back on your browser or use the search form below.', 'builders' ); ?></h2>
					<?php get_search_form(); ?>
				</div><!--noResults-->
			<?php endif;

			if ( $j !== 0 ) { // No pagination if there is no posts
				mts_pagination();
			} ?>
		</div>
	</div>
	<?php get_sidebar();
get_footer(); ?>