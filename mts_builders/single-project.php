<?php get_header();
$mts_options = get_option(MTS_THEME_NAME);
$entries = get_post_meta( get_the_ID(), 'mts_project_info_group', true );
$project_des = get_post_meta( get_the_ID(), 'mts_project_info_des', true );
?>

<div class="sub-header clearfix">
  <div class="container">
	<h2 class="title"><?php the_title(); ?></h2>
  </div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>

<div id="page" class="project-single">
	<div class="article <?php if(!$entries && !$project_des) echo 'ss-full-width'; ?>">
		<div id="content_box">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="single_post">
					<div class="post-single-content box mark-links entry-content">
						<?php if( mts_get_thumbnail_url() ) : ?>
							<div class="single-project-image" <?php echo 'style="background-image: url('.mts_get_thumbnail_url().'); min-height: 300px; background-position: center; background-repeat: no-repeat; background-size: cover;"'; ?>></div>
						<?php endif; ?>
						<div class="thecontent">
							<?php the_content(); ?>
						</div>
						<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => __('Next', 'builders' ), 'previouspagelink' => __('Previous', 'builders' ), 'pagelink' => '%','echo' => 1 ));
						if ($mts_options['mts_postend_adcode'] != '') {
							$endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
								<div class="bottomad">
									<?php echo do_shortcode($mts_options['mts_postend_adcode']); ?>
								</div>
							<?php }
						} ?> 
					</div><!--.post-single-content-->
				</div><!--.single_post-->
			<?php endwhile; /* end loop */ ?>					  
		</div>
	</div>

	<?php if($entries || $project_des) { ?>
		<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
			<?php if($entries) { ?>
				<div id="categories-2" class="widget widget_categories widget_project_details">
					<h3 class="widget-title"><?php _e('Project Details', 'builders') ?></h3>
					<ul>
					 	<?php foreach ( (array) $entries as $key => $entry ) {
							$icon = $label = $url = '';
							if ( isset( $entry['icon'] ) ) $icon = esc_html( $entry['icon'] );
							if ( isset( $entry['label'] ) ) $label = esc_html( $entry['label'] );
							if ( isset( $entry['url'] ) ) $url = esc_html( $entry['url'] );

							if ( !empty( $label ) ) { ?>
									<li class="cat-item">
										<?php if( !empty($url) ) : ?>
											<a href="<?php echo $url; ?>" title="Posts">
										<?php endif; ?>
											<i class="fa fa-<?php print isset($icon) ? $icon : ''; ?>"></i><?php echo $label; ?>
										<?php if( !empty($url) ) : ?>
											</a>
										<?php endif; ?>
									</li>
							<?php }
						} ?>
					</ul>
				</div>
			<?php } ?>

			<?php if($project_des) { ?>
				<div id="text-6" class="widget widget_text">
					<h3 class="widget-title"><?php _e('Project Description', 'builders') ?></h3>
					<?php if( isset($project_des) ) : ?>
						<div class="textwidget"><?php echo $project_des; ?></div>
					<?php endif; ?>
				</div>
			<?php } ?>

			<div class="pagination pagination-previous-next">
				<ul class="">
					<li class="nav-previous"><?php previous_post_link('%link', '<i class="fa fa-angle-left"></i><span>Prev</span>'); ?></li>
					<li class="nav-next"><?php next_post_link('%link', '<span>Next</span><i class="fa fa-angle-right"></i>'); ?></li>
				</ul>
			</div>
		</aside><!--#sidebar-->
	<?php } ?>

		<div class="related-posts-projects">
			<?php
			// related projects based on categories
			$post_id = get_the_ID();
			$categories = get_the_terms($post_id, 'mts_project_categories');
			$category_ids = array();
			if ( $categories && !is_wp_error( $categories ) ) {
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			}
			$args = array(
				'post_type' => 'project',
				'tax_query' => array(
					array(
						'taxonomy' => 'mts_project_categories',
						'field'    => 'term_id',
						'terms'    => $category_ids,
					),
				),
				'post_status' => 'publish',
				'posts_per_page' => 3,  
				'ignore_sticky_posts' => 1, 
				'orderby' => 'rand'
			);
			$my_query = new WP_Query( $args ); if( $my_query->have_posts() ) {
			echo '<h4>'.__('Related Projects', 'builders' ).'</h4>';
			echo '<div class="clear">';
			$j = 0;
			
			while( $my_query->have_posts() ) { $my_query->the_post();
			$location = get_post_meta( get_the_ID(), 'mts_project_info_location', true ); ?>
			<div class="mix ">
				<a href="<?php the_permalink(); ?>" class="project-image">
					<img src="<?php echo mts_get_thumbnail_url( 'builders-project' ); ?>" width="370" height="230">
					<div class="project-caption">
						<div class="readMore"><?php _e( 'View', 'builders' ); ?></div>
					</div>
				</a>
				<div class="mix-content">
					<?php if ( !empty($location) ) : ?>
						<div class="post-info"><span class="theplace"><i class="fa fa-globe"></i><?php echo esc_html($location); ?></span></div>
					<?php endif; ?>
					<h5 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
				</div>
			</div><!--mix interior-design-->
			<?php } echo '</div>'; } wp_reset_postdata(); ?>
		</div><!--related-posts-project-->

<?php get_footer(); ?>