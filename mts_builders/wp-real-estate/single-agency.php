<?php
/**
 * The Template for displaying a single agency
 *
 * This template can be overridden by copying it to yourtheme/wp-real-estate/single-listing.php.
 *
 */
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

get_header('wp-real-estate'); ?>

	<div id="page" class="<?php mts_single_page_class(); ?>">
		<article class="ss-full-width">

			<?php
			/**
			 * @hooked wre_output_content_wrapper (outputs opening divs for the content)
			 *
			 */
			do_action('wre_before_main_content');

				while (have_posts()) : the_post();

					wre_get_part('content-single-agency.php');

				endwhile;

			/**
			 * @hooked wre_output_content_wrapper_end (outputs closing divs for the content)
			 *
			 */
			do_action('wre_after_main_content');
			//do_action('wre_sidebar'); ?>

		</article>
	</div>

<?php get_footer('wp-real-estate');