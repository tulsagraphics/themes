<?php
/**
 * The Template for displaying the listings archive
 *
 * This template can be overridden by copying it to yourtheme/wp-real-estate/archive-listing.php.
 *
 */
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

get_header('wp-real-estate'); ?>

	<div id="page"> 

		<?php
		/**
		 * @hooked wre_output_content_wrapper (outputs opening divs for the content)
		 *
		 */
		do_action('wre_before_main_content');

		if (apply_filters('wre_show_page_title', true)) :
			?>

			<h2 class="page-title"><?php wre_page_title(); ?></h2>

		<?php
		endif;

		if (have_posts()) :

			/**
			 * @hooked wre_ordering (the ordering dropdown)
			 * @hooked wre_pagination (the pagination)
			 * 
			 */
			do_action('wre_before_agency_loop');

				$default_listing_mode = wre_default_display_mode();
				echo '<div id="wre-archive-wrapper">';
					echo '<ul class="wre-items '. esc_attr( $default_listing_mode ) .'">';

						while (have_posts()) : the_post();
							wre_get_part('content-agency.php');
						endwhile;

					echo '</ul>';
					echo '<div class="wre-orderby-loader"><img src="'. WRE_PLUGIN_URL .'assets/images/loading.svg" /></div>';
				echo '</div>';

			/**
			 * @hooked wre_pagination (the pagination)
			 * 
			 */
			do_action('wre_after_agency_loop');

			else :
			?>

				<p class="alert wre-no-results"><?php _e('Sorry, no agenices were found.', 'wp-real-estate'); ?></p>

			<?php
			endif;

		/**
		 * @hooked wre_output_content_wrapper_end (outputs closing divs for the content)
		 *
		 */
		do_action('wre_after_main_content'); ?>

	</div>

<?php get_footer('wp-real-estate');