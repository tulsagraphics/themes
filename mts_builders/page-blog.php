<?php
/**
 * Template Name: Blog Page
 */
$mts_options = get_option(MTS_THEME_NAME);
get_header(); ?>

<?php if($mts_options['mts_blog_subheader']) { ?>
<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><?php echo $mts_options['mts_blog_subheader_title']; ?></h2>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>
<?php } ?> 

<div id="page" class="blog-page">
	<div class="article">
		<div id="content_box"<?php if ($mts_options['mts_blog_post_layout'] == 1) { echo ' class="blog2"'; } ?>>
			<?php
			if ( get_query_var('paged') && get_query_var('paged') > 1 ){
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') && get_query_var('page') > 1  ){
				$paged = get_query_var('page');
			} else {
				$paged = 1;
			}

			$args = array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'paged' => $paged,
				'ignore_sticky_posts'=> 1,
			);
			$latest_posts = new WP_Query( $args );

			global $wp_query;
			// Put default query object in a temp variable
			$tmp_query = $wp_query;
			// Now wipe it out completely
			$wp_query = null;
			// Re-populate the global with our custom query
			$wp_query = $latest_posts;
			if ( !is_paged() ) {
				$featured_categories = array();
				if ( !empty( $mts_options['mts_featured_categories'] ) ) {
					foreach ( $mts_options['mts_featured_categories'] as $section ) {
						$category_id = $section['mts_featured_category'];
						$featured_categories[] = $category_id;
						$posts_num = $section['mts_featured_category_postsnum'];
						if ( 'latest' == $category_id ) {
							
							if( $latest_posts->have_posts() ) :

							$j = 0; while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>
								<article class="latestPost excerpt<?php echo (++$j % 2 == 0) ? ' last' : ' first'; ?>">
									<?php mts_archive_post();
									if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
								</article>
							<?php endwhile; endif;
							
							if ( $j !== 0 ) { // No pagination if there is no posts
								mts_pagination();
							}
							
						} else { // if $category_id != 'latest': ?>
							<h3 class="featured-category-title featured-title"><a href="<?php echo esc_url( get_category_link( $category_id ) ); ?>" title="<?php echo esc_attr( get_cat_name( $category_id ) ); ?>"><?php echo esc_html( get_cat_name( $category_id ) ); ?></a></h3>
							<?php
							$j = 0;
							$cat_query = new WP_Query('cat='.$category_id.'&posts_per_page='.$posts_num);
							if ( $cat_query->have_posts() ) : while ( $cat_query->have_posts() ) : $cat_query->the_post(); ?>
								<article class="latestPost excerpt <?php echo (++$j % 2 == 0) ? 'last' : ' first'; ?>">
									<?php mts_archive_post();
								if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
								</article>
							<?php endwhile; endif; wp_reset_postdata();
						}
					}
				}
			} else { //Paged
				$j = 0;
				if( $latest_posts->have_posts() ) :

				while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>
					<article class="latestPost excerpt<?php echo (++$j % 2 == 0) ? ' last' : ' first'; ?>">
						<?php mts_archive_post();
						if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
					</article>
				<?php endwhile; endif;
				
				if ( $j !== 0 ) { // No pagination if there is no posts
					mts_pagination();
				}
			}
			// Restore original query object
			$wp_query = $tmp_query;
			// Be kind; rewind
			wp_reset_postdata(); ?>
		</div>
	</div>
	<?php get_sidebar();
get_footer(); ?>