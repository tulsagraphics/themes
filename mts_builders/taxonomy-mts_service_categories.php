<?php
/**
 * The template for displaying archive pages.
 *
 * Used for displaying archive-type pages. These views can be further customized by
 * creating a separate template for each one.
 *
 * - author.php (Author archive)
 * - category.php (Category archive)
 * - date.php (Date archive)
 * - tag.php (Tag archive)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
$mts_options = get_option(MTS_THEME_NAME);
$queried_object = get_queried_object();
get_header(); ?>
<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><?php echo $queried_object->name; ?></h2>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>
<div id="page" class="blog-page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box"<?php if ($mts_options['mts_blog_post_layout'] == 1) { echo ' class="blog2"'; } ?>>
			<?php $j = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="latestPost excerpt<?php echo (++$j % 2 == 0) ? ' last' : ' first'; ?>">
					<?php mts_archive_post();
					if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
				</article>
			<?php endwhile; endif;

			if ( $j !== 0 ) { // No pagination if there is no posts
				mts_pagination('', 3, 'mts_service_pagenavigation_type');
			} ?>
		</div>
	</div>
	<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
		<div id="categories-2" class="widget widget_categories widget_services">
			<ul>
				<li><a href="<?php echo esc_url( get_post_type_archive_link( 'service' ) ); ?>" title="Posts"><i class="fa fa-ellipsis-h"></i><?php _e('All Services','builders'); ?></a></li>
				<?php
				
				$current_term_id = $queried_object->term_id;
				$terms = get_terms( 'mts_service_categories' );
				foreach ( $terms as $term ) {
					// The $term is an object, so we don't need to specify the $taxonomy.
					$term_link = get_term_link( $term );
					// If there was an error, continue to the next term.
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					if($current_term_id == $term->term_id) {
						$active_class = ' class="active"';
					} else {
						$active_class = '';
					}
					echo '<li'.$active_class.'><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
				} ?>
			</ul>
		</div>
		<div id="categories-2" class="widget widget_categories widget_brochure">
		<?php if ( !empty($mts_options['mts_brochures']) && is_array($mts_options['mts_brochures']) ) { ?>
			<h3 class="widget-title"><?php _e('Brochures', 'builders') ?></h3>
			<ul>
				<?php foreach( $mts_options['mts_brochures'] as $brochure ) : ?>
					<li class="cat-item">
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							<a href="<?php echo $brochure['mts_brochures_link']; ?>" title="<?php echo $brochure['mts_brochures_title']; ?>">
						<?php endif; ?>
								<i class="fa fa-<?php print $brochure['mts_brochures_icon'] ?>"></i><?php echo $brochure['mts_brochures_title']; ?>
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							</a>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php } ?>
		</div><!-- /widget widget_services -->
	</aside><!--#sidebar-->
<?php get_footer(); ?>