<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * HTML Template Email Request a Quote
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @version 1.3.4
 * @author  Yithemes
 */

$order_id          = $raq_data['order_id'];
$order             = wc_get_order( $order_id );
$customer          = yit_get_prop( $order, '_customer_user', true );
$page_detail_admin = ( get_option( 'ywraq_quote_detail_link' ) == 'editor' ) ? true : false;
$quote_number      = apply_filters( 'ywraq_quote_number', $raq_data['order_id'] );
do_action( 'woocommerce_email_header', $email_heading, $email );

?>

<p><?php echo $email_description ?></p>


<?php
wc_get_template( 'emails/request-quote-table.php', array(
	'raq_data' => $raq_data
), '', YITH_YWRAQ_TEMPLATE_PATH . '/' );
?>
<p></p>

<?php if ( ( $customer != 0 && ( get_option( 'ywraq_enable_link_details' ) == "yes" && get_option( 'ywraq_enable_order_creation', 'yes' ) == 'yes' ) ) || ( $page_detail_admin && get_option( 'ywraq_enable_order_creation', 'yes' ) == 'yes' ) ): ?>
	<p><?php printf( __( 'You can see details here: <a href="%s">#%s</a>', 'yith-woocommerce-request-a-quote' ), YITH_YWRAQ_Order_Request()->get_view_order_url( $order_id, $page_detail_admin ), $quote_number ); ?></p>
<?php endif ?>


<?php if ( ! empty( $raq_data['user_message'] ) ): ?>
	<h2><?php _e( 'Customer\'s message', 'yith-woocommerce-request-a-quote' ); ?></h2>
	<p><?php echo $raq_data['user_message'] ?></p>
<?php endif ?>
<h2><?php _e( 'Customer\'s details', 'yith-woocommerce-request-a-quote' ); ?></h2>

<?php

$country_code = isset( $raq_data['user_country'] ) ? $raq_data['user_country'] : '';

foreach ( $raq_data as $key => $field ) {

	$avoid_key = array( 'customer_id', 'raq_content', 'user_country', 'user_message', 'user_email', 'user_name', 'order_id', 'lang', 'message', 'user_additional_field', 'user_additional_field_2', 'user_additional_field_3' );

	if ( in_array( $key, $avoid_key ) ) {
		continue;
	}

	switch ( $field['type'] ) {

		case 'ywraq_heading':
			?><h3><?php echo $field['label'] ?></h3><?php
			break;

		case 'email':
			?><p><strong><?php echo $field['label']; ?></strong>: <a href="mailto:<?php echo $field['value']; ?>"><?php echo $field['value']; ?></a></p><?php
			break;

		case 'country':
			$countries = WC()->countries->get_countries();
			?><p><strong><?php echo $field['label']; ?></strong>: <?php echo $countries[ $country_code ] ?></p><?php
			break;

		case 'state':
			$states = WC()->countries->get_states( $country_code );
			$state = $states[ $field['value'] ]
			?><p><strong><?php echo $field['label']; ?></strong>: <?php echo( $state == '' ? $field['value'] : $state ) ?></p><?php
			break;

		case 'checkbox':
			?><p><strong><?php echo $field['label']; ?></strong>: <?php echo( $field['value'] == 1 ? __( 'Yes', 'yith-woocommerce-request-a-quote' ) : __( 'No', 'yith-woocommerce-request-a-quote' ) ); ?></p><?php
			break;

		case 'ywraq_multiselect':
			?><p><strong><?php echo $field['label']; ?></strong>: <?php echo implode( ', ', $field['value'] ); ?></p><?php
			break;

		default:
			?><p><strong><?php echo $field['label']; ?></strong>: <?php echo $field['value']; ?></p><?php

	}

}

?>

<?php if ( ! empty( $raq_data['user_additional_field'] ) || ! empty( $raq_data['user_additional_field_2'] ) || ! empty( $raq_data['user_additional_field_3'] ) ): ?>
	<h2><?php _e( 'Customer\'s additional fields', 'yith-woocommerce-request-a-quote' ); ?></h2>

	<?php if ( ! empty( $raq_data['user_additional_field'] ) ): ?>
		<p><?php printf( '<strong>%s</strong>: %s', get_option( 'ywraq_additional_text_field_label' ), $raq_data['user_additional_field'] ) ?></p>
	<?php endif ?>

	<?php if ( ! empty( $raq_data['user_additional_field_2'] ) ): ?>
		<p><?php printf( '<strong>%s</strong>: %s', get_option( 'ywraq_additional_text_field_label_2' ), $raq_data['user_additional_field_2'] ) ?></p>
	<?php endif ?>

	<?php if ( ! empty( $raq_data['user_additional_field_3'] ) ): ?>
		<p><?php printf( '<strong>%s</strong>: %s', get_option( 'ywraq_additional_text_field_label_3' ), $raq_data['user_additional_field_3'] ) ?></p>
	<?php endif ?>

<?php endif ?>
<?php

do_action( 'woocommerce_email_footer', $email );

?>
