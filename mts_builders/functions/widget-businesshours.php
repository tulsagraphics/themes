<?php
/*-----------------------------------------------------------------------------------

  Plugin Name: MyThemeShop Business Hours Widget
  Version: 1.0
  
-----------------------------------------------------------------------------------*/
class Business_Hours_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
	parent::__construct(
	  'business_hours_widget', // Base ID
	  __( 'MTS Business Hours', 'builders' ), // Name
	  array( 'description' => __( 'Add Business Hours here', 'builders' ), ) // Args
	);
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args	 Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
	echo $args['before_widget'];
	if ( ! empty( $instance['title'] ) ) {
	  echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
	}

	echo '<div class="businesshours">';
	  if ( ! empty( $instance['content'] ) ) { echo '<p>' . $instance['content'] . '</p>'; }
	  echo "<ul>";
		if ( ! empty( $instance['weekdays'] ) ) { echo '<li><span class="left">' . __( 'Monday - Friday', 'builders' ) . '</span><span class="right">' . $instance['weekdays'] . '</span></li>'; }
		if ( ! empty( $instance['weekend'] ) ) { echo '<li><span class="left">' . __( 'Saturday', 'builders' ) . '</span><span class="right">' . $instance['weekend'] . '</span></li>'; }
		if ( ! empty( $instance['sunday'] ) ) { echo '<li><span class="left">' . __( 'Sunday', 'builders' ) . '</span><span class="right">' . $instance['sunday'] . '</span></li>'; }
	  echo "</ul>";
	echo '</div>';

	echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
	$defaults = array( 'title' => '', 'content' => '', 'weekdays' => '', 'weekend' => '', 'sunday' => '' );
	$instance = wp_parse_args( (array) $instance, $defaults );
	$title = $instance['title'];
	$content = $instance['content'];
	$weekdays = $instance['weekdays'];
	$weekend = $instance['weekend'];
	$sunday = $instance['sunday'];
	?>
	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php _e( 'Text Content:', 'builders' ); ?></label> 
	<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" type="text" rows="4"><?php echo esc_attr( $content ); ?></textarea>
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'weekdays' ) ); ?>"><?php _e( 'Monday - Friday:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'weekdays' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'weekdays' ) ); ?>" type="text" value="<?php echo esc_attr( $weekdays ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'weekend' ) ); ?>"><?php _e( 'Saturday:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'weekend' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'weekend' ) ); ?>" type="text" value="<?php echo esc_attr( $weekend ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'sunday' ) ); ?>"><?php _e( 'Sunday:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'sunday' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sunday' ) ); ?>" type="text" value="<?php echo esc_attr( $sunday ); ?>">
	</p>
	<?php 
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
	$instance = $old_instance;
	$instance['title'] = strip_tags( $new_instance['title'] );
	$instance['content'] = strip_tags( $new_instance['content'] );
	$instance['weekdays'] = strip_tags( $new_instance['weekdays'] );
	$instance['weekend'] = strip_tags( $new_instance['weekend'] );
	$instance['sunday'] = strip_tags( $new_instance['sunday'] );

	return $instance;
  }

} // class Business_Hours_Widget

// register Business_Hours_Widget
function register_business_hours_widget() {
	register_widget( 'Business_Hours_Widget' );
}
add_action( 'widgets_init', 'register_business_hours_widget' );