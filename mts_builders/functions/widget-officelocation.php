<?php
/*-----------------------------------------------------------------------------------

  Plugin Name: MyThemeShop Office Location Widget
  Version: 1.0
  
-----------------------------------------------------------------------------------*/
class Office_Location_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
	parent::__construct(
	  'office_location_widget', // Base ID
	  __( 'MTS Office Location', 'builders' ), // Name
	  array( 'description' => __( 'Add Office Location Here', 'builders' ), ) // Args
	);
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args	 Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
	echo $args['before_widget'];
	if ( ! empty( $instance['title'] ) ) {
		echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
	}

	echo '<div class="ouroffice">';
		echo "<ul>";
			if ( ! empty( $instance['location'] ) || ! empty( $instance['location_2'] ) ) { 
				echo '<li><i class="fa fa-location-arrow"></i>';
				if ( ! empty( $instance['location'] ) ) { echo '<strong>' . $instance['location'] . '</strong>'; }
				if ( ! empty( $instance['location_2'] ) ) { echo '<span>' . $instance['location_2'] . '</span>'; }
			echo '</li>';
			}
			if ( ! empty( $instance['phone'] ) ) { echo '<li><i class="fa fa-phone"></i>' . $instance['phone'] . '</li>'; }
			if ( ! empty( $instance['fax'] ) ) { echo '<li><i class="fa fa-print"></i>' . $instance['fax'] . '</li>'; }
			if ( ! empty( $instance['mail'] ) ) { echo '<li><a href="mailto:' . $instance['mail'] . '"><i class="fa fa-envelope"></i>' . $instance['mail'] . '</a></li>'; }
		echo "</ul>";
	echo '</div>';

	echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
  	$defaults = array( 'title' => '', 'location' => '', 'location_2' => '', 'phone' => '', 'fax' => '', 'mail' => '' );
	$instance = wp_parse_args( (array) $instance, $defaults );
	$title = $instance['title'];
	$location = $instance['location'];
	$location_2 = $instance['location_2'];
	$phone = $instance['phone'];
	$fax = $instance['fax'];
	$mail = $instance['mail'];
	?>
	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>"><?php _e( 'Location Line 1:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'location' ) ); ?>" type="text" value="<?php echo esc_attr( $location ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'location_2' ) ); ?>"><?php _e( 'Location Line 2:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'location_2' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'location_2' ) ); ?>" type="text" value="<?php echo esc_attr( $location_2 ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php _e( 'Phone:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>"><?php _e( 'Fax:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'fax' ) ); ?>" type="text" value="<?php echo esc_attr( $fax ); ?>">
	</p>

	<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>"><?php _e( 'Mail:', 'builders' ); ?></label> 
	<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mail' ) ); ?>" type="text" value="<?php echo esc_attr( $mail ); ?>">
	</p>
	<?php 
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
	$instance = $old_instance;
	$instance['title'] = strip_tags( $new_instance['title'] );
	$instance['location'] = strip_tags( $new_instance['location'] );
	$instance['location_2'] = strip_tags( $new_instance['location_2'] );
	$instance['phone'] = strip_tags( $new_instance['phone'] );
	$instance['fax'] = strip_tags( $new_instance['fax'] );
	$instance['mail'] = strip_tags( $new_instance['mail'] );

	return $instance;
  }

} // class Office_Location_Widget

// register Office_Location_Widget
function register_office_location_widget() {
	register_widget( 'Office_Location_Widget' );
}
add_action( 'widgets_init', 'register_office_location_widget' );