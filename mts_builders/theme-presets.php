<?php
// make sure to not include translations
$args['presets']['default'] = array(
	'title' => 'Default',
	'demo' => 'http://demo.mythemeshop.com/builders/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/default/thumb.jpg',
	'menus' => array( 'primary' => 'Primary Menu', 'secondary' => 'Top Menu', 'mobile' => '' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts' ),
);

$args['presets']['interior'] = array(
	'title' => 'Interior',
	'demo' => 'http://demo.mythemeshop.com/builders-interior/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/interior/thumb.jpg',
	'menus' => array( 'primary' => 'Primary Menu', 'secondary' => '', 'mobile' => '' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts' ),
);

$args['presets']['tech'] = array(
	'title' => 'Tech',
	'demo' => 'http://demo.mythemeshop.com/builders-tech/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/tech/thumb.jpg',
	'menus' => array( 'primary' => 'Primary Menu', 'secondary' => 'Top Menu', 'mobile' => '' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page', 'page_on_front' => '537'),
);

$args['presets']['viral'] = array(
	'title' => 'Viral',
	'demo' => 'http://demo.mythemeshop.com/builders-viral/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/viral/thumb.jpg',
	'menus' => array( 'primary' => 'Primary Menu', 'secondary' => '', 'mobile' => '' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page', 'page_on_front' => '90'),
);

global $mts_presets;
$mts_presets = $args['presets'];