<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header('shop'); ?>
<div class="sub-header clearfix">
	<div class="container">
		<header>
			<h1 class="title page-title"><?php woocommerce_page_title(); ?></h1>
		</header>
	</div>
	<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
		<div class="container">
			<?php woocommerce_breadcrumb(); ?>
		</div>
	</div>
</div>
<div id="page">
	<article class="<?php mts_article_class(); ?>">
		<div id="content_box" >
			<?php do_action('woocommerce_before_main_content'); ?>
				<?php do_action( 'woocommerce_archive_description' ); ?>
				<?php if ( have_posts() ) : ?>
					<?php do_action( 'woocommerce_before_shop_loop' ); ?>
					<?php woocommerce_product_loop_start(); ?>
						<?php woocommerce_product_subcategories(); ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php wc_get_template_part( 'content', 'product' ); ?>
						<?php endwhile; // end of the loop. ?>
					<?php woocommerce_product_loop_end(); ?>
					<?php do_action( 'woocommerce_after_shop_loop' ); ?>
				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
					<?php wc_get_template( 'loop/no-products-found.php' ); ?>
				<?php endif; ?>
			<?php do_action('woocommerce_after_main_content'); ?>
		</div>
	</article>
	<?php /*do_action('woocommerce_sidebar');*/ ?>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
