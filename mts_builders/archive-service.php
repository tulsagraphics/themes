<?php
get_header();
$mts_options = get_option(MTS_THEME_NAME);
$mts_services_title = $mts_options['mts_services_title'];
$location = get_post_meta( get_the_ID(), 'mts_project_info_location', true ); ?>

<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><?php echo $mts_services_title; ?></h2>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>

<div id="page" class="services blog-page">
	<div class="article">
		<div id="content_box"<?php if ($mts_options['mts_blog_post_layout'] == 1) { echo ' class="blog2"'; } ?>>
			<div class="service-wrap">
				<?php
				$j = 0; 
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article class="latestPost excerpt<?php echo (++$j % 2 == 0) ? ' last' : ' first'; ?>">
						<?php mts_archive_post('', $post->ID, 'builders-services');
						if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
					</article>
				<?php endwhile; endif; ?>
			</div>
			<?php if ( $j !== 0 ) { // No pagination if there is no results ?>
				<!--Start Pagination-->
				<?php mts_pagination('', 3, 'mts_service_pagenavigation_type');
				// End Pagination
			} ?>
		</div>
	</div>
	<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
		<div id="categories-2" class="widget widget_categories widget_services">
			<ul>
				<li class="active"><a href="<?php echo esc_url( get_post_type_archive_link( 'service' ) ); ?>" title="Posts"><i class="fa fa-ellipsis-h"></i><?php _e('All Services','builders'); ?></a></li>
				<?php $terms = get_terms( 'mts_service_categories' );
				foreach ( $terms as $term ) {
					// The $term is an object, so we don't need to specify the $taxonomy.
					$term_link = get_term_link( $term );
					// If there was an error, continue to the next term.
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
				} ?>
			</ul>
		</div>
		<div id="categories-2" class="widget widget_categories widget_brochure">
		<?php if ( !empty($mts_options['mts_brochures']) && is_array($mts_options['mts_brochures']) ) { ?>
			<h3 class="widget-title"><?php _e('Brochures', 'builders') ?></h3>
			<ul>
				<?php foreach( $mts_options['mts_brochures'] as $brochure ) : ?>
					<li class="cat-item">
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							<a href="<?php echo $brochure['mts_brochures_link']; ?>" title="<?php echo $brochure['mts_brochures_title']; ?>">
						<?php endif; ?>
							<i class="fa fa-<?php print $brochure['mts_brochures_icon'] ?>"></i><?php echo $brochure['mts_brochures_title']; ?>
						<?php if( !empty( $brochure['mts_brochures_link'] ) ) : ?>
							</a>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php } ?>
		</div><!-- /widget widget_services -->
	</aside><!--#sidebar-->
<?php get_footer(); ?>