/*jQuery(document).ready(function(){
	jQuery('select.mts-fontawesome-icon-select').select2({
		formatResult: function(state) {
			if (!state.id) return state.text; // optgroup
			return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
		},
		formatSelection: function(state) {
			if (!state.id) return state.text; // optgroup
			return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
		},
		escapeMarkup: function(m) { return m; }
	});
});
*/
(function ($) {
	'use strict';

	$('.mts-fontawesome-icon-select').each(function () {
		$(this).select2({
			formatResult: function(state) {
			if (!state.id) return state.text; // optgroup
				return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
			},
			formatSelection: function(state) {
				if (!state.id) return state.text; // optgroup
				return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
			},
			escapeMarkup: function(m) { return m; }
		});
	});

	// Before a new group row is added, destroy Select2. We'll reinitialise after the row is added
	$('.cmb-repeatable-group').on('cmb2_add_group_row_start', function (event, instance) {
		var $table = $(document.getElementById($(instance).data('selector')));
		var $oldRow = $table.find('.cmb-repeatable-grouping').last();

		$oldRow.find('.mts-fontawesome-icon-select').each(function () {
			$(this).select2('destroy');
		});
	});

	// When a new group row is added, clear selection and initialise Select2
	$('.cmb-repeatable-group').on('cmb2_add_row', function (event, newRow) {
		$(newRow).find('.mts-fontawesome-icon-select').each(function () {
			$('option:selected', this).removeAttr("selected");
			$(this).select2({
				formatResult: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				formatSelection: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				escapeMarkup: function(m) { return m; }
			});
		});

		// Reinitialise the field we previously destroyed
		$(newRow).prev().find('.mts-fontawesome-icon-select').each(function () {
			$(this).select2({
				formatResult: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				formatSelection: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				escapeMarkup: function(m) { return m; }
			});
		});
	});

	// Before a group row is shifted, destroy Select2. We'll reinitialise after the row shift
	$('.cmb-repeatable-group').on('cmb2_shift_rows_start', function (event, instance) {
		var groupWrap = $(instance).closest('.cmb-repeatable-group');
		groupWrap.find('.mts-fontawesome-icon-select').each(function () {
			$(this).select2('destroy');
		});

	});

	// When a group row is shifted, reinitialise Select2
	$('.cmb-repeatable-group').on('cmb2_shift_rows_complete', function (event, instance) {
		var groupWrap = $(instance).closest('.cmb-repeatable-group');
		groupWrap.find('.mts-fontawesome-icon-select').each(function () {
			$(this).select2({
				formatResult: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				formatSelection: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				escapeMarkup: function(m) { return m; }
			});
		});
	});

	// Before a new repeatable field row is added, destroy Select2. We'll reinitialise after the row is added
	$('.cmb-add-row-button').on('click', function (event) {
		var $table = $(document.getElementById($(event.target).data('selector')));
		var $oldRow = $table.find('.cmb-row').last();

		$oldRow.find('.mts-fontawesome-icon-select').each(function () {
			$(this).select2('destroy');
		});
	});

	// When a new repeatable field row is added, clear selection and initialise Select2
	$('.cmb-repeat-table').on('cmb2_add_row', function (event, newRow) {

		// Reinitialise the field we previously destroyed
		$(newRow).prev().find('.mts-fontawesome-icon-select').each(function () {
			$('option:selected', this).removeAttr("selected");
			$(this).select2({
				formatResult: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				formatSelection: function(state) {
					if (!state.id) return state.text; // optgroup
					return '<i class="fa fa-' + state.id + '"></i>&nbsp;&nbsp;' + state.text;
				},
				escapeMarkup: function(m) { return m; }
			});
		});
	});
})(jQuery);