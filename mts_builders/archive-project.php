<?php
get_header();
$mts_options = get_option(MTS_THEME_NAME);
$mts_projects_title = $mts_options['mts_projects_title']; ?>

<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><?php echo $mts_projects_title; ?></h2>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>

<div id="project-page">
	<div class="container">

		<div class="project-category">
			<?php echo '<h6 class="filter" data-filter="all"><a href="javascript:void(0)">' . __('All Projects','builders') . '</a></h6>';
			$terms = get_terms( 'mts_project_categories' );
			foreach ( $terms as $term ) {
				echo '<h6 class="filter" data-filter=".' .$term->slug. '"><a href="javascript:void(0)">' . $term->name . '</a></h6>';
			} ?>
		</div>
		
		<div id="project">
			<?php
			$j = 0; $c = 0; if ( have_posts() ) : while ( have_posts() ) : the_post();
			$location = get_post_meta( get_the_ID(), 'mts_project_info_location', true );
			$cpt_category = get_the_terms( $post->ID, 'mts_project_categories' ); ?>
				<div class="mix <?php if(!empty($cpt_category)) { foreach ( $cpt_category as $cpt_cat ) { echo $cpt_cat->slug. " "; }} ?>" data-myorder="<?php echo ++$c; ?>">
					<a href="<?php the_permalink(); ?>" class="project-image">
						<?php the_post_thumbnail('builders-project',array('title' => '')); ?>
						<div class="project-caption">
							<div class="readMore"><?php _e( 'View', 'builders' ); ?></div>
						</div>
					</a>
					<div class="mix-content">
						<?php if ( !empty($location) ) : ?>
							<div class="post-info"><span class="theplace"><i class="fa fa-globe"></i><?php echo esc_html($location); ?></span></div>
						<?php endif; ?>
						<h5 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
					</div>
				</div><!--mix interior-design-->
			<?php $j++; endwhile; endif; ?>
		</div><!--#Project-->
	</div>
<?php get_footer(); ?>