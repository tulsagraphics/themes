<?php
/**
 * The template for displaying archive pages.
 *
 * Used for displaying archive-type pages. These views can be further customized by
 * creating a separate template for each one.
 *
 * - author.php (Author archive)
 * - category.php (Category archive)
 * - date.php (Date archive)
 * - tag.php (Tag archive)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
$mts_options = get_option(MTS_THEME_NAME);
get_header(); ?>
<div class="sub-header clearfix">
	<div class="container">
		<h2 class="title"><?php the_archive_title(); ?></h2>
		<p><?php the_archive_description(); ?></p>
	</div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>
<div id="page" class="blog-page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box"<?php if ($mts_options['mts_blog_post_layout'] == 1) { echo ' class="blog2"'; } ?>>
			<?php $j = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="latestPost excerpt<?php echo (++$j % 2 == 0) ? ' last' : ' first'; ?>">
					<?php mts_archive_post();
					if ($mts_options['mts_blog_post_layout'] == 1) { echo "</div>"; } ?>
				</article>
			<?php endwhile; endif;

			if ( $j !== 0 ) { // No pagination if there is no posts
				mts_pagination();
			} ?>
		</div>
	</div>
	<?php get_sidebar();
get_footer(); ?>