<?php
/**
 * The template for displaying all single posts.
 */
get_header();
$mts_options = get_option(MTS_THEME_NAME); ?>
<?php if($mts_options['mts_single_post_subheader']) { ?>
<div class="sub-header clearfix">
  <div class="container">
	<h2 class="title"><?php _e("Our Blog", 'builders') ?></h2>
  </div>
	<?php if ( $mts_options['mts_breadcrumb'] == '1' && !is_home() ) { ?>
		<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="container">
				<?php mts_the_breadcrumb(); ?>
			</div>
		</div>
	<?php } ?> 
</div>
<?php } ?> 

<div id="page" class="<?php mts_single_page_class(); ?>">
	<article class="<?php mts_article_class(); ?>">
		<div id="content_box" >
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
					<?php
					// Single post parts ordering
					if ( isset( $mts_options['mts_single_post_layout'] ) && is_array( $mts_options['mts_single_post_layout'] ) && array_key_exists( 'enabled', $mts_options['mts_single_post_layout'] ) ) {
						$single_post_parts = $mts_options['mts_single_post_layout']['enabled'];
					} else {
						$single_post_parts = array( 'content' => 'content', 'related' => 'related', 'author' => 'author' );
					}
					foreach( $single_post_parts as $part => $label ) { 
						switch ($part) {
							case 'content':
								?>
								<div class="single_post">
								<?php $header_animation = mts_get_post_header_effect();
									if ( 'parallax' === $header_animation ) {
										if (mts_get_thumbnail_url()) : ?>
											<div id="parallax" <?php echo 'style="background-image: url('.mts_get_thumbnail_url().');"'; ?>></div>
										<?php endif;
									} elseif ( 'zoomout' === $header_animation ) {
										if (mts_get_thumbnail_url()) : ?>
											<div id="zoom-out-effect"><div id="zoom-out-bg" <?php echo 'style="background-image: url('.mts_get_thumbnail_url().');"'; ?>></div></div>
										<?php endif;
									} elseif ( mts_get_thumbnail_url() && $mts_options['mts_single_image'] ) { ?>										 
										<div <?php echo 'style="background-image: url('.mts_get_thumbnail_url().'); min-height: 400px; background-position: center; background-repeat: no-repeat; background-size: cover;"'; ?>></div>				
									<?php } ?>

									<header>
										<?php mts_the_postinfo( 'single' ); ?>
										<h1 class="title single-title entry-title"><?php the_title(); ?></h1>
									</header><!--.headline_area-->

									<div class="post-single-content box mark-links entry-content">
										<?php if ($mts_options['mts_posttop_adcode'] != '') {
											$toptime = $mts_options['mts_posttop_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
												<div class="topad">
													<?php echo do_shortcode($mts_options['mts_posttop_adcode']); ?>
												</div>
											<?php }
										}
										if (isset($mts_options['mts_social_button_position']) && $mts_options['mts_social_button_position'] == 'top') mts_social_buttons(); ?>
										
										<div class="thecontent">
											<?php the_content(); ?>
										</div>
										
										<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => '<i class="fa fa-angle-right"></i>', 'previouspagelink' => '<i class="fa fa-angle-left"></i>', 'pagelink' => '%','echo' => 1 ));
										
										if ($mts_options['mts_postend_adcode'] != '') {
											$endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
												<div class="bottomad">
													<?php echo do_shortcode($mts_options['mts_postend_adcode']); ?>
												</div>
											<?php }
										}
										if (isset($mts_options['mts_social_button_position']) && $mts_options['mts_social_button_position'] !== 'top') mts_social_buttons(); ?>
									</div><!--.post-single-content-->
								</div><!--.single_post-->
								<?php
							break;

							case 'tags':
								mts_the_tags('<div class="tags"><span class="tagtext">'.__('Tags', 'builders' ).':</span>',', ');
							break;

							case 'related':
								mts_related_posts();
							break;

							case 'author':
								?>
								<div class="postauthor">
									<h4><?php _e('About The Author', 'builders' ); ?></h4>
									<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?>
									<h5 class="vcard author"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="fn"><?php the_author_meta( 'display_name' ); ?></a></h5>
									<p><?php the_author_meta('description') ?></p>
									<?php
									$userID = get_current_user_id();
									$facebook = get_the_author_meta( 'facebook', $userID );
									$twitter = get_the_author_meta( 'twitter', $userID );
									$google = get_the_author_meta( 'google', $userID );
									$pinterest = get_the_author_meta( 'pinterest', $userID );
									$instagram = get_the_author_meta( 'instagram', $userID );
									$linkedin = get_the_author_meta( 'linkedin', $userID );

									if(!empty($facebook) || !empty($twitter) || !empty($google) || !empty($pinterest) || !empty($instagram) || !empty($linkedin)){
										echo '<div class="author-social">';
											if(!empty($facebook)){
												echo '<a href="'.$facebook.'" class="facebook"><i class="fa fa-facebook"></i></a>';
											}
											if(!empty($twitter)){
												echo '<a href="'.$twitter.'" class="twitter"><i class="fa fa-twitter"></i></a>';
											}
											if(!empty($google)){
												echo '<a href="'.$google.'" class="google-plus"><i class="fa fa-google-plus"></i></a>';
											}
											if(!empty($pinterest)){
												echo '<a href="'.$pinterest.'" class="pinterest"><i class="fa fa-pinterest"></i></a>';
											}
											if(!empty($instagram)){
												echo '<a href="'.$instagram.'" class="stumble"><i class="fa fa-instagram"></i></a>';
											}
											if(!empty($linkedin)){
												echo '<a href="'.$linkedin.'" class="linkedin"><i class="fa fa-linkedin"></i></a>';
											}
										echo '</div>';
										}
									?>
								</div>
								<?php
							break;
						}
					}
					?>
				</div><!--.g post-->
				<?php comments_template( '', true );
			endwhile; /* end loop */ ?>
		</div>
	</article>
	<?php get_sidebar();
get_footer(); ?>
