<?php

defined('ABSPATH') or die;

/*
 * 
 * Require the framework class before doing anything else, so we can use the defined urls and dirs
 *
 */
require_once( dirname( __FILE__ ) . '/options/options.php' );
/*
 * 
 * Add support tab
 *
 */
if ( ! defined('MTS_THEME_WHITE_LABEL') || ! MTS_THEME_WHITE_LABEL ) {
	require_once( dirname( __FILE__ ) . '/options/support.php' );
	$mts_options_tab_support = MTS_Options_Tab_Support::get_instance();
}
/*
 * 
 * Custom function for filtering the sections array given by theme, good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constants for urls, and dir will NOT be available at this point in a child theme, so you must use
 * get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
	
	//$sections = array();
	$sections[] = array(
		'title' => __('A Section added by hook', 'builders' ),
		'desc' => '<p class="description">' . __('This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.', 'builders' ) . '</p>',
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => trailingslashit(get_template_directory_uri()).'options/img/glyphicons/glyphicons_062_attach.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array()
	);
	
	return $sections;
	
}//function
//add_filter('nhp-opts-sections-twenty_eleven', 'add_another_section');


/*
 * 
 * Custom function for filtering the args array given by theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
	
	//$args['dev_mode'] = false;
	
	return $args;
	
}//function
//add_filter('nhp-opts-args-twenty_eleven', 'change_framework_args');

/*
 * This is the meat of creating the options page
 *
 * Override some of the default values, uncomment the args and change the values
 * - no $args are required, but there there to be overridden if needed.
 *
 *
 */

function setup_framework_options(){
	$args = array();

	//Set it to dev mode to view the class settings/info in the form - default is false
	$args['dev_mode'] = false;
	//Remove the default stylesheet? make sure you enqueue another one all the page will look whack!
	//$args['stylesheet_override'] = true;

	//Add HTML before the form
	//$args['intro_text'] = __('<p>This is the HTML which can be displayed before the form, it isnt required, but more info is always better. Anything goes in terms of markup here, any HTML.</p>', 'builders' );

	if ( ! MTS_THEME_WHITE_LABEL ) {
		//Setup custom links in the footer for share icons
		$args['share_icons']['twitter'] = array(
			'link' => 'http://twitter.com/mythemeshopteam',
			'title' => __( 'Follow Us on Twitter', 'builders' ),
			'img' => 'fa fa-twitter-square'
		);
		$args['share_icons']['facebook'] = array(
			'link' => 'http://www.facebook.com/mythemeshop',
			'title' => __( 'Like us on Facebook', 'builders' ),
			'img' => 'fa fa-facebook-square'
		);
	}

	//Choose to disable the import/export feature
	//$args['show_import_export'] = false;

	//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
	$args['opt_name'] = MTS_THEME_NAME;

	//Custom menu icon
	//$args['menu_icon'] = '';

	//Custom menu title for options page - default is "Options"
	$args['menu_title'] = __('Theme Options', 'builders' );

	//Custom Page Title for options page - default is "Options"
	$args['page_title'] = __('Theme Options', 'builders' );

	//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "nhp_theme_options"
	$args['page_slug'] = 'theme_options';

	//Custom page capability - default is set to "manage_options"
	//$args['page_cap'] = 'manage_options';

	//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
	//$args['page_type'] = 'submenu';

	//parent menu - default is set to "themes.php" (Appearance)
	//the list of available parent menus is available here: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	//$args['page_parent'] = 'themes.php';

	//custom page location - default 100 - must be unique or will override other items
	$args['page_position'] = 62;

	//Custom page icon class (used to override the page icon next to heading)
	//$args['page_icon'] = 'icon-themes';

	if ( ! MTS_THEME_WHITE_LABEL ) {
		//Set ANY custom page help tabs - displayed using the new help tab API, show in order of definition
		$args['help_tabs'][] = array(
			'id' => 'nhp-opts-1',
			'title' => __('Support', 'builders' ),
			'content' => '<p>' . sprintf( __('If you are facing any problem with our theme or theme option panel, head over to our %s.', 'builders' ), '<a href="http://community.mythemeshop.com/">'. __( 'Support Forums', 'builders' ) . '</a>' ) . '</p>'
		);
		$args['help_tabs'][] = array(
			'id' => 'nhp-opts-2',
			'title' => __('Earn Money', 'builders' ),
			'content' => '<p>' . sprintf( __('Earn 70%% commision on every sale by refering your friends and readers. Join our %s.', 'builders' ), '<a href="http://mythemeshop.com/affiliate-program/">' . __( 'Affiliate Program', 'builders' ) . '</a>' ) . '</p>'
		);
	}

	//Set the Help Sidebar for the options page - no sidebar by default										
	//$args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'builders' );

	$mts_patterns = array(
		'nobg' => array('img' => NHP_OPTIONS_URL.'img/patterns/nobg.png'),
		'pattern0' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern0.png'),
		'pattern1' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern1.png'),
		'pattern2' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern2.png'),
		'pattern3' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern3.png'),
		'pattern4' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern4.png'),
		'pattern5' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern5.png'),
		'pattern6' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern6.png'),
		'pattern7' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern7.png'),
		'pattern8' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern8.png'),
		'pattern9' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern9.png'),
		'pattern10' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern10.png'),
		'pattern11' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern11.png'),
		'pattern12' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern12.png'),
		'pattern13' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern13.png'),
		'pattern14' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern14.png'),
		'pattern15' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern15.png'),
		'pattern16' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern16.png'),
		'pattern17' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern17.png'),
		'pattern18' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern18.png'),
		'pattern19' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern19.png'),
		'pattern20' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern20.png'),
		'pattern21' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern21.png'),
		'pattern22' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern22.png'),
		'pattern23' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern23.png'),
		'pattern24' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern24.png'),
		'pattern25' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern25.png'),
		'pattern26' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern26.png'),
		'pattern27' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern27.png'),
		'pattern28' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern28.png'),
		'pattern29' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern29.png'),
		'pattern30' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern30.png'),
		'pattern31' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern31.png'),
		'pattern32' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern32.png'),
		'pattern33' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern33.png'),
		'pattern34' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern34.png'),
		'pattern35' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern35.png'),
		'pattern36' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern36.png'),
		'pattern37' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern37.png'),
		'pattern38' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern38.jpg'),
		'pattern39' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern39.jpg'),
		'hbg' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg.png'),
		'hbg2' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg2.png'),
		'hbg3' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg3.png'),
		'hbg4' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg4.png'),
		'hbg5' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg5.png'),
		'hbg6' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg6.png'),
		'hbg7' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg7.png'),
		'hbg8' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg8.png'),
		'hbg9' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg9.png'),
		'hbg10' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg10.png'),
		'hbg11' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg11.png'),
		'hbg12' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg12.png'),
		'hbg13' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg13.png'),
		'hbg14' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg14.png'),
		'hbg15' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg15.png'),
		'hbg16' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg16.png'),
		'hbg17' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg17.png'),
		'hbg18' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg18.png'),
		'hbg19' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg19.png'),
		'hbg20' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg20.png'),
		'hbg21' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg21.png'),
		'hbg22' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg22.png'),
		'hbg23' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg23.png'),
		'hbg24' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg24.png'),
		'hbg25' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg25.png'),
		'hbg26' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg26.jpg'),
		'hbg27' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg27.jpg'),
		'hbg28' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg28.jpg')
	);

	$sections = array();

	$sections[] = array(
		'icon' => 'fa fa-cogs',
		'title' => __('General Settings', 'builders' ),
		'desc' => '<p class="description">' . __('This tab contains common setting options which will be applied to the whole theme.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_logo',
				'type' => 'upload',
				'title' => __('Logo Image', 'builders' ),
				'sub_desc' => __('Upload your logo using the Upload Button or insert image URL.', 'builders' )
			),
			array(
				'id' => 'mts_favicon',
				'type' => 'upload',
				'title' => __('Favicon', 'builders' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s favicon.', 'builders' ), '<strong>32 x 32 px</strong>' )
			),
			array(
				'id' => 'mts_touch_icon',
				'type' => 'upload',
				'title' => __('Touch icon', 'builders' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s touch icon for iOS 2.0+ and Android 2.1+ devices.', 'builders' ), '<strong>152 x 152 px</strong>' )
			),
			array(
				'id' => 'mts_metro_icon',
				'type' => 'upload',
				'title' => __('Metro icon', 'builders' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s IE 10 Metro tile icon.', 'builders' ), '<strong>144 x 144 px</strong>' )
			),
			array(
				'id' => 'mts_twitter_username',
				'type' => 'text',
				'title' => __('Twitter Username', 'builders' ),
				'sub_desc' => __('Enter your Username here.', 'builders' ),
			),
			array(
				'id' => 'mts_feedburner',
				'type' => 'text',
				'title' => __('FeedBurner URL', 'builders' ),
				'sub_desc' => sprintf( __('Enter your FeedBurner\'s URL here, ex: %s and your main feed (http://example.com/feed) will get redirected to the FeedBurner ID entered here.)', 'builders' ), '<strong>http://feeds.feedburner.com/mythemeshop</strong>' ),
				'validate' => 'url'
			),
			array(
				'id' => 'mts_header_code',
				'type' => 'textarea',
				'title' => __('Header Code', 'builders' ),
				'sub_desc' => wp_kses( __('Enter the code which you need to place <strong>before closing &lt;/head&gt; tag</strong>. (ex: Google Webmaster Tools verification, Bing Webmaster Center, BuySellAds Script, Alexa verification etc.)', 'builders' ), array( 'strong' => array() ) )
			),
			array(
				'id' => 'mts_analytics_code',
				'type' => 'textarea',
				'title' => __('Footer Code', 'builders' ),
				'sub_desc' => wp_kses( __('Enter the codes which you need to place in your footer. <strong>(ex: Google Analytics, Clicky, STATCOUNTER, Woopra, Histats, etc.)</strong>.', 'builders' ), array( 'strong' => array() ) )
			),
			array(
				'id' => 'mts_ajax_search',
				'type' => 'button_set',
				'title' => __('AJAX Quick search', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Enable or disable search results appearing instantly below the search form', 'builders' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_responsive',
				'type' => 'button_set',
				'title' => __('Responsiveness', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('MyThemeShop themes are responsive, which means they adapt to tablet and mobile devices, ensuring that your content is always displayed beautifully no matter what device visitors are using. Enable or disable responsiveness using this option.', 'builders' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_shop_products',
				'type' => 'text',
				'title' => __('No. of Products', 'builders' ),
				'sub_desc' => __('Enter the total number of products which you want to show on shop page (WooCommerce plugin must be enabled).', 'builders' ),
				'validate' => 'numeric',
				'std' => '9',
				'class' => 'small-text'
			)
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-bolt',
		'title' => __('Performance', 'builders' ),
		'desc' => '<p class="description">' . __('This tab contains performance-related options which can help speed up your website.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_prefetching',
				'type' => 'button_set',
				'title' => __('Prefetching', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Enable or disable prefetching. If user is on homepage, then single page will load faster and if user is on single page, homepage will load faster in modern browsers.', 'builders' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_lazy_load',
				'type' => 'button_set_hide_below',
				'title' => __('Lazy Load', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Delay loading of images outside of viewport, until user scrolls to them.', 'builders' ),
				'std' => '0',
				'args' => array('hide' => 2)
			),
			array(
				'id' => 'mts_lazy_load_thumbs',
				'type' => 'button_set',
				'title' => __('Lazy load featured images', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Enable or disable Lazy load of featured images across site.', 'builders' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_lazy_load_content',
				'type' => 'button_set',
				'title' => __('Lazy load post content images', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Enable or disable Lazy load of images inside post/page content.', 'builders' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_async_js',
				'type' => 'button_set',
				'title' => __('Async JavaScript', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => sprintf( __('Add %s attribute to script tags to improve page download speed.', 'builders' ), '<code>async</code>' ),
				'std' => '1',
			),
			array(
				'id' => 'mts_remove_ver_params',
				'type' => 'button_set',
				'title' => __('Remove ver parameters', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => sprintf( __('Remove %s parameter from CSS and JS file calls. It may improve speed in some browsers which do not cache files having the parameter.', 'builders' ), '<code>ver</code>' ),
				'std' => '1',
			),
			array(
				'id' => 'mts_optimize_wc',
				'type' => 'button_set',
				'title' => __('Optimize WooCommerce scripts', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Load WooCommerce scripts and styles only on WooCommerce pages (WooCommerce plugin must be enabled).', 'builders' ),
				'std' => '1'
			),
			'cache_message' => array(
				'id' => 'mts_cache_message',
				'type' => 'info',
				'title' => __('Use Cache', 'builders' ),
				/* Translators: %1$s = popup link to W3 Total Cache, %2$s = popup link to WP Super Cache */
				'desc' => sprintf(
					__('A cache plugin can increase page download speed dramatically. We recommend using %1$s or %2$s.', 'builders' ),
					'<a href="https://community.mythemeshop.com/tutorials/article/8-make-your-website-load-faster-using-w3-total-cache-plugin/" target="_blank" title="W3 Total Cache">W3 Total Cache</a>',
					'<a href="'.admin_url( 'plugin-install.php?tab=plugin-information&plugin=wp-super-cache&TB_iframe=true&width=772&height=574' ).'" class="thickbox" title="WP Super Cache">WP Super Cache</a>'
				),
			),
		)
	);

	// Hide cache message on multisite or if a chache plugin is active already
	if ( is_multisite() || strstr( join( ';', get_option( 'active_plugins' ) ), 'cache' ) ) {
		unset( $sections[1]['fields']['cache_message'] );
	}

	$sections[] = array(
		'icon' => 'fa fa-adjust',
		'title' => __('Styling Options', 'builders' ),
		'desc' => '<p class="description">' . __('Control the visual appearance of your theme, such as colors, layout and patterns, from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_color_scheme',
				'type' => 'color',
				'title' => __('Color Scheme', 'builders' ),
				'sub_desc' => __('The theme comes with unlimited color schemes for your theme\'s styling.', 'builders' ),
				'std' => '#fbd713'
			),
			array(
				'id' => 'mts_layout',
				'type' => 'radio_img',
				'title' => __('Layout Style', 'builders' ),
				'sub_desc' => wp_kses( __('Choose the <strong>default sidebar position</strong> for your site. The position of the sidebar for individual posts can be set in the post editor.', 'builders' ), array( 'strong' => array() ) ),
				'options' => array(
					'cslayout' => array('img' => NHP_OPTIONS_URL.'img/layouts/cs.png'),
					'sclayout' => array('img' => NHP_OPTIONS_URL.'img/layouts/sc.png')
				),
				'std' => 'cslayout'
			),
			array(
				'id' => 'mts_background',
				'type' => 'background',
				'title' => __('Site Background', 'builders' ),
				'sub_desc' => __('Set background color, pattern and image from here.', 'builders' ),
				'options' => array(
					'color'			=> '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'		=> array(),
					'size'			=> array(),
					'gradient'		=> '',
					'parallax'		=> array(),
				),
				'std' => array(
					'color'			=> '#fafafa',
					'use'			=> 'pattern',
					'image_pattern'	=> 'nobg',
					'image_upload'	=> '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'		=> 'left top',
					'size'			=> 'cover',
					'gradient'		=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'		=> '0',
				)
			),
			array(
				'id' => 'mts_custom_css',
				'type' => 'textarea',
				'title' => __('Custom CSS', 'builders' ),
				'sub_desc' => __('You can enter custom CSS code here to further customize your theme. This will override the default CSS used on your site.', 'builders' )
			),
			array(
				'id' => 'mts_lightbox',
				'type' => 'button_set',
				'title' => __('Lightbox', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('A lightbox is a stylized pop-up that allows your visitors to view larger versions of images without leaving the current page. You can enable or disable the lightbox here.', 'builders' ),
				'std' => '0'
			),
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-credit-card',
		'title' => __('Header', 'builders' ),
		'desc' => '<p class="description">' . __('From here, you can control the elements of header section.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_header_background',
				'type' => 'background',
				'title' => __('Header Background', 'builders' ), 
				'sub_desc' => __('Configure header background.', 'builders' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#434343',
					'use'		   => 'pattern',
					'image_pattern' => 'hbg28',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#434343', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				),
			),
			array(
				'id' => 'mts_sticky_nav',
				'type' => 'button_set',
				'title' => __('Floating Navigation Menu', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'builders' ), '<strong>' . __('Floating Navigation Menu', 'builders' ) . '</strong>' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_show_primary_nav',
				'type' => 'button_set',
				'title' => __('Show Primary Menu', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'builders' ), '<strong>' . __( 'Primary Navigation Menu', 'builders' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_show_secondary_nav',
				'type' => 'button_set',
				'title' => __('Show Secondary Menu', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'builders' ), '<strong>' . __( 'Secondary Navigation Menu', 'builders' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_header_section2',
				'type' => 'button_set',
				'title' => __('Show Logo', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => wp_kses( __('Use this button to Show or Hide the <strong>Logo</strong> completely.', 'builders' ), array( 'strong' => array() ) ),
				'std' => '1'
			),
			array(
				'id' => 'mts_top_background',
				'type' => 'background',
				'title' => __('Top Navigation Background', 'builders' ), 
				'sub_desc' => __('Configure top navigation background.', 'builders' ),
				'options' => array(
					'color'			=> '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'		=> array(),
					'size'			=> array(),
					'gradient'		=> '',
					'parallax'		=> array(),
				),
				'std' => array(
					'color'			=> '#252525',
					'use'			=> 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'		=> 'left top',
					'size'			=> 'cover',
					'gradient'		=> array('from' => '#434343', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'		=> '0',
				),
			),
			array(
				'id' => 'mts_top_color',
				'type' => 'color',
				'title' => __('Top Navigation Text Color', 'builders' ),
				'sub_desc' => __('Choose font color for Top Navigation text.', 'builders' ),
				'std' => '#757575'
			),
			array(
				'id' => 'mts_top_description',
				'type' => 'button_set_hide_below',
				'title' => __('Top Navigation Description', 'builders' ),
				'sub_desc' => __('Enable or disable top navigation description with this option.', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'std' => '1'
			),
			array(
				'id' => 'mts_top_description_text',
				'type' => 'text',
				'title' => __('Top Navigation Tagline', 'builders' ), 
				'sub_desc' => __('Add your top navigation tagline here.', 'builders' ),
				'std' => 'We build future!'
			),
			 array(
				'id' => 'mts_social_icon_header',
				'type' => 'button_set_hide_below',
				'title' => __('Social Icons','builders'),
				'sub_desc' => __('You can enable/disable social icon', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'std' => '1'
			),
			array(
			 	'id' => 'mts_header_social',
			 	'title' => __('Add Social Icons','builders'), 
			 	'sub_desc' => __( 'Add Social Media icons in header.', 'builders' ),
			 	'type' => 'group',
			 	'groupname' => __('Header Icons','builders'), // Group name
			 	'subfields' => array(
					array(
						'id' => 'mts_header_icon_title',
						'type' => 'text',
						'title' => __('Title', 'builders'), 
					),
					array(
						'id' => 'mts_header_icon',
						'type' => 'icon_select',
						'title' => __('Icon', 'builders')
					),
					array(
						'id' => 'mts_header_icon_link',
						'type' => 'text',
						'title' => __('URL', 'builders'), 
					),
				),
				'std' => array(
					'facebook' => array(
						'group_title' => 'Facebook',
						'group_sort' => '1',
						'mts_header_icon_title' => 'Facebook',
						'mts_header_icon' => 'facebook',
						'mts_header_icon_link' => '#',
					),
					'twitter' => array(
						'group_title' => 'Twitter',
						'group_sort' => '2',
						'mts_header_icon_title' => 'Twitter',
						'mts_header_icon' => 'twitter',
						'mts_header_icon_link' => '#',
					),
					'gplus' => array(
						'group_title' => 'Google Plus',
						'group_sort' => '3',
						'mts_header_icon_title' => 'Google Plus',
						'mts_header_icon' => 'google-plus',
						'mts_header_icon_link' => '#',
					)
				)
			),
			array(
			 	'id' => 'mts_contact_info_items',
			 	'title' => __('Add Contact Info Item', 'builders'), 
			 	'sub_desc' => __( 'From here you can add multiple contact info items in the header.', 'builders' ),
			 	'type' => 'group',
			 	'groupname' => __('Contact Info Items', 'builders'), // Group name
			 	'subfields' => array(
					array(
						'id' => 'mts_contact_info_item_number',
						'type' => 'text',
						'title' => __('Contact Item Title', 'builders' ), 
						'sub_desc' => __('Add item title text here', 'builders' ),
						'std' => '0 (123) 456 879'
					),
					array(
						'id' => 'mts_contact_info_item_icon',
						'type' => 'icon_select',
						'title' => __('Contact Item Icon', 'builders' ), 
						'sub_desc' => __('Select an icon from the vector icon set.', 'builders' ),
						'std' => 'phone'
					),
					array(
						'id' => 'mts_contact_info_item_tagline',
						'type' => 'text',
						'title' => __('Contact Item Tagline', 'builders' ), 
						'sub_desc' => __('Add item tagline text here', 'builders' ),
						'std' => 'info@buliders.com'
					),
					array(
						'id' => 'mts_contact_info_item_url',
						'type' => 'text',
						'title' => __('Contact Item URL(optional)', 'builders' ), 
						'sub_desc' => __('Add item URL here.', 'builders' ),
						'std' => ''
					),
				),
				'std' => array(
					'phone' => array(
						'group_title' => '0 (123) 456 879',
						'group_sort' => '1',
						'mts_contact_info_item_icon' => 'phone',
						'mts_contact_info_item_number' => '0 (123) 456 879',
						'mts_contact_info_item_tagline' => 'info@yourdomain.com',
						'mts_contact_info_item_link' => ''
					),
					'location-arrow' => array(
						'group_title' => '17 Ronson Avenue',
						'group_sort' => '1',
						'mts_contact_info_item_icon' => 'location-arrow',
						'mts_contact_info_item_number' => '17 Ronson Avenue',
						'mts_contact_info_item_tagline' => 'Long Beach, CA',
						'mts_contact_info_item_link' => ''
					),
				)
			),
			array(
				'id' => 'mts_header_heading_color',
				'type' => 'color',
				'title' => __('Contact Info Title Color', 'builders' ),
				'sub_desc' => __('Choose font color for contact info title.', 'builders' ),
				'std' => '#dedede'
			),
			array(
				'id' => 'mts_header_text_color',
				'type' => 'color',
				'title' => __('Contact Info Tagline Color', 'builders' ),
				'sub_desc' => __('Choose font color for contact info tagline.', 'builders' ),
				'std' => '#757575'
			),
			array(
				'id' => 'mts_main_nav_background',
				'type' => 'background',
				'title' => __('Main Navigation Background', 'builders' ), 
				'sub_desc' => __('Set background color for main navigation from here. Font color can be changed from Typography Tab.', 'builders' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#e5e5e5',
					'use'		   => 'gradient',
					'image_pattern' => 'pattern38',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#e5e5e5', 'to' => '#dedede', 'direction' => 'vertical' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_subhead_background',
				'type' => 'background',
				'title' => __('Sub Header Background', 'builders' ), 
				'sub_desc' => __('Sub Header appears on Blog, Projects & Services Pages.', 'builders' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#f1f1f1',
					'use'		   => 'pattern',
					'image_pattern' => 'pattern38',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_subheader_text_color',
				'type' => 'color',
				'title' => __('Subheader Title Color', 'builders' ),
				'sub_desc' => __('Choose color for subheader title.', 'builders' ),
				'std' => '#252525'
			),
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-table',
		'title' => __('Footer', 'builders' ),
		'desc' => '<p class="description">' . __('From here, you can control the elements of Footer section.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_first_footer',
				'type' => 'button_set_hide_below',
				'title' => __('Footer Widgets', 'builders' ),
				'sub_desc' => __('Enable or disable footer widgets with this option.', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'std' => '0'
			),
			array(
				'id' => 'mts_first_footer_num',
				'type' => 'button_set',
				'class' => 'green',
				'title' => __('Footer Layout', 'builders' ),
				'sub_desc' => wp_kses( __('Choose the number of widget areas in the <strong>footer</strong>', 'builders' ), array( 'strong' => array() ) ),
				'options' => array(
					'3' => __( '3 Widgets', 'builders' ),
					'4' => __( '4 Widgets', 'builders' ),
				),
				'std' => '4'
			),
			array(
				'id' => 'mts_footer_background',
				'type' => 'background',
				'title' => __('Footer Background', 'builders' ), 
				'sub_desc' => __('Configure background.', 'builders' ),
				'options' => array(
					'color'			=> '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'		=> array(),
					'size'			=> array(),
					'gradient'		=> '',
					'parallax'		=> array(),
				),
				'std' => array(
					'color'			=> '#434343',
					'use'			=> 'pattern',
					'image_pattern' => 'hbg27',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'		=> 'left top',
					'size'			=> 'cover',
					'gradient'		=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'		=> '0',
				)
			),
			array(
				'id' => 'mts_social_icon_footer',
				'type' => 'button_set_hide_below',
				'title' => __('Social Icons', 'builders'),
				'sub_desc' => __('You can enable/disable social icon', 'builders'),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'std' => '1'
			),
			array(
			 	'id' => 'mts_footer_social',
			 	'title' => __('Add Social Icons', 'builders'), 
			 	'sub_desc' => __( 'Add Social Media icons in footer.', 'builders' ),
			 	'type' => 'group',
			 	'groupname' => __('Footer Icons', 'builders'), // Group name
			 	'subfields' => array(
					array(
						'id' => 'mts_footer_icon_title',
						'type' => 'text',
						'title' => __('Title', 'builders'), 
					),
					array(
						'id' => 'mts_footer_icon',
						'type' => 'icon_select',
						'title' => __('Icon', 'builders')
					),
					array(
						'id' => 'mts_footer_icon_link',
						'type' => 'text',
						'title' => __('URL', 'builders'), 
					),
				),
				'std' => array(
					'facebook' => array(
						'group_title' => 'Facebook',
						'group_sort' => '1',
						'mts_footer_icon_title' => 'Facebook',
						'mts_footer_icon' => 'facebook',
						'mts_footer_icon_link' => '#',
					),
					'twitter' => array(
						'group_title' => 'Twitter',
						'group_sort' => '2',
						'mts_footer_icon_title' => 'Twitter',
						'mts_footer_icon' => 'twitter',
						'mts_footer_icon_link' => '#',
					),
					'gplus' => array(
						'group_title' => 'Google Plus',
						'group_sort' => '3',
						'mts_footer_icon_title' => 'Google Plus',
						'mts_footer_icon' => 'google-plus',
						'mts_footer_icon_link' => '#',
					)
				)
			),
			array(
				'id' => 'mts_copyrights',
				'type' => 'textarea',
				'title' => __('Copyrights Text', 'builders' ),
				'sub_desc' => __( 'You can change or remove our link from footer and use your own custom text.', 'builders' ) . ( MTS_THEME_WHITE_LABEL ? '' : wp_kses( __('(You can also use your affiliate link to <strong>earn 70% of sales</strong>. Ex: <a href="https://mythemeshop.com/go/aff/aff" target="_blank">https://mythemeshop.com/?ref=username</a>)', 'builders' ), array( 'strong' => array(), 'a' => array( 'href' => array(), 'target' => array() ) ) ) ),
				'std' => MTS_THEME_WHITE_LABEL ? null : sprintf( __( 'Theme by %s', 'builders' ), '<a href="http://mythemeshop.com/" rel="nofollow">MyThemeShop</a>' )
			),
			array(
				'id' => 'mts_copyrights_bg',
				'type' => 'color',
				'title' => __('Copyrights Background Color', 'builders' ),
				'sub_desc' => __('Choose background color for copyrights area present in the footer.', 'builders' ),
				'std' => '#252525'
			),
		)
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Home Layout', 'builders' ),
		'desc' => '<p class="description">' . __('From here, you can control the Homepage Layout.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id'	=> 'mts_homepage_layout',
				'type'	=> 'layout',
				'title'   => __( 'Homepage Layout Manager', 'builders' ),
				'sub_desc'	=> __( 'Organize how you want the layout to appear on the homepage.', 'builders' ),
				'options' => array(
					'enabled'  => array(
						'slider' => __('Homepage Slider','builders'),
						'quote'  => __('Request Quote','builders'),
						'ourwork' => __('Our Work','builders'),
						'tabs' => __('Homepage Tabs','builders'),
						'stats'   => __('Company Stats','builders'),
						'whyus'  => __('Why Us?','builders'),
						'testimonials' => __('Testimonials Slider','builders'),
						'general-quote' => __('Text Quote','builders'),
						'clients' => __('Clients & Partners','builders')
					),
					'disabled' => array(
						'text' => __('Text/HTML','builders')
					)
				),
				'std'  => array(
					'enabled'  => array(
						'slider' => __('Homepage Slider','builders'),
						'quote'  => __('Request Quote','builders'),
						'ourwork' => __('Our Work','builders'),
						'tabs' => __('Homepage Tabs','builders'),
						'stats'   => __('Company Stats','builders'),
						'whyus'  => __('Why Us?','builders'),
						'testimonials' => __('Testimonials Slider','builders'),
						'general-quote' => __('Text Quote','builders'),
						'clients' => __('Clients & Partners','builders')
					),
					'disabled' => array(
						'text' => __('Text/HTML','builders')
					)
				)
			)
		)
	);

	/* ==========================================================================
	Slider
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Slider', 'builders' ),
		'desc' => '<p class="description">' . __('From here, you can control the slider on homepage.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_featured_slider',
				'type' => 'button_set_hide_below',
				'title' => __('Homepage Slider', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => wp_kses( __('<strong>Enable or Disable</strong> homepage slider with this button. The slider will show recent articles from the selected categories.', 'builders' ), array( 'strong' => array() ) ),
				'std' => '0',
				'args' => array('hide' => 3)
			),
			array(
				'id' => 'mts_featured_slider_cat',
				'type' => 'cats_multi_select',
				'title' => __('Slider Category(s)', 'builders' ),
				'sub_desc' => wp_kses( __('Select a category from the drop-down menu, latest articles from this category will be shown <strong>in the slider</strong>.', 'builders' ), array( 'strong' => array() ) ),
			),
			array(
				'id' => 'mts_featured_slider_num',
				'type' => 'text',
				'class' => 'small-text',
				'title' => __('Number of posts', 'builders' ),
				'sub_desc' => __('Enter the number of posts to show in the slider', 'builders' ),
				'std' => '3',
				'args' => array('type' => 'number')
			),	
			array(
				'id' => 'mts_custom_slider',
				'type' => 'group',
				'title' => __('Custom Slider', 'builders' ),
				'sub_desc' => __('With this option you can set up a slider with custom image and text instead of the default slider automatically generated from your posts.', 'builders' ),
				'groupname' => __('Slider', 'builders' ), // Group name
				'subfields' => array(
					array(
						'id' => 'mts_custom_slider_title',
						'type' => 'text',
						'title' => __('Title', 'builders' ),
						'sub_desc' => __('Title of the slide', 'builders' ),
					),
					array(
						'id' => 'mts_custom_slider_image',
						'type' => 'upload',
						'title' => __('Image', 'builders' ),
						'sub_desc' => __('Upload or select an image for this slide', 'builders' ),
						'return' => 'id'
					),	
					array('id' => 'mts_custom_slider_text',
						'type' => 'textarea',
						'title' => __('Text', 'builders' ),
						'sub_desc' => __('Description of the slide', 'builders' ),
					), 
					array(
						'id' => 'mts_custom_slider_readmore_text',
						'type' => 'text',
						'title' => __('Read More', 'builders' ),
						'sub_desc' => __('Insert a text for Read more button.', 'builders' ),
						'std' => 'Learn More'
					),
					array('id' => 'mts_custom_slider_link',
						'type' => 'text',
						'title' => __('Link', 'builders' ),
						'sub_desc' => __('Insert a link URL for the slide', 'builders' ),
						'std' => '#'
					),
				),
			),
		)
	);

	/* ==========================================================================
	Quotes
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Request Quote', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Request Quote section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_quote_background',
				'type' => 'background',
				'title' => __('Quote Section Background', 'builders' ), 
				'options' => array(
					'color'			=> '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'		=> array(),
					'size'			=> array(),
					'gradient'		=> '',
					'parallax'		=> array(),
				),
				'std' => array(
					'color'			=> '#f1f1f1',
					'use'			=> 'pattern',
					'image_pattern' => 'pattern39',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'		=> 'left top',
					'size'			=> 'cover',
					'gradient'		=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'		=> '0',
				)
			),
			array(
				'id' => 'mts_quote_heading',
				'type' => 'text',
				'title' => __('Quote Section Heading', 'builders' ),
				'std'=> __('We provide fast and affordable services for your projects.', 'builders' ),
			),
			array(
				'id' => 'mts_quote_heading_color',
				'type' => 'color',
				'title' => __('Quote Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for Quote headline.', 'builders' ),
				'std' => '#757575'
			),
			array(
				'id' => 'mts_quote_button_background',
				'type' => 'color',
				'title' => __('Quote Button Color', 'builders' ),
				'sub_desc' => __('Choose color for Quote button.', 'builders' ),
				'std' => '#fbd713'
			),
			array(
				'id' => 'mts_quote_button_text',
				'type' => 'text',
				'title' => __('Button Text', 'builders' ),
				'std' => 'Request A Quote',
				'class' => 'medium-text'
			),
			array(
				'id' => 'mts_quote_button_url',
				'type' => 'text',
				'title' => __('Button Link', 'builders' ),
				'std' => '#',
			),					
		),
	);

	/* ==========================================================================
	Our Work
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Our Work', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Our Work section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_ourwork_background',
				'type' => 'background',
				'title' => __('Our Work Section Background', 'builders' ), 
				'options' => array(
					'color'			=> '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'		=> array(),
					'size'			=> array(),
					'gradient'		=> '',
					'parallax'		=> array(),
				),
				'std' => array(
					'color'			=> '#ffffff',
					'use'			=> 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'		=> 'left top',
					'size'			=> 'cover',
					'gradient'		=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'		=> '0',
				)
			),
			array(
				'id' => 'mts_ourwork_heading',
				'type' => 'text',
				'title' => __('Our Work Section Heading', 'builders'),
				'std'=> __('Our Work', 'builders'),
				'class' => 'medium-text'
			),
			array(
				'id' => 'mts_ourwork_headline_color',
				'type' => 'color',
				'title' => __('Our Work Section Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_ourwork_section',
				'type' => 'group',
				'title' => __('Our Work Items', 'builders'), 
				'groupname' => __('Our Work Item', 'builders'), // Group name
				'subfields' =>  array(
					array(
						'id' => 'mts_ourwork_title',
						'type' => 'text',
						'title' => __('Title', 'builders' ),
						'class' => 'medium-text'
					),
					array(
						'id' => 'mts_ourwork_title_color',
						'type' => 'color',
						'title' => __('Title Color', 'builders' ),
						'sub_desc' => __('Choose color for title text.', 'builders' ),
						'std' => '#252525'
					),
					array(
						'id' => 'mts_ourwork_icon_select',
						'type' => 'icon_select',
						'allow_empty' => false,
						'title' => __('Icon Select', 'builders' ), 
						'sub_desc' => __('Select an icon from the vector icon set.', 'builders' )
					),
					array(
						'id' => 'mts_ourwork_icon_color',
						'type' => 'color',
						'title' => __('Icon Color', 'builders' ),
						'sub_desc' => __('Choose color for icon.', 'builders' ),
						'std' => '#252525'
					),
					array(
						'id' => 'mts_ourwork_url',
						'type' => 'text',
						'title' => __('Link', 'builders' ),
						'std' => ''
					),
					array(
						'id' => 'mts_ourwork_text',
						'type' => 'textarea',
						'title' => __('Description', 'builders' ),
						'std' => '#',
						'class' => 'medium-text'
					),
					array(
						'id' => 'mts_ourwork_text_color',
						'type' => 'color',
						'title' => __('Description Text Color', 'builders' ),
						'sub_desc' => __('Choose color for description from here.', 'builders' ),
						'std' => '#757575'
					),
				),
			)	
		),
	);

	/* ==========================================================================
	Tabs
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Tabs', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Tabs section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_tabs_background',
				'type' => 'background',
				'title' => __('Tabs Section Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#434343',
					'use'		   => 'pattern',
					'image_pattern' => 'hbg26',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_tab_active_color',
				'type' => 'color',
				'title' => __('Active Tab Text Color', 'builders' ),
				'sub_desc' => __('Choose text color for Active Tab.', 'builders' ),
				'std' => '#ffffff'
			),
			array(
				'id' => 'mts_tab_inactive_color',
				'type' => 'color',
				'title' => __('Inactive Tab Text Color', 'builders' ),
				'sub_desc' => __('Choose text color for Inactive Tab.', 'builders' ),
				'std' => '#757575'
			),
			array(
				'id' => 'mts_tab_bar_background',
				'type' => 'background',
				'title' => __('Tab Bar Background', 'builders' ), 
				'sub_desc' => __('Configure Top background.', 'builders' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#252525',
					'use'		   => 'gradient',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#303030', 'to' => '#252525', 'direction' => 'vertical' ),
					'parallax'	  => '0',
				),
			),
			array(
				'id' => 'mts_homepage_tabs_order',
				'type' => 'layout2',
				'title'	=> __('Tabs Order', 'builders' ),
				'options'  => array(
					'enabled'  => array(
						'latest'   => array(
							'label' 	=> __('From Our Blog', 'builders' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_tab_category',
									'type' => 'cats_select',
									'title' => __('Category', 'builders'  ), 
									'sub_desc' => __('Select a category or the latest posts for this section', 'builders'  ),
									'args' => array('include_latest' => 1, 'hide_empty' => 0),
									'std' => 'latest'
								),
								array(
									'id' => 'mts_tab_category_num',
									'type' => 'text',
									'title' => __('Number of posts', 'builders'), 
									'args' => array('type' => 'number'),
									'std' => '6',
									'class' => 'small-text',
								),
							)
						)
					),
					'disabled'  => array(
						'project'   => array(
							'label' 	=> __('Projects', 'builders' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_project_tab_heading',
									'type' => 'text',
									'title' => __('Project Tab Heading', 'builders' ),
									'std'=> __('Projects', 'builders' ),
									'class' => 'medium-text'
								),
								array(
									'id' => 'mts_tab_project_num',
									'type' => 'text',
									'title' => __('Number of projects', 'builders'), 
									'args' => array('type' => 'number'),
									'std' => '6',
									'class' => 'small-text',
								),
							)
						),
						'services'   => array(
							'label' 	=> __('Services', 'builders' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_service_tab_heading',
									'type' => 'text',
									'title' => __('Service Tab Heading', 'builders' ),
									'std'=> __('Services', 'builders' ),
									'class' => 'medium-text'
								),
								array(
									'id' => 'mts_tab_services_num',
									'type' => 'text',
									'title' => __('Number of services', 'builders'), 
									'args' => array('type' => 'number'),
									'std' => '6',
									'class' => 'small-text',
								),
							)
						)
					)
				),
				'std'  => array(
					'enabled'  => array(
						'latest'   => array(
							'label' 	=> __('Latest Blog Articles', 'builders' ),
						)
					),
					'disabled'  => array(
						'project'   => array(
							'label' 	=> __('Projects', 'builders' ),
						),
						'services'   => array(
							'label' 	=> __('Services', 'builders' ),
						)
					)
				),
			)
		),
	);

	/* ==========================================================================
	   Company Stats
	   ========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Company Stats', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Company Stats section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_stats_background',
				'type' => 'background',
				'title' => __('Stats Section Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#ffffff',
					'use'		   => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_stats_heading',
				'type' => 'text',
				'title' => __('Stats Section Heading', 'builders' ),
				'std'=> __('Company Stats', 'builders' )
			),
			array(
				'id' => 'mts_stats_heading_color',
				'type' => 'color',
				'title' => __('Stats Section Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_stats_counter',
				'title' => 'Stats Counter',
				'sub_desc' => __( 'Counter section to display statistics.', 'builders' ),
				'type' => 'group',
				'groupname' => __('Statistic', 'builders'), // Group name
				'subfields' =>  array(
					array(
						'id' => 'mts_stats_counter_small_text',
						'type' => 'text',
						'title' => __('Small Text', 'builders'),
					),
					array(
						'id' => 'mts_stats_counter_small_text_color',
						'type' => 'color',
						'title' => __('Stats small text Color', 'builders' ),
						'sub_desc' => __('Set color for small text from here.', 'builders' ),
						'std' => '#757575'
					),
					array(
						'id' => 'mts_stats_counter_number',
						'type' => 'text',
						'title' => __('Number', 'builders'), 
						'args' => array('type' => 'number')
					),
					array(
						'id' => 'mts_stats_counter_number_color',
						'type' => 'color',
						'title' => __('Stats Number Color', 'builders' ),
						'sub_desc' => __('Set color for Number from here.', 'builders' ),
						'std' => '#252525'
					),
				),
			)						
		),
	);

	/* ==========================================================================
	Why Us
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Why Us?', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Why Us section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_whyus_background',
				'type' => 'background',
				'title' => __('Why Us Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#f1f1f1',
					'use'		   => 'pattern',
					'image_pattern' => 'pattern38',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_whyus_heading',
				'type' => 'text',
				'title' => __('Why Us Section Heading', 'builders' ),
				'std'=> __('Why Us?', 'builders' )
			),
			array(
				'id' => 'mts_whyus_heading_color',
				'type' => 'color',
				'title' => __('Why Us Section Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_whyus_section',
				'type'	  => 'group',
				'title'	 => __('Why Us Posts', 'builders' ), 
				'groupname' => __('Post', 'builders' ), // Group name
				'subfields' =>  array(
					array(
						'id' => 'mts_whyus_title',
						'type' => 'text',
						'title' => __('Title', 'builders' )
					),
					array(
						'id' => 'mts_whyus_title_color',
						'type' => 'color',
						'title' => __('Title Color', 'builders' ),
						'sub_desc' => __('Choose color for title text.', 'builders' ),
						'std' => '#252525'
					),
					array(
						'id' => 'mts_whyus_icon_select',
						'type' => 'icon_select',
						'allow_empty' => false,
						'title' => __('Icon Select', 'builders'  ), 
						'sub_desc' => __('Select an icon from the vector icon set.', 'builders'  )
					),
					array(
						'id' => 'mts_whyus_icon_color',
						'type' => 'color',
						'title' => __('Icon Color', 'builders' ),
						'sub_desc' => __('Choose color for icon.', 'builders' ),
						'std' => '#252525'
					),
					array(
						'id' => 'mts_whyus_url',
						'type' => 'text',
						'title' => __('Link', 'builders' ),
						'std' => '#'
					),
					array(
						'id' => 'mts_whyus_text',
						'type' => 'textarea',
						'title' => __('Description', 'builders' ),
						'std' => '#'
					),
					array(
						'id' => 'mts_whyus_text_color',
						'type' => 'color',
						'title' => __('Description Text Color', 'builders' ),
						'sub_desc' => __('Choose color for description text.', 'builders' ),
						'std' => '#757575'
					),
				),
			),
			array(
				'id' => 'mts_whoweare_heading',
				'type' => 'text',
				'title' => __('Who We Are Section Heading', 'builders' ),
				'std'=> __('Who We Are?', 'builders' )
			),
			array(
				'id' => 'mts_whoweare_heading_color',
				'type' => 'color',
				'title' => __('Who We Are Section Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_whoweare_image',
				'type' => 'upload',
				'title' => __('Who We Are Section Image', 'builders'  ), 
				'sub_desc' => __('Upload or select an image. Recommended image size: 270x403', 'builders'  ),
				'return' => 'id'
			),
			array(
				'id' => 'mts_whoweare_text',
				'type' => 'textarea',
				'title' => __('Who We Are Text along with Image', 'builders' )
			),
			array(
				'id' => 'mts_whoweare_button_text',
				'type' => 'text',
				'title' => __('Who We Are Button Text', 'builders' ),
				'std' => 'Learn More'
			),
			array(
				'id' => 'mts_whoweare_url',
				'type' => 'text',
				'title' => __('Who We Are Link', 'builders' ),
				'std'=> '#'
			),
			array(
				'id' => 'mts_whoweare_button_color',
				'type' => 'color',
				'title' => __('Who We Are Button Color', 'builders' ),
				'sub_desc' => __('Choose color for button.', 'builders' ),
				'std' => '#fbd713'
			),					  
		),
	);

	/*==========================================================================
	Testimonial Slider
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Testimonials', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Testimonial Slider section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_testimonial_background',
				'type' => 'background',
				'title' => __('Testimonial Section Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#ffffff',
					'use'		   => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_testimonial_heading_color',
				'type' => 'color',
				'title' => __('Testimonial Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for Testimonial headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_testimonial_text_color',
				'type' => 'color',
				'title' => __('Testimonial Content Color', 'builders' ),
				'sub_desc' => __('Choose color for Testimonial content.', 'builders' ),
				'std' => '#757575'
			),
			array(
				'id' => 'mts_testimonial_border_color',
				'type' => 'color',
				'title' => __('Testimonial Border Color', 'builders' ),
				'sub_desc' => __('Choose color for Testimonial border.', 'builders' ),
				'std' => '#dedede'
			),
			array(
				'id' => 'mts_testimonial_box_color',
				'type' => 'color',
				'title' => __('Testimonial Box Color', 'builders' ),
				'sub_desc' => __('Choose color for Testimonial box.', 'builders' ),
				'std' => '#ffffff'
			),
			array(
				'id' => 'mts_testimonials_heading',
				'type' => 'text',
				'title' => __('Testimonials Section Heading', 'builders' ),
				'std'=> __('Testimonials', 'builders' ),
				'class' => 'medium-text'
			),
			array(
				'id' => 'mts_testimonials_section',
				'type'	  => 'group',
				'title'	 => __('Testimonials', 'builders' ), 
				'groupname' => __('Testimonial', 'builders' ), // Group name
				'subfields' =>  array(
					array(
						'id' => 'mts_testimonial_user_name',
						'type' => 'text',
						'title' => __('Author Name', 'builders' )
					),
					array(
						'id' => 'mts_testimonial_text',
						'type' => 'textarea',
						'title' => __('Testimonial', 'builders' ),
						'class' => 'medium-text'
					),
					array(
						'id' => 'mts_testimonial_image',
						'type' => 'upload',
						'title' => __('Testimonial Image', 'builders'  ), 
						'sub_desc' => __('Upload or select an image for this slide. Recommended image size: 70x70', 'builders'  ),
						'return' => 'id'
					),
					array(
						'id' => 'mts_testimonial_user_tagline',
						'type' => 'text',
						'title' => __('Author Tagline', 'builders' )
					),
					array(
						'id' => 'mts_testimonial_user_url',
						'type' => 'text',
						'title' => __('Author URL', 'builders' ),
						'std' => '#'
					),
				),
			)
		),
	);

	/*==========================================================================
	General Quote
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('General Quote', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Quote section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_general_quote_background',
				'type' => 'background',
				'title' => __('General Quote Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#434343',
					'use'		   => 'pattern',
					'image_pattern' => 'hbg28',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_general_quote_color',
				'type' => 'color',
				'title' => __('General Quote Color', 'builders' ),
				'sub_desc' => __('Choose color for general quote.', 'builders' ),
				'std' => '#ffffff'
			),
			array(
				'id' => 'mts_general_quote',
				'type' => 'textarea',
				'title' => __('General Quote Text', 'builders' ),
				'std' => 'We are building a fire, and everyday we train, we add more fuel. At just the right moment, we light the match.'
			),													
		),
	);

	/*==========================================================================
	Clients & Partners
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Client & Partners', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Clients & Partners section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_clients_background',
				'type' => 'background',
				'title' => __('Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#ffffff',
					'use'		   => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_clients_heading_color',
				'type' => 'color',
				'title' => __('Heading Color', 'builders' ),
				'sub_desc' => __('Choose color for headline.', 'builders' ),
				'std' => '#252525'
			),
			array(
				'id' => 'mts_clients_border_color',
				'type' => 'color',
				'title' => __('Borders Color', 'builders' ),
				'sub_desc' => __('Choose border color for client box.', 'builders' ),
				'std' => '#dedede'
			),
			array(
				'id' => 'mts_clients_heading',
				'type' => 'text',
				'title' => __('Section Heading', 'builders' ),
				'std'=> 'Clients & Partners',
				'class' => 'medium-text'
			),
			array(
				'id' => 'mts_clients_section',
				'type'	=> 'group',
				'title'	 => __('Clients', 'builders' ), 
				'groupname' => __('Client', 'builders' ), // Group name
				'subfields' =>  array(
					array(
						'id' => 'mts_partner_title',
						'type' => 'text',
						'title' => __('Title', 'builders'), 
					),
					array(
						'id' => 'mts_clients_image',
						'type' => 'upload',
						'title' => __('Image', 'builders'  ), 
						'sub_desc' => __('Upload or select an image.(Recommended size: 170x170px)', 'builders'  )
						//'return' => 'id'
					),
					array(
						'id' => 'mts_clients_url',
						'type' => 'text',
						'title' => __('Client URL', 'builders' ),
						'std' => '#'
					)
				),
			)
		),
	);

	/*==========================================================================
	Text
	========================================================================== */
	$sections[] = array(
		'icon' => '',
		'title' => __('Text/HTML', 'builders'),
		'desc' => '<p class="description">' . __('Control settings related to Text/HTML section from here.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_text_background',
				'type' => 'background',
				'title' => __('Text Section Background', 'builders' ), 
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#f1f1f1',
					'use'		   => 'pattern',
					'image_pattern' => 'hbg28',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				),
				'reset_at_version' => '1.2'
			),
			array(
				'id' => 'mts_text_color',
				'type' => 'color',
				'title' => __('Text Color', 'builders' ),
				'sub_desc' => __('Choose color for general quote.', 'builders' ),
				'std' => '#757575',
				'reset_at_version' => '1.2'
			),
			array(
				'id' => 'mts_text_full_width',
				'type' => 'button_set',
				'title' => __('Full Width', 'builders'),
				'sub_desc' => __('Enable this button to make text section full width.', 'builders'),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'std' => '1',
				'reset_at_version' => '1.2'
			),
			array(
				'id' => 'mts_text',
				'type' => 'textarea',
				'title' => __('Text', 'builders' ),
				'std' => '',
				'reset_at_version' => '1.2'
			),													
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Blog Page', 'builders' ),
		'desc' => '<p class="description">' . __('From here, you can control the appearance and functionality of your blog page.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_blog_subheader',
				'type' => 'button_set_hide_below',
				'title' => __('Blog Subheader', 'builders' ),
				'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
				'sub_desc' => __('Use this option to show or hide the subheader on Blog Page', 'builders' ),
				'std' => '1'
				),
				array(
				'id' => 'mts_blog_subheader_title',
				'type' => 'text',
				'title' => __( 'Blog Subheader Title', 'builders' ),
				'sub_desc' => __( 'Add blog Subheader title here.', 'builders' ),
				'std' => 'Our Blog',
				'class' => 'medium-text',
			),
			array(
				'id' => 'mts_blog_post_layout',
				'type' => 'button_set',
				'title' => __('Post Layout on Blog Page', 'builders' ),
				'options' => array('0' => __('Grid', 'builders' ), '1' => __('Traditional', 'builders' )),
				'sub_desc' => __('Choose Grid or Traditional layout for blog page.', 'builders' ),
				'std' => '0',
				'class' => 'green'
			),
			array(
				'id'	=> 'mts_featured_categories',
				'type'	  => 'group',
				'title'	 => __('Featured Categories', 'builders' ), 
				'sub_desc'  => __('Select categories appearing on the homepage.', 'builders' ),
				'groupname' => __('Section', 'builders' ), // Group name
				'subfields' => array(
					array(
						'id' => 'mts_featured_category',
						'type' => 'cats_select',
						'title' => __('Category', 'builders' ), 
						'sub_desc' => __('Select a category or the latest posts for this section', 'builders' ),
						'std' => 'latest',
						'args' => array('include_latest' => 1, 'hide_empty' => 0),
					),
					array(
						'id' => 'mts_featured_category_postsnum',
						'type' => 'text',
						'class' => 'small-text',
						'title' => __('Number of posts', 'builders' ), 
						'sub_desc' => sprintf(__('Enter the number of posts to show in this section.<br/><strong>For Latest Posts</strong>, this setting will be ignored, and number set in <a href="%s" target="_blank">Settings&nbsp;&gt;&nbsp;Reading</a> will be used instead.', 'builders' ), admin_url('options-reading.php')),
						'std' => '4',
						'args' => array('type' => 'number')
					),
				),
				'std' => array(
					'1' => array(
						'group_title' => '',
						'group_sort' => '1',
						'mts_featured_category' => 'latest',
						'mts_featured_category_postsnum' => get_option('posts_per_page')
					)
				)
			),
			array(
				'id' => 'mts_pagenavigation_type',
				'type' => 'radio',
				'title' => __('Pagination Type', 'builders' ),
				'sub_desc' => __('Select pagination type.', 'builders' ),
				'options' => array(
					'0'=> __('Default (Next / Previous)', 'builders' ),
					'1' => __('Numbered (1 2 3 4...)', 'builders' ),
					'2' => __( 'AJAX (Load More Button)', 'builders' ),
					'3' => __( 'AJAX (Auto Infinite Scroll)', 'builders' )
				),
				'std' => '1'
			),
			array(
				'id'	   => 'mts_home_headline_meta_info',
				'type'	 => 'layout',
				'title'	=> __('HomePage Post Meta Info', 'builders' ),
				'sub_desc' => __('Organize how you want the post meta info to appear on the homepage', 'builders' ),
				'options'  => array(
					'enabled'  => array(
						'date'	 => __('Date', 'builders' ),
						'author'   => __('Author Name', 'builders' )
					),
					'disabled' => array(
						'category' => __('Categories', 'builders' )
					)
				),
				'std'  => array(
					'enabled'  => array(
						'date'	 => __('Date', 'builders' ),
						'author'   => __('Author Name', 'builders' )
					),
					'disabled' => array(
						'category' => __('Categories', 'builders' )
					)
				)
			),
		)
	);

	$sections[] = array(
	'icon' => '',
	'title' => __('Single Posts', 'builders' ),
	'desc' => '<p class="description">' . __('From here, you can control the appearance and functionality of your single posts page.', 'builders' ) . '</p>',
	'fields' => array(
		array(
			'id' => 'mts_single_post_subheader',
			'type' => 'button_set_hide_below',
			'title' => __('Single Post Subheader', 'builders' ),
			'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
			'sub_desc' => __('Use this option to show or hide the subheader on Single Posts.', 'builders' ),
			'std' => '1'
			),
			array(
			'id' => 'mts_breadcrumb',
			'type' => 'button_set',
			'title' => __('Breadcrumbs', 'builders' ),
			'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
			'sub_desc' => __('Breadcrumbs are a great way to make your site more user-friendly. You can enable them by checking this box.', 'builders' ),
			'std' => '1'
		),
		array(
			'id'	   => 'mts_single_post_layout',
			'type'	 => 'layout2',
			'title'	=> __('Single Post Layout', 'builders' ),
			'sub_desc' => __('Customize the look of single posts', 'builders' ),
			'options'  => array(
				'enabled'  => array(
					'content'   => array(
						'label' 	=> __('Post Content', 'builders' ),
						'subfields'	=> array(
							array(
								'id' => 'mts_single_image',
								'type' => 'button_set',
								'title' => __('Featured Image', 'builders' ),
								'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
								'sub_desc' => __('Show or hide featured image in single posts.', 'builders' ),
								'std' => '1'
							),
						)
					),
					'related'   => array(
						'label' 	=> __('Related Posts', 'builders' ),
						'subfields'	=> array(
							array(
								'id' => 'mts_related_posts_taxonomy',
								'type' => 'button_set',
								'title' => __('Related Posts Taxonomy', 'builders' ) ,
								'options' => array(
									'tags' => __( 'Tags', 'builders' ),
									'categories' => __( 'Categories', 'builders' )
								) ,
								'class' => 'green',
								'sub_desc' => __('Related Posts based on tags or categories.', 'builders' ) ,
								'std' => 'categories'
							),
							array(
								'id' => 'mts_related_postsnum',
								'type' => 'text',
								'class' => 'small-text',
								'title' => __('Number of related posts', 'builders' ) ,
								'sub_desc' => __('Enter the number of posts to show in the related posts section.', 'builders' ) ,
								'std' => '2',
								'args' => array(
									'type' => 'number'
								)
							),

						)
					),
					'author'   => array(
						'label' 	=> __('Author Box', 'builders' ),
						'subfields'	=> array(

						)
					),
				),
				'disabled' => array(
					'tags'   => array(
						'label' 	=> __('Tags', 'builders' ),
						'subfields'	=> array(
						)
					),
				)
			)
		),
		array(
			'id'	=> 'mts_single_headline_meta_info',
			'type'	 => 'layout',
			'title'	=> __('Meta Info to Show', 'builders' ),
			'sub_desc' => __('Organize how you want the post meta info to appear', 'builders' ),
			'options'  => array(
				'enabled'  => array(
					'author'   => __('Author Name', 'builders' ),
					'date'	 => __('Date', 'builders' ),
					'category' => __('Categories', 'builders' ),
					'comment'  => __('Comment Count', 'builders' )
				),
				'disabled' => array()
			),
			'std'  => array(
				'enabled'  => array(
					'author'   => __('Author Name', 'builders' ),
					'date'	 => __('Date', 'builders' ),
					'category' => __('Categories', 'builders' ),
					'comment'  => __('Comment Count', 'builders' )
				),
				'disabled' => array()
			)
		),
		array(
			'id' => 'mts_author_comment',
			'type' => 'button_set',
			'title' => __('Highlight Author Comment', 'builders' ),
			'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
			'sub_desc' => __('Use this button to highlight author comments.', 'builders' ),
			'std' => '1'
		),
		array(
			'id' => 'mts_comment_date',
			'type' => 'button_set',
			'title' => __('Date in Comments', 'builders' ),
			'options' => array( '0' => __( 'Off', 'builders' ), '1' => __( 'On', 'builders' ) ),
			'sub_desc' => __('Use this button to show the date for comments.', 'builders' ),
			'std' => '1'
			),
		)
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Social Buttons', 'builders' ),
		'desc' => '<p class="description">' . __('Enable or disable social sharing buttons on single posts using these buttons.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_social_button_position',
				'type' => 'button_set',
				'title' => __('Social Sharing Buttons Position', 'builders' ),
				'options' => array('top' => __('Above Content', 'builders' ), 'bottom' => __('Below Content', 'builders' ), 'floating' => __('Floating', 'builders' )),
				'sub_desc' => __('Choose position for Social Sharing Buttons.', 'builders' ),
				'std' => 'floating',
				'class' => 'green'
			),
			array(
				'id' => 'mts_social_buttons_on_pages',
				'type' => 'button_set',
				'title' => __('Social Sharing Buttons on Pages', 'builders' ),
				'options' => array('0' => __('Off', 'builders' ), '1' => __('On', 'builders' )),
				'sub_desc' => __('Enable the sharing buttons for pages too, not just posts.', 'builders' ),
				'std' => '0',
			),
			array(
				'id'	   => 'mts_social_buttons',
				'type'	 => 'layout',
				'title'	=> __('Social Media Buttons', 'builders' ),
				'sub_desc' => __('Organize how you want the social sharing buttons to appear on single posts', 'builders' ),
				'options'  => array(
					'enabled'  => array(
						'facebookshare'   => __('Facebook Share', 'builders' ),
						'facebook'  => __('Facebook Like', 'builders' ),
						'twitter'   => __('Twitter', 'builders' ),
						'gplus'	 => __('Google Plus', 'builders' ),
						'pinterest' => __('Pinterest', 'builders' ),
					),
					'disabled' => array(
						'linkedin'  => __('LinkedIn', 'builders' ),
						'stumble'   => __('StumbleUpon', 'builders' ),
					)
				),
				'std'  => array(
					'enabled'  => array(
						'facebookshare'   => __('Facebook Share', 'builders' ),
						'facebook'  => __('Facebook Like', 'builders' ),
						'twitter'   => __('Twitter', 'builders' ),
						'gplus'	 => __('Google Plus', 'builders' ),
						'pinterest' => __('Pinterest', 'builders' ),
					),
					'disabled' => array(
						'linkedin'  => __('LinkedIn', 'builders' ),
						'stumble'   => __('StumbleUpon', 'builders' ),
					)
				)
			),
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-bar-chart-o',
		'title' => __('Ad Management', 'builders' ),
		'desc' => '<p class="description">' . __('Now, ad management is easy with our options panel. You can control everything from here, without using separate plugins.', 'builders' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_posttop_adcode',
				'type' => 'textarea',
				'title' => __('Below Post Title', 'builders' ),
				'sub_desc' => __('Paste your Adsense, BSA or other ad code here to show ads below your article title on single posts.', 'builders' )
			),
			array(
				'id' => 'mts_posttop_adcode_time',
				'type' => 'text',
				'title' => __('Show After X Days', 'builders' ),
				'sub_desc' => __('Enter the number of days after which you want to show the Below Post Title Ad. Enter 0 to disable this feature.', 'builders' ),
				'validate' => 'numeric',
				'std' => '0',
				'class' => 'small-text',
				'args' => array('type' => 'number')
			),
			array(
				'id' => 'mts_postend_adcode',
				'type' => 'textarea',
				'title' => __('Below Post Content', 'builders' ),
				'sub_desc' => __('Paste your Adsense, BSA or other ad code here to show ads below the post content on single posts.', 'builders' )
			),
			array(
				'id' => 'mts_postend_adcode_time',
				'type' => 'text',
				'title' => __('Show After X Days', 'builders' ),
				'sub_desc' => __('Enter the number of days after which you want to show the Below Post Title Ad. Enter 0 to disable this feature.', 'builders' ),
				'validate' => 'numeric',
				'std' => '0',
				'class' => 'small-text',
				'args' => array('type' => 'number')
			),
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-building',
		'title' => __('Projects', 'builders'),
		'desc' => '<p class="description">' . __('Manage the content displayed on the Projects section. You can manage projects items in the <a target="_blank" href="'.admin_url("edit.php?post_type=project").'">Projects</a> section of your WordPress dashboard.', 'builders') . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_projects_title',
				'type' => 'text',
				'title' => __( 'Title', 'builders' ),
				'sub_desc' => __( 'Section Title', 'builders' ),
				'std' => 'Projects'
			),
			array(
				'id' => 'mts_projects_count',
				'type' => 'text',
				'class' => 'small-text',
				'title' => __( 'No. of Projects posts', 'builders' ),
				'sub_desc' => __( 'Enter the number of projects posts you want to show', 'builders' ),
				'std' => '6',
				'args' => array('type' => 'number')
			),
			array(
				'id' => 'mts_project_pagenavigation_type',
				'type' => 'radio',
				'title' => __('Pagination Type - Projects page', 'builders'),
				'sub_desc' => __('Select pagination type for projects page.', 'builders'),
				'options' => array(
					'0'=> __('Default (Next / Previous)', 'builders'),
					'1' => __('Numbered (1 2 3 4...)', 'builders')
					),
				'std' => '1'
			)
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-phone',
		'title' => __('Services', 'builders'),
		'desc' => '<p class="description">' . __('Manage the content displayed on the Services section. You can manage services items in the <a target="_blank" href="'.admin_url("edit.php?post_type=service").'">Services</a> section of your WordPress dashboard.', 'builders') . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_services_title',
				'type' => 'text',
				'title' => __( 'Title', 'builders' ),
				'sub_desc' => __( 'Section Title', 'builders' ),
				'std' => 'Services'
			),
			array(
				'id' => 'mts_services_count',
				'type' => 'text',
				'class' => 'small-text',
				'title' => __( 'No. of Services posts', 'builders' ),
				'sub_desc' => __( 'Enter the number of services posts you want to show', 'builders' ),
				'std' => '6',
				'args' => array('type' => 'number')
			),
			array(
				'id' => 'mts_service_pagenavigation_type',
				'type' => 'radio',
				'title' => __('Pagination Type - Services page', 'builders'),
				'sub_desc' => __('Select pagination type for services page.', 'builders'),
				'options' => array(
					'0'=> __('Default (Next / Previous)', 'builders'),
					'1' => __('Numbered (1 2 3 4...)', 'builders')
					),
				'std' => '1'
			),
			array(
			 	'id' => 'mts_brochures',
			 	'title' => __('Add Brochures', 'builders'), 
			 	'sub_desc' => __( 'Add Brochures in Services widget area.', 'builders' ),
			 	'type' => 'group',
			 	'groupname' => __('Brochures', 'builders'), // Group name
			 	'subfields' => array(
					array(
						'id' => 'mts_brochures_title',
						'type' => 'text',
						'title' => __('Title', 'builders')
					),
					array(
						'id' => 'mts_brochures_icon',
						'type' => 'icon_select',
						'title' => __('Icon', 'builders')
					),
					array(
						'id' => 'mts_brochures_link',
						'type' => 'text',
						'title' => __('URL', 'builders'), 
						'std' => '#'
					),
				)
			),
		)
	);

	$sections[] = array(
		'icon' => 'fa fa-columns',
		'title' => __('Sidebars', 'builders' ),
		'desc' => '<p class="description">' . __('Now you have full control over the sidebars. Here you can manage sidebars and select one for each section of your site, or select a custom sidebar on a per-post basis in the post editor.', 'builders' ) . '<br></p>',
		'fields' => array(
			array(
				'id' => 'mts_custom_sidebars',
				'type' => 'group', //doesn't need to be called for callback fields
				'title' => __('Custom Sidebars', 'builders' ),
				'sub_desc'  => wp_kses( __('Add custom sidebars. <strong style="font-weight: 800;">You need to save the changes to use the sidebars in the dropdowns below.</strong><br />You can add content to the sidebars in Appearance &gt; Widgets.', 'builders' ), array( 'strong' => array(), 'br' => array() ) ),
				'groupname' => __('Sidebar', 'builders' ), // Group name
				'subfields' => array(
					array(
						'id' => 'mts_custom_sidebar_name',
						'type' => 'text',
						'title' => __('Name', 'builders' ),
						'sub_desc' => __('Example: Homepage Sidebar', 'builders' )
					),	
					array(
						'id' => 'mts_custom_sidebar_id',
						'type' => 'text',
						'title' => __('ID', 'builders' ),
						'sub_desc' => __('Enter a unique ID for the sidebar. Use only alphanumeric characters, underscores (_) and dashes (-), eg. "sidebar-home"', 'builders' ),
						'std' => 'sidebar-'
					),
				),
			),
			array(
				'id' => 'mts_sidebar_for_home',
				'type' => 'sidebars_select',
				'title' => __('Homepage', 'builders' ),
				'sub_desc' => __('Select a sidebar for the homepage.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_post',
				'type' => 'sidebars_select',
				'title' => __('Single Post', 'builders' ),
				'sub_desc' => __('Select a sidebar for the single posts. If a post has a custom sidebar set, it will override this.', 'builders' ),
				'args' => array('exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_page',
				'type' => 'sidebars_select',
				'title' => __('Single Page', 'builders' ),
				'sub_desc' => __('Select a sidebar for the single pages. If a page has a custom sidebar set, it will override this.', 'builders' ),
				'args' => array('exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_archive',
				'type' => 'sidebars_select',
				'title' => __('Archive', 'builders' ),
				'sub_desc' => __('Select a sidebar for the archives. Specific archive sidebars will override this setting (see below).', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_category',
				'type' => 'sidebars_select',
				'title' => __('Category Archive', 'builders' ),
				'sub_desc' => __('Select a sidebar for the category archives.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_tag',
				'type' => 'sidebars_select',
				'title' => __('Tag Archive', 'builders' ),
				'sub_desc' => __('Select a sidebar for the tag archives.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_date',
				'type' => 'sidebars_select',
				'title' => __('Date Archive', 'builders' ),
				'sub_desc' => __('Select a sidebar for the date archives.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_author',
				'type' => 'sidebars_select',
				'title' => __('Author Archive', 'builders' ),
				'sub_desc' => __('Select a sidebar for the author archives.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_search',
				'type' => 'sidebars_select',
				'title' => __('Search', 'builders' ),
				'sub_desc' => __('Select a sidebar for the search results.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_notfound',
				'type' => 'sidebars_select',
				'title' => __('404 Error', 'builders' ),
				'sub_desc' => __('Select a sidebar for the 404 Not found pages.', 'builders' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_shop',
				'type' => 'sidebars_select',
				'title' => __('Shop Pages', 'builders' ),
				'sub_desc' => wp_kses( __('Select a sidebar for Shop main page and product archive pages (WooCommerce plugin must be enabled). Default is <strong>Shop Page Sidebar</strong>.', 'builders' ), array( 'strong' => array() ) ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => 'shop-sidebar'
			),
			array(
				'id' => 'mts_sidebar_for_product',
				'type' => 'sidebars_select',
				'title' => __('Single Product', 'builders' ),
				'sub_desc' => wp_kses( __('Select a sidebar for single products (WooCommerce plugin must be enabled). Default is <strong>Single Product Sidebar</strong>.', 'builders' ), array( 'strong' => array() ) ),
				'args' => array('allow_nosidebar' => false, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar')),
				'std' => 'product-sidebar'
			),
			array(
				'id' => 'mts_sidebar_for_listings',
				'type' => 'sidebars_select',
				'title' => __('WP Real Estate Pages', 'builders' ),
				'sub_desc' => wp_kses( __('Select a sidebar for WP Real Estate Listings archive and single pages (WP Real Estate plugin must be enabled).', 'builders' ), array( 'strong' => array() ) ),
				'args' => array('allow_nosidebar' => true, 'exclude' => array('sidebar', 'footer-first', 'footer-first-2', 'footer-first-3', 'footer-first-4', 'footer-second', 'footer-second-2', 'footer-second-3', 'footer-second-4', 'widget-header','shop-sidebar', 'product-sidebar', 'shop-sidebar')),
				'std' => 'mts_nosidebar',
				'reset_at_version' => '1.1.17'
			),
		),
	);

	$sections[] = array(
		'icon' => 'fa fa-list-alt',
		'title' => __('Navigation', 'builders' ),
		'desc' => '<p class="description"><div class="controls">' . sprintf( __('Navigation settings can now be modified from the %s.', 'builders' ), '<a href="nav-menus.php"><b>' . __( 'Menus Section', 'builders' ) . '</b></a>' ) . '<br></div></p>'
	);

				
	$tabs = array();
	
	$args['presets'] = array();
	$args['show_translate'] = false;
	include('theme-presets.php');
	
	global $NHP_Options;
	$NHP_Options = new NHP_Options($sections, $args, $tabs);

}//function
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value){
	print_r($field);
	print_r($value);

}//function

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value){
	
	$error = false;
	$value =  'just testing';
	/*
	do your validation
	
	if(something){
		$value = $value;
	}elseif(somthing else){
		$error = true;
		$value = $existing_value;
		$field['msg'] = 'your custom error message';
	}
	*/
	$return['value'] = $value;
	if($error == true){
		$return['error'] = $field;
	}
	return $return;
	
}//function

/*--------------------------------------------------------------------
 * 
 * Default Font Settings
 *
 --------------------------------------------------------------------*/
if(function_exists('mts_register_typography')) { 
  mts_register_typography(array(
  	'logo_font' => array(
	  'preview_text' => __( 'Logo Font', 'builders' ),
	  'preview_color' => 'dark',
	  'font_family' => 'Hind',
	  'font_variant' => '700',
	  'font_size' => '34px',
	  'font_color' => '#ffffff',
	  'css_selectors' => '#logo a'
	),
	'top_navigation_font' => array(
	  'preview_text' => __( 'Top Navigation Font', 'builders' ),
	  'preview_color' => 'dark',
	  'font_family' => 'Hind',
	  'font_variant' => 'normal',
	  'font_size' => '14px',
	  'font_color' => '#757575',
	  'css_selectors' => '#secondary-navigation a'
	),
	'navigation_font' => array(
	  'preview_text' => __( 'Primary Navigation Font', 'builders' ),
	  'preview_color' => 'dark',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '14px',
	  'font_color' => '#757575',
	  'css_selectors' => '#primary-navigation a',
	  'additional_css' => 'text-transform: uppercase;'
	),
	'home_title_font' => array(
	  'preview_text' => __( 'Blog Page Article Title', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_size' => '20px',
	  'font_variant' => '700',
	  'font_color' => '#252525',
	  'css_selectors' => '.latestPost .title a'
	),
	'single_title_font' => array(
	  'preview_text' => __( 'Single Blog Article Title', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_size' => '30px',
	  'font_variant' => '700',
	  'font_color' => '#252525',
	  'css_selectors' => '.single-title'
	),
	'content_font' => array(
	  'preview_text' => __( 'Content Font', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Hind',
	  'font_size' => '14px',
	  'font_variant' => 'normal',
	  'font_color' => '#757575',
	  'css_selectors' => 'body'
	),
	'widget_title_font' => array(
	  'preview_text' => __( 'Sidebar Widget Title', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '24px',
	  'font_color' => '#252525',
	  'css_selectors' => '.widget h3'
	),
	'widget_font' => array(
	  'preview_text' => __( 'Sidebar Widget Font', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '14px',
	  'font_color' => '#757575',
	  'css_selectors' => '.widget'
	),
	'footer_title_font' => array(
	  'preview_text' => __( 'Footer Widget Title', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '22px',
	  'font_color' => '#dedede',
	  'css_selectors' => '#site-footer .widget h3'
	),
	'footer_font' => array(
	  'preview_text' => __( 'Footer Widget Font', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => 'normal',
	  'font_size' => '14px',
	  'font_color' => '#dedede',
	  'css_selectors' => '#site-footer, .footer-widgets .widget'
	),
	'post_info_font' => array(
	  'preview_text' => __( 'Post Info Font', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '12px',
	  'font_color' => '#757575',
	  'css_selectors' => '.breadcrumb, .post-info, .owl-dot:before'
	),
	'h1_headline' => array(
	  'preview_text' => __( 'Content H1', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '28px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h1'
	),
	'h2_headline' => array(
	  'preview_text' => __( 'Content H2', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '24px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h2'
	),
	'h3_headline' => array(
	  'preview_text' => __( 'Content H3', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '22px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h3'
	),
	'h4_headline' => array(
	  'preview_text' => __( 'Content H4', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '20px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h4'
	),
	'h5_headline' => array(
	  'preview_text' => __( 'Content H5', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '18px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h5'
	),
	'h6_headline' => array(
	  'preview_text' => __( 'Content H6', 'builders' ),
	  'preview_color' => 'light',
	  'font_family' => 'Montserrat',
	  'font_variant' => '700',
	  'font_size' => '16px',
	  'font_color' => '#252525',
	  'css_selectors' => 'h6'
	)
  ));
}