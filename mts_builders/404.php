<?php
/**
 * The template for displaying 404 (Not Found) pages.
 */
get_header(); ?>
<div id="page">
	<article class="article ss-full-width">
		<div id="content_box" >
			<header>
				<div class="title">
					<h1><?php _e('Error 404 - Page Not Found', 'builders' ); ?></h1>
				</div>
			</header>
			<div class="post-content">
				<p><?php _e('Oops! We couldn\'t find this Page.', 'builders' ); ?></p>
				<p><?php _e('Please check your URL or use the search form below.', 'builders' ); ?></p>
				<?php get_search_form();?>
			</div><!--.post-content--><!--#error404 .post-->
		</div><!--#content-->
	</article>
	<?php get_footer(); ?>