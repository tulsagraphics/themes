<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<div class="company-stats clearfix">
	<div class="container">
		<?php if( !empty($mts_options['mts_stats_heading']) ) { ?>
			<h3 class="featured-category-title" style="color: <?php echo $mts_options['mts_stats_heading_color']; ?>;"><?php echo $mts_options['mts_stats_heading']; ?></h3>
		<?php } ?>
		<div class="counter_wrap">
			<?php
			if ( !empty( $mts_options['mts_stats_counter'] ) ) {
				$counter_items = count( $mts_options['mts_stats_counter'] );
				if ($counter_items):
					$i=0;
					foreach( $mts_options['mts_stats_counter'] as $count ) :
						$counter_small_text = $count['mts_stats_counter_small_text'];
						$counter_number = $count['mts_stats_counter_number']; ?>
						<div class="counting count-<?php echo $i; ?>" style="width: <?php echo 100 / $counter_items; ?>%;">
							<h4 class="counter" style="color: <?php echo $count['mts_stats_counter_number_color']; ?>;">0</h4>
							<div class="description" style="color: <?php echo $count['mts_stats_counter_small_text_color']; ?>;">
								<?php echo $counter_small_text; ?>
							</div>
						</div>
						<script type="text/javascript">
							jQuery(window).load(function() {
								jQuery(document).scroll(function(){
									if(jQuery(this).scrollTop()>=jQuery('.counter_wrap').position().top - 200){
										jQuery('.counter_wrap .count-<?php echo $i; ?> .counter').animateNumbers(<?php echo $counter_number; ?>,200);
									}
								});
							});
						</script>
						<?php
						$i++;
					endforeach;
				endif;
			}?>
		</div>
	</div><!-- /container -->
</div><!-- /company-stats -->