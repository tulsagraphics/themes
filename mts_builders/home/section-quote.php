<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php if( !empty($mts_options['mts_quote_heading']) && !empty($mts_options['mts_quote_button_text']) ) { ?>
<div class="request-quote clearfix">
	<div class="container"> 
		<div class="description"><?php echo !empty($mts_options['mts_quote_heading']) ? $mts_options['mts_quote_heading'] : ''; ?></div>
		<div class="readMore">
			<a href="<?php echo !empty($mts_options['mts_quote_button_url']) ? $mts_options['mts_quote_button_url'] : ''; ?>" title="<?php echo !empty($mts_options['mts_quote_button_text']) ? $mts_options['mts_quote_button_text'] : ''; ?>"><?php echo !empty($mts_options['mts_quote_button_text']) ? $mts_options['mts_quote_button_text'] : ''; ?></a>
		</div>
	</div>  
</div>
<?php } ?>