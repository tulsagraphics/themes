<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php if( !empty($mts_options['mts_text']) || $mts_options['mts_text'] != '' ) { ?>
<div class="mts-text clearfix" style="color: <?php echo $mts_options['mts_text_color']; ?>">
	<?php if( !empty($mts_options['mts_text_full_width']) && $mts_options['mts_text_full_width'] == '1' ) echo '<div class="container">'; ?>
		<?php echo do_shortcode($mts_options['mts_text']); ?>
	<?php if( !empty($mts_options['mts_text_full_width']) && $mts_options['mts_text_full_width'] == '1' ) echo '</div>'; ?>
</div>
<?php } ?>