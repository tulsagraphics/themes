<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php if ( !empty($mts_options['mts_ourwork_section']) && is_array($mts_options['mts_ourwork_section']) ) { ?>
	<div class="ourwork-section clearfix">
		<div class="container">
			<?php if( !empty($mts_options['mts_ourwork_heading']) ) : ?>
				<h3 class="featured-category-title" style="color: <?php echo $mts_options['mts_ourwork_headline_color']; ?>"><?php echo $mts_options['mts_ourwork_heading']; ?></h3>
			<?php endif; ?>
			<?php foreach( $mts_options['mts_ourwork_section'] as $ourworkpost ) : ?>
				<div class="ourwork-content">
					<div class="icon"><i class="fa fa-<?php print $ourworkpost['mts_ourwork_icon_select'] ?>" style="color: <?php echo $ourworkpost['mts_ourwork_icon_color']; ?>"></i></div>
					<header>
						<?php if( !empty($ourworkpost['mts_ourwork_title']) ) : ?>
							<h2 class="title front-view-title" style="color: <?php echo $ourworkpost['mts_ourwork_title_color']; ?>">
								<?php if( !empty( $ourworkpost['mts_ourwork_url'] ) ) : ?>
									<a href="<?php echo $ourworkpost['mts_ourwork_url']; ?>" title="<?php echo $ourworkpost['mts_ourwork_title']; ?>">
								<?php endif; ?>
									<?php echo $ourworkpost['mts_ourwork_title']; ?>
								<?php if( !empty( $ourworkpost['mts_ourwork_url'] ) ) : ?>
									</a>
								<?php endif; ?>
							</h2>
						<?php endif; ?>
						<div class="front-view-content" style="color: <?php echo $ourworkpost['mts_ourwork_text_color']; ?>">
							<?php echo $ourworkpost['mts_ourwork_text']; ?>
						</div>
					</header>
				</div>
			<?php endforeach; ?>
		</div>
	</div> <!--Work Content-->
<?php } ?>