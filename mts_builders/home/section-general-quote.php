<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php if( !empty($mts_options['mts_general_quote']) ) { ?>
	<div class="today-quotes clearfix">
		<div class="container">
			<h4 class="text">
				<?php echo $mts_options['mts_general_quote']; ?>
			</h4>
		</div>
	</div>
<?php } ?>