<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<div class="testimonial-container homepage-section owl-loading clearfix">
	<div class="container">
		<?php if ( !empty($mts_options['mts_testimonials_section']) && is_array($mts_options['mts_testimonials_section']) ) { ?>
		<h3 class="featured-category-title"><?php echo !empty($mts_options['mts_testimonials_heading']) ? $mts_options['mts_testimonials_heading'] : ''; ?></h3>
		
		<div id="slider" class="testimonial-slider">
			<?php foreach( $mts_options['mts_testimonials_section'] as $testimonial ) : ?>
				<div> 
					<div class="slide-caption">
						<?php if( !empty($testimonial['mts_testimonial_text']) ) { ?>
							<div class="testimonial-quotes">
								<?php echo $testimonial['mts_testimonial_text']; ?>
							</div>
						<?php } ?>
						<div class="testimonial-author clearfix">
							<?php if( !empty($testimonial['mts_testimonial_image']) ) {
								echo wp_get_attachment_image( $testimonial['mts_testimonial_image'], 'builders-testimonial', false, array('title' => '') );
							} ?>
							<div class="testimonial-author-wrap">
								<?php if( !empty($testimonial['mts_testimonial_user_name']) ) { ?>
									<h4 class="thename"><?php echo $testimonial['mts_testimonial_user_name']; ?></h4>
								<?php } ?>
								<?php if( !empty($testimonial['mts_testimonial_user_tagline']) ) { ?>
									<div class="theaddress">
										<?php if( !empty( $testimonial['mts_testimonial_user_url'] ) ) : ?>
											<a href="<?php echo $testimonial['mts_testimonial_user_url']; ?>">
										<?php endif;
											echo $testimonial['mts_testimonial_user_tagline'];
										if( !empty( $testimonial['mts_testimonial_user_url'] ) ) : ?>
											</a>
										<?php endif; ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div> <!--testimonial slider-->

		<div class="custom-nav">
		   <a class="btn prev"><i class="fa fa-angle-left"></i></a>
		   <a class="btn next"><i class="fa fa-angle-right"></i></a>
		</div>
		<?php } ?>
	</div><!-- /container	-->
</div> <!--testimonial slider-->
