<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<div class="tab-projects projects clearfix">

	<!--Project Section Starting-->
	<div class="featured-view">
		<?php if ( !empty($mts_options['mts_homepage_tabs_order']) && is_array($mts_options['mts_homepage_tabs_order']) ) { ?>
			<div class="links">
				<ul class="container">
					<?php
					// Tabs ordering
					if ( isset( $mts_options['mts_homepage_tabs_order'] ) && is_array( $mts_options['mts_homepage_tabs_order'] ) && array_key_exists( 'enabled', $mts_options['mts_homepage_tabs_order'] ) ) {
						$tabs = $mts_options['mts_homepage_tabs_order']['enabled'];
					} else {
						$tabs = array( 'latest' => 'latest', 'project' => 'project', 'services' => 'services' );
					}

					$i = 0;
					foreach( $tabs as $tab => $label ) {
						$tab_active_class = 0 === $i ? ' class="loaded active"' : ''; ?>
						<li <?php echo $tab_active_class;?>>
							<?php
							switch ($tab) {
								case 'latest':
								?>

									<a href="#latest">
										<?php if(!empty($mts_options['mts_tab_category']) && $mts_options['mts_tab_category'] != 'latest') {
											echo get_cat_name( $mts_options['mts_tab_category'] );
										} else {
											_e('From Our Blog','builders');
										} ?>
									</a>

								<?php
								break;
								case 'project':

									if( !empty($mts_options['mts_project_tab_heading']) ) : ?>
										<a href="#project"><?php echo $mts_options['mts_project_tab_heading']; ?></a>
									<?php endif;

								break;
								case 'services':

									if( !empty($mts_options['mts_service_tab_heading']) ) : ?>
										<a href="#services"><?php echo $mts_options['mts_service_tab_heading']; ?></a>
									<?php endif;

								break;
							} ?>
						</li>
						<?php $i++;
					} ?>
				</ul>
			</div>
		<?php } ?>

		<div class="featured-content">
			<div class="container">
				<?php
				// Tabs ordering
				if ( isset( $mts_options['mts_homepage_tabs_order'] ) && is_array( $mts_options['mts_homepage_tabs_order'] ) && array_key_exists( 'enabled', $mts_options['mts_homepage_tabs_order'] ) ) {
					$tabs = $mts_options['mts_homepage_tabs_order']['enabled'];
				} else { 
					$tabs = array( 'latest' => 'latest', 'project' => 'project', 'services' => 'services' );
				}

				$i = 0;
				foreach( $tabs as $tab => $label ) {
					$tab_active_class = 0 === $i ? ' loaded active' : '';
					?>
					<div class="<?php echo $tab.$tab_active_class; ?> featured-view-posts">
						<?php if ( 0 === $i ) mts_homepage_tab( $tab ); ?>
					</div>
					<?php
					$i++;
				}
				?>
			</div>
		</div>
	</div>
</div>
