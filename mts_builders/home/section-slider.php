<?php $mts_options = get_option(MTS_THEME_NAME); 

if ( !is_paged() && $mts_options['mts_featured_slider'] == '1' ) { ?>

	<div class="primary-slider-container clearfix loading">
		<div id="slider" class="primary-slider">
		<?php if ( empty( $mts_options['mts_custom_slider'] ) ) {
			// prevent implode error
			if ( empty( $mts_options['mts_featured_slider_cat'] ) || !is_array( $mts_options['mts_featured_slider_cat'] ) ) {
				$mts_options['mts_featured_slider_cat'] = array('0');
			}

			$slider_cat = implode( ",", $mts_options['mts_featured_slider_cat'] );
			$slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page='.$mts_options['mts_featured_slider_num']);
			while ( $slider_query->have_posts() ) : $slider_query->the_post();
			?>
			<div class="primary-slider-item" style="background:url(<?php echo mts_get_thumbnail_url( 'builders-slider' ); ?>); min-height: 400px; background-position: center center; background-size: cover;"> 
				<a href="<?php echo esc_url( get_the_permalink() ); ?>">
					<div class="slide-caption">
						<div class="slide-caption-inner">
							<h2 class="slide-title"><?php echo substr(the_title( $before = '', $after = '', FALSE), 0, 30) . '...'; ?>
							</h2>
							<div class="slide-description"><?php echo mts_excerpt(23); ?></div>
							<div class="readMore">
								<?php _e( 'Read More', 'builders' ); ?>
							</div>
						</div>
					</div>
				</a> 
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
		<?php } else { ?>
			<?php foreach( $mts_options['mts_custom_slider'] as $slide ) : ?>
				<div class="primary-slider-item" style="background:url(<?php echo wp_get_attachment_url( $slide['mts_custom_slider_image'], 'builders-slider', false, array('title' => '') ); ?>); min-height: 400px; background-position: center center; background-size: cover;">
					<a href="<?php echo esc_url( $slide['mts_custom_slider_link'] ); ?>">
						<div class="slide-caption">
							<div class="slide-caption-inner">
								<h2 class="slide-title"><?php echo esc_html( mts_truncate( $slide['mts_custom_slider_title'], 28 ) ) ?></h2>
								<p class="slide-description"><?php echo esc_html( mts_truncate( $slide['mts_custom_slider_text'], 200 ) ); ?></p>
								<?php if( !empty($slide['mts_custom_slider_readmore_text']) ) { ?>
									<div class="readMore"><?php _e( $slide['mts_custom_slider_readmore_text'], 'builders' ); ?></div>
								<?php } ?>
							</div>
							<!-- slide-caption-inner -->
						</div>
						<!-- slide-caption -->
					</a>
				</div>
			<?php endforeach; ?>
		<?php } ?>
		</div><!-- .primary-slider -->
	</div><!-- .primary-slider-container -->

<?php } ?>