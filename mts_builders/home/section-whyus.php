<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php if( !empty($mts_options['mts_whyus_section']) || !empty($mts_options['whoweare_image']) || !empty($mts_options['whoweare_text']) ) { ?>
	<div class="why-us-section clearfix">  <!--Why Us section-->
		<div class="container">	
			<?php if ( !empty($mts_options['mts_whyus_section']) && is_array($mts_options['mts_whyus_section']) ) { ?>
				 <div class="why-us-left">
					<h3 class="featured-category-title" style="color: <?php echo $mts_options['mts_whyus_heading_color']; ?>;"><?php echo !empty($mts_options['mts_whyus_heading']) ? $mts_options['mts_whyus_heading'] : ''; ?></h3>
					<?php foreach( $mts_options['mts_whyus_section'] as $whyuspost ) : ?>
						<div class="why-us-left-content">
						   <div class="icon"><i class="fa fa-<?php echo $whyuspost['mts_whyus_icon_select']; ?>" style="color: <?php echo $whyuspost['mts_whyus_icon_color']; ?>"></i></div>
						   <header>
							 <h2 class="title front-view-title" style="color: <?php echo $whyuspost['mts_whyus_title_color']; ?>">
								<?php if( !empty( $whyuspost['mts_whyus_url'] ) ) : ?>
									<a href="<?php echo $whyuspost['mts_whyus_url']; ?>" title="<?php echo $whyuspost['mts_whyus_title']; ?>">
								<?php endif; ?>
									<?php echo ($whyuspost['mts_whyus_title']) ? $whyuspost['mts_whyus_title'] : ''; ?>
								<?php if( !empty( $whyuspost['mts_whyus_url'] ) ) : ?>
									</a>
								<?php endif; ?>
							 </h2>
							 <div class="front-view-content" style="color: <?php echo $whyuspost['mts_whyus_text_color']; ?>">
								<?php echo $whyuspost['mts_whyus_text']; ?>
							 </div>
						   </header>
						</div>
					<?php endforeach; ?>

				 </div><!-- /why-us-left -->
			<?php } ?>

			<?php if( !empty($mts_options['mts_whoweare_image']) || !empty($mts_options['mts_whoweare_text']) ) { ?>
				<div class="why-us-right">
					<h3 class="featured-category-title" style="color: <?php echo $mts_options['mts_whoweare_heading_color']; ?>;"><?php echo !empty($mts_options['mts_whoweare_heading']) ? $mts_options['mts_whoweare_heading'] : ''; ?></h3>

					<?php if( !empty($mts_options['mts_whoweare_image']) ) {
						echo wp_get_attachment_image( $mts_options['mts_whoweare_image'], 'builders-whoweare', false, array('title' => '') );
					} ?>

					<?php echo !empty($mts_options['mts_whoweare_text']) ? '<p  style="color:'.$whyuspost['mts_whyus_text_color'].'">'.$mts_options['mts_whoweare_text'].'</p>' : ''; ?>

					<?php if( !empty($mts_options['mts_whoweare_button_text']) ) { ?>
						<div class="readMore">
						   <a href="<?php echo !empty($mts_options['mts_whoweare_url']) ? $mts_options['mts_whoweare_url'] : ''; ?>" style="background: <?php echo $mts_options['mts_whoweare_button_color']; ?>"><?php echo $mts_options['mts_whoweare_button_text']; ?></a>
						</div>
					<?php } ?>
				</div><!-- /why-us-right -->
			<?php } ?>
		</div><!-- /container -->
	</div> <!--End Why Us Section-->
<?php } ?>