<?php $mts_options = get_option(MTS_THEME_NAME); ?>



<?php if ( !empty($mts_options['mts_clients_section']) && is_array($mts_options['mts_clients_section']) ) { ?>

<div class="our-clients-section homepage-section clearfix">

	<div class="container">

		<?php if( !empty($mts_options['mts_clients_heading']) ) : ?>
			<h3 class="featured-category-title"><?php echo $mts_options['mts_clients_heading']; ?></h3>
		<?php endif; ?>

		<ul class="row"> 

			<?php foreach( $mts_options['mts_clients_section'] as $partner_image ) : ?>

			<li class="client-info">

				<?php if( $partner_image['mts_clients_url'] ) { ?>

					<a href="<?php echo $partner_image['mts_clients_url']; ?>">

				<?php }

					if( !empty($partner_image['mts_clients_image']) ) { ?>

						<img src="<?php echo $partner_image['mts_clients_image']; ?>" alt="">

					<?php }

				if( $partner_image['mts_clients_url'] ) { ?>

					</a>

				<?php } ?>

			</li>

			<?php endforeach; ?>

		</ul>

	</div> 

</div>

<?php } ?>