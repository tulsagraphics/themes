<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 */
?>
<!DOCTYPE html>
<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<html class="no-js" <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php mts_meta(); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>
<body id="blog" <?php body_class('main home'); ?> itemscope itemtype="http://schema.org/WebPage">	   
	<div class="main-container">
		<header id="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
			<div class="top-nav">
				<div class="container clearfix">
					<div class="top-description">
						<?php echo $mts_options['mts_top_description'] == 1 ? $mts_options['mts_top_description_text'] : "" ?>
					</div>
					<?php if ( $mts_options['mts_social_icon_header'] == '1' && !empty($mts_options['mts_header_social']) && is_array($mts_options['mts_header_social']) ) { ?>
						<div class="social_icons">
							<?php foreach( $mts_options['mts_header_social'] as $header_icons ) :
								if( ! empty( $header_icons['mts_header_icon'] ) && isset( $header_icons['mts_header_icon'] ) ) : ?>
									<a href="<?php print $header_icons['mts_header_icon_link'] ?>" class="header-<?php print $header_icons['mts_header_icon'] ?>"><span class="fa fa-<?php print $header_icons['mts_header_icon'] ?>"></span></a>
								<?php endif;
							endforeach; ?>
						</div>
					<?php }
					if ( $mts_options['mts_show_secondary_nav'] == '1' ) { ?>
						<div id="secondary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'builders' ); ?></a>
							<?php if ( has_nav_menu( 'mobile' ) ) { ?>
							<nav class="navigation clearfix">
								<?php if ( has_nav_menu( 'secondary' ) ) {
									wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) );
								} else { ?>
									<ul class="menu clearfix">
										<?php wp_list_categories('title_li='); ?>
									</ul>
								<?php } ?>
							</nav>
							<nav class="navigation mobile-only clearfix mobile-menu-wrapper">
								<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
							</nav>
							<?php } else { ?>
								<nav class="navigation clearfix mobile-menu-wrapper">
									<?php if ( has_nav_menu( 'secondary' ) ) {
										wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) );
									} else { ?>
										<ul class="menu clearfix">
											<?php wp_list_categories('title_li='); ?>
										</ul>
									<?php } ?>
								</nav>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<div id="header">
				<div class="container">

					<div class="logo-wrap">
						<?php if ($mts_options['mts_logo'] != '') {
							$logo_id = mts_get_image_id_from_url( $mts_options['mts_logo'] );
							$logo_w_h = '';
							if ( $logo_id ) {
								$logo	 = wp_get_attachment_image_src( $logo_id, 'full' );
								$logo_w_h = ' width="'.$logo[1].'" height="'.$logo[2].'"';
							}
							if( is_front_page() || is_home() || is_404() ) { ?>
								<h1 id="logo" class="image-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $mts_options['mts_logo'] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"<?php echo $logo_w_h; ?>></a>
								</h1><!-- END #logo -->
							<?php } else { ?>
								<h2 id="logo" class="image-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $mts_options['mts_logo'] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"<?php echo $logo_w_h; ?>></a>
								</h2><!-- END #logo -->
							<?php }
						} else {
							if( is_front_page() || is_home() || is_404() ) { ?>
								<h1 id="logo" class="text-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
								</h1><!-- END #logo -->
							<?php } else { ?>
								<h2 id="logo" class="text-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
								</h2><!-- END #logo -->
							<?php } ?>
							<div class="site-description" itemprop="description">
								<?php bloginfo( 'description' ); ?>
							</div>
						<?php } ?>
					</div>

					<?php if ( !empty($mts_options['mts_contact_info_items']) && is_array($mts_options['mts_contact_info_items']) ) { ?>
						<div class="contact-info-wrap">
							<?php foreach( $mts_options['mts_contact_info_items'] as $contact_items) : ?>
								<div class="contact-info">
									<?php if( !empty( $contact_items['mts_contact_info_item_url'] ) ) : ?>
									<a href="<?php echo $contact_items['mts_contact_info_item_url']; ?>">
									<?php endif;
									if($contact_items['mts_contact_info_item_icon']): ?>
										<div class="icon"><i class="fa fa-<?php echo $contact_items['mts_contact_info_item_icon']; ?>"></i></div>
									<?php endif; ?>
									<div class="description">
										<?php if($contact_items['mts_contact_info_item_number']): ?>
								 			<div class=""><?php echo $contact_items['mts_contact_info_item_number']; ?></div>
										<?php endif; ?>
										
										<?php if($contact_items['mts_contact_info_item_tagline']): ?>
											<div>
												<?php echo $contact_items['mts_contact_info_item_tagline']; ?>
											</div>
										<?php endif; ?>
								 	</div>
								 	<?php if( !empty( $contact_items['mts_contact_info_item_url'] ) ) : ?>
									</a>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php } ?>
				</div><!--.container-->
			</div><!--#header-->
			<?php if ( $mts_options['mts_show_primary_nav'] == '1' ) {
			if( $mts_options['mts_sticky_nav'] == '1' ) { ?>
			<div id="catcher" class="clear" ></div>
			<div id="primary-navigation" class="sticky-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
			<?php } else { ?>
			<div id="primary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
			<?php }
			if ( $mts_options['mts_show_secondary_nav'] !== '1' ) {?><a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'builders' ); ?></a><?php } ?>
				<div class="container clearfix">
					<nav class="navigation clearfix<?php if ( $mts_options['mts_show_secondary_nav'] !== '1' ) echo ' mobile-menu-wrapper'; ?>">
						<?php if ( has_nav_menu( 'primary' ) ) {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) );
						} else { ?>
							<ul class="menu clearfix">
								<?php wp_list_pages('title_li='); ?>
							</ul>
						<?php } ?>
					</nav>	
				</div>
			</div>
			<?php } ?>	
		</header>