<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Bege already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */

$bege_opt = get_option( 'bege_opt' );

get_header('large-container');
?>
<?php 
$bege_bloglayout = 'sidebar';
if(isset($bege_opt['blog_layout']) && $bege_opt['blog_layout']!=''){
	$bege_bloglayout = $bege_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bege_bloglayout = $_GET['layout'];
}
$bege_blogsidebar = 'right';
if(isset($bege_opt['sidebarblog_pos']) && $bege_opt['sidebarblog_pos']!=''){
	$bege_blogsidebar = $bege_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bege_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$bege_bloglayout = 'nosidebar';
}
switch($bege_bloglayout) {
	case 'sidebar':
		$bege_blogclass = 'blog-sidebar';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	case 'largeimage':
		$bege_blogclass = 'blog-large';
		$bege_blogcolclass = 9;
		$bege_postthumb = '';
		break;
	default:
		$bege_blogclass = 'blog-nosidebar';
		$bege_blogcolclass = 12;
		$bege_blogsidebar = 'none';
		Bege_Class::bege_post_thumbnail_size('bege-post-thumb');
}
?>
<div class="main-container">
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			
			<?php if($bege_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$bege_blogcolclass; ?>">
				<div class="page-content blog-page <?php echo esc_attr($bege_blogclass); if($bege_blogsidebar=='left') {echo ' left-sidebar'; } if($bege_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<div class="entry-header">
						<h1 class="entry-title"><?php if(isset($bege_opt)) { echo esc_html($bege_opt['blog_header_text']); } else { esc_html_e('Blog', 'bege');}  ?></h1>
					</div>
					<?php if ( have_posts() ) : ?>
						<header class="archive-header">
							<?php
								the_archive_title( '<h1 class="archive-title">', '</h1>' );
								the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</header>

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/* Include the post format-specific template for the content. If you want to
							 * this in a child theme then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						endwhile;
						?>
						
						<div class="pagination">
							<?php Bege_Class::bege_pagination(); ?>
						</div>
						
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $bege_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div> 
</div>
<?php get_footer(); ?>