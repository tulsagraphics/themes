<?php
/**
 * Template Name: Service page
 *
 * Description: Service page template
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */
$bege_opt = get_option( 'bege_opt' );

get_header('large-container');
?>
<div class="main-container service-page">
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="page-content">
		<div class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</div>
		<div class="service-container">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>