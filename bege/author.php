<?php
/**
 * The template for displaying Author Archive pages
 *
 * Used to display archive-type pages for posts by an author.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */

$bege_opt = get_option( 'bege_opt' );

get_header('large-container');

$bege_bloglayout = 'sidebar';
if(isset($bege_opt['blog_layout']) && $bege_opt['blog_layout']!=''){
	$bege_bloglayout = $bege_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bege_bloglayout = $_GET['layout'];
}
$bege_blogsidebar = 'right';
if(isset($bege_opt['sidebarblog_pos']) && $bege_opt['sidebarblog_pos']!=''){
	$bege_blogsidebar = $bege_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bege_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$bege_bloglayout = 'nosidebar';
}
switch($bege_bloglayout) {
	case 'sidebar':
		$bege_blogclass = 'blog-sidebar';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	case 'largeimage':
		$bege_blogclass = 'blog-large';
		$bege_blogcolclass = 9;
		$bege_postthumb = '';
		break;
	default:
		$bege_blogclass = 'blog-nosidebar';
		$bege_blogcolclass = 12;
		$bege_blogsidebar = 'none';
		Bege_Class::bege_post_thumbnail_size('bege-post-thumb');
}
?>
<div class="main-container">
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php if($bege_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			<div class="col-xs-12 <?php echo 'col-md-'.$bege_blogcolclass; ?>">
				<div class="page-content blog-page <?php echo esc_attr($bege_blogclass); if($bege_blogsidebar=='left') {echo ' left-sidebar'; } if($bege_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<div class="entry-header">
						<h1 class="entry-title"><?php if(isset($bege_opt)) { echo esc_html($bege_opt['blog_header_text']); } else { esc_html_e('Blog', 'bege');}  ?></h1>
					</div>
					<?php if ( have_posts() ) : ?>

						<?php
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							 *
							 * We reset this later so we can run the loop
							 * properly with a call to rewind_posts().
							 */
							the_post();
						?>

						<header class="archive-header">
							<h1 class="archive-title"><?php printf( esc_html__( 'Author Archives: %s', 'bege' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?></h1>
						</header><!-- .archive-header -->

						<?php
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();
						?>

						<?php
						// If a user has filled out their description, show a bio on their entries.
						if ( get_the_author_meta( 'description' ) ) : ?>
						<div class="author-info archives">
							<div class="author-avatar">
								<?php
								/**
								 * Filter the author bio avatar size.
								 *
								 * @since Bege 1.0
								 *
								 * @param int $size The height and width of the avatar in pixels.
								 */
								$author_bio_avatar_size = apply_filters( 'bege_author_bio_avatar_size', 68 );
								echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
								?>
							</div><!-- .author-avatar -->
							<div class="author-description">
								<h2><?php printf( esc_html__( 'About %s', 'bege' ), get_the_author() ); ?></h2>
								<p><?php the_author_meta( 'description' ); ?></p>
							</div><!-- .author-description	-->
						</div><!-- .author-info -->
						<?php endif; ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', get_post_format() ); ?>
						<?php endwhile; ?>
						
						<div class="pagination">
							<?php Bege_Class::bege_pagination(); ?>
						</div>

					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $bege_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div>
 
</div>
<?php get_footer(); ?>