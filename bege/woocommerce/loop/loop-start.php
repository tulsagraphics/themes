<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 	3.3.0
 */
global $wp_query, $woocommerce_loop;

$bege_opt = get_option( 'bege_opt' );

$shoplayout = 'sidebar';
if(isset($bege_opt['shop_layout']) && $bege_opt['shop_layout']!=''){
	$shoplayout = $bege_opt['shop_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$shoplayout = $_GET['layout'];
}
$shopsidebar = 'left';
if(isset($bege_opt['sidebarshop_pos']) && $bege_opt['sidebarshop_pos']!=''){
	$shopsidebar = $bege_opt['sidebarshop_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$shopsidebar = $_GET['sidebar'];
}
switch($shoplayout) {
	case 'fullwidth':
		Bege_Class::bege_shop_class('shop-fullwidth');
		$shopcolclass = 12;
		$shopsidebar = 'none';
		$productcols = 4;
		break;
	default:
		Bege_Class::bege_shop_class('shop-sidebar');
		$shopcolclass = 10;
		$productcols = 3;
}

$bege_viewmode = Bege_Class::bege_show_view_mode();
?>
<div class="shop-products products <?php echo esc_attr($bege_viewmode);?> <?php echo esc_attr($shoplayout);?>">