<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */

$bege_opt = get_option( 'bege_opt' );

get_header('large-container');

$bege_bloglayout = 'sidebar';

if(isset($bege_opt['blog_layout']) && $bege_opt['blog_layout']!=''){
	$bege_bloglayout = $bege_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bege_bloglayout = $_GET['layout'];
}
$bege_blogsidebar = 'right';
if(isset($bege_opt['sidebarblog_pos']) && $bege_opt['sidebarblog_pos']!=''){
	$bege_blogsidebar = $bege_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bege_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$bege_bloglayout = 'nosidebar';
}
switch($bege_bloglayout) {
	case 'sidebar':
		$bege_blogclass = 'blog-sidebar';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	case 'largeimage':
		$bege_blogclass = 'blog-large';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	case 'grid':
		$bege_blogclass = 'grid';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	default:
		$bege_blogclass = 'blog-nosidebar';
		$bege_blogcolclass = 12;
		$bege_blogsidebar = 'none';
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
}
?>

<div class="main-container"> 
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		
		<div class="row">

			<?php if($bege_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>

			<div class="col-xs-12 <?php echo 'col-md-'.$bege_blogcolclass; ?>">
			
				<div class="page-content blog-page <?php echo esc_attr($bege_blogclass); if($bege_blogsidebar=='left') {echo ' left-sidebar'; } if($bege_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<div class="entry-header">
						<h1 class="entry-title"><?php if(isset($bege_opt)) { echo esc_html($bege_opt['blog_header_text']); } else { esc_html_e('Blog', 'bege');}  ?></h1>
					</div>
					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							
							<?php get_template_part( 'content', get_post_format() ); ?>
							
						<?php endwhile; ?>

						<div class="pagination">
							<?php Bege_Class::bege_pagination(); ?>
						</div>
						
					<?php else : ?>

						<article id="post-0" class="post no-results not-found">

						<?php if ( current_user_can( 'edit_posts' ) ) :
							// Show a different message to a logged-in user who can add posts.
						?>
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'bege' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'bege' ), array('a'=>array('href'=>array()))), admin_url( 'post-new.php' ) ); ?></p>
							</div><!-- .entry-content -->

						<?php else :
							// Show the default message to everyone else.
						?>
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'bege' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'bege' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						<?php endif; // end current_user_can() check ?>

						</article><!-- #post-0 -->

					<?php endif; // end have_posts() check ?>
				</div>
				
			</div>
			

			<?php if( $bege_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
		</div>
	</div> 
</div>
<?php get_footer(); ?>