<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */

$bege_opt = get_option( 'bege_opt' );

get_header('large-container');

$bege_bloglayout = 'sidebar';
if(isset($bege_opt['blog_layout']) && $bege_opt['blog_layout']!=''){
	$bege_bloglayout = $bege_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bege_bloglayout = $_GET['layout'];
}
$bege_blogsidebar = 'right';
if(isset($bege_opt['sidebarblog_pos']) && $bege_opt['sidebarblog_pos']!=''){
	$bege_blogsidebar = $bege_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bege_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$bege_bloglayout = 'nosidebar';
}
switch($bege_bloglayout) {
	case 'sidebar':
		$bege_blogclass = 'blog-sidebar';
		$bege_blogcolclass = 9;
		break;
	default:
		$bege_blogclass = 'blog-nosidebar'; //for both fullwidth and no sidebar
		$bege_blogcolclass = 12;
		$bege_blogsidebar = 'none';
}
?>
<div class="main-container page-wrapper">
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">

			<?php
			$customsidebar = get_post_meta( $post->ID, '_bege_custom_sidebar', true );
			$customsidebar_pos = get_post_meta( $post->ID, '_bege_custom_sidebar_pos', true );

			if($customsidebar != ''){
				if($customsidebar_pos == 'left' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-xs-12 col-md-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($bege_blogsidebar=='left') {
					get_sidebar();
				}
			} ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$bege_blogcolclass; ?>">
				<div class="page-content blog-page single <?php echo esc_attr($bege_blogclass); if($bege_blogsidebar=='left') {echo ' left-sidebar'; } if($bege_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<div class="entry-header">
						<h1 class="entry-title"><?php if(isset($bege_opt)) { echo esc_html($bege_opt['blog_header_text']); } else { esc_html_e('Blog', 'bege');}  ?></h1>
					</div>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', get_post_format() ); ?>

						<?php comments_template( '', true ); ?>
						
						<!--<nav class="nav-single">
							<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'bege' ); ?></h3>
							<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'bege' ) . '</span> %title' ); ?></span>
							<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'bege' ) . '</span>' ); ?></span>
						</nav><!-- .nav-single -->
						
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			<?php
			if($customsidebar != ''){
				if($customsidebar_pos == 'right' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-xs-12 col-md-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($bege_blogsidebar=='right') {
					get_sidebar();
				}
			} ?>
		</div>
	</div> 
</div>

<?php get_footer(); ?>