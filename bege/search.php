<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Bege_Theme
 * @since Bege 1.0
 */

$bege_opt = get_option( 'bege_opt' );

get_header();

$bege_bloglayout = 'sidebar';
if(isset($bege_opt['blog_layout']) && $bege_opt['blog_layout']!=''){
	$bege_bloglayout = $bege_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bege_bloglayout = $_GET['layout'];
}
$bege_blogsidebar = 'right';
if(isset($bege_opt['sidebarblog_pos']) && $bege_opt['sidebarblog_pos']!=''){
	$bege_blogsidebar = $bege_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bege_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$bege_bloglayout = 'nosidebar';
}
switch($bege_bloglayout) {
	case 'sidebar':
		$bege_blogclass = 'blog-sidebar';
		$bege_blogcolclass = 9;
		Bege_Class::bege_post_thumbnail_size('bege-category-thumb');
		break;
	case 'largeimage':
		$bege_blogclass = 'blog-large';
		$bege_blogcolclass = 9;
		$bege_postthumb = '';
		break;
	default:
		$bege_blogclass = 'blog-nosidebar';
		$bege_blogcolclass = 12;
		$bege_blogsidebar = 'none';
		Bege_Class::bege_post_thumbnail_size('bege-post-thumb');
}
?>
<div class="main-container">
	<div class="breadcrumbs-container">
		<div class="container">
			<?php Bege_Class::bege_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php if($bege_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$bege_blogcolclass; ?>">
			
				<div class="page-content blog-page <?php echo esc_attr($bege_blogclass); if($bege_blogsidebar=='left') {echo ' left-sidebar'; } if($bege_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<div class="entry-header">
						<h1 class="entry-title"><?php if(isset($bege_opt)) { echo esc_html($bege_opt['blog_header_text']); } else { esc_html_e('Blog', 'bege');}  ?></h1>
					</div>
					<?php if ( have_posts() ) : ?>
						
						<header class="archive-header">
							<h1 class="archive-title"><?php printf( wp_kses(__( 'Search Results for: %s', 'bege' ), array('span'=>array())), '<span>' . get_search_query() . '</span>' ); ?></h1>
						</header><!-- .archive-header -->

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', get_post_format() ); ?>
						<?php endwhile; ?>

						<div class="pagination">
							<?php Bege_Class::bege_pagination(); ?>
						</div>

					<?php else : ?>

						<article id="post-0" class="post no-results not-found">
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'bege' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'bege' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-0 -->

					<?php endif; ?>
				</div>
			</div>
			<?php if( $bege_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div>
</div>
<?php get_footer(); ?>